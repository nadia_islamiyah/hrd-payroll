﻿Imports System.Data.Sql
Imports System.Data.SqlClient

Module Module1
    Public strconn1 As String
    Public servername As String
    Public dbname As String
    Public username As String
    Public password As String
    Public msgboxTitle As String = "3PM-Solutions"
    Public Function GetAppPath() As String
        Dim i As Integer
        Dim strAppPath As String
        strAppPath = System.Reflection.Assembly.GetExecutingAssembly.Location()
        i = strAppPath.Length - 1
        Do Until strAppPath.Substring(i, 1) = "\"
            i = i - 1
        Loop
        strAppPath = strAppPath.Substring(0, i)
        Return strAppPath
    End Function
    
    
    Public Function newid(ByVal tabel As String, ByVal nama_id As String, ByVal tanggal As Date, ByVal prefix As String) As String
        Dim conn As New SqlConnection
        Dim cmd As SqlCommand
        Dim dr As SqlDataReader
        Dim query As String
        Dim start As Byte
        conn.ConnectionString = strconn1
        conn.Open()


        start = Len(prefix) + 1
        query = "select top 1 [nama_id] from [tabel] where substring([nama_id],[start],2)='" & Format(tanggal, "yy") & "' and substring([nama_id],[start]+2,2)='" & Format(tanggal, "MM") & "'  and substring([nama_id],[start]+4,2)='" & Format(tanggal, "dd") & "' and left([nama_id],[start]-1)='" & prefix & "' order by [nama_id] desc"
        query = Replace(query, "[tabel]", tabel)
        query = Replace(query, "[nama_id]", nama_id)
        query = Replace(query, "[start]", start)
        cmd = New SqlCommand(query, conn)
        dr = cmd.ExecuteReader
        If dr.Read Then
            newid = prefix & Format(tanggal, "yy") & Format(tanggal, "MM") & Format(tanggal, "dd") & Format((CLng(Right(dr.Item(0), 4)) + 1), "0000")
        Else
            newid = prefix & Format(tanggal, "yy") & Format(tanggal, "MM") & Format(tanggal, "dd") & Format(1, "0000")
        End If
        dr.Close()
        conn.Close()
    End Function
    
    Public Function App_Path() As String
        Return System.AppDomain.CurrentDomain.BaseDirectory()
    End Function
    Public Function getPaperIndex(ByVal printername As String, ByVal papername As String) As Integer
        Dim printDoc As New System.Drawing.Printing.PrintDocument
        Dim paperindex As Integer
        Dim i As Byte
        paperindex = 0
        printDoc.PrinterSettings.PrinterName = printername
        For i = 0 To printDoc.PrinterSettings.PaperSizes.Count
            If printDoc.PrinterSettings.PaperSizes(i).PaperName = papername Then
                paperindex = CInt(printDoc.PrinterSettings.PaperSizes(i).GetType().GetField("kind", Reflection.BindingFlags.Instance Or Reflection.BindingFlags.NonPublic).GetValue(printDoc.PrinterSettings.PaperSizes(i)))
                Exit For
            End If


        Next
        getPaperIndex = paperindex
    End Function
    Public Function setcombotextleft(ByRef cmb As ComboBox, ByVal text As String) As Boolean
        setcombotextleft = False
        For i As Integer = 0 To cmb.Items.Count - 1
            If Mid(cmb.Items(i), 1, Len(text)) = text Then cmb.Text = cmb.Items(i)
        Next
        setcombotextleft = True
    End Function
    Public Sub setconnectionstring1()
        strconn1 = "Persist Security Info=False;Initial Catalog=HRD;Data Source=.\sqlexpress;Integrated Security=SSPI;"
    End Sub
    Public Sub setconnectionstring(ByVal servername As String, ByVal dbname As String, ByVal username As String, ByVal password As String)

        strconn1 = "Persist Security Info=False;Initial Catalog=" & dbname & ";Data Source=" & servername & ";User ID=" & username & ";Password=" & password & ";"
    End Sub
    Public Function query_tambah_data(ByVal table_name As String, ByVal field() As String, ByVal nilai() As String) As String
        Dim query As String
        Dim i As Integer
        query = "insert into " & table_name & " ("
        For i = 0 To UBound(field) - 1
            If field(i) <> "" Then query = query & field(i) & ","
        Next
        query = Left(query, Len(query) - 1) & ") values ("
        For i = 0 To UBound(nilai) - 1
            If field(i) <> "" Then
                query = query & "'" & nilai(i) & "',"
            End If
        Next
        query = Left(query, Len(query) - 1) & ")"
        query_tambah_data = query

    End Function

End Module
