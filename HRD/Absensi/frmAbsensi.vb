﻿Imports System.Data.SqlClient
Imports System.Data.Sql
Imports System.IO

Public Class frmAbsensi

    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        lblTanggal.Text = Format(Now, "dd/MM/yyyy")
        lblJam.Text = Format(Now, "HH:mm")
        lblStatus.Text = ""
        lblNamaDepan.Text = ""
        lblNamaBelakang.Text = ""
        lblNIK.Text = ""
        Timer1.Interval = 1000
    End Sub

    Private Sub frmAbsensi_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim xm As New Ini(GetAppPath() & "\setting.ini")
        servername = xm.GetString("Server", "Server name", "")
        dbname = xm.GetString("Server", "Database name", "")

        lblTanggal.Text = Format(Now, "dd/MM/yyyy")
        lblJam.Text = Format(Now, "HH:mm")
        lblJenisAbsen.Text = "Masuk"
        reset_form()
        setconnectionstring(servername, dbname, "sa", "3pmadmin")
    End Sub

    Private Sub txtCommand_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtCommand.KeyDown
        If e.KeyCode = Keys.Enter Then
            absen()
        End If
    End Sub
    Private Sub reset_form()
        lblJenisAbsen.Text = "Masuk"
        lblNamaDepan.Text = ""
        lblNamaBelakang.Text = ""
        lblNIK.Text = ""
        txtCommand.Text = ""

    End Sub
    Private Sub absen()
        Dim conn As New SqlConnection
        Dim cmd As New SqlCommand
        Dim dr As SqlDataReader
        Dim urut As Double
        Dim tanggal As Date
        conn.ConnectionString = strconn1
        conn.Open()
        If txtCommand.Text = "22222" Then
            lblJenisAbsen.Text = "Istirahat"
            GoTo Keluar
        End If

        If txtCommand.Text = "11111" Then
            lblJenisAbsen.Text = "Masuk"
            GoTo Keluar
        End If

        If txtCommand.Text = "33333" Then
            lblJenisAbsen.Text = "Istirahat Masuk"
            GoTo Keluar
        End If

        If txtCommand.Text = "44444" Then
            lblJenisAbsen.Text = "Pulang"
            GoTo Keluar
        End If


        Try
            cmd = New SqlCommand("select * from ms_karyawan where nik='" & txtCommand.Text & "'", conn)
            dr = cmd.ExecuteReader
            'If Hour(Now) <= 5 And lblJenisAbsen.Text = "Pulang" Then
            ' tanggal = DateAdd(DateInterval.Day, -1, Now)
            ' Else
            tanggal = Now
            'End If
            If dr.Read Then
                lblNamaDepan.Text = dr.Item("nama_depan")
                lblNamaBelakang.Text = dr.Item("nama_belakang")
                lblNIK.Text = dr.Item("NIK")
                dr.Close()
                cmd = New SqlCommand("insert into t_absensi (nik,jenis,waktu) values ('" & txtCommand.Text & "','" & lblJenisAbsen.Text & "','" & Format(Now, "yyyy/MM/dd HH:mm:ss") & "')", conn)
                cmd.ExecuteNonQuery()
                If lblJenisAbsen.Text = "Masuk" Then
                    cmd = New SqlCommand("select * from t_rekapabsensi where nik='" & txtCommand.Text & "' and convert(varchar(20),tanggal,112)='" & Format(tanggal, "yyyyMMdd") & "' and status_pulang='False'", conn)
                    dr = cmd.ExecuteReader
                    If Not dr.Read Then
                        dr.Close()
                        cmd = New SqlCommand("insert into t_rekapabsensi (nik,tanggal) values ('" & txtCommand.Text & "','" & Format(tanggal, "yyyy/MM/dd") & "')", conn)
                        cmd.ExecuteNonQuery()
                    End If
                    dr.Close()
                
                End If
                cmd = New SqlCommand("select top 1 urut from t_rekapabsensi where nik='" & txtCommand.Text & "' order by tanggal desc", conn)
                dr = cmd.ExecuteReader
                If dr.Read Then
                    urut = dr.Item("urut")
                End If
                dr.Close()
                Select Case lblJenisAbsen.Text
                    Case "Masuk"
                        cmd = New SqlCommand("update t_rekapabsensi set datang='" & Format(Now, "yyyy/MM/dd HH:mm:ss") & "',status_datang='True' where nik='" & txtCommand.Text & "' and  urut='" & urut & "'", conn)
                        cmd.ExecuteNonQuery()
                    Case "Istirahat"
                        cmd = New SqlCommand("update t_rekapabsensi set istirahat_out='" & Format(Now, "yyyy/MM/dd HH:mm:ss") & "',status_istirahat_out='True' where nik='" & txtCommand.Text & "' and urut='" & urut & "'", conn)
                        cmd.ExecuteNonQuery()
                    Case "Istirahat Masuk"
                        cmd = New SqlCommand("update t_rekapabsensi set istirahat_in='" & Format(Now, "yyyy/MM/dd HH:mm:ss") & "',status_istirahat_in='True' where nik='" & txtCommand.Text & "' and urut='" & urut & "'", conn)
                        cmd.ExecuteNonQuery()
                    Case "Pulang"
                        cmd = New SqlCommand("update t_rekapabsensi set pulang='" & Format(Now, "yyyy/MM/dd HH:mm:ss") & "',status_pulang='True' where nik='" & txtCommand.Text & "' and urut='" & urut & "'", conn)
                        cmd.ExecuteNonQuery()

                End Select

                lblStatus.Text = "Berhasil"


                Timer1.Interval = 4000
                cmd.ExecuteNonQuery()
            Else
                lblStatus.Text = "Salah, silahkan ulangi lagi"
                Timer1.Interval = 4000
            End If
            dr.Close()
            'cmd = New SqlCommand("delete from ms_karyawan where nik='" & txtNIK.Text & "'", conn, tr)
        Catch e As Exception
            MsgBox(e.Message)

        End Try
Keluar:
        txtCommand.Text = ""
        txtCommand.Focus()
    End Sub
    Private Sub txtCommand_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtCommand.TextChanged

    End Sub
End Class