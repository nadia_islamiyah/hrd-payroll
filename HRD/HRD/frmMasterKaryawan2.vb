﻿Imports System.Data.OleDb
Imports System.Data.SqlClient

Public Class frmMasterKaryawan2
    Dim transaction As SqlTransaction = Nothing

    Private Sub frmMasterKaryawan_KeyDown(sender As Object, e As KeyEventArgs) Handles MyBase.KeyDown
        If e.KeyCode = Keys.F2 Then btnSimpan.PerformClick()
        If e.KeyCode = Keys.F5 Then btnSearch.PerformClick()
        If e.Control And e.KeyCode = Keys.R Then btnReset.PerformClick()
        If e.KeyCode = Keys.Enter Then SendKeys.Send("{TAB}")
        If e.KeyCode = Keys.Escape Then Me.Close()
    End Sub

    Private Sub btnReset_Click(sender As Object, e As EventArgs) Handles btnReset.Click
        reset_form()
    End Sub

    Private Sub reset_form()
        chkBorongan.Checked = False
        txt_fingerID.Text = ""
        txtNIK.Text = ""
        txtNama.Text = ""
        cmblokasi.Text = ""
        cmbpenempatan.Text = ""
        cmbbagian.Text = ""
        txtNIK.Focus()
        loadComboBoxAll(cmblokasi, "distinct lokasi", "absensi.dbo.ms_karyawan", "", "lokasi", "lokasi")
        loadComboBoxAll(cmbpenempatan, "distinct penempatan", "absensi.dbo.ms_karyawan", "", "penempatan", "penempatan")
        loadComboBoxAll(cmbbagian, "distinct bagian", "absensi.dbo.ms_karyawan", "", "bagian", "bagian")
        loadComboBoxAll(cmbgrade, "distinct grade", "[ms_gaji]", "", "grade", "grade")
    End Sub

    Private Sub btnHapus_Click(sender As Object, e As EventArgs) Handles btnHapus.Click
        Dim conn2 As New SqlConnection(strcon)
        conn2.Open()
        transaction = conn2.BeginTransaction(IsolationLevel.ReadCommitted)
        Try
            cmd = New SqlCommand("delete from absensi.dbo.ms_karyawan where NIK = '" & txtNIK.Text & "' ", conn2, transaction)
            cmd.ExecuteNonQuery()
            transaction.Commit()
            MsgBox("Data berhasil dihapus . . .", MsgBoxStyle.Information)

            conn2.Close()
            reset_form()
        Catch ex As Exception
            MessageBox.Show(ex.Message.ToString)
            transaction.Rollback()

        End Try
        If conn2.State Then conn2.Close()
    End Sub

    Private Function authentication() As Boolean
        Dim a As Boolean
        If txtNIK.Text = "" Then a = True
        If txtNama.Text = "" Then a = True

        authentication = a
    End Function

    Private Sub btnSimpan_Click(sender As Object, e As EventArgs) Handles btnSimpan.Click
        If authentication() = True Then
            MsgBox("Yang bertanda * harus diisi.", MsgBoxStyle.Information)
            Exit Sub
        End If
        Dim conn2 As New SqlConnection(strcon)
        Dim cmd As SqlCommand
        Dim reader As SqlDataReader = Nothing
        conn2.Open()
        transaction = conn2.BeginTransaction(IsolationLevel.ReadCommitted)
        Try
            cmd = New SqlCommand("select * from absensi.dbo.ms_karyawan where NIK = '" & txtNIK.Text & "'", conn2, transaction)
            reader = cmd.ExecuteReader()
            If reader.Read() Then
                cmd = New SqlCommand(add_dataheader("update"), conn2, transaction)
                cmd.ExecuteNonQuery()
            Else
                cmd = New SqlCommand(add_dataheader("insert"), conn2, transaction)
                cmd.ExecuteNonQuery()
            End If
            reader.Close()

            transaction.Commit()
            conn2.Close()
            If MsgBox("Data sudah tersimpan, masukkan data baru?", vbYesNo) = vbYes Then reset_form()


        Catch ex As Exception
            MessageBox.Show(ex.Message.ToString)
            transaction.Rollback()
            If reader IsNot Nothing Then reader.Close()

        End Try
        If conn2.State Then conn2.Close()
    End Sub

    Private Function add_dataheader(ByVal jenis As String) As String
        add_dataheader = ""
        Dim fields() As String
        Dim nilai() As String
        Dim table_name As String
        Dim fieldPK() As String
        Dim nilaiPK() As String

        ReDim fields(9)
        ReDim nilai(9)
        ReDim fieldPK(1)
        ReDim nilaiPK(1)

        table_name = "absensi.dbo.ms_karyawan"
        fields(0) = "NIK"
        fields(1) = "nama"
        fields(2) = "lokasi"
        fields(3) = "penempatan"
        fields(4) = "bagian"
        fields(5) = "borongan"
        fields(6) = "finger_id"
        fields(7) = "grade"
        fields(8) = "harian"

        nilai(0) = txtNIK.Text
        nilai(1) = txtNama.Text
        nilai(2) = cmblokasi.Text
        nilai(3) = cmbpenempatan.Text
        nilai(4) = cmbbagian.Text
        If chkBorongan.Checked = True Then nilai(5) = 1 Else nilai(5) = 0
        nilai(6) = txt_fingerID.Text
        nilai(7) = cmbgrade.Text
        If chkHarian.Checked = True Then nilai(8) = 1 Else nilai(8) = 0

        fieldPK(0) = "NIK"
        nilaiPK(0) = txtNIK.Text

        If jenis = "insert" Then
            add_dataheader = tambah_data2(table_name, fields, nilai)
        ElseIf jenis = "update" Then
            add_dataheader = update_data2(table_name, fields, nilai, fieldPK, nilaiPK)
        End If
    End Function

    Private Sub frmMasterKaryawan_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        reset_form()

    End Sub

    Private Sub txtLimitKredit_KeyPress(sender As Object, e As KeyPressEventArgs)
        NumericOnly(e)
    End Sub

    Private Sub txtGajiPokok_KeyPress(sender As Object, e As KeyPressEventArgs)
        NumericOnly(e)
    End Sub

    Private Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click
        Dim f As New frmSearch
        f.setCol(0)
        f.setTableName("absensi.dbo.ms_karyawan")
        f.setColumns("*")
        f.setOrder("order by nama")
        If f.ShowDialog = System.Windows.Forms.DialogResult.OK Then
            txtNIK.Text = f.value
            loadMasterKaryawan()
        End If
        f.Dispose()
    End Sub

    Private Sub loadMasterKaryawan()
        Dim conn2 As New SqlConnection(strcon)
        Dim cmd As SqlCommand
        Dim reader As SqlDataReader = Nothing
        conn2.Open()
        Try
            cmd = New SqlCommand("select isnull(nama,'') nama,isnull(lokasi,'') lokasi,isnull(penempatan,'') penempatan,isnull(bagian,'') bagian,borongan, isnull(finger_id,'') finger_id, isnull(grade,'') grade,harian from absensi.dbo.ms_karyawan where NIK = '" & txtNIK.Text & "'", conn2)
            reader = cmd.ExecuteReader()
            If reader.Read() Then
                txtNama.Text = reader("nama")
                cmblokasi.Text = reader("lokasi")
                cmbpenempatan.Text = reader("penempatan")
                cmbbagian.Text = reader("bagian")
                If reader("borongan") = True Then chkBorongan.Checked = True Else chkBorongan.Checked = False
                If reader("harian") = True Then chkHarian.Checked = True Else chkHarian.Checked = False
                txt_fingerID.Text = reader("finger_id")
                cmbgrade.Text = reader("grade")
            End If
            reader.Close()

        Catch ex As Exception
            MessageBox.Show(ex.Message.ToString)
        End Try
        conn2.Close()
    End Sub

    Private Sub txtNIK_Leave(sender As Object, e As System.EventArgs) Handles txtNIK.Leave
        loadMasterKaryawan()
    End Sub

    Private Sub btnNext_Click(sender As System.Object, e As System.EventArgs) Handles btnNext.Click
        Dim conn As New SqlConnection(strcon)
        Dim cmd As SqlCommand
        Dim dr As SqlDataReader
        conn.Open()
        'load next bill
        cmd = New SqlCommand("select nik from absensi.dbo.ms_karyawan where nik>'" & txtNIK.Text & "' order by nik asc", conn)
        dr = cmd.ExecuteReader
        If dr.Read Then
            txtNIK.Text = dr.Item("nik")
            loadMasterKaryawan()
        End If
        conn.Close()
    End Sub

    Private Sub btnPrev_Click(sender As System.Object, e As System.EventArgs) Handles btnPrev.Click
        Dim conn As New SqlConnection(strcon)
        Dim cmd As SqlCommand
        Dim dr As SqlDataReader
        conn.Open()
        'load next bill
        cmd = New SqlCommand("select nik from absensi.dbo.ms_karyawan where nik<'" & txtNIK.Text & "' order by nik asc", conn)
        dr = cmd.ExecuteReader
        If dr.Read Then
            txtNIK.Text = dr.Item("nik")
            loadMasterKaryawan()
        End If
        conn.Close()
    End Sub

    Private Sub btnKeluar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnKeluar.Click
        Me.Close()
    End Sub

    Private Sub GroupBox1_Enter(sender As System.Object, e As System.EventArgs) Handles GroupBox1.Enter

    End Sub
End Class