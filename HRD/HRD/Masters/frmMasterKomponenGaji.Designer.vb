﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMasterKomponenGaji
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnSave = New System.Windows.Forms.Button()
        Me.txtKode = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.btnHapus = New System.Windows.Forms.Button()
        Me.btnSearch = New System.Windows.Forms.Button()
        Me.btnExit = New System.Windows.Forms.Button()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.cmbTipe = New System.Windows.Forms.ComboBox()
        Me.txtNama = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.chkLembur = New System.Windows.Forms.CheckBox()
        Me.chkMasukterus = New System.Windows.Forms.CheckBox()
        Me.chkManual = New System.Windows.Forms.CheckBox()
        Me.chkHarian = New System.Windows.Forms.CheckBox()
        Me.chkJam = New System.Windows.Forms.CheckBox()
        Me.chkCustom = New System.Windows.Forms.CheckBox()
        Me.cmbCustom = New System.Windows.Forms.ComboBox()
        Me.chkLibur = New System.Windows.Forms.CheckBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.chknormal = New System.Windows.Forms.CheckBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.chkTelat = New System.Windows.Forms.CheckBox()
        Me.txtMinimal = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.lblSatuan = New System.Windows.Forms.Label()
        Me.txtKelipatan = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.btnReset = New System.Windows.Forms.Button()
        Me.chkPC = New System.Windows.Forms.CheckBox()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnSave
        '
        Me.btnSave.Location = New System.Drawing.Point(60, 365)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(75, 29)
        Me.btnSave.TabIndex = 8
        Me.btnSave.Text = "Simpan"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'txtKode
        '
        Me.txtKode.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtKode.Location = New System.Drawing.Point(111, 26)
        Me.txtKode.MaxLength = 3
        Me.txtKode.Name = "txtKode"
        Me.txtKode.Size = New System.Drawing.Size(69, 23)
        Me.txtKode.TabIndex = 0
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.BackColor = System.Drawing.Color.Transparent
        Me.Label2.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.Black
        Me.Label2.Location = New System.Drawing.Point(18, 26)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(36, 16)
        Me.Label2.TabIndex = 14
        Me.Label2.Text = "Kode"
        '
        'btnHapus
        '
        Me.btnHapus.Location = New System.Drawing.Point(141, 365)
        Me.btnHapus.Name = "btnHapus"
        Me.btnHapus.Size = New System.Drawing.Size(75, 29)
        Me.btnHapus.TabIndex = 9
        Me.btnHapus.Text = "Hapus"
        Me.btnHapus.UseVisualStyleBackColor = True
        '
        'btnSearch
        '
        Me.btnSearch.Location = New System.Drawing.Point(186, 26)
        Me.btnSearch.Name = "btnSearch"
        Me.btnSearch.Size = New System.Drawing.Size(37, 23)
        Me.btnSearch.TabIndex = 21
        Me.btnSearch.Text = "F3"
        Me.btnSearch.UseVisualStyleBackColor = True
        '
        'btnExit
        '
        Me.btnExit.Location = New System.Drawing.Point(303, 365)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(75, 29)
        Me.btnExit.TabIndex = 10
        Me.btnExit.Text = "Keluar"
        Me.btnExit.UseVisualStyleBackColor = True
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        Me.Label26.BackColor = System.Drawing.Color.Transparent
        Me.Label26.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label26.ForeColor = System.Drawing.Color.Black
        Me.Label26.Location = New System.Drawing.Point(18, 86)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(33, 16)
        Me.Label26.TabIndex = 27
        Me.Label26.Text = "Tipe"
        '
        'cmbTipe
        '
        Me.cmbTipe.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbTipe.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbTipe.FormattingEnabled = True
        Me.cmbTipe.Items.AddRange(New Object() {"Pendapatan", "Potongan"})
        Me.cmbTipe.Location = New System.Drawing.Point(111, 84)
        Me.cmbTipe.Name = "cmbTipe"
        Me.cmbTipe.Size = New System.Drawing.Size(150, 22)
        Me.cmbTipe.TabIndex = 2
        '
        'txtNama
        '
        Me.txtNama.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNama.Location = New System.Drawing.Point(111, 55)
        Me.txtNama.MaxLength = 0
        Me.txtNama.Name = "txtNama"
        Me.txtNama.Size = New System.Drawing.Size(296, 23)
        Me.txtNama.TabIndex = 1
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.Black
        Me.Label1.Location = New System.Drawing.Point(18, 55)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(41, 16)
        Me.Label1.TabIndex = 88
        Me.Label1.Text = "Nama"
        '
        'chkLembur
        '
        Me.chkLembur.AutoSize = True
        Me.chkLembur.Location = New System.Drawing.Point(101, 61)
        Me.chkLembur.Name = "chkLembur"
        Me.chkLembur.Size = New System.Drawing.Size(61, 17)
        Me.chkLembur.TabIndex = 5
        Me.chkLembur.Text = "Lembur"
        Me.chkLembur.UseVisualStyleBackColor = True
        '
        'chkMasukterus
        '
        Me.chkMasukterus.AutoSize = True
        Me.chkMasukterus.Location = New System.Drawing.Point(200, 61)
        Me.chkMasukterus.Name = "chkMasukterus"
        Me.chkMasukterus.Size = New System.Drawing.Size(88, 17)
        Me.chkMasukterus.TabIndex = 6
        Me.chkMasukterus.Text = "Masuk Terus"
        Me.chkMasukterus.UseVisualStyleBackColor = True
        '
        'chkManual
        '
        Me.chkManual.AutoSize = True
        Me.chkManual.Location = New System.Drawing.Point(19, 325)
        Me.chkManual.Name = "chkManual"
        Me.chkManual.Size = New System.Drawing.Size(181, 17)
        Me.chkManual.TabIndex = 7
        Me.chkManual.Text = "Manual Entry (Boleh edit manual)"
        Me.chkManual.UseVisualStyleBackColor = True
        '
        'chkHarian
        '
        Me.chkHarian.AutoSize = True
        Me.chkHarian.Location = New System.Drawing.Point(200, 36)
        Me.chkHarian.Name = "chkHarian"
        Me.chkHarian.Size = New System.Drawing.Size(57, 17)
        Me.chkHarian.TabIndex = 3
        Me.chkHarian.Text = "Harian"
        Me.chkHarian.UseVisualStyleBackColor = True
        '
        'chkJam
        '
        Me.chkJam.AutoSize = True
        Me.chkJam.Location = New System.Drawing.Point(101, 36)
        Me.chkJam.Name = "chkJam"
        Me.chkJam.Size = New System.Drawing.Size(45, 17)
        Me.chkJam.TabIndex = 4
        Me.chkJam.Text = "Jam"
        Me.chkJam.UseVisualStyleBackColor = True
        '
        'chkCustom
        '
        Me.chkCustom.AutoSize = True
        Me.chkCustom.Location = New System.Drawing.Point(19, 292)
        Me.chkCustom.Name = "chkCustom"
        Me.chkCustom.Size = New System.Drawing.Size(61, 17)
        Me.chkCustom.TabIndex = 89
        Me.chkCustom.Text = "Custom"
        Me.chkCustom.UseVisualStyleBackColor = True
        '
        'cmbCustom
        '
        Me.cmbCustom.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbCustom.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbCustom.FormattingEnabled = True
        Me.cmbCustom.Items.AddRange(New Object() {"Pendapatan", "Potongan"})
        Me.cmbCustom.Location = New System.Drawing.Point(123, 290)
        Me.cmbCustom.Name = "cmbCustom"
        Me.cmbCustom.Size = New System.Drawing.Size(98, 22)
        Me.cmbCustom.TabIndex = 90
        '
        'chkLibur
        '
        Me.chkLibur.AutoSize = True
        Me.chkLibur.Location = New System.Drawing.Point(200, 11)
        Me.chkLibur.Name = "chkLibur"
        Me.chkLibur.Size = New System.Drawing.Size(71, 17)
        Me.chkLibur.TabIndex = 91
        Me.chkLibur.Text = "Hari Libur"
        Me.chkLibur.UseVisualStyleBackColor = True
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.BackColor = System.Drawing.Color.Transparent
        Me.Label3.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.Black
        Me.Label3.Location = New System.Drawing.Point(8, 10)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(65, 16)
        Me.Label3.TabIndex = 92
        Me.Label3.Text = "Hari Kerja"
        '
        'chknormal
        '
        Me.chknormal.AutoSize = True
        Me.chknormal.Location = New System.Drawing.Point(101, 10)
        Me.chknormal.Name = "chknormal"
        Me.chknormal.Size = New System.Drawing.Size(81, 17)
        Me.chknormal.TabIndex = 93
        Me.chknormal.Text = "Hari Normal"
        Me.chknormal.UseVisualStyleBackColor = True
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.BackColor = System.Drawing.Color.Transparent
        Me.Label4.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.Black
        Me.Label4.Location = New System.Drawing.Point(8, 35)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(76, 16)
        Me.Label4.TabIndex = 94
        Me.Label4.Text = "Perhitungan"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.BackColor = System.Drawing.Color.Transparent
        Me.Label5.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.Color.Black
        Me.Label5.Location = New System.Drawing.Point(8, 60)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(45, 16)
        Me.Label5.TabIndex = 95
        Me.Label5.Text = "Syarat"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.chkPC)
        Me.GroupBox1.Controls.Add(Me.chkTelat)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.chknormal)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.chkLibur)
        Me.GroupBox1.Controls.Add(Me.chkMasukterus)
        Me.GroupBox1.Controls.Add(Me.chkLembur)
        Me.GroupBox1.Controls.Add(Me.chkJam)
        Me.GroupBox1.Controls.Add(Me.chkHarian)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 112)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(477, 85)
        Me.GroupBox1.TabIndex = 96
        Me.GroupBox1.TabStop = False
        '
        'chkTelat
        '
        Me.chkTelat.AutoSize = True
        Me.chkTelat.Location = New System.Drawing.Point(307, 61)
        Me.chkTelat.Name = "chkTelat"
        Me.chkTelat.Size = New System.Drawing.Size(50, 17)
        Me.chkTelat.TabIndex = 96
        Me.chkTelat.Text = "Telat"
        Me.chkTelat.UseVisualStyleBackColor = True
        '
        'txtMinimal
        '
        Me.txtMinimal.Location = New System.Drawing.Point(113, 20)
        Me.txtMinimal.Name = "txtMinimal"
        Me.txtMinimal.Size = New System.Drawing.Size(49, 20)
        Me.txtMinimal.TabIndex = 97
        Me.txtMinimal.Text = "0"
        Me.txtMinimal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.BackColor = System.Drawing.Color.Transparent
        Me.Label6.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.Color.Black
        Me.Label6.Location = New System.Drawing.Point(8, 20)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(52, 16)
        Me.Label6.TabIndex = 97
        Me.Label6.Text = "Minimal"
        '
        'lblSatuan
        '
        Me.lblSatuan.AutoSize = True
        Me.lblSatuan.BackColor = System.Drawing.Color.Transparent
        Me.lblSatuan.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSatuan.ForeColor = System.Drawing.Color.Black
        Me.lblSatuan.Location = New System.Drawing.Point(168, 21)
        Me.lblSatuan.Name = "lblSatuan"
        Me.lblSatuan.Size = New System.Drawing.Size(31, 16)
        Me.lblSatuan.TabIndex = 98
        Me.lblSatuan.Text = "Jam"
        '
        'txtKelipatan
        '
        Me.txtKelipatan.Location = New System.Drawing.Point(110, 5)
        Me.txtKelipatan.Name = "txtKelipatan"
        Me.txtKelipatan.Size = New System.Drawing.Size(49, 20)
        Me.txtKelipatan.TabIndex = 99
        Me.txtKelipatan.Text = "0"
        Me.txtKelipatan.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.BackColor = System.Drawing.Color.Transparent
        Me.Label7.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.ForeColor = System.Drawing.Color.Black
        Me.Label7.Location = New System.Drawing.Point(168, 6)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(31, 16)
        Me.Label7.TabIndex = 100
        Me.Label7.Text = "Jam"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.BackColor = System.Drawing.Color.Transparent
        Me.Label8.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.ForeColor = System.Drawing.Color.Black
        Me.Label8.Location = New System.Drawing.Point(5, 6)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(60, 16)
        Me.Label8.TabIndex = 101
        Me.Label8.Text = "Kelipatan"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.Panel1)
        Me.GroupBox2.Controls.Add(Me.lblSatuan)
        Me.GroupBox2.Controls.Add(Me.Label6)
        Me.GroupBox2.Controls.Add(Me.txtMinimal)
        Me.GroupBox2.Location = New System.Drawing.Point(12, 203)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(477, 71)
        Me.GroupBox2.TabIndex = 102
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Syarat Lembur"
        Me.GroupBox2.Visible = False
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.Label8)
        Me.Panel1.Controls.Add(Me.Label7)
        Me.Panel1.Controls.Add(Me.txtKelipatan)
        Me.Panel1.Location = New System.Drawing.Point(3, 39)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(266, 31)
        Me.Panel1.TabIndex = 102
        Me.Panel1.Visible = False
        '
        'btnReset
        '
        Me.btnReset.Location = New System.Drawing.Point(222, 365)
        Me.btnReset.Name = "btnReset"
        Me.btnReset.Size = New System.Drawing.Size(75, 29)
        Me.btnReset.TabIndex = 103
        Me.btnReset.Text = "Reset"
        Me.btnReset.UseVisualStyleBackColor = True
        '
        'chkPC
        '
        Me.chkPC.AutoSize = True
        Me.chkPC.Location = New System.Drawing.Point(380, 61)
        Me.chkPC.Name = "chkPC"
        Me.chkPC.Size = New System.Drawing.Size(72, 17)
        Me.chkPC.TabIndex = 97
        Me.chkPC.Text = "Plg Cepat"
        Me.chkPC.UseVisualStyleBackColor = True
        '
        'frmMasterKomponenGaji
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(501, 422)
        Me.Controls.Add(Me.btnReset)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.cmbCustom)
        Me.Controls.Add(Me.chkCustom)
        Me.Controls.Add(Me.chkManual)
        Me.Controls.Add(Me.txtNama)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.cmbTipe)
        Me.Controls.Add(Me.Label26)
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.btnSearch)
        Me.Controls.Add(Me.btnHapus)
        Me.Controls.Add(Me.txtKode)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.btnSave)
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmMasterKomponenGaji"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Komponen Gaji"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnSave As System.Windows.Forms.Button
    Friend WithEvents txtKode As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents btnHapus As System.Windows.Forms.Button
    Friend WithEvents btnSearch As System.Windows.Forms.Button
    Friend WithEvents btnExit As System.Windows.Forms.Button
    Friend WithEvents Label26 As System.Windows.Forms.Label
    Friend WithEvents cmbTipe As System.Windows.Forms.ComboBox
    Friend WithEvents txtNama As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents chkLembur As System.Windows.Forms.CheckBox
    Friend WithEvents chkMasukterus As System.Windows.Forms.CheckBox
    Friend WithEvents chkManual As System.Windows.Forms.CheckBox
    Friend WithEvents chkHarian As System.Windows.Forms.CheckBox
    Friend WithEvents chkJam As System.Windows.Forms.CheckBox
    Friend WithEvents chkCustom As System.Windows.Forms.CheckBox
    Friend WithEvents cmbCustom As System.Windows.Forms.ComboBox
    Friend WithEvents chkLibur As System.Windows.Forms.CheckBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents chknormal As System.Windows.Forms.CheckBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents chkTelat As System.Windows.Forms.CheckBox
    Friend WithEvents txtMinimal As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents lblSatuan As System.Windows.Forms.Label
    Friend WithEvents txtKelipatan As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents btnReset As System.Windows.Forms.Button
    Friend WithEvents chkPC As System.Windows.Forms.CheckBox
End Class
