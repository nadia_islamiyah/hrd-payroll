﻿Imports System.Data.SqlClient
Imports System.Data.Sql
Imports System.IO
Public Class frmMasterKomponenGaji
    Dim da As SqlDataAdapter
    Dim ds As New DataSet
    Private Sub reloadcombo()
        Dim conn As New SqlConnection
        Dim cmd As New SqlCommand
        Dim dr As SqlDataReader
        conn.ConnectionString = strcon
        conn.Open()


        cmd = New SqlCommand("select * from var_custompayroll order by jenis", conn)
        dr = cmd.ExecuteReader
        cmbCustom.Items.Clear()
        cmbCustom.Items.Add("")
        While dr.Read
            cmbCustom.Items.Add(dr.Item(0))
        End While
        conn.Close()
    End Sub
    Private Sub frmMastergroupkerja_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown

        If e.KeyCode = Keys.Escape Then Me.Close()
        If e.KeyCode = Keys.Enter Then SendKeys.Send("{tab}")
    End Sub

    Private Sub frmMastergroupkerja_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        reloadcombo()
        reset()
    End Sub
    Private Sub reload()
        Dim conn As New SqlConnection
        Dim cmd As New SqlCommand
        Dim dr As SqlDataReader
        conn.ConnectionString = strcon
        conn.Open()


        cmd = New SqlCommand("select * from ms_komponengaji where kode_komponengaji='" & txtKode.Text & "'", conn)
        dr = cmd.ExecuteReader
        dr.Read()
        If dr.HasRows Then
            cmbTipe.Text = dr.Item(1)
            txtNama.Text = dr.Item("nama")
            chkHarian.Checked = dr.Item("harian")
            txtKelipatan.Text = dr.Item("lembur_kelipatan")
            txtMinimal.Text = dr.Item("lembur_minimal")
            chkJam.Checked = dr.Item("jam")
            chkLembur.Checked = dr.Item("lembur")
            chkMasukterus.Checked = dr.Item("masukpenuh")
            chkManual.Checked = dr.Item("manual")
            chkLibur.Checked = dr.Item("libur")
            chknormal.Checked = dr.Item("normal")
            chkCustom.Checked = dr.Item("custom")
            cmbCustom.Text = dr.Item("custom_name")
            chkTelat.Checked = dr.Item("telat")
            chkPC.Checked = dr.Item("pc")
        End If
        conn.Close()

    End Sub
    Private Sub reset()
        txtNama.Text = ""
        txtKode.Text = ""
        txtKode.Focus()
        cmbCustom.Text = ""
        txtMinimal.Text = 0
        txtKelipatan.Text = 0
        chkCustom.Checked = False
        chkHarian.Checked = False
        chkJam.Checked = False
        chkLembur.Checked = False
        chkLibur.Checked = False
        chkManual.Checked = False
        chkMasukterus.Checked = False
        chknormal.Checked = False
        chkTelat.Checked = False
        chkPC.Checked = False
    End Sub
    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim conn As New SqlConnection
        Dim cmd As New SqlCommand
        conn.ConnectionString = strcon
        conn.Open()

        cmd = New SqlCommand("delete from ms_komponengaji where kode_komponengaji='" & txtKode.Text & "'", conn)
        cmd.ExecuteNonQuery()
        cmd = New SqlCommand(add_data, conn)
        cmd.ExecuteNonQuery()
        conn.Close()
        reset()
    End Sub
    Private Function add_data() As String
        Dim fields() As String
        Dim nilai() As String
        Dim table_name As String
        ReDim fields(16)
        ReDim nilai(16)
        table_name = "ms_komponengaji"
        fields(0) = "kode_komponengaji"
        nilai(0) = txtKode.Text
        fields(1) = "nama"
        nilai(1) = txtNama.Text
        fields(2) = "tipe"
        nilai(2) = cmbTipe.Text
        fields(3) = "harian"
        nilai(3) = chkHarian.Checked
        fields(4) = "manual"
        nilai(4) = chkManual.Checked
        fields(5) = "lembur"
        nilai(5) = chkLembur.Checked
        fields(6) = "jam"
        nilai(6) = chkJam.Checked
        fields(7) = "masukpenuh"
        nilai(7) = chkMasukterus.Checked
        fields(8) = "libur"
        nilai(8) = chkLibur.Checked
        fields(9) = "custom"
        nilai(9) = chkCustom.Checked
        fields(10) = "custom_name"
        nilai(10) = cmbCustom.Text
        fields(11) = "normal"
        nilai(11) = chknormal.Checked
        fields(12) = "telat"
        nilai(12) = chkTelat.Checked
        fields(13) = "lembur_minimal"
        nilai(13) = txtMinimal.Text
        fields(14) = "lembur_kelipatan"
        nilai(14) = txtKelipatan.Text
        fields(15) = "pc"
        nilai(15) = chkPC.Checked

        add_data = query_tambah_data(table_name, fields, nilai)
    End Function
    Private Sub btnHapus_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnHapus.Click
        If MsgBox("Apakah anda yakin hendak menghapus " & txtKode.Text & " ?", vbYesNo) = MsgBoxResult.Yes Then
            Dim conn As New SqlConnection
            Dim cmd As New SqlCommand
            conn.ConnectionString = strcon
            conn.Open()

            cmd = New SqlCommand("delete from ms_komponengaji where kode_komponengaji='" & txtKode.Text & "'", conn)
            cmd.ExecuteNonQuery()

            conn.Close()

        End If
    End Sub

    Private Sub txtNama_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtKode.LostFocus
        reload()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Dim f As New frmSearch
        f.setCol(0)
        f.setTableName("ms_komponengaji")
        f.setColumns("*")

        If f.ShowDialog = System.Windows.Forms.DialogResult.OK Then
            txtKode.Text = f.value
            reload()
        End If
        f.Dispose()
    End Sub

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        Me.Close()
    End Sub

    Private Sub txtNama_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtKode.TextChanged

    End Sub


    Private Sub chkCustom_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkCustom.CheckedChanged
        If chkCustom.Checked = False Then cmbCustom.Text = ""
    End Sub

    Private Sub chkLembur_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles chkLembur.CheckedChanged
        If chkLembur.Checked Then
            GroupBox2.Visible = True
        Else
            GroupBox2.Visible = False
        End If
    End Sub

    Private Sub chkJam_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles chkJam.CheckedChanged
        If chkJam.Checked Then
            Panel1.Visible = True
        Else
            Panel1.Visible = False
        End If
    End Sub

    Private Sub btnReset_Click(sender As System.Object, e As System.EventArgs) Handles btnReset.Click
        reset()
    End Sub
End Class