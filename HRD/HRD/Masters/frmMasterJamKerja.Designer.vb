﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMasterJamKerja
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnSave = New System.Windows.Forms.Button()
        Me.txtNama = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.grpKontrak = New System.Windows.Forms.GroupBox()
        Me.cmbMasukPembulatan = New System.Windows.Forms.ComboBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txtMasukToleransiSesudah = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtMasukToleransiSebelum = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.jamMasuk = New System.Windows.Forms.DateTimePicker()
        Me.chkMasukPembulatan = New System.Windows.Forms.CheckBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.btnHapus = New System.Windows.Forms.Button()
        Me.btnSearch = New System.Windows.Forms.Button()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.cmbIstirahatPembulatan = New System.Windows.Forms.ComboBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.txtIstirahatToleransiSesudah = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.txtIstirahatToleransiSebelum = New System.Windows.Forms.TextBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.jamIstirahat = New System.Windows.Forms.DateTimePicker()
        Me.chkIstirahatPembulatan = New System.Windows.Forms.CheckBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.cmbKembaliPembulatan = New System.Windows.Forms.ComboBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.txtKembaliToleransiSesudah = New System.Windows.Forms.TextBox()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.txtKembaliToleransiSebelum = New System.Windows.Forms.TextBox()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.jamKembali = New System.Windows.Forms.DateTimePicker()
        Me.chkKembaliPembulatan = New System.Windows.Forms.CheckBox()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.cmbPulangPembulatan = New System.Windows.Forms.ComboBox()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.txtPulangToleransiSesudah = New System.Windows.Forms.TextBox()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.txtPulangToleransiSebelum = New System.Windows.Forms.TextBox()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.jamPulang = New System.Windows.Forms.DateTimePicker()
        Me.chkPulangPembulatan = New System.Windows.Forms.CheckBox()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.btnExit = New System.Windows.Forms.Button()
        Me.txtJamKerja = New System.Windows.Forms.TextBox()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.Label27 = New System.Windows.Forms.Label()
        Me.chknormal = New System.Windows.Forms.CheckBox()
        Me.chkLibur = New System.Windows.Forms.CheckBox()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.btnNext = New System.Windows.Forms.Button()
        Me.btnPrev = New System.Windows.Forms.Button()
        Me.Label28 = New System.Windows.Forms.Label()
        Me.cmbgolongan = New System.Windows.Forms.ComboBox()
        Me.grpKontrak.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnSave
        '
        Me.btnSave.Location = New System.Drawing.Point(541, 95)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(88, 35)
        Me.btnSave.TabIndex = 6
        Me.btnSave.Text = "&Simpan"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'txtNama
        '
        Me.txtNama.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNama.Location = New System.Drawing.Point(118, 26)
        Me.txtNama.Name = "txtNama"
        Me.txtNama.Size = New System.Drawing.Size(134, 23)
        Me.txtNama.TabIndex = 0
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.BackColor = System.Drawing.Color.Transparent
        Me.Label2.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.Black
        Me.Label2.Location = New System.Drawing.Point(18, 26)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(71, 16)
        Me.Label2.TabIndex = 14
        Me.Label2.Text = "Nama Shift"
        '
        'grpKontrak
        '
        Me.grpKontrak.Controls.Add(Me.cmbMasukPembulatan)
        Me.grpKontrak.Controls.Add(Me.Label7)
        Me.grpKontrak.Controls.Add(Me.Label5)
        Me.grpKontrak.Controls.Add(Me.txtMasukToleransiSesudah)
        Me.grpKontrak.Controls.Add(Me.Label6)
        Me.grpKontrak.Controls.Add(Me.Label4)
        Me.grpKontrak.Controls.Add(Me.txtMasukToleransiSebelum)
        Me.grpKontrak.Controls.Add(Me.Label3)
        Me.grpKontrak.Controls.Add(Me.jamMasuk)
        Me.grpKontrak.Controls.Add(Me.chkMasukPembulatan)
        Me.grpKontrak.Controls.Add(Me.Label1)
        Me.grpKontrak.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grpKontrak.Location = New System.Drawing.Point(12, 120)
        Me.grpKontrak.Name = "grpKontrak"
        Me.grpKontrak.Size = New System.Drawing.Size(491, 92)
        Me.grpKontrak.TabIndex = 18
        Me.grpKontrak.TabStop = False
        Me.grpKontrak.Text = "Masuk"
        '
        'cmbMasukPembulatan
        '
        Me.cmbMasukPembulatan.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbMasukPembulatan.FormattingEnabled = True
        Me.cmbMasukPembulatan.Items.AddRange(New Object() {"Keatas                                                            True", "Kebawah                                                       False"})
        Me.cmbMasukPembulatan.Location = New System.Drawing.Point(106, 50)
        Me.cmbMasukPembulatan.Name = "cmbMasukPembulatan"
        Me.cmbMasukPembulatan.Size = New System.Drawing.Size(79, 26)
        Me.cmbMasukPembulatan.TabIndex = 30
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.BackColor = System.Drawing.Color.Transparent
        Me.Label7.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.ForeColor = System.Drawing.Color.Black
        Me.Label7.Location = New System.Drawing.Point(256, 26)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(58, 16)
        Me.Label7.TabIndex = 29
        Me.Label7.Text = "Sebelum"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.BackColor = System.Drawing.Color.Transparent
        Me.Label5.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.Color.Black
        Me.Label5.Location = New System.Drawing.Point(376, 55)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(19, 16)
        Me.Label5.TabIndex = 28
        Me.Label5.Text = "m"
        '
        'txtMasukToleransiSesudah
        '
        Me.txtMasukToleransiSesudah.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMasukToleransiSesudah.Location = New System.Drawing.Point(317, 52)
        Me.txtMasukToleransiSesudah.Name = "txtMasukToleransiSesudah"
        Me.txtMasukToleransiSesudah.Size = New System.Drawing.Size(53, 23)
        Me.txtMasukToleransiSesudah.TabIndex = 26
        Me.txtMasukToleransiSesudah.Text = "0"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.BackColor = System.Drawing.Color.Transparent
        Me.Label6.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.Color.Black
        Me.Label6.Location = New System.Drawing.Point(256, 55)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(57, 16)
        Me.Label6.TabIndex = 27
        Me.Label6.Text = "Sesudah"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.BackColor = System.Drawing.Color.Transparent
        Me.Label4.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.Black
        Me.Label4.Location = New System.Drawing.Point(376, 26)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(19, 16)
        Me.Label4.TabIndex = 25
        Me.Label4.Text = "m"
        '
        'txtMasukToleransiSebelum
        '
        Me.txtMasukToleransiSebelum.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMasukToleransiSebelum.Location = New System.Drawing.Point(317, 23)
        Me.txtMasukToleransiSebelum.Name = "txtMasukToleransiSebelum"
        Me.txtMasukToleransiSebelum.Size = New System.Drawing.Size(53, 23)
        Me.txtMasukToleransiSebelum.TabIndex = 23
        Me.txtMasukToleransiSebelum.Text = "0"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.BackColor = System.Drawing.Color.Transparent
        Me.Label3.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.Black
        Me.Label3.Location = New System.Drawing.Point(196, 26)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(61, 16)
        Me.Label3.TabIndex = 24
        Me.Label3.Text = "Toleransi"
        '
        'jamMasuk
        '
        Me.jamMasuk.CustomFormat = "HH:mm"
        Me.jamMasuk.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.jamMasuk.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.jamMasuk.Location = New System.Drawing.Point(52, 23)
        Me.jamMasuk.Name = "jamMasuk"
        Me.jamMasuk.ShowUpDown = True
        Me.jamMasuk.Size = New System.Drawing.Size(68, 23)
        Me.jamMasuk.TabIndex = 2
        Me.jamMasuk.Value = New Date(2013, 3, 2, 0, 0, 0, 0)
        '
        'chkMasukPembulatan
        '
        Me.chkMasukPembulatan.AutoSize = True
        Me.chkMasukPembulatan.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkMasukPembulatan.Location = New System.Drawing.Point(18, 56)
        Me.chkMasukPembulatan.Name = "chkMasukPembulatan"
        Me.chkMasukPembulatan.Size = New System.Drawing.Size(82, 17)
        Me.chkMasukPembulatan.TabIndex = 21
        Me.chkMasukPembulatan.Text = "Pembulatan"
        Me.chkMasukPembulatan.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.Black
        Me.Label1.Location = New System.Drawing.Point(15, 26)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(31, 16)
        Me.Label1.TabIndex = 19
        Me.Label1.Text = "Jam"
        '
        'btnHapus
        '
        Me.btnHapus.Location = New System.Drawing.Point(541, 136)
        Me.btnHapus.Name = "btnHapus"
        Me.btnHapus.Size = New System.Drawing.Size(88, 35)
        Me.btnHapus.TabIndex = 7
        Me.btnHapus.Text = "&Hapus"
        Me.btnHapus.UseVisualStyleBackColor = True
        '
        'btnSearch
        '
        Me.btnSearch.Location = New System.Drawing.Point(258, 26)
        Me.btnSearch.Name = "btnSearch"
        Me.btnSearch.Size = New System.Drawing.Size(37, 23)
        Me.btnSearch.TabIndex = 21
        Me.btnSearch.Text = "F3"
        Me.btnSearch.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.cmbIstirahatPembulatan)
        Me.GroupBox1.Controls.Add(Me.Label8)
        Me.GroupBox1.Controls.Add(Me.Label9)
        Me.GroupBox1.Controls.Add(Me.txtIstirahatToleransiSesudah)
        Me.GroupBox1.Controls.Add(Me.Label10)
        Me.GroupBox1.Controls.Add(Me.Label11)
        Me.GroupBox1.Controls.Add(Me.txtIstirahatToleransiSebelum)
        Me.GroupBox1.Controls.Add(Me.Label12)
        Me.GroupBox1.Controls.Add(Me.jamIstirahat)
        Me.GroupBox1.Controls.Add(Me.chkIstirahatPembulatan)
        Me.GroupBox1.Controls.Add(Me.Label13)
        Me.GroupBox1.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(12, 218)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(491, 92)
        Me.GroupBox1.TabIndex = 22
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Istirahat"
        '
        'cmbIstirahatPembulatan
        '
        Me.cmbIstirahatPembulatan.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbIstirahatPembulatan.FormattingEnabled = True
        Me.cmbIstirahatPembulatan.Items.AddRange(New Object() {"Keatas                                                            True", "Kebawah                                                       False"})
        Me.cmbIstirahatPembulatan.Location = New System.Drawing.Point(106, 49)
        Me.cmbIstirahatPembulatan.Name = "cmbIstirahatPembulatan"
        Me.cmbIstirahatPembulatan.Size = New System.Drawing.Size(79, 26)
        Me.cmbIstirahatPembulatan.TabIndex = 30
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.BackColor = System.Drawing.Color.Transparent
        Me.Label8.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.ForeColor = System.Drawing.Color.Black
        Me.Label8.Location = New System.Drawing.Point(256, 25)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(58, 16)
        Me.Label8.TabIndex = 29
        Me.Label8.Text = "Sebelum"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.BackColor = System.Drawing.Color.Transparent
        Me.Label9.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.ForeColor = System.Drawing.Color.Black
        Me.Label9.Location = New System.Drawing.Point(376, 54)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(19, 16)
        Me.Label9.TabIndex = 28
        Me.Label9.Text = "m"
        '
        'txtIstirahatToleransiSesudah
        '
        Me.txtIstirahatToleransiSesudah.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtIstirahatToleransiSesudah.Location = New System.Drawing.Point(317, 51)
        Me.txtIstirahatToleransiSesudah.Name = "txtIstirahatToleransiSesudah"
        Me.txtIstirahatToleransiSesudah.Size = New System.Drawing.Size(53, 23)
        Me.txtIstirahatToleransiSesudah.TabIndex = 26
        Me.txtIstirahatToleransiSesudah.Text = "0"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.BackColor = System.Drawing.Color.Transparent
        Me.Label10.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.ForeColor = System.Drawing.Color.Black
        Me.Label10.Location = New System.Drawing.Point(256, 54)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(57, 16)
        Me.Label10.TabIndex = 27
        Me.Label10.Text = "Sesudah"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.BackColor = System.Drawing.Color.Transparent
        Me.Label11.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.ForeColor = System.Drawing.Color.Black
        Me.Label11.Location = New System.Drawing.Point(376, 25)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(19, 16)
        Me.Label11.TabIndex = 25
        Me.Label11.Text = "m"
        '
        'txtIstirahatToleransiSebelum
        '
        Me.txtIstirahatToleransiSebelum.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtIstirahatToleransiSebelum.Location = New System.Drawing.Point(317, 22)
        Me.txtIstirahatToleransiSebelum.Name = "txtIstirahatToleransiSebelum"
        Me.txtIstirahatToleransiSebelum.Size = New System.Drawing.Size(53, 23)
        Me.txtIstirahatToleransiSebelum.TabIndex = 23
        Me.txtIstirahatToleransiSebelum.Text = "0"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.BackColor = System.Drawing.Color.Transparent
        Me.Label12.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.ForeColor = System.Drawing.Color.Black
        Me.Label12.Location = New System.Drawing.Point(196, 25)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(61, 16)
        Me.Label12.TabIndex = 24
        Me.Label12.Text = "Toleransi"
        '
        'jamIstirahat
        '
        Me.jamIstirahat.CustomFormat = "HH:mm"
        Me.jamIstirahat.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.jamIstirahat.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.jamIstirahat.Location = New System.Drawing.Point(52, 22)
        Me.jamIstirahat.Name = "jamIstirahat"
        Me.jamIstirahat.ShowUpDown = True
        Me.jamIstirahat.Size = New System.Drawing.Size(68, 23)
        Me.jamIstirahat.TabIndex = 3
        Me.jamIstirahat.Value = New Date(2013, 3, 2, 0, 0, 0, 0)
        '
        'chkIstirahatPembulatan
        '
        Me.chkIstirahatPembulatan.AutoSize = True
        Me.chkIstirahatPembulatan.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkIstirahatPembulatan.Location = New System.Drawing.Point(18, 55)
        Me.chkIstirahatPembulatan.Name = "chkIstirahatPembulatan"
        Me.chkIstirahatPembulatan.Size = New System.Drawing.Size(82, 17)
        Me.chkIstirahatPembulatan.TabIndex = 21
        Me.chkIstirahatPembulatan.Text = "Pembulatan"
        Me.chkIstirahatPembulatan.UseVisualStyleBackColor = True
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.BackColor = System.Drawing.Color.Transparent
        Me.Label13.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.ForeColor = System.Drawing.Color.Black
        Me.Label13.Location = New System.Drawing.Point(15, 25)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(31, 16)
        Me.Label13.TabIndex = 19
        Me.Label13.Text = "Jam"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.cmbKembaliPembulatan)
        Me.GroupBox2.Controls.Add(Me.Label14)
        Me.GroupBox2.Controls.Add(Me.Label15)
        Me.GroupBox2.Controls.Add(Me.txtKembaliToleransiSesudah)
        Me.GroupBox2.Controls.Add(Me.Label16)
        Me.GroupBox2.Controls.Add(Me.Label17)
        Me.GroupBox2.Controls.Add(Me.txtKembaliToleransiSebelum)
        Me.GroupBox2.Controls.Add(Me.Label18)
        Me.GroupBox2.Controls.Add(Me.jamKembali)
        Me.GroupBox2.Controls.Add(Me.chkKembaliPembulatan)
        Me.GroupBox2.Controls.Add(Me.Label19)
        Me.GroupBox2.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.Location = New System.Drawing.Point(12, 316)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(491, 92)
        Me.GroupBox2.TabIndex = 23
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Kembali"
        '
        'cmbKembaliPembulatan
        '
        Me.cmbKembaliPembulatan.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbKembaliPembulatan.FormattingEnabled = True
        Me.cmbKembaliPembulatan.Items.AddRange(New Object() {"Keatas                                                            True", "Kebawah                                                       False"})
        Me.cmbKembaliPembulatan.Location = New System.Drawing.Point(106, 49)
        Me.cmbKembaliPembulatan.Name = "cmbKembaliPembulatan"
        Me.cmbKembaliPembulatan.Size = New System.Drawing.Size(79, 26)
        Me.cmbKembaliPembulatan.TabIndex = 30
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.BackColor = System.Drawing.Color.Transparent
        Me.Label14.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.ForeColor = System.Drawing.Color.Black
        Me.Label14.Location = New System.Drawing.Point(256, 25)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(58, 16)
        Me.Label14.TabIndex = 29
        Me.Label14.Text = "Sebelum"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.BackColor = System.Drawing.Color.Transparent
        Me.Label15.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.ForeColor = System.Drawing.Color.Black
        Me.Label15.Location = New System.Drawing.Point(376, 54)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(19, 16)
        Me.Label15.TabIndex = 28
        Me.Label15.Text = "m"
        '
        'txtKembaliToleransiSesudah
        '
        Me.txtKembaliToleransiSesudah.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtKembaliToleransiSesudah.Location = New System.Drawing.Point(317, 51)
        Me.txtKembaliToleransiSesudah.Name = "txtKembaliToleransiSesudah"
        Me.txtKembaliToleransiSesudah.Size = New System.Drawing.Size(53, 23)
        Me.txtKembaliToleransiSesudah.TabIndex = 26
        Me.txtKembaliToleransiSesudah.Text = "0"
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.BackColor = System.Drawing.Color.Transparent
        Me.Label16.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.ForeColor = System.Drawing.Color.Black
        Me.Label16.Location = New System.Drawing.Point(256, 54)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(57, 16)
        Me.Label16.TabIndex = 27
        Me.Label16.Text = "Sesudah"
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.BackColor = System.Drawing.Color.Transparent
        Me.Label17.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label17.ForeColor = System.Drawing.Color.Black
        Me.Label17.Location = New System.Drawing.Point(376, 25)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(19, 16)
        Me.Label17.TabIndex = 25
        Me.Label17.Text = "m"
        '
        'txtKembaliToleransiSebelum
        '
        Me.txtKembaliToleransiSebelum.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtKembaliToleransiSebelum.Location = New System.Drawing.Point(317, 22)
        Me.txtKembaliToleransiSebelum.Name = "txtKembaliToleransiSebelum"
        Me.txtKembaliToleransiSebelum.Size = New System.Drawing.Size(53, 23)
        Me.txtKembaliToleransiSebelum.TabIndex = 23
        Me.txtKembaliToleransiSebelum.Text = "0"
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.BackColor = System.Drawing.Color.Transparent
        Me.Label18.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label18.ForeColor = System.Drawing.Color.Black
        Me.Label18.Location = New System.Drawing.Point(196, 25)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(61, 16)
        Me.Label18.TabIndex = 24
        Me.Label18.Text = "Toleransi"
        '
        'jamKembali
        '
        Me.jamKembali.CustomFormat = "HH:mm"
        Me.jamKembali.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.jamKembali.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.jamKembali.Location = New System.Drawing.Point(52, 22)
        Me.jamKembali.Name = "jamKembali"
        Me.jamKembali.ShowUpDown = True
        Me.jamKembali.Size = New System.Drawing.Size(68, 23)
        Me.jamKembali.TabIndex = 4
        Me.jamKembali.Value = New Date(2013, 3, 2, 0, 0, 0, 0)
        '
        'chkKembaliPembulatan
        '
        Me.chkKembaliPembulatan.AutoSize = True
        Me.chkKembaliPembulatan.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkKembaliPembulatan.Location = New System.Drawing.Point(18, 55)
        Me.chkKembaliPembulatan.Name = "chkKembaliPembulatan"
        Me.chkKembaliPembulatan.Size = New System.Drawing.Size(82, 17)
        Me.chkKembaliPembulatan.TabIndex = 21
        Me.chkKembaliPembulatan.Text = "Pembulatan"
        Me.chkKembaliPembulatan.UseVisualStyleBackColor = True
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.BackColor = System.Drawing.Color.Transparent
        Me.Label19.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label19.ForeColor = System.Drawing.Color.Black
        Me.Label19.Location = New System.Drawing.Point(15, 25)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(31, 16)
        Me.Label19.TabIndex = 19
        Me.Label19.Text = "Jam"
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.cmbPulangPembulatan)
        Me.GroupBox3.Controls.Add(Me.Label20)
        Me.GroupBox3.Controls.Add(Me.Label21)
        Me.GroupBox3.Controls.Add(Me.txtPulangToleransiSesudah)
        Me.GroupBox3.Controls.Add(Me.Label22)
        Me.GroupBox3.Controls.Add(Me.Label23)
        Me.GroupBox3.Controls.Add(Me.txtPulangToleransiSebelum)
        Me.GroupBox3.Controls.Add(Me.Label24)
        Me.GroupBox3.Controls.Add(Me.jamPulang)
        Me.GroupBox3.Controls.Add(Me.chkPulangPembulatan)
        Me.GroupBox3.Controls.Add(Me.Label25)
        Me.GroupBox3.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox3.Location = New System.Drawing.Point(12, 414)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(491, 92)
        Me.GroupBox3.TabIndex = 24
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Pulang"
        '
        'cmbPulangPembulatan
        '
        Me.cmbPulangPembulatan.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbPulangPembulatan.FormattingEnabled = True
        Me.cmbPulangPembulatan.Items.AddRange(New Object() {"Keatas                                                            True", "Kebawah                                                       False"})
        Me.cmbPulangPembulatan.Location = New System.Drawing.Point(106, 49)
        Me.cmbPulangPembulatan.Name = "cmbPulangPembulatan"
        Me.cmbPulangPembulatan.Size = New System.Drawing.Size(79, 26)
        Me.cmbPulangPembulatan.TabIndex = 30
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.BackColor = System.Drawing.Color.Transparent
        Me.Label20.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label20.ForeColor = System.Drawing.Color.Black
        Me.Label20.Location = New System.Drawing.Point(256, 25)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(58, 16)
        Me.Label20.TabIndex = 29
        Me.Label20.Text = "Sebelum"
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.BackColor = System.Drawing.Color.Transparent
        Me.Label21.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label21.ForeColor = System.Drawing.Color.Black
        Me.Label21.Location = New System.Drawing.Point(376, 54)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(19, 16)
        Me.Label21.TabIndex = 28
        Me.Label21.Text = "m"
        '
        'txtPulangToleransiSesudah
        '
        Me.txtPulangToleransiSesudah.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPulangToleransiSesudah.Location = New System.Drawing.Point(317, 51)
        Me.txtPulangToleransiSesudah.Name = "txtPulangToleransiSesudah"
        Me.txtPulangToleransiSesudah.Size = New System.Drawing.Size(53, 23)
        Me.txtPulangToleransiSesudah.TabIndex = 26
        Me.txtPulangToleransiSesudah.Text = "0"
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.BackColor = System.Drawing.Color.Transparent
        Me.Label22.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label22.ForeColor = System.Drawing.Color.Black
        Me.Label22.Location = New System.Drawing.Point(256, 54)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(57, 16)
        Me.Label22.TabIndex = 27
        Me.Label22.Text = "Sesudah"
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.BackColor = System.Drawing.Color.Transparent
        Me.Label23.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label23.ForeColor = System.Drawing.Color.Black
        Me.Label23.Location = New System.Drawing.Point(376, 25)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(19, 16)
        Me.Label23.TabIndex = 25
        Me.Label23.Text = "m"
        '
        'txtPulangToleransiSebelum
        '
        Me.txtPulangToleransiSebelum.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPulangToleransiSebelum.Location = New System.Drawing.Point(317, 22)
        Me.txtPulangToleransiSebelum.Name = "txtPulangToleransiSebelum"
        Me.txtPulangToleransiSebelum.Size = New System.Drawing.Size(53, 23)
        Me.txtPulangToleransiSebelum.TabIndex = 23
        Me.txtPulangToleransiSebelum.Text = "0"
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.BackColor = System.Drawing.Color.Transparent
        Me.Label24.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label24.ForeColor = System.Drawing.Color.Black
        Me.Label24.Location = New System.Drawing.Point(196, 25)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(61, 16)
        Me.Label24.TabIndex = 24
        Me.Label24.Text = "Toleransi"
        '
        'jamPulang
        '
        Me.jamPulang.CustomFormat = "HH:mm"
        Me.jamPulang.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.jamPulang.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.jamPulang.Location = New System.Drawing.Point(52, 22)
        Me.jamPulang.Name = "jamPulang"
        Me.jamPulang.ShowUpDown = True
        Me.jamPulang.Size = New System.Drawing.Size(68, 23)
        Me.jamPulang.TabIndex = 5
        Me.jamPulang.Value = New Date(2013, 3, 2, 0, 0, 0, 0)
        '
        'chkPulangPembulatan
        '
        Me.chkPulangPembulatan.AutoSize = True
        Me.chkPulangPembulatan.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkPulangPembulatan.Location = New System.Drawing.Point(18, 55)
        Me.chkPulangPembulatan.Name = "chkPulangPembulatan"
        Me.chkPulangPembulatan.Size = New System.Drawing.Size(82, 17)
        Me.chkPulangPembulatan.TabIndex = 21
        Me.chkPulangPembulatan.Text = "Pembulatan"
        Me.chkPulangPembulatan.UseVisualStyleBackColor = True
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.BackColor = System.Drawing.Color.Transparent
        Me.Label25.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label25.ForeColor = System.Drawing.Color.Black
        Me.Label25.Location = New System.Drawing.Point(15, 25)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(31, 16)
        Me.Label25.TabIndex = 19
        Me.Label25.Text = "Jam"
        '
        'btnExit
        '
        Me.btnExit.Location = New System.Drawing.Point(541, 177)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(88, 35)
        Me.btnExit.TabIndex = 8
        Me.btnExit.Text = "&Keluar"
        Me.btnExit.UseVisualStyleBackColor = True
        '
        'txtJamKerja
        '
        Me.txtJamKerja.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtJamKerja.Location = New System.Drawing.Point(118, 55)
        Me.txtJamKerja.Name = "txtJamKerja"
        Me.txtJamKerja.Size = New System.Drawing.Size(79, 23)
        Me.txtJamKerja.TabIndex = 1
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        Me.Label26.BackColor = System.Drawing.Color.Transparent
        Me.Label26.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label26.ForeColor = System.Drawing.Color.Black
        Me.Label26.Location = New System.Drawing.Point(18, 55)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(65, 16)
        Me.Label26.TabIndex = 27
        Me.Label26.Text = "Jam Kerja"
        '
        'Label27
        '
        Me.Label27.AutoSize = True
        Me.Label27.BackColor = System.Drawing.Color.Transparent
        Me.Label27.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label27.ForeColor = System.Drawing.Color.Black
        Me.Label27.Location = New System.Drawing.Point(203, 59)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(31, 16)
        Me.Label27.TabIndex = 28
        Me.Label27.Text = "Jam"
        '
        'chknormal
        '
        Me.chknormal.AutoSize = True
        Me.chknormal.Location = New System.Drawing.Point(18, 23)
        Me.chknormal.Name = "chknormal"
        Me.chknormal.Size = New System.Drawing.Size(81, 17)
        Me.chknormal.TabIndex = 95
        Me.chknormal.Text = "Hari Normal"
        Me.chknormal.UseVisualStyleBackColor = True
        '
        'chkLibur
        '
        Me.chkLibur.AutoSize = True
        Me.chkLibur.Location = New System.Drawing.Point(18, 47)
        Me.chkLibur.Name = "chkLibur"
        Me.chkLibur.Size = New System.Drawing.Size(71, 17)
        Me.chkLibur.TabIndex = 94
        Me.chkLibur.Text = "Hari Libur"
        Me.chkLibur.UseVisualStyleBackColor = True
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.chknormal)
        Me.GroupBox4.Controls.Add(Me.chkLibur)
        Me.GroupBox4.Location = New System.Drawing.Point(375, 12)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(128, 77)
        Me.GroupBox4.TabIndex = 96
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = "Shift Untuk"
        '
        'btnNext
        '
        Me.btnNext.ForeColor = System.Drawing.Color.Black
        Me.btnNext.Location = New System.Drawing.Point(339, 27)
        Me.btnNext.Name = "btnNext"
        Me.btnNext.Size = New System.Drawing.Size(32, 23)
        Me.btnNext.TabIndex = 164
        Me.btnNext.TabStop = False
        Me.btnNext.Text = ">>"
        Me.btnNext.UseVisualStyleBackColor = True
        '
        'btnPrev
        '
        Me.btnPrev.ForeColor = System.Drawing.Color.Black
        Me.btnPrev.Location = New System.Drawing.Point(301, 27)
        Me.btnPrev.Name = "btnPrev"
        Me.btnPrev.Size = New System.Drawing.Size(32, 23)
        Me.btnPrev.TabIndex = 163
        Me.btnPrev.TabStop = False
        Me.btnPrev.Text = "<<"
        Me.btnPrev.UseVisualStyleBackColor = True
        '
        'Label28
        '
        Me.Label28.AutoSize = True
        Me.Label28.BackColor = System.Drawing.Color.Transparent
        Me.Label28.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label28.ForeColor = System.Drawing.Color.Black
        Me.Label28.Location = New System.Drawing.Point(18, 86)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(91, 16)
        Me.Label28.TabIndex = 165
        Me.Label28.Text = "Golongan Shift"
        '
        'cmbgolongan
        '
        Me.cmbgolongan.FormattingEnabled = True
        Me.cmbgolongan.Location = New System.Drawing.Point(118, 85)
        Me.cmbgolongan.Name = "cmbgolongan"
        Me.cmbgolongan.Size = New System.Drawing.Size(134, 21)
        Me.cmbgolongan.TabIndex = 166
        '
        'frmMasterJamKerja
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(664, 531)
        Me.Controls.Add(Me.cmbgolongan)
        Me.Controls.Add(Me.Label28)
        Me.Controls.Add(Me.btnNext)
        Me.Controls.Add(Me.btnPrev)
        Me.Controls.Add(Me.GroupBox4)
        Me.Controls.Add(Me.Label27)
        Me.Controls.Add(Me.txtJamKerja)
        Me.Controls.Add(Me.Label26)
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.btnSearch)
        Me.Controls.Add(Me.btnHapus)
        Me.Controls.Add(Me.grpKontrak)
        Me.Controls.Add(Me.txtNama)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.btnSave)
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmMasterJamKerja"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Jam Kerja"
        Me.grpKontrak.ResumeLayout(False)
        Me.grpKontrak.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnSave As System.Windows.Forms.Button
    Friend WithEvents txtNama As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents grpKontrak As System.Windows.Forms.GroupBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents btnHapus As System.Windows.Forms.Button
    Friend WithEvents btnSearch As System.Windows.Forms.Button
    Friend WithEvents chkMasukPembulatan As System.Windows.Forms.CheckBox
    Friend WithEvents cmbMasukPembulatan As System.Windows.Forms.ComboBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txtMasukToleransiSesudah As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtMasukToleransiSebelum As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents jamMasuk As System.Windows.Forms.DateTimePicker
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents cmbIstirahatPembulatan As System.Windows.Forms.ComboBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents txtIstirahatToleransiSesudah As System.Windows.Forms.TextBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents txtIstirahatToleransiSebelum As System.Windows.Forms.TextBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents jamIstirahat As System.Windows.Forms.DateTimePicker
    Friend WithEvents chkIstirahatPembulatan As System.Windows.Forms.CheckBox
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents cmbKembaliPembulatan As System.Windows.Forms.ComboBox
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents txtKembaliToleransiSesudah As System.Windows.Forms.TextBox
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents txtKembaliToleransiSebelum As System.Windows.Forms.TextBox
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents jamKembali As System.Windows.Forms.DateTimePicker
    Friend WithEvents chkKembaliPembulatan As System.Windows.Forms.CheckBox
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents cmbPulangPembulatan As System.Windows.Forms.ComboBox
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents txtPulangToleransiSesudah As System.Windows.Forms.TextBox
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents txtPulangToleransiSebelum As System.Windows.Forms.TextBox
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents jamPulang As System.Windows.Forms.DateTimePicker
    Friend WithEvents chkPulangPembulatan As System.Windows.Forms.CheckBox
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents btnExit As System.Windows.Forms.Button
    Friend WithEvents txtJamKerja As System.Windows.Forms.TextBox
    Friend WithEvents Label26 As System.Windows.Forms.Label
    Friend WithEvents Label27 As System.Windows.Forms.Label
    Friend WithEvents chknormal As System.Windows.Forms.CheckBox
    Friend WithEvents chkLibur As System.Windows.Forms.CheckBox
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents btnNext As System.Windows.Forms.Button
    Friend WithEvents btnPrev As System.Windows.Forms.Button
    Friend WithEvents Label28 As System.Windows.Forms.Label
    Friend WithEvents cmbgolongan As System.Windows.Forms.ComboBox
End Class
