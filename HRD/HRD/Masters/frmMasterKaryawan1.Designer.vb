﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMasterKaryawan2
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.txtNIK = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtNamaDepan = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtNamaBelakang = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtNamaPanggilan = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.TabAlamat = New System.Windows.Forms.TabPage()
        Me.txtEmail = New System.Windows.Forms.TextBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.txtHP = New System.Windows.Forms.TextBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.txtTelp = New System.Windows.Forms.TextBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.txtKodePos = New System.Windows.Forms.TextBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.txtKota = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.txtKecamatan = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.txtKelurahan = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.txtRW = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.txtRT = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txtAlamat = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.TabIdentitas = New System.Windows.Forms.TabPage()
        Me.txtJamsostek = New System.Windows.Forms.TextBox()
        Me.Label34 = New System.Windows.Forms.Label()
        Me.txtNPWP = New System.Windows.Forms.TextBox()
        Me.Label33 = New System.Windows.Forms.Label()
        Me.cmbStatusNikah = New System.Windows.Forms.ComboBox()
        Me.Label29 = New System.Windows.Forms.Label()
        Me.rdJenisKelamin2 = New System.Windows.Forms.RadioButton()
        Me.rdJenisKelamin1 = New System.Windows.Forms.RadioButton()
        Me.Label28 = New System.Windows.Forms.Label()
        Me.chkWNI = New System.Windows.Forms.CheckBox()
        Me.Label27 = New System.Windows.Forms.Label()
        Me.dtTanggalLahir = New System.Windows.Forms.DateTimePicker()
        Me.txtTempatLahir = New System.Windows.Forms.TextBox()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.TextBox19 = New System.Windows.Forms.TextBox()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.txtNoPassport = New System.Windows.Forms.TextBox()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.txtNoSim = New System.Windows.Forms.TextBox()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.txtNoKTP = New System.Windows.Forms.TextBox()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.cmbAgama = New System.Windows.Forms.ComboBox()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.txtSuku = New System.Windows.Forms.TextBox()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.TabKeluarga = New System.Windows.Forms.TabPage()
        Me.DBGridKeluarga = New System.Windows.Forms.DataGridView()
        Me.Nama = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Hubungan = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Alamat = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Telp = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TabPendidikan = New System.Windows.Forms.TabPage()
        Me.DBGridPendidikan = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Jenjang = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Mulai = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Selesai = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TabRiwayat = New System.Windows.Forms.TabPage()
        Me.TabKeahlian = New System.Windows.Forms.TabPage()
        Me.TabBank = New System.Windows.Forms.TabPage()
        Me.txtAtasNama = New System.Windows.Forms.TextBox()
        Me.Label32 = New System.Windows.Forms.Label()
        Me.txtNoRekening = New System.Windows.Forms.TextBox()
        Me.Label31 = New System.Windows.Forms.Label()
        Me.cmbBank = New System.Windows.Forms.ComboBox()
        Me.Label30 = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.DTMasuk = New System.Windows.Forms.DateTimePicker()
        Me.grpKontrak = New System.Windows.Forms.GroupBox()
        Me.DTSelesai = New System.Windows.Forms.DateTimePicker()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.cmbTipe = New System.Windows.Forms.ComboBox()
        Me.cmbArea = New System.Windows.Forms.ComboBox()
        Me.btnSearch = New System.Windows.Forms.Button()
        Me.cmbDivisi = New System.Windows.Forms.ComboBox()
        Me.Label35 = New System.Windows.Forms.Label()
        Me.btnSave = New System.Windows.Forms.Button()
        Me.btnNonAktif = New System.Windows.Forms.Button()
        Me.btnPrint = New System.Windows.Forms.Button()
        Me.btnExit = New System.Windows.Forms.Button()
        Me.cmbWorkGroup = New System.Windows.Forms.ComboBox()
        Me.Label36 = New System.Windows.Forms.Label()
        Me.cmbGroupGaji = New System.Windows.Forms.ComboBox()
        Me.Label37 = New System.Windows.Forms.Label()
        Me.btnNext = New System.Windows.Forms.Button()
        Me.btnPrev = New System.Windows.Forms.Button()
        Me.btnGroupGaji = New System.Windows.Forms.Button()
        Me.TabControl1.SuspendLayout()
        Me.TabAlamat.SuspendLayout()
        Me.TabIdentitas.SuspendLayout()
        Me.TabKeluarga.SuspendLayout()
        CType(Me.DBGridKeluarga, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPendidikan.SuspendLayout()
        CType(Me.DBGridPendidikan, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabBank.SuspendLayout()
        Me.grpKontrak.SuspendLayout()
        Me.SuspendLayout()
        '
        'txtNIK
        '
        Me.txtNIK.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNIK.Location = New System.Drawing.Point(128, 37)
        Me.txtNIK.Name = "txtNIK"
        Me.txtNIK.Size = New System.Drawing.Size(92, 22)
        Me.txtNIK.TabIndex = 0
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.BackColor = System.Drawing.Color.Transparent
        Me.Label2.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.Black
        Me.Label2.Location = New System.Drawing.Point(17, 37)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(26, 14)
        Me.Label2.TabIndex = 16
        Me.Label2.Text = "NIK"
        '
        'txtNamaDepan
        '
        Me.txtNamaDepan.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNamaDepan.Location = New System.Drawing.Point(128, 66)
        Me.txtNamaDepan.Name = "txtNamaDepan"
        Me.txtNamaDepan.Size = New System.Drawing.Size(161, 22)
        Me.txtNamaDepan.TabIndex = 1
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.Black
        Me.Label1.Location = New System.Drawing.Point(17, 66)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(76, 14)
        Me.Label1.TabIndex = 18
        Me.Label1.Text = "Nama Depan"
        '
        'txtNamaBelakang
        '
        Me.txtNamaBelakang.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNamaBelakang.Location = New System.Drawing.Point(128, 95)
        Me.txtNamaBelakang.Name = "txtNamaBelakang"
        Me.txtNamaBelakang.Size = New System.Drawing.Size(161, 22)
        Me.txtNamaBelakang.TabIndex = 2
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.BackColor = System.Drawing.Color.Transparent
        Me.Label3.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.Black
        Me.Label3.Location = New System.Drawing.Point(17, 95)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(89, 14)
        Me.Label3.TabIndex = 20
        Me.Label3.Text = "Nama Belakang"
        '
        'txtNamaPanggilan
        '
        Me.txtNamaPanggilan.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNamaPanggilan.Location = New System.Drawing.Point(128, 124)
        Me.txtNamaPanggilan.Name = "txtNamaPanggilan"
        Me.txtNamaPanggilan.Size = New System.Drawing.Size(161, 22)
        Me.txtNamaPanggilan.TabIndex = 3
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.BackColor = System.Drawing.Color.Transparent
        Me.Label4.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.Black
        Me.Label4.Location = New System.Drawing.Point(17, 124)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(58, 14)
        Me.Label4.TabIndex = 22
        Me.Label4.Text = "Panggilan"
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.TabAlamat)
        Me.TabControl1.Controls.Add(Me.TabIdentitas)
        Me.TabControl1.Controls.Add(Me.TabKeluarga)
        Me.TabControl1.Controls.Add(Me.TabPendidikan)
        Me.TabControl1.Controls.Add(Me.TabRiwayat)
        Me.TabControl1.Controls.Add(Me.TabKeahlian)
        Me.TabControl1.Controls.Add(Me.TabBank)
        Me.TabControl1.Location = New System.Drawing.Point(12, 153)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(841, 268)
        Me.TabControl1.TabIndex = 45
        '
        'TabAlamat
        '
        Me.TabAlamat.Controls.Add(Me.txtEmail)
        Me.TabAlamat.Controls.Add(Me.Label14)
        Me.TabAlamat.Controls.Add(Me.txtHP)
        Me.TabAlamat.Controls.Add(Me.Label13)
        Me.TabAlamat.Controls.Add(Me.txtTelp)
        Me.TabAlamat.Controls.Add(Me.Label12)
        Me.TabAlamat.Controls.Add(Me.txtKodePos)
        Me.TabAlamat.Controls.Add(Me.Label11)
        Me.TabAlamat.Controls.Add(Me.txtKota)
        Me.TabAlamat.Controls.Add(Me.Label10)
        Me.TabAlamat.Controls.Add(Me.txtKecamatan)
        Me.TabAlamat.Controls.Add(Me.Label9)
        Me.TabAlamat.Controls.Add(Me.txtKelurahan)
        Me.TabAlamat.Controls.Add(Me.Label8)
        Me.TabAlamat.Controls.Add(Me.txtRW)
        Me.TabAlamat.Controls.Add(Me.Label7)
        Me.TabAlamat.Controls.Add(Me.txtRT)
        Me.TabAlamat.Controls.Add(Me.Label6)
        Me.TabAlamat.Controls.Add(Me.txtAlamat)
        Me.TabAlamat.Controls.Add(Me.Label5)
        Me.TabAlamat.Location = New System.Drawing.Point(4, 22)
        Me.TabAlamat.Name = "TabAlamat"
        Me.TabAlamat.Padding = New System.Windows.Forms.Padding(3)
        Me.TabAlamat.Size = New System.Drawing.Size(833, 242)
        Me.TabAlamat.TabIndex = 0
        Me.TabAlamat.Text = "Alamat"
        Me.TabAlamat.UseVisualStyleBackColor = True
        '
        'txtEmail
        '
        Me.txtEmail.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEmail.Location = New System.Drawing.Point(507, 149)
        Me.txtEmail.Name = "txtEmail"
        Me.txtEmail.Size = New System.Drawing.Size(250, 23)
        Me.txtEmail.TabIndex = 61
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.BackColor = System.Drawing.Color.Transparent
        Me.Label14.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.ForeColor = System.Drawing.Color.Black
        Me.Label14.Location = New System.Drawing.Point(396, 149)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(40, 18)
        Me.Label14.TabIndex = 62
        Me.Label14.Text = "Email"
        '
        'txtHP
        '
        Me.txtHP.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtHP.Location = New System.Drawing.Point(507, 120)
        Me.txtHP.Name = "txtHP"
        Me.txtHP.Size = New System.Drawing.Size(250, 23)
        Me.txtHP.TabIndex = 59
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.BackColor = System.Drawing.Color.Transparent
        Me.Label13.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.ForeColor = System.Drawing.Color.Black
        Me.Label13.Location = New System.Drawing.Point(396, 120)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(24, 18)
        Me.Label13.TabIndex = 60
        Me.Label13.Text = "HP"
        '
        'txtTelp
        '
        Me.txtTelp.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTelp.Location = New System.Drawing.Point(507, 91)
        Me.txtTelp.Name = "txtTelp"
        Me.txtTelp.Size = New System.Drawing.Size(250, 23)
        Me.txtTelp.TabIndex = 57
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.BackColor = System.Drawing.Color.Transparent
        Me.Label12.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.ForeColor = System.Drawing.Color.Black
        Me.Label12.Location = New System.Drawing.Point(396, 91)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(33, 18)
        Me.Label12.TabIndex = 58
        Me.Label12.Text = "Telp"
        '
        'txtKodePos
        '
        Me.txtKodePos.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtKodePos.Location = New System.Drawing.Point(117, 212)
        Me.txtKodePos.Name = "txtKodePos"
        Me.txtKodePos.Size = New System.Drawing.Size(132, 23)
        Me.txtKodePos.TabIndex = 55
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.BackColor = System.Drawing.Color.Transparent
        Me.Label11.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.ForeColor = System.Drawing.Color.Black
        Me.Label11.Location = New System.Drawing.Point(6, 212)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(60, 18)
        Me.Label11.TabIndex = 56
        Me.Label11.Text = "Kode Pos"
        '
        'txtKota
        '
        Me.txtKota.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtKota.Location = New System.Drawing.Point(117, 183)
        Me.txtKota.Name = "txtKota"
        Me.txtKota.Size = New System.Drawing.Size(132, 23)
        Me.txtKota.TabIndex = 53
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.BackColor = System.Drawing.Color.Transparent
        Me.Label10.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.ForeColor = System.Drawing.Color.Black
        Me.Label10.Location = New System.Drawing.Point(6, 183)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(34, 18)
        Me.Label10.TabIndex = 54
        Me.Label10.Text = "Kota"
        '
        'txtKecamatan
        '
        Me.txtKecamatan.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtKecamatan.Location = New System.Drawing.Point(117, 154)
        Me.txtKecamatan.Name = "txtKecamatan"
        Me.txtKecamatan.Size = New System.Drawing.Size(132, 23)
        Me.txtKecamatan.TabIndex = 51
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.BackColor = System.Drawing.Color.Transparent
        Me.Label9.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.ForeColor = System.Drawing.Color.Black
        Me.Label9.Location = New System.Drawing.Point(6, 154)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(72, 18)
        Me.Label9.TabIndex = 52
        Me.Label9.Text = "Kecamatan"
        '
        'txtKelurahan
        '
        Me.txtKelurahan.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtKelurahan.Location = New System.Drawing.Point(117, 125)
        Me.txtKelurahan.Name = "txtKelurahan"
        Me.txtKelurahan.Size = New System.Drawing.Size(132, 23)
        Me.txtKelurahan.TabIndex = 49
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.BackColor = System.Drawing.Color.Transparent
        Me.Label8.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.ForeColor = System.Drawing.Color.Black
        Me.Label8.Location = New System.Drawing.Point(6, 125)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(65, 18)
        Me.Label8.TabIndex = 50
        Me.Label8.Text = "Kelurahan"
        '
        'txtRW
        '
        Me.txtRW.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRW.Location = New System.Drawing.Point(210, 96)
        Me.txtRW.Name = "txtRW"
        Me.txtRW.Size = New System.Drawing.Size(63, 23)
        Me.txtRW.TabIndex = 47
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.BackColor = System.Drawing.Color.Transparent
        Me.Label7.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.ForeColor = System.Drawing.Color.Black
        Me.Label7.Location = New System.Drawing.Point(176, 96)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(28, 18)
        Me.Label7.TabIndex = 48
        Me.Label7.Text = "RW"
        '
        'txtRT
        '
        Me.txtRT.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRT.Location = New System.Drawing.Point(117, 96)
        Me.txtRT.Name = "txtRT"
        Me.txtRT.Size = New System.Drawing.Size(47, 23)
        Me.txtRT.TabIndex = 45
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.BackColor = System.Drawing.Color.Transparent
        Me.Label6.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.Color.Black
        Me.Label6.Location = New System.Drawing.Point(6, 96)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(24, 18)
        Me.Label6.TabIndex = 46
        Me.Label6.Text = "RT"
        '
        'txtAlamat
        '
        Me.txtAlamat.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAlamat.Location = New System.Drawing.Point(117, 12)
        Me.txtAlamat.Multiline = True
        Me.txtAlamat.Name = "txtAlamat"
        Me.txtAlamat.Size = New System.Drawing.Size(252, 78)
        Me.txtAlamat.TabIndex = 43
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.BackColor = System.Drawing.Color.Transparent
        Me.Label5.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.Color.Black
        Me.Label5.Location = New System.Drawing.Point(6, 12)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(49, 18)
        Me.Label5.TabIndex = 44
        Me.Label5.Text = "Alamat"
        '
        'TabIdentitas
        '
        Me.TabIdentitas.Controls.Add(Me.txtJamsostek)
        Me.TabIdentitas.Controls.Add(Me.Label34)
        Me.TabIdentitas.Controls.Add(Me.txtNPWP)
        Me.TabIdentitas.Controls.Add(Me.Label33)
        Me.TabIdentitas.Controls.Add(Me.cmbStatusNikah)
        Me.TabIdentitas.Controls.Add(Me.Label29)
        Me.TabIdentitas.Controls.Add(Me.rdJenisKelamin2)
        Me.TabIdentitas.Controls.Add(Me.rdJenisKelamin1)
        Me.TabIdentitas.Controls.Add(Me.Label28)
        Me.TabIdentitas.Controls.Add(Me.chkWNI)
        Me.TabIdentitas.Controls.Add(Me.Label27)
        Me.TabIdentitas.Controls.Add(Me.dtTanggalLahir)
        Me.TabIdentitas.Controls.Add(Me.txtTempatLahir)
        Me.TabIdentitas.Controls.Add(Me.Label26)
        Me.TabIdentitas.Controls.Add(Me.TextBox19)
        Me.TabIdentitas.Controls.Add(Me.Label25)
        Me.TabIdentitas.Controls.Add(Me.txtNoPassport)
        Me.TabIdentitas.Controls.Add(Me.Label24)
        Me.TabIdentitas.Controls.Add(Me.txtNoSim)
        Me.TabIdentitas.Controls.Add(Me.Label23)
        Me.TabIdentitas.Controls.Add(Me.txtNoKTP)
        Me.TabIdentitas.Controls.Add(Me.Label22)
        Me.TabIdentitas.Controls.Add(Me.Label21)
        Me.TabIdentitas.Controls.Add(Me.cmbAgama)
        Me.TabIdentitas.Controls.Add(Me.Label20)
        Me.TabIdentitas.Controls.Add(Me.txtSuku)
        Me.TabIdentitas.Controls.Add(Me.Label15)
        Me.TabIdentitas.Location = New System.Drawing.Point(4, 22)
        Me.TabIdentitas.Name = "TabIdentitas"
        Me.TabIdentitas.Padding = New System.Windows.Forms.Padding(3)
        Me.TabIdentitas.Size = New System.Drawing.Size(833, 242)
        Me.TabIdentitas.TabIndex = 1
        Me.TabIdentitas.Text = "Identitas"
        Me.TabIdentitas.UseVisualStyleBackColor = True
        '
        'txtJamsostek
        '
        Me.txtJamsostek.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtJamsostek.Location = New System.Drawing.Point(370, 151)
        Me.txtJamsostek.Name = "txtJamsostek"
        Me.txtJamsostek.Size = New System.Drawing.Size(132, 23)
        Me.txtJamsostek.TabIndex = 89
        '
        'Label34
        '
        Me.Label34.AutoSize = True
        Me.Label34.BackColor = System.Drawing.Color.Transparent
        Me.Label34.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label34.ForeColor = System.Drawing.Color.Black
        Me.Label34.Location = New System.Drawing.Point(261, 151)
        Me.Label34.Name = "Label34"
        Me.Label34.Size = New System.Drawing.Size(70, 18)
        Me.Label34.TabIndex = 90
        Me.Label34.Text = "Jamsostek"
        '
        'txtNPWP
        '
        Me.txtNPWP.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNPWP.Location = New System.Drawing.Point(370, 122)
        Me.txtNPWP.Name = "txtNPWP"
        Me.txtNPWP.Size = New System.Drawing.Size(132, 23)
        Me.txtNPWP.TabIndex = 87
        '
        'Label33
        '
        Me.Label33.AutoSize = True
        Me.Label33.BackColor = System.Drawing.Color.Transparent
        Me.Label33.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label33.ForeColor = System.Drawing.Color.Black
        Me.Label33.Location = New System.Drawing.Point(261, 122)
        Me.Label33.Name = "Label33"
        Me.Label33.Size = New System.Drawing.Size(42, 18)
        Me.Label33.TabIndex = 88
        Me.Label33.Text = "NPWP"
        '
        'cmbStatusNikah
        '
        Me.cmbStatusNikah.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbStatusNikah.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbStatusNikah.FormattingEnabled = True
        Me.cmbStatusNikah.Items.AddRange(New Object() {"Belum Menikah", "Menikah", "Cerai"})
        Me.cmbStatusNikah.Location = New System.Drawing.Point(370, 92)
        Me.cmbStatusNikah.Name = "cmbStatusNikah"
        Me.cmbStatusNikah.Size = New System.Drawing.Size(178, 22)
        Me.cmbStatusNikah.TabIndex = 80
        '
        'Label29
        '
        Me.Label29.AutoSize = True
        Me.Label29.BackColor = System.Drawing.Color.Transparent
        Me.Label29.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label29.ForeColor = System.Drawing.Color.Black
        Me.Label29.Location = New System.Drawing.Point(259, 95)
        Me.Label29.Name = "Label29"
        Me.Label29.Size = New System.Drawing.Size(75, 14)
        Me.Label29.TabIndex = 79
        Me.Label29.Text = "Status Nikah"
        '
        'rdJenisKelamin2
        '
        Me.rdJenisKelamin2.AutoSize = True
        Me.rdJenisKelamin2.Location = New System.Drawing.Point(427, 64)
        Me.rdJenisKelamin2.Name = "rdJenisKelamin2"
        Me.rdJenisKelamin2.Size = New System.Drawing.Size(79, 17)
        Me.rdJenisKelamin2.TabIndex = 78
        Me.rdJenisKelamin2.TabStop = True
        Me.rdJenisKelamin2.Text = "Perempuan"
        Me.rdJenisKelamin2.UseVisualStyleBackColor = True
        '
        'rdJenisKelamin1
        '
        Me.rdJenisKelamin1.AutoSize = True
        Me.rdJenisKelamin1.Location = New System.Drawing.Point(370, 64)
        Me.rdJenisKelamin1.Name = "rdJenisKelamin1"
        Me.rdJenisKelamin1.Size = New System.Drawing.Size(51, 17)
        Me.rdJenisKelamin1.TabIndex = 77
        Me.rdJenisKelamin1.TabStop = True
        Me.rdJenisKelamin1.Text = "Laki2"
        Me.rdJenisKelamin1.UseVisualStyleBackColor = True
        '
        'Label28
        '
        Me.Label28.AutoSize = True
        Me.Label28.BackColor = System.Drawing.Color.Transparent
        Me.Label28.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label28.ForeColor = System.Drawing.Color.Black
        Me.Label28.Location = New System.Drawing.Point(259, 64)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(88, 18)
        Me.Label28.TabIndex = 76
        Me.Label28.Text = "Jenis Kelamin"
        '
        'chkWNI
        '
        Me.chkWNI.AutoSize = True
        Me.chkWNI.Checked = True
        Me.chkWNI.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkWNI.Location = New System.Drawing.Point(112, 35)
        Me.chkWNI.Name = "chkWNI"
        Me.chkWNI.Size = New System.Drawing.Size(15, 14)
        Me.chkWNI.TabIndex = 75
        Me.chkWNI.UseVisualStyleBackColor = True
        '
        'Label27
        '
        Me.Label27.AutoSize = True
        Me.Label27.BackColor = System.Drawing.Color.Transparent
        Me.Label27.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label27.ForeColor = System.Drawing.Color.Black
        Me.Label27.Location = New System.Drawing.Point(470, 36)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(15, 18)
        Me.Label27.TabIndex = 74
        Me.Label27.Text = "/"
        '
        'dtTanggalLahir
        '
        Me.dtTanggalLahir.CustomFormat = "dd/MM/yyyy"
        Me.dtTanggalLahir.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtTanggalLahir.Location = New System.Drawing.Point(486, 35)
        Me.dtTanggalLahir.Name = "dtTanggalLahir"
        Me.dtTanggalLahir.Size = New System.Drawing.Size(101, 20)
        Me.dtTanggalLahir.TabIndex = 73
        '
        'txtTempatLahir
        '
        Me.txtTempatLahir.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTempatLahir.Location = New System.Drawing.Point(370, 35)
        Me.txtTempatLahir.Name = "txtTempatLahir"
        Me.txtTempatLahir.Size = New System.Drawing.Size(97, 23)
        Me.txtTempatLahir.TabIndex = 71
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        Me.Label26.BackColor = System.Drawing.Color.Transparent
        Me.Label26.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label26.ForeColor = System.Drawing.Color.Black
        Me.Label26.Location = New System.Drawing.Point(259, 35)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(112, 18)
        Me.Label26.TabIndex = 72
        Me.Label26.Text = "Tempat/Tgl Lahir"
        '
        'TextBox19
        '
        Me.TextBox19.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox19.Location = New System.Drawing.Point(112, 202)
        Me.TextBox19.Name = "TextBox19"
        Me.TextBox19.Size = New System.Drawing.Size(132, 23)
        Me.TextBox19.TabIndex = 69
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.BackColor = System.Drawing.Color.Transparent
        Me.Label25.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label25.ForeColor = System.Drawing.Color.Black
        Me.Label25.Location = New System.Drawing.Point(1, 202)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(36, 18)
        Me.Label25.TabIndex = 70
        Me.Label25.Text = "Suku"
        '
        'txtNoPassport
        '
        Me.txtNoPassport.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNoPassport.Location = New System.Drawing.Point(112, 122)
        Me.txtNoPassport.Name = "txtNoPassport"
        Me.txtNoPassport.Size = New System.Drawing.Size(132, 23)
        Me.txtNoPassport.TabIndex = 67
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.BackColor = System.Drawing.Color.Transparent
        Me.Label24.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label24.ForeColor = System.Drawing.Color.Black
        Me.Label24.Location = New System.Drawing.Point(1, 122)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(77, 18)
        Me.Label24.TabIndex = 68
        Me.Label24.Text = "No Passport"
        '
        'txtNoSim
        '
        Me.txtNoSim.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNoSim.Location = New System.Drawing.Point(112, 93)
        Me.txtNoSim.Name = "txtNoSim"
        Me.txtNoSim.Size = New System.Drawing.Size(132, 23)
        Me.txtNoSim.TabIndex = 65
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.BackColor = System.Drawing.Color.Transparent
        Me.Label23.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label23.ForeColor = System.Drawing.Color.Black
        Me.Label23.Location = New System.Drawing.Point(1, 93)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(48, 18)
        Me.Label23.TabIndex = 66
        Me.Label23.Text = "No SIM"
        '
        'txtNoKTP
        '
        Me.txtNoKTP.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNoKTP.Location = New System.Drawing.Point(112, 64)
        Me.txtNoKTP.Name = "txtNoKTP"
        Me.txtNoKTP.Size = New System.Drawing.Size(132, 23)
        Me.txtNoKTP.TabIndex = 63
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.BackColor = System.Drawing.Color.Transparent
        Me.Label22.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label22.ForeColor = System.Drawing.Color.Black
        Me.Label22.Location = New System.Drawing.Point(1, 64)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(49, 18)
        Me.Label22.TabIndex = 64
        Me.Label22.Text = "No KTP"
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.BackColor = System.Drawing.Color.Transparent
        Me.Label21.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label21.ForeColor = System.Drawing.Color.Black
        Me.Label21.Location = New System.Drawing.Point(1, 35)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(32, 18)
        Me.Label21.TabIndex = 62
        Me.Label21.Text = "WNI"
        '
        'cmbAgama
        '
        Me.cmbAgama.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbAgama.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbAgama.FormattingEnabled = True
        Me.cmbAgama.Location = New System.Drawing.Point(370, 5)
        Me.cmbAgama.Name = "cmbAgama"
        Me.cmbAgama.Size = New System.Drawing.Size(178, 22)
        Me.cmbAgama.TabIndex = 60
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.BackColor = System.Drawing.Color.Transparent
        Me.Label20.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label20.ForeColor = System.Drawing.Color.Black
        Me.Label20.Location = New System.Drawing.Point(259, 8)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(44, 14)
        Me.Label20.TabIndex = 59
        Me.Label20.Text = "Agama"
        '
        'txtSuku
        '
        Me.txtSuku.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSuku.Location = New System.Drawing.Point(112, 6)
        Me.txtSuku.Name = "txtSuku"
        Me.txtSuku.Size = New System.Drawing.Size(132, 23)
        Me.txtSuku.TabIndex = 45
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.BackColor = System.Drawing.Color.Transparent
        Me.Label15.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.ForeColor = System.Drawing.Color.Black
        Me.Label15.Location = New System.Drawing.Point(1, 6)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(36, 18)
        Me.Label15.TabIndex = 46
        Me.Label15.Text = "Suku"
        '
        'TabKeluarga
        '
        Me.TabKeluarga.Controls.Add(Me.DBGridKeluarga)
        Me.TabKeluarga.Location = New System.Drawing.Point(4, 22)
        Me.TabKeluarga.Name = "TabKeluarga"
        Me.TabKeluarga.Size = New System.Drawing.Size(833, 242)
        Me.TabKeluarga.TabIndex = 2
        Me.TabKeluarga.Text = "Keluarga"
        Me.TabKeluarga.UseVisualStyleBackColor = True
        '
        'DBGridKeluarga
        '
        Me.DBGridKeluarga.AllowUserToOrderColumns = True
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle5.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DBGridKeluarga.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle5
        Me.DBGridKeluarga.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DBGridKeluarga.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Nama, Me.Hubungan, Me.Alamat, Me.Telp})
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle6.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DBGridKeluarga.DefaultCellStyle = DataGridViewCellStyle6
        Me.DBGridKeluarga.Location = New System.Drawing.Point(14, 14)
        Me.DBGridKeluarga.Name = "DBGridKeluarga"
        Me.DBGridKeluarga.RowHeadersWidth = 25
        Me.DBGridKeluarga.Size = New System.Drawing.Size(793, 205)
        Me.DBGridKeluarga.TabIndex = 1
        '
        'Nama
        '
        Me.Nama.HeaderText = "Nama"
        Me.Nama.Name = "Nama"
        Me.Nama.Width = 180
        '
        'Hubungan
        '
        Me.Hubungan.HeaderText = "Hubungan"
        Me.Hubungan.Name = "Hubungan"
        '
        'Alamat
        '
        Me.Alamat.HeaderText = "Alamat"
        Me.Alamat.Name = "Alamat"
        Me.Alamat.Width = 200
        '
        'Telp
        '
        Me.Telp.HeaderText = "Telp"
        Me.Telp.Name = "Telp"
        Me.Telp.Width = 150
        '
        'TabPendidikan
        '
        Me.TabPendidikan.Controls.Add(Me.DBGridPendidikan)
        Me.TabPendidikan.Location = New System.Drawing.Point(4, 22)
        Me.TabPendidikan.Name = "TabPendidikan"
        Me.TabPendidikan.Size = New System.Drawing.Size(833, 242)
        Me.TabPendidikan.TabIndex = 3
        Me.TabPendidikan.Text = "Pendidikan"
        Me.TabPendidikan.UseVisualStyleBackColor = True
        '
        'DBGridPendidikan
        '
        Me.DBGridPendidikan.AllowUserToOrderColumns = True
        DataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle7.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DBGridPendidikan.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle7
        Me.DBGridPendidikan.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DBGridPendidikan.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn1, Me.Jenjang, Me.Mulai, Me.Selesai})
        DataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle8.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DBGridPendidikan.DefaultCellStyle = DataGridViewCellStyle8
        Me.DBGridPendidikan.Location = New System.Drawing.Point(20, 19)
        Me.DBGridPendidikan.Name = "DBGridPendidikan"
        Me.DBGridPendidikan.RowHeadersWidth = 25
        Me.DBGridPendidikan.Size = New System.Drawing.Size(793, 205)
        Me.DBGridPendidikan.TabIndex = 2
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.HeaderText = "Nama"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.Width = 180
        '
        'Jenjang
        '
        Me.Jenjang.HeaderText = "Jenjang"
        Me.Jenjang.Name = "Jenjang"
        '
        'Mulai
        '
        Me.Mulai.HeaderText = "Mulai"
        Me.Mulai.Name = "Mulai"
        '
        'Selesai
        '
        Me.Selesai.HeaderText = "Selesai"
        Me.Selesai.Name = "Selesai"
        '
        'TabRiwayat
        '
        Me.TabRiwayat.Location = New System.Drawing.Point(4, 22)
        Me.TabRiwayat.Name = "TabRiwayat"
        Me.TabRiwayat.Size = New System.Drawing.Size(833, 242)
        Me.TabRiwayat.TabIndex = 4
        Me.TabRiwayat.Text = "Riwayat"
        Me.TabRiwayat.UseVisualStyleBackColor = True
        '
        'TabKeahlian
        '
        Me.TabKeahlian.Location = New System.Drawing.Point(4, 22)
        Me.TabKeahlian.Name = "TabKeahlian"
        Me.TabKeahlian.Size = New System.Drawing.Size(833, 242)
        Me.TabKeahlian.TabIndex = 5
        Me.TabKeahlian.Text = "Keahlian"
        Me.TabKeahlian.UseVisualStyleBackColor = True
        '
        'TabBank
        '
        Me.TabBank.Controls.Add(Me.txtAtasNama)
        Me.TabBank.Controls.Add(Me.Label32)
        Me.TabBank.Controls.Add(Me.txtNoRekening)
        Me.TabBank.Controls.Add(Me.Label31)
        Me.TabBank.Controls.Add(Me.cmbBank)
        Me.TabBank.Controls.Add(Me.Label30)
        Me.TabBank.Location = New System.Drawing.Point(4, 22)
        Me.TabBank.Name = "TabBank"
        Me.TabBank.Padding = New System.Windows.Forms.Padding(3)
        Me.TabBank.Size = New System.Drawing.Size(833, 242)
        Me.TabBank.TabIndex = 6
        Me.TabBank.Text = "Bank"
        Me.TabBank.UseVisualStyleBackColor = True
        '
        'txtAtasNama
        '
        Me.txtAtasNama.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAtasNama.Location = New System.Drawing.Point(123, 62)
        Me.txtAtasNama.Name = "txtAtasNama"
        Me.txtAtasNama.Size = New System.Drawing.Size(132, 23)
        Me.txtAtasNama.TabIndex = 91
        '
        'Label32
        '
        Me.Label32.AutoSize = True
        Me.Label32.BackColor = System.Drawing.Color.Transparent
        Me.Label32.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label32.ForeColor = System.Drawing.Color.Black
        Me.Label32.Location = New System.Drawing.Point(12, 62)
        Me.Label32.Name = "Label32"
        Me.Label32.Size = New System.Drawing.Size(71, 18)
        Me.Label32.TabIndex = 92
        Me.Label32.Text = "Atas Nama"
        '
        'txtNoRekening
        '
        Me.txtNoRekening.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNoRekening.Location = New System.Drawing.Point(123, 33)
        Me.txtNoRekening.Name = "txtNoRekening"
        Me.txtNoRekening.Size = New System.Drawing.Size(132, 23)
        Me.txtNoRekening.TabIndex = 89
        '
        'Label31
        '
        Me.Label31.AutoSize = True
        Me.Label31.BackColor = System.Drawing.Color.Transparent
        Me.Label31.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label31.ForeColor = System.Drawing.Color.Black
        Me.Label31.Location = New System.Drawing.Point(12, 33)
        Me.Label31.Name = "Label31"
        Me.Label31.Size = New System.Drawing.Size(81, 18)
        Me.Label31.TabIndex = 90
        Me.Label31.Text = "No Rekening"
        '
        'cmbBank
        '
        Me.cmbBank.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbBank.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbBank.FormattingEnabled = True
        Me.cmbBank.Location = New System.Drawing.Point(123, 6)
        Me.cmbBank.Name = "cmbBank"
        Me.cmbBank.Size = New System.Drawing.Size(178, 22)
        Me.cmbBank.TabIndex = 88
        '
        'Label30
        '
        Me.Label30.AutoSize = True
        Me.Label30.BackColor = System.Drawing.Color.Transparent
        Me.Label30.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label30.ForeColor = System.Drawing.Color.Black
        Me.Label30.Location = New System.Drawing.Point(12, 9)
        Me.Label30.Name = "Label30"
        Me.Label30.Size = New System.Drawing.Size(33, 14)
        Me.Label30.TabIndex = 87
        Me.Label30.Text = "Bank"
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.BackColor = System.Drawing.Color.Transparent
        Me.Label16.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.ForeColor = System.Drawing.Color.Black
        Me.Label16.Location = New System.Drawing.Point(314, 42)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(87, 14)
        Me.Label16.TabIndex = 47
        Me.Label16.Text = "Tipe Karyawan"
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.BackColor = System.Drawing.Color.Transparent
        Me.Label17.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label17.ForeColor = System.Drawing.Color.Black
        Me.Label17.Location = New System.Drawing.Point(314, 69)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(70, 14)
        Me.Label17.TabIndex = 49
        Me.Label17.Text = "Payroll Area"
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.BackColor = System.Drawing.Color.Transparent
        Me.Label18.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label18.ForeColor = System.Drawing.Color.Black
        Me.Label18.Location = New System.Drawing.Point(620, 42)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(87, 14)
        Me.Label18.TabIndex = 51
        Me.Label18.Text = "Tanggal Masuk"
        '
        'DTMasuk
        '
        Me.DTMasuk.CustomFormat = "dd/MM/yyyy"
        Me.DTMasuk.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DTMasuk.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.DTMasuk.Location = New System.Drawing.Point(736, 42)
        Me.DTMasuk.Name = "DTMasuk"
        Me.DTMasuk.Size = New System.Drawing.Size(117, 22)
        Me.DTMasuk.TabIndex = 5
        '
        'grpKontrak
        '
        Me.grpKontrak.BackColor = System.Drawing.Color.Transparent
        Me.grpKontrak.Controls.Add(Me.DTSelesai)
        Me.grpKontrak.Controls.Add(Me.Label19)
        Me.grpKontrak.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grpKontrak.Location = New System.Drawing.Point(618, 64)
        Me.grpKontrak.Margin = New System.Windows.Forms.Padding(0)
        Me.grpKontrak.Name = "grpKontrak"
        Me.grpKontrak.Padding = New System.Windows.Forms.Padding(0)
        Me.grpKontrak.Size = New System.Drawing.Size(245, 29)
        Me.grpKontrak.TabIndex = 55
        Me.grpKontrak.TabStop = False
        Me.grpKontrak.Visible = False
        '
        'DTSelesai
        '
        Me.DTSelesai.CustomFormat = "dd/MM/yyyy"
        Me.DTSelesai.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.DTSelesai.Location = New System.Drawing.Point(116, 9)
        Me.DTSelesai.Name = "DTSelesai"
        Me.DTSelesai.Size = New System.Drawing.Size(117, 22)
        Me.DTSelesai.TabIndex = 6
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.BackColor = System.Drawing.Color.Transparent
        Me.Label19.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label19.ForeColor = System.Drawing.Color.Black
        Me.Label19.Location = New System.Drawing.Point(2, 6)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(98, 18)
        Me.Label19.TabIndex = 55
        Me.Label19.Text = "Selesai Kontrak"
        '
        'cmbTipe
        '
        Me.cmbTipe.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbTipe.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbTipe.FormattingEnabled = True
        Me.cmbTipe.Location = New System.Drawing.Point(426, 37)
        Me.cmbTipe.Name = "cmbTipe"
        Me.cmbTipe.Size = New System.Drawing.Size(150, 22)
        Me.cmbTipe.TabIndex = 4
        '
        'cmbArea
        '
        Me.cmbArea.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbArea.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbArea.FormattingEnabled = True
        Me.cmbArea.Location = New System.Drawing.Point(426, 66)
        Me.cmbArea.Name = "cmbArea"
        Me.cmbArea.Size = New System.Drawing.Size(150, 22)
        Me.cmbArea.TabIndex = 7
        '
        'btnSearch
        '
        Me.btnSearch.Location = New System.Drawing.Point(226, 36)
        Me.btnSearch.Name = "btnSearch"
        Me.btnSearch.Size = New System.Drawing.Size(28, 23)
        Me.btnSearch.TabIndex = 59
        Me.btnSearch.Text = "F3"
        Me.btnSearch.UseVisualStyleBackColor = True
        '
        'cmbDivisi
        '
        Me.cmbDivisi.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbDivisi.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbDivisi.FormattingEnabled = True
        Me.cmbDivisi.Location = New System.Drawing.Point(426, 95)
        Me.cmbDivisi.Name = "cmbDivisi"
        Me.cmbDivisi.Size = New System.Drawing.Size(150, 22)
        Me.cmbDivisi.TabIndex = 60
        '
        'Label35
        '
        Me.Label35.AutoSize = True
        Me.Label35.BackColor = System.Drawing.Color.Transparent
        Me.Label35.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label35.ForeColor = System.Drawing.Color.Black
        Me.Label35.Location = New System.Drawing.Point(314, 98)
        Me.Label35.Name = "Label35"
        Me.Label35.Size = New System.Drawing.Size(32, 14)
        Me.Label35.TabIndex = 61
        Me.Label35.Text = "Divisi"
        '
        'btnSave
        '
        Me.btnSave.Location = New System.Drawing.Point(273, 460)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(75, 23)
        Me.btnSave.TabIndex = 62
        Me.btnSave.Text = "Simpan"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnNonAktif
        '
        Me.btnNonAktif.Location = New System.Drawing.Point(354, 460)
        Me.btnNonAktif.Name = "btnNonAktif"
        Me.btnNonAktif.Size = New System.Drawing.Size(75, 23)
        Me.btnNonAktif.TabIndex = 63
        Me.btnNonAktif.Text = "NonAktif"
        Me.btnNonAktif.UseVisualStyleBackColor = True
        '
        'btnPrint
        '
        Me.btnPrint.Location = New System.Drawing.Point(435, 460)
        Me.btnPrint.Name = "btnPrint"
        Me.btnPrint.Size = New System.Drawing.Size(75, 23)
        Me.btnPrint.TabIndex = 64
        Me.btnPrint.Text = "Cetak"
        Me.btnPrint.UseVisualStyleBackColor = True
        '
        'btnExit
        '
        Me.btnExit.Location = New System.Drawing.Point(516, 460)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(75, 23)
        Me.btnExit.TabIndex = 65
        Me.btnExit.Text = "Keluar"
        Me.btnExit.UseVisualStyleBackColor = True
        '
        'cmbWorkGroup
        '
        Me.cmbWorkGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbWorkGroup.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbWorkGroup.FormattingEnabled = True
        Me.cmbWorkGroup.Location = New System.Drawing.Point(426, 125)
        Me.cmbWorkGroup.Name = "cmbWorkGroup"
        Me.cmbWorkGroup.Size = New System.Drawing.Size(150, 22)
        Me.cmbWorkGroup.TabIndex = 66
        '
        'Label36
        '
        Me.Label36.AutoSize = True
        Me.Label36.BackColor = System.Drawing.Color.Transparent
        Me.Label36.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label36.ForeColor = System.Drawing.Color.Black
        Me.Label36.Location = New System.Drawing.Point(314, 128)
        Me.Label36.Name = "Label36"
        Me.Label36.Size = New System.Drawing.Size(68, 14)
        Me.Label36.TabIndex = 67
        Me.Label36.Text = "Workgroup"
        '
        'cmbGroupGaji
        '
        Me.cmbGroupGaji.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbGroupGaji.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbGroupGaji.FormattingEnabled = True
        Me.cmbGroupGaji.Location = New System.Drawing.Point(682, 128)
        Me.cmbGroupGaji.Name = "cmbGroupGaji"
        Me.cmbGroupGaji.Size = New System.Drawing.Size(150, 22)
        Me.cmbGroupGaji.TabIndex = 68
        '
        'Label37
        '
        Me.Label37.AutoSize = True
        Me.Label37.BackColor = System.Drawing.Color.Transparent
        Me.Label37.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label37.ForeColor = System.Drawing.Color.Black
        Me.Label37.Location = New System.Drawing.Point(620, 132)
        Me.Label37.Name = "Label37"
        Me.Label37.Size = New System.Drawing.Size(56, 14)
        Me.Label37.TabIndex = 69
        Me.Label37.Text = "Grup Gaji"
        '
        'btnNext
        '
        Me.btnNext.Location = New System.Drawing.Point(435, 431)
        Me.btnNext.Name = "btnNext"
        Me.btnNext.Size = New System.Drawing.Size(75, 23)
        Me.btnNext.TabIndex = 71
        Me.btnNext.Text = "Next"
        Me.btnNext.UseVisualStyleBackColor = True
        '
        'btnPrev
        '
        Me.btnPrev.Location = New System.Drawing.Point(354, 431)
        Me.btnPrev.Name = "btnPrev"
        Me.btnPrev.Size = New System.Drawing.Size(75, 23)
        Me.btnPrev.TabIndex = 70
        Me.btnPrev.Text = "Prev"
        Me.btnPrev.UseVisualStyleBackColor = True
        '
        'btnGroupGaji
        '
        Me.btnGroupGaji.Location = New System.Drawing.Point(838, 128)
        Me.btnGroupGaji.Name = "btnGroupGaji"
        Me.btnGroupGaji.Size = New System.Drawing.Size(23, 23)
        Me.btnGroupGaji.TabIndex = 72
        Me.btnGroupGaji.UseVisualStyleBackColor = True
        '
        'frmMasterKaryawan
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(865, 492)
        Me.Controls.Add(Me.btnGroupGaji)
        Me.Controls.Add(Me.btnNext)
        Me.Controls.Add(Me.btnPrev)
        Me.Controls.Add(Me.cmbGroupGaji)
        Me.Controls.Add(Me.Label37)
        Me.Controls.Add(Me.cmbWorkGroup)
        Me.Controls.Add(Me.Label36)
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.btnPrint)
        Me.Controls.Add(Me.btnNonAktif)
        Me.Controls.Add(Me.btnSave)
        Me.Controls.Add(Me.cmbDivisi)
        Me.Controls.Add(Me.Label35)
        Me.Controls.Add(Me.btnSearch)
        Me.Controls.Add(Me.cmbArea)
        Me.Controls.Add(Me.cmbTipe)
        Me.Controls.Add(Me.grpKontrak)
        Me.Controls.Add(Me.DTMasuk)
        Me.Controls.Add(Me.Label18)
        Me.Controls.Add(Me.Label17)
        Me.Controls.Add(Me.Label16)
        Me.Controls.Add(Me.TabControl1)
        Me.Controls.Add(Me.txtNamaPanggilan)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.txtNamaBelakang)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.txtNamaDepan)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.txtNIK)
        Me.Controls.Add(Me.Label2)
        Me.Name = "frmMasterKaryawan"
        Me.Text = "Master Karyawan"
        Me.TabControl1.ResumeLayout(False)
        Me.TabAlamat.ResumeLayout(False)
        Me.TabAlamat.PerformLayout()
        Me.TabIdentitas.ResumeLayout(False)
        Me.TabIdentitas.PerformLayout()
        Me.TabKeluarga.ResumeLayout(False)
        CType(Me.DBGridKeluarga, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPendidikan.ResumeLayout(False)
        CType(Me.DBGridPendidikan, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabBank.ResumeLayout(False)
        Me.TabBank.PerformLayout()
        Me.grpKontrak.ResumeLayout(False)
        Me.grpKontrak.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents txtNIK As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtNamaDepan As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtNamaBelakang As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtNamaPanggilan As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents TabAlamat As System.Windows.Forms.TabPage
    Friend WithEvents TabIdentitas As System.Windows.Forms.TabPage
    Friend WithEvents txtEmail As System.Windows.Forms.TextBox
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents txtHP As System.Windows.Forms.TextBox
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents txtTelp As System.Windows.Forms.TextBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents txtKodePos As System.Windows.Forms.TextBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents txtKota As System.Windows.Forms.TextBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents txtKecamatan As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents txtKelurahan As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents txtRW As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents txtRT As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents txtAlamat As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txtSuku As System.Windows.Forms.TextBox
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents TabKeluarga As System.Windows.Forms.TabPage
    Friend WithEvents TabPendidikan As System.Windows.Forms.TabPage
    Friend WithEvents TabRiwayat As System.Windows.Forms.TabPage
    Friend WithEvents TabKeahlian As System.Windows.Forms.TabPage
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents DTMasuk As System.Windows.Forms.DateTimePicker
    Friend WithEvents grpKontrak As System.Windows.Forms.GroupBox
    Friend WithEvents DTSelesai As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents cmbTipe As System.Windows.Forms.ComboBox
    Friend WithEvents cmbArea As System.Windows.Forms.ComboBox
    Friend WithEvents txtJamsostek As System.Windows.Forms.TextBox
    Friend WithEvents Label34 As System.Windows.Forms.Label
    Friend WithEvents txtNPWP As System.Windows.Forms.TextBox
    Friend WithEvents Label33 As System.Windows.Forms.Label
    Friend WithEvents cmbStatusNikah As System.Windows.Forms.ComboBox
    Friend WithEvents Label29 As System.Windows.Forms.Label
    Friend WithEvents rdJenisKelamin2 As System.Windows.Forms.RadioButton
    Friend WithEvents rdJenisKelamin1 As System.Windows.Forms.RadioButton
    Friend WithEvents Label28 As System.Windows.Forms.Label
    Friend WithEvents chkWNI As System.Windows.Forms.CheckBox
    Friend WithEvents Label27 As System.Windows.Forms.Label
    Friend WithEvents dtTanggalLahir As System.Windows.Forms.DateTimePicker
    Friend WithEvents txtTempatLahir As System.Windows.Forms.TextBox
    Friend WithEvents Label26 As System.Windows.Forms.Label
    Friend WithEvents TextBox19 As System.Windows.Forms.TextBox
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents txtNoPassport As System.Windows.Forms.TextBox
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents txtNoSim As System.Windows.Forms.TextBox
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents txtNoKTP As System.Windows.Forms.TextBox
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents cmbAgama As System.Windows.Forms.ComboBox
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents TabBank As System.Windows.Forms.TabPage
    Friend WithEvents txtAtasNama As System.Windows.Forms.TextBox
    Friend WithEvents Label32 As System.Windows.Forms.Label
    Friend WithEvents txtNoRekening As System.Windows.Forms.TextBox
    Friend WithEvents Label31 As System.Windows.Forms.Label
    Friend WithEvents cmbBank As System.Windows.Forms.ComboBox
    Friend WithEvents Label30 As System.Windows.Forms.Label
    Friend WithEvents btnSearch As System.Windows.Forms.Button
    Friend WithEvents cmbDivisi As System.Windows.Forms.ComboBox
    Friend WithEvents Label35 As System.Windows.Forms.Label
    Friend WithEvents btnSave As System.Windows.Forms.Button
    Friend WithEvents btnNonAktif As System.Windows.Forms.Button
    Friend WithEvents btnPrint As System.Windows.Forms.Button
    Friend WithEvents btnExit As System.Windows.Forms.Button
    Friend WithEvents DBGridKeluarga As System.Windows.Forms.DataGridView
    Friend WithEvents Nama As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Hubungan As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Alamat As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Telp As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DBGridPendidikan As System.Windows.Forms.DataGridView
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Jenjang As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Mulai As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Selesai As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cmbWorkGroup As System.Windows.Forms.ComboBox
    Friend WithEvents Label36 As System.Windows.Forms.Label
    Friend WithEvents cmbGroupGaji As System.Windows.Forms.ComboBox
    Friend WithEvents Label37 As System.Windows.Forms.Label
    Friend WithEvents btnNext As System.Windows.Forms.Button
    Friend WithEvents btnPrev As System.Windows.Forms.Button
    Friend WithEvents btnGroupGaji As System.Windows.Forms.Button
End Class
