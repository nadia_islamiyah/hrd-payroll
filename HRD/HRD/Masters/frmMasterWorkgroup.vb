﻿Imports System.Data.SqlClient
Imports System.Data.Sql
Imports System.IO
Public Class frmMasterWorkgroup
    Dim da As SqlDataAdapter
    Dim ds As New DataSet
    Private Sub reloadcombo()
        Dim conn As New SqlConnection
        Dim cmd As New SqlCommand
        Dim dr As SqlDataReader
        conn.ConnectionString = strcon
        conn.Open()


        cmd = New SqlCommand("select * from ms_jamkerja order by nama_shift", conn)
        dr = cmd.ExecuteReader
        cmbShift.Items.Clear()
        While dr.Read
            cmbShift.Items.Add(dr.Item(0))
        End While
        conn.Close()
    End Sub
    Private Sub frmMastergroupkerja_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown

        If e.KeyCode = Keys.Escape Then Me.Dispose()
        If e.KeyCode = Keys.Enter Then SendKeys.Send("{tab}")
    End Sub

    Private Sub frmMastergroupkerja_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        reloadcombo()
        reset()
    End Sub
    Private Sub reload()
        Dim conn As New SqlConnection
        Dim cmd As New SqlCommand
        Dim dr As SqlDataReader
        conn.ConnectionString = strcon
        conn.Open()


        cmd = New SqlCommand("select * from ms_groupkerja where nama_groupkerja='" & txtNama.Text & "'", conn)
        dr = cmd.ExecuteReader
        dr.Read()
        If dr.HasRows Then
            cmbShift.Text = dr.Item(1)
        End If
        conn.Close()

    End Sub
    Private Sub reset()
        txtNama.Text = ""
        txtNama.Focus()
    End Sub
    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim conn As New SqlConnection
        Dim cmd As New SqlCommand
        conn.ConnectionString = strcon
        conn.Open()

        cmd = New SqlCommand("delete from ms_groupkerja where nama_groupkerja='" & txtNama.Text & "'", conn)
        cmd.ExecuteNonQuery()
        cmd = New SqlCommand(add_data, conn)
        cmd.ExecuteNonQuery()
        conn.Close()
        reset()
    End Sub
    Private Function add_data() As String
        Dim fields() As String
        Dim nilai() As String
        Dim table_name As String
        ReDim fields(2)
        ReDim nilai(2)
        table_name = "ms_groupkerja"
        fields(0) = "nama_groupkerja"
        nilai(0) = txtNama.Text
        fields(1) = "default_jamkerja"
        nilai(1) = cmbShift.Text

        add_data = query_tambah_data(table_name, fields, nilai)
    End Function
    Private Sub btnHapus_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnHapus.Click
        If MsgBox("Apakah anda yakin hendak menghapus " & txtNama.Text & " ?", vbYesNo) = MsgBoxResult.Yes Then
            Dim conn As New SqlConnection
            Dim cmd As New SqlCommand
            conn.ConnectionString = strcon
            conn.Open()

            cmd = New SqlCommand("delete from ms_groupkerja where nama_groupkerja='" & txtNama.Text & "'", conn)
            cmd.ExecuteNonQuery()

            conn.Close()
            MsgBox("Data berhasil di hapus")
            txtNama.Text = ""
        End If
    End Sub

    Private Sub txtNama_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtNama.LostFocus
        reload()
    End Sub



    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Dim f As New frmSearch
        f.setCol(0)
        f.setTableName("ms_groupkerja")
        f.setColumns("*")

        If f.ShowDialog = System.Windows.Forms.DialogResult.OK Then
            txtNama.Text = f.value
            reload()
        End If
        f.Dispose()
    End Sub

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        Me.Dispose()
    End Sub

    Private Sub txtNama_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtNama.TextChanged

    End Sub


End Class