﻿Imports System.Data.SqlClient
Imports System.Data.Sql
Imports System.IO
Imports System
Imports System.Data.OleDb
Imports System.Text
Imports System.IO.Directory
Imports Microsoft.Office.Interop.Excel
Imports Microsoft.Office.Interop

Public Class frmMasterGroupGaji
    Dim transaction As SqlTransaction
    Private Sub load_shift()
        Dim conn As New SqlConnection
        Dim cmd As New SqlCommand
        Dim dr As SqlDataReader
        conn.ConnectionString = strcon
        conn.Open()
        cmd = New SqlCommand("select distinct golongan from ms_jamkerja where normal<>0 or libur<>0", conn)
        dr = cmd.ExecuteReader
        While dr.Read
            DBGrid.Columns.Add(dr.Item(0), dr.Item(0))
        End While
        dr.Close()
        conn.Close()

    End Sub
    Public Sub reload()
        Dim conn As New SqlConnection
        Dim cmd As New SqlCommand
        Dim dr As SqlDataReader
        conn.ConnectionString = strcon
        conn.Open()

        cmd = New SqlCommand("select * from ms_groupgaji where kode_groupgaji='" & txtKode.Text & "'", conn)
        dr = cmd.ExecuteReader
        dr.Read()
        If dr.HasRows Then
            txtNama.Text = dr.Item("nama")
        End If
        dr.Close()
        'Load komponen
        cmd = New SqlCommand("select distinct m.kode_komponengaji,m.nama, case when m.harian='True' then 'Hari' when m.jam='True' then 'Jam' else '' end from ms_komponengaji m left join ms_groupgajid d on m.kode_komponengaji=d.kode_komponen and kode_groupgaji='" & txtKode.Text & "' where normal<>0 or libur<>0 order by kode_komponengaji", conn)
        dr = cmd.ExecuteReader
        DBGrid.Rows.Clear()
        While dr.Read
            DBGrid.Rows.Add()
            DBGrid.Rows(DBGrid.Rows.Count - 1).Cells(0).Value = dr.Item(0)
            DBGrid.Rows(DBGrid.Rows.Count - 1).Cells(1).Value = dr.Item(1)
            DBGrid.Rows(DBGrid.Rows.Count - 1).Cells(2).Value = dr.Item(2)
            For x = 3 To DBGrid.Columns.Count - 1
                DBGrid.Rows(DBGrid.Rows.Count - 1).Cells(x).Value = 0
            Next
        End While
        dr.Close()

        'Load Nilai
        cmd = New SqlCommand("select kode_komponengaji,d.golongan,isnull(nilai,0) from ms_komponengaji m left join ms_groupgajid d on m.kode_komponengaji=d.kode_komponen where kode_groupgaji='" & txtKode.Text & "' and d.golongan is not null and (m.normal<>0 or m.libur<>0)  order by kode_komponengaji", conn)
        dr = cmd.ExecuteReader
        While dr.Read
            For i = 0 To DBGrid.Rows.Count - 1
                For x = 3 To DBGrid.Columns.Count - 1
                    If DBGrid.Rows(i).Cells(0).Value = dr.Item(0) And DBGrid.Columns(x).Name = dr.Item(1) Then
                        DBGrid.Rows(i).Cells(x).Value = Format(dr.Item(2), "#,##0")
                    End If
                Next
            Next
        End While
        dr.Close()

        'Load komponen bawah
        cmd = New SqlCommand("select distinct m.kode_komponengaji,m.nama,ISNULL(d.nilai,0) nilai  from ms_komponengaji m left join ms_groupgajid d on m.kode_komponengaji=d.kode_komponen and kode_groupgaji='" & txtKode.Text & "' where m.normal=0 and m.libur=0 order by kode_komponengaji", conn)
        dr = cmd.ExecuteReader
        DBGrid1.Rows.Clear()
        While dr.Read
            DBGrid1.Rows.Add()
            DBGrid1.Rows(DBGrid1.Rows.Count - 1).Cells(0).Value = dr.Item(0)
            DBGrid1.Rows(DBGrid1.Rows.Count - 1).Cells(1).Value = dr.Item(1)
            DBGrid1.Rows(DBGrid1.Rows.Count - 1).Cells(2).Value = Format(dr.Item(2), "#,##0")
        End While
        dr.Close()
        conn.Close()
    End Sub

    Private Sub reset_form()
        txtKode.Text = ""
        txtNama.Text = ""
        reload()
    End Sub

    Private Sub save_data()
        Dim conn As New SqlConnection
        Dim cmd As New SqlCommand
        Dim tr As SqlTransaction
        conn.ConnectionString = strcon
        conn.Open()
        tr = conn.BeginTransaction

        Try
            cmd = New SqlCommand("delete from ms_groupgaji where kode_groupgaji='" & txtKode.Text & "'", conn, tr)
            cmd.ExecuteNonQuery()
            cmd = New SqlCommand("delete from ms_groupgajid where kode_groupgaji='" & txtKode.Text & "'", conn, tr)
            cmd.ExecuteNonQuery()
            cmd = New SqlCommand(add_data, conn, tr)
            cmd.ExecuteNonQuery()
            Dim query As String = ""
            For i = 0 To DBGrid.Rows.Count - 1
                For x = 3 To DBGrid.Columns.Count - 1
                    query += add_datadetail(DBGrid.Rows(i).Cells(0).Value, DBGrid.Rows(i).Cells(x).Value, DBGrid.Rows(i).Cells(2).Value, i, DBGrid.Columns(x).Name)
                Next
            Next
            For i = 0 To DBGrid1.Rows.Count - 1
                query += add_datadetail(DBGrid1.Rows(i).Cells(0).Value, DBGrid1.Rows(i).Cells(2).Value, "", i + DBGrid.Rows.Count, "")
            Next
            cmd = New SqlCommand(query, conn, tr)
            cmd.ExecuteNonQuery()

            tr.Commit()
            MsgBox("Data sudah tersimpan")
            reset_form()
        Catch ex As Exception
            MsgBox(ex.Message)
            tr.Rollback()
        End Try

        conn.Close()
        reset_form()
    End Sub
    Private Function add_data() As String
        Dim fields() As String
        Dim nilai() As String
        Dim table_name As String
        ReDim fields(2)
        ReDim nilai(2)
        table_name = "ms_groupgaji"
        fields(0) = "kode_groupgaji"
        nilai(0) = txtKode.Text
        fields(1) = "nama"
        nilai(1) = txtNama.Text

        add_data = query_tambah_data(table_name, fields, nilai)
    End Function
    Private Function add_datadetail(ByVal kode_komponen As String, ByVal value As Double, ByVal satuan As String, ByVal urut As Integer, ByVal golongan As String) As String
        Dim fields() As String
        Dim nilai() As String
        Dim table_name As String
        ReDim fields(6)
        ReDim nilai(6)
        table_name = "ms_groupgajid"
        fields(0) = "kode_groupgaji"
        nilai(0) = txtKode.Text
        fields(1) = "kode_komponen"
        nilai(1) = kode_komponen
        fields(2) = "nilai"
        nilai(2) = CDbl(value)
        fields(3) = "satuan"
        nilai(3) = satuan
        fields(4) = "urut"
        nilai(4) = urut
        fields(5) = "golongan"
        nilai(5) = golongan

        add_datadetail = query_tambah_data(table_name, fields, nilai)
    End Function

    Private Sub frmMasterGroupGaji_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        If e.KeyCode = Keys.Enter Then SendKeys.Send("{tab}")
        If e.KeyCode = Keys.Escape Then Me.Close()
    End Sub
    Private Sub frmMasterGroupGaji_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        DBGrid.Columns("Komponen").Frozen = True
        chkSemua.Checked = True
        load_shift()
        reload()
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If txtKode.Text = "" Then
            MsgBox("Isi kode terlebih dahulu")
            Exit Sub
        End If
        If txtNama.Text = "" Then
            MsgBox("Isi nama terlebih dahulu")
            Exit Sub
        End If
        save_data()
        If txtKode.Enabled = False And btnSearch.Visible = False Then Me.Close()
    End Sub

    Private Sub btnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        search()
    End Sub
    Private Sub search()
        Dim f As New frmSearch
        f.setCol(0)
        f.setTableName("ms_groupgaji")
        f.setColumns("*")

        If f.ShowDialog = System.Windows.Forms.DialogResult.OK Then
            txtKode.Text = f.value
            reload()
        End If
        f.Dispose()
    End Sub
    Private Sub txtKode_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtKode.KeyDown
        If e.KeyCode = Keys.F3 Then search()
    End Sub

    Private Sub DBGrid_CellEndEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DBGrid.CellEndEdit
        If e.ColumnIndex <> 0 Or e.ColumnIndex <> 1 Or e.ColumnIndex <> 2 Then
            DBGrid.Rows(e.RowIndex).Cells(e.ColumnIndex).Value = Format(CDbl(DBGrid.Rows(e.RowIndex).Cells(e.ColumnIndex).Value), "#,##0")
        End If
        If chkSemua.Checked = True Then
            For x = 3 To DBGrid.Columns.Count - 1
                DBGrid.Rows(e.RowIndex).Cells(x).Value = DBGrid.Rows(e.RowIndex).Cells(e.ColumnIndex).Value
            Next
        End If
    End Sub

    Private Sub DBGrid_CellValidating(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellValidatingEventArgs) Handles DBGrid.CellValidating
        If e.ColumnIndex = 2 Then
            If Not IsNumeric(e.FormattedValue) Then e.Cancel = True
        End If
    End Sub

    Private Sub btnHapus_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnHapus.Click
        Dim conn As New SqlConnection
        Dim cmd As New SqlCommand
        conn.ConnectionString = strcon
        conn.Open()

        cmd = New SqlCommand("delete from ms_groupgaji where kode_groupgaji='" & txtKode.Text & "'", conn)
        cmd.ExecuteNonQuery()
        cmd = New SqlCommand("delete from ms_groupgaji where kode_groupgaji='" & txtKode.Text & "'", conn)
        cmd.ExecuteNonQuery()
        conn.Close()
    End Sub

    Private Sub txtKode_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtKode.LostFocus
        reload()
    End Sub

    Private Sub txtKode_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtKode.TextChanged

    End Sub

    Private Sub btnNext_Click(sender As System.Object, e As System.EventArgs) Handles btnNext.Click
        On Error GoTo err
        Dim kode As String
        Dim conn As New SqlConnection
        Dim cmd As New SqlCommand
        Dim dr As SqlDataReader
        conn.ConnectionString = strcon
        conn.Open()
        kode = txtKode.Text

        cmd = New SqlCommand("select top 1 [kode_groupgaji] from ms_groupgaji where [kode_groupgaji]>'" & kode & "' order by [kode_groupgaji] asc", conn)
        dr = cmd.ExecuteReader
        If dr.Read Then
            txtKode.Text = dr.Item(0)
            GoTo search
        Else
            reset_form()
        End If
        dr.Close()
        conn.Close()
        Exit Sub
search:
        dr.Close()
        conn.Close()

        reload()
        Exit Sub
err:
        MsgBox(Err.Description)
        conn.Close()
    End Sub

    Private Sub btnPrev_Click(sender As System.Object, e As System.EventArgs) Handles btnPrev.Click
        On Error GoTo err
        Dim kode As String
        Dim conn As New SqlConnection
        Dim cmd As New SqlCommand
        Dim dr As SqlDataReader
        conn.ConnectionString = strcon
        conn.Open()
        kode = txtKode.Text

        cmd = New SqlCommand("select top 1 [kode_groupgaji] from ms_groupgaji where [kode_groupgaji]<'" & kode & "' order by [kode_groupgaji] desc", conn)
        dr = cmd.ExecuteReader
        If dr.Read Then
            txtKode.Text = dr.Item(0)
            GoTo search
        Else
            reset_form()
        End If
        dr.Close()
        conn.Close()
        Exit Sub
search:
        dr.Close()
        conn.Close()

        reload()
        Exit Sub
err:
        MsgBox(Err.Description)
        conn.Close()
    End Sub

    Private Sub btnExit_Click(sender As System.Object, e As System.EventArgs) Handles btnExit.Click
        Me.Close()
    End Sub

    Private Sub DBGrid_CellContentClick(sender As System.Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DBGrid.CellContentClick

    End Sub

    Private Sub DBGrid1_CellContentClick(sender As System.Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DBGrid1.CellContentClick

    End Sub

    Private Sub DBGrid1_CellEndEdit(sender As Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DBGrid1.CellEndEdit
        If e.ColumnIndex = 2 Then
            DBGrid1.Rows(e.RowIndex).Cells(e.ColumnIndex).Value = Format(CDbl(DBGrid1.Rows(e.RowIndex).Cells(e.ColumnIndex).Value), "#,##0")
        End If
    End Sub

    Private Sub btnExport_Click(sender As System.Object, e As System.EventArgs) Handles btnExport.Click
        Dim conn As New SqlConnection
        Dim cmd As New SqlCommand
        Dim dataAdapter As New SqlClient.SqlDataAdapter()
        Dim dataSet As New DataSet
        Dim datatableMain As New System.Data.DataTable()
        conn.ConnectionString = strcon
        conn.Open()

        cmd = New SqlCommand("exec sp_exportgroupgaji", conn)
        dataAdapter.SelectCommand = cmd
        'Fill data to datatable
        dataAdapter.Fill(datatableMain)
        conn.Close()

        Dim f As FolderBrowserDialog = New FolderBrowserDialog
        Try
            If f.ShowDialog() = DialogResult.OK Then
                'This section help you if your language is not English.
                System.Threading.Thread.CurrentThread.CurrentCulture = _
                System.Globalization.CultureInfo.CreateSpecificCulture("en-US")
                Dim oExcel As Excel.Application
                Dim oBook As Excel.Workbook
                Dim oSheet As Excel.Worksheet
                oExcel = CreateObject("Excel.Application")
                oBook = oExcel.Workbooks.Add(Type.Missing)
                oSheet = oBook.Worksheets(1)

                Dim dc As System.Data.DataColumn
                Dim dr As System.Data.DataRow
                Dim colIndex As Integer = 0
                Dim rowIndex As Integer = 0

                'Export the Columns to excel file
                For Each dc In datatableMain.Columns
                    colIndex = colIndex + 1
                    oSheet.Cells(1, colIndex) = dc.ColumnName
                Next

                'Export the rows to excel file
                For Each dr In datatableMain.Rows
                    rowIndex = rowIndex + 1
                    colIndex = 0
                    For Each dc In datatableMain.Columns
                        colIndex = colIndex + 1
                        oSheet.Cells(rowIndex + 1, colIndex) = dr(dc.ColumnName)
                    Next
                Next

                'Set final path
                Dim fileName As String = "3pmExportGroupGaji" + ".xls"
                Dim finalPath = f.SelectedPath + fileName
                txtPath.Text = finalPath
                oSheet.Columns.AutoFit()
                'Save file in final path
                oBook.SaveAs(finalPath, XlFileFormat.xlWorkbookNormal, Type.Missing, _
                Type.Missing, Type.Missing, Type.Missing, XlSaveAsAccessMode.xlExclusive, _
                Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing)

                'Release the objects
                ReleaseObject(oSheet)
                oBook.Close(False, Type.Missing, Type.Missing)
                ReleaseObject(oBook)
                oExcel.Quit()
                ReleaseObject(oExcel)
                'Some time Office application does not quit after automation: 
                'so i am calling GC.Collect method.
                GC.Collect()

                MessageBox.Show("Export done successfully!")

            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Warning", MessageBoxButtons.OK)
        End Try
    End Sub

    Private Sub ReleaseObject(ByVal o As Object)
        Try
            While (System.Runtime.InteropServices.Marshal.ReleaseComObject(o) > 0)
            End While
        Catch
        Finally
            o = Nothing
        End Try
    End Sub

    Private Sub btnimport_Click(sender As System.Object, e As System.EventArgs) Handles btnimport.Click
        Dim fname As String
        Dim Ex As OpenFileDialog = New OpenFileDialog()
        Ex.Filter = "Excel files(*.xls)|*.xls|All files (*.*)|*.*"
        Ex.FilterIndex = 2
        Ex.RestoreDirectory = True
        Ex.Title = "Import data from Excel file"
        If (Ex.ShowDialog() = DialogResult.OK) Then
            fname = Ex.FileName
        Else
            Exit Sub
        End If
        Try
            Dim MyConnection As System.Data.OleDb.OleDbConnection
            Dim DtSet As System.Data.DataSet
            Dim MyCommand As System.Data.OleDb.OleDbDataAdapter
            MyConnection = New System.Data.OleDb.OleDbConnection("provider=Microsoft.Jet.OLEDB.4.0;Data Source='" & fname & "'; Extended Properties=Excel 8.0;")
            MyCommand = New System.Data.OleDb.OleDbDataAdapter _
                ("select * from [Sheet1$]", MyConnection)
            MyCommand.TableMappings.Add("Table", "TestTable")
            DtSet = New System.Data.DataSet
            MyCommand.Fill(DtSet)
            DataGridView1.DataSource = DtSet.Tables(0)
            MyConnection.Close()
            If DataGridView1.RowCount > 0 Then simpan()
        Catch exs As Exception
            MsgBox(exs.ToString)
        End Try
    End Sub
    Private Function simpan() As Boolean
        simpan = False
        Dim cmd As SqlCommand
        Dim conn As New SqlConnection
        conn.ConnectionString = strcon
        conn.Open()
        transaction = conn.BeginTransaction(IsolationLevel.ReadCommitted)
        Try
            Dim sqldetail As String = Nothing
            cmd = New SqlCommand("delete from tmp_importgroupgaji;", conn, transaction)
            cmd.ExecuteNonQuery()
            For i As Integer = 0 To DataGridView1.RowCount - 1
                sqldetail += add_dataa(i) + ";"
            Next
            If sqldetail <> Nothing Then
                cmd = New SqlCommand(sqldetail, conn, transaction)
                cmd.ExecuteNonQuery()
            End If

            cmd = New SqlCommand("exec sp_importgroupgaji ", conn, transaction)
            cmd.ExecuteNonQuery()
            transaction.Commit()
            conn.Close()
            MsgBox("Import successfully!")
        Catch ex As Exception
            MessageBox.Show(ex.Message.ToString)
            If reader IsNot Nothing Then reader.Close()
            transaction.Rollback()
            If conn.State Then conn.Close()
        End Try
    End Function

    Private Function add_dataa(ByVal row As Integer) As String
        Dim fields() As String
        Dim nilai() As String
        Dim table_name As String

        ReDim fields(6)
        ReDim nilai(6)

        table_name = "tmp_importgroupgaji"
        fields(0) = "nik"
        fields(1) = "nama"
        fields(2) = "gajipokok"
        fields(3) = "nilai1"
        fields(4) = "nilai2"
        fields(5) = "nilai3"

        nilai(0) = CStr(DataGridView1.Item(0, row).Value)
        nilai(1) = CStr(DataGridView1.Item(1, row).Value)
        nilai(2) = CDbl(DataGridView1.Item(2, row).Value)
        nilai(3) = CDbl(DataGridView1.Item(3, row).Value)
        If IsDBNull(DataGridView1.Item(4, row).Value) Then nilai(4) = 0 Else nilai(4) = CDbl(DataGridView1.Item(4, row).Value)
        If IsDBNull(DataGridView1.Item(5, row).Value) Then nilai(5) = 0 Else nilai(5) = CDbl(DataGridView1.Item(5, row).Value)

        add_dataa = query_tambah_data(table_name, fields, nilai)
    End Function

End Class