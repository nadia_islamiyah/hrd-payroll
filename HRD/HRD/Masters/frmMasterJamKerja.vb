﻿Imports System.Data.SqlClient
Imports System.Data.Sql
Imports System.IO
Public Class frmMasterJamKerja
    Dim da As SqlDataAdapter
    Dim ds As New DataSet

    Private Sub frmMasterJamKerja_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown

        If e.KeyCode = Keys.Escape Then Me.Dispose()
        If e.KeyCode = Keys.Enter Then SendKeys.Send("{tab}")
    End Sub

    Private Sub frmMasterJamKerja_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress

    End Sub
    Private Sub frmMasterJamKerja_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        reset()
    End Sub
    Private Sub reload()
        Dim conn As New SqlConnection
        Dim cmd As New SqlCommand
        Dim dr As SqlDataReader
        conn.ConnectionString = strcon
        conn.Open()


        cmd = New SqlCommand("select * from ms_jamkerja where nama_shift='" & txtNama.Text & "'", conn)
        dr = cmd.ExecuteReader
        dr.Read()
        If dr.HasRows Then
            cmbgolongan.Text = dr.Item("golongan")
            jamMasuk.Value = dr.Item("masuk_jam")
            txtJamKerja.Text = CDbl(dr.Item("jumlah_jamkerja"))
            txtMasukToleransiSebelum.Text = dr.Item("masuk_toleransi_sebelum")
            txtMasukToleransiSesudah.Text = dr.Item("masuk_toleransi_sesudah")
            If dr.Item("masuk_pembulatan") Then chkMasukPembulatan.Checked = True Else chkMasukPembulatan.Checked = False
            setcombotextright(cmbMasukPembulatan, dr.Item("masuk_arahpembulatan"))

            jamIstirahat.Value = dr.Item("istirahat_jam")
            txtIstirahatToleransiSebelum.Text = dr.Item("istirahat_toleransi_sebelum")
            txtIstirahatToleransiSesudah.Text = dr.Item("istirahat_toleransi_sesudah")
            If dr.Item("istirahat_pembulatan") Then chkIstirahatPembulatan.Checked = True Else chkIstirahatPembulatan.Checked = False
            setcombotextright(cmbIstirahatPembulatan, dr.Item("istirahat_arahpembulatan"))

            jamKembali.Value = dr.Item("kembali_jam")
            txtKembaliToleransiSebelum.Text = dr.Item("kembali_toleransi_sebelum")
            txtKembaliToleransiSesudah.Text = dr.Item("kembali_toleransi_sesudah")
            If dr.Item("kembali_pembulatan") Then chkKembaliPembulatan.Checked = True Else chkKembaliPembulatan.Checked = False
            setcombotextright(cmbKembaliPembulatan, dr.Item("kembali_arahpembulatan"))

            jamPulang.Value = dr.Item("pulang_jam")
            txtPulangToleransiSebelum.Text = dr.Item("pulang_toleransi_sebelum")
            txtPulangToleransiSesudah.Text = dr.Item("pulang_toleransi_sesudah")
            If dr.Item("pulang_pembulatan") Then chkPulangPembulatan.Checked = True Else chkPulangPembulatan.Checked = False
            setcombotextright(cmbPulangPembulatan, dr.Item("pulang_arahpembulatan"))

            chkLibur.Checked = dr.Item("libur")
            chknormal.Checked = dr.Item("normal")
        End If
        conn.Close()

    End Sub
    Private Sub reset()
        txtJamKerja.Text = "1"
        jamMasuk.Value = "1900/01/01 00:00"

        txtMasukToleransiSebelum.Text = "0"
        txtMasukToleransiSesudah.Text = "0"
        chkMasukPembulatan.Checked = True
        cmbMasukPembulatan.SelectedIndex = 0

        jamIstirahat.Value = "1900/01/01 00:00"
        txtIstirahatToleransiSebelum.Text = "0"
        txtIstirahatToleransiSesudah.Text = "0"
        chkIstirahatPembulatan.Checked = True
        cmbIstirahatPembulatan.SelectedIndex = 0

        jamKembali.Value = "1900/01/01 00:00"
        txtKembaliToleransiSebelum.Text = "0"
        txtKembaliToleransiSesudah.Text = "0"
        chkKembaliPembulatan.Checked = True
        cmbKembaliPembulatan.SelectedIndex = 0

        jamPulang.Value = "1900/01/01 00:00"
        txtPulangToleransiSebelum.Text = "0"
        txtPulangToleransiSesudah.Text = "0"
        chkPulangPembulatan.Checked = True
        cmbPulangPembulatan.SelectedIndex = 0
        txtNama.Text = ""
        txtNama.Focus()
        cmbgolongan.Text = ""
        cmbgolongan.SelectedIndex = -1
        loadComboBoxAll(cmbgolongan, "distinct golongan", " ms_jamkerja", "", "golongan", "golongan")
    End Sub
    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim conn As New SqlConnection
        Dim cmd As New SqlCommand
        conn.ConnectionString = strcon
        conn.Open()

        cmd = New SqlCommand("delete from ms_jamkerja where nama_shift='" & txtNama.Text & "'", conn)
        cmd.ExecuteNonQuery()
        cmd = New SqlCommand(add_data, conn)
        cmd.ExecuteNonQuery()
        conn.Close()
        reset()
    End Sub
    Private Function add_data() As String
        Dim fields() As String
        Dim nilai() As String
        Dim table_name As String
        ReDim fields(25)
        ReDim nilai(25)
        table_name = "ms_jamkerja"
        fields(0) = "nama_shift"
        nilai(0) = txtNama.Text
        fields(1) = "masuk_jam"
        nilai(1) = Format(jamMasuk.Value, "HH:mm")
        fields(2) = "masuk_toleransi_sebelum"
        nilai(2) = txtMasukToleransiSebelum.Text
        fields(3) = "masuk_toleransi_sesudah"
        nilai(3) = txtMasukToleransiSesudah.Text
        fields(4) = "masuk_pembulatan"
        nilai(4) = chkMasukPembulatan.Checked
        fields(5) = "masuk_arahpembulatan"
        nilai(5) = LTrim(Strings.Right(cmbMasukPembulatan.Text, 10))

        fields(6) = "istirahat_jam"
        nilai(6) = Format(jamIstirahat.Value, "HH:mm")
        fields(7) = "istirahat_toleransi_sebelum"
        nilai(7) = txtIstirahatToleransiSebelum.Text
        fields(8) = "istirahat_toleransi_sesudah"
        nilai(8) = txtIstirahatToleransiSesudah.Text
        fields(9) = "istirahat_pembulatan"
        nilai(9) = chkIstirahatPembulatan.Checked
        fields(10) = "istirahat_arahpembulatan"
        nilai(10) = LTrim(Strings.Right(cmbIstirahatPembulatan.Text, 10))

        fields(11) = "kembali_jam"
        nilai(11) = Format(jamKembali.Value, "HH:mm")
        fields(12) = "kembali_toleransi_sebelum"
        nilai(12) = txtKembaliToleransiSebelum.Text
        fields(13) = "kembali_toleransi_sesudah"
        nilai(13) = txtKembaliToleransiSesudah.Text
        fields(14) = "kembali_pembulatan"
        nilai(14) = chkKembaliPembulatan.Checked
        fields(15) = "kembali_arahpembulatan"
        nilai(15) = LTrim(Strings.Right(cmbKembaliPembulatan.Text, 10))

        fields(16) = "pulang_jam"
        nilai(16) = Format(jamPulang.Value, "HH:mm")
        fields(17) = "pulang_toleransi_sebelum"
        nilai(17) = txtPulangToleransiSebelum.Text
        fields(18) = "pulang_toleransi_sesudah"
        nilai(18) = txtPulangToleransiSesudah.Text
        fields(19) = "pulang_pembulatan"
        nilai(19) = chkPulangPembulatan.Checked
        fields(20) = "pulang_arahpembulatan"
        nilai(20) = LTrim(Strings.Right(cmbPulangPembulatan.Text, 10))
        fields(21) = "jumlah_jamkerja"
        nilai(21) = CDbl(txtJamKerja.Text)

        fields(22) = "libur"
        nilai(22) = chkLibur.Checked
        fields(23) = "normal"
        nilai(23) = chknormal.Checked
        fields(24) = "golongan"
        nilai(24) = cmbgolongan.Text

        add_data = query_tambah_data(table_name, fields, nilai)
    End Function
    Private Sub btnHapus_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnHapus.Click
        If MsgBox("Apakah anda yakin hendak menghapus " & txtNama.Text & " ?", vbYesNo) = MsgBoxResult.Yes Then
            Dim conn As New SqlConnection
            Dim cmd As New SqlCommand
            conn.ConnectionString = strcon
            conn.Open()

            cmd = New SqlCommand("delete from ms_jamkerja where nama_shift='" & txtNama.Text & "'", conn)
            cmd.ExecuteNonQuery()

            conn.Close()

        End If
    End Sub

    Private Sub txtNama_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtNama.LostFocus
        reload()
    End Sub


    Private Sub chkKontrak_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        If chkMasukPembulatan.Checked Then grpKontrak.Visible = True Else grpKontrak.Visible = False
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Dim f As New frmSearch
        f.setCol(0)
        f.setTableName("ms_jamkerja")
        f.setColumns("nama_shift,convert(char(5), masuk_jam, 108) masuk_jam,convert(char(5), istirahat_jam, 108) istirahat_jam,convert(char(5), kembali_jam, 108) kembali_jam,convert(char(5), pulang_jam, 108) pulang_jam")
        If f.ShowDialog = System.Windows.Forms.DialogResult.OK Then
            txtNama.Text = f.value
            reload()
        End If
        f.Dispose()
    End Sub

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        Me.Dispose()
    End Sub

    Private Sub txtNama_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtNama.TextChanged

    End Sub

    Private Sub jamMasuk_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles jamMasuk.ValueChanged
        hitung_jamkerja()
    End Sub
    Private Sub hitung_jamkerja()
        On Error Resume Next
        Dim istirahat As Double
        Dim kerja As Double

        kerja = CDbl(txtJamKerja.Text)
        istirahat = DateDiff(DateInterval.Hour, jamIstirahat.Value, jamKembali.Value)
        jamPulang.Value = DateAdd(DateInterval.Hour, kerja + istirahat, jamMasuk.Value)
    End Sub

    Private Sub jamIstirahat_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles jamIstirahat.ValueChanged
        hitung_jamkerja()
    End Sub

    Private Sub jamKembali_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles jamKembali.ValueChanged
        hitung_jamkerja()
    End Sub

    Private Sub txtJamKerja_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtJamKerja.LostFocus
        hitung_jamkerja()
    End Sub

    Private Sub btnNext_Click(sender As System.Object, e As System.EventArgs) Handles btnNext.Click
        Dim conn As New SqlConnection(strcon)
        Dim cmd As SqlCommand
        Dim dr As SqlDataReader
        conn.Open()
        cmd = New SqlCommand("select nama_shift from  ms_jamkerja where nama_shift>'" & txtNama.Text & "' order by nama_shift asc", conn)
        dr = cmd.ExecuteReader
        If dr.Read Then
            txtNama.Text = dr.Item("nama_shift")
            reload()
        End If
        conn.Close()
    End Sub

    Private Sub btnPrev_Click(sender As System.Object, e As System.EventArgs) Handles btnPrev.Click
        Dim conn As New SqlConnection(strcon)
        Dim cmd As SqlCommand
        Dim dr As SqlDataReader
        conn.Open()
        cmd = New SqlCommand("select nama_shift from ms_jamkerja where nama_shift<'" & txtNama.Text & "' order by nama_shift asc", conn)
        dr = cmd.ExecuteReader
        If dr.Read Then
            txtNama.Text = dr.Item("nama_shift")
            reload()
        End If
        conn.Close()
    End Sub
End Class