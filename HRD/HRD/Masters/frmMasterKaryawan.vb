﻿Imports System.Data.OleDb
Imports System.Data.SqlClient

Public Class frmMasterKaryawan
    Dim transaction As SqlTransaction = Nothing

    Private Sub frmMasterKaryawan_KeyDown(sender As Object, e As KeyEventArgs) Handles MyBase.KeyDown
        If e.KeyCode = Keys.F2 Then btnSimpan.PerformClick()
        If e.KeyCode = Keys.F5 Then btnSearch.PerformClick()
        If e.Control And e.KeyCode = Keys.R Then btnReset.PerformClick()
        If e.KeyCode = Keys.Enter Then SendKeys.Send("{TAB}")
        If e.KeyCode = Keys.Escape Then Me.Close()
        End Sub

    Private Sub btnReset_Click(sender As Object, e As EventArgs) Handles btnReset.Click
        reset_form()
    End Sub

    Private Sub reset_form()
        txt_fingerID.Text = ""
        txtNIK.Text = ""
        txtNama.Text = ""
        txtNIK.Focus()
        cmbGroupKerja.Text = ""
        loadComboBoxAll(cmbDivisi, "divisi", "var_divisi", "", "divisi", "divisi")
        loadComboBoxAll(cmbTipe, "tipe", "var_tipekaryawan", "", "tipe", "tipe")
        loadComboBoxAll(cmbArea, "area", "var_PayrollArea", "", "area", "area")
        loadComboBoxAll(cmbGroupKerja, "nama_groupkerja", "ms_groupkerja", "", "nama_groupkerja", "nama_groupkerja")
        loadComboBoxAll(cmbGroupGaji, "kode_groupgaji", "ms_groupgaji", "", "kode_groupgaji", "kode_groupgaji")
    End Sub

    Private Sub btnHapus_Click(sender As Object, e As EventArgs) Handles btnHapus.Click
        Dim conn2 As New SqlConnection(strcon)
        Dim cmd As New SqlCommand
        conn2.Open()
        transaction = conn2.BeginTransaction(IsolationLevel.ReadCommitted)
        Try
            cmd = New SqlCommand("delete from  ms_karyawan where NIK = '" & txtNIK.Text & "' ", conn2, transaction)
            cmd.ExecuteNonQuery()
            transaction.Commit()
            MsgBox("Data berhasil dihapus . . .", MsgBoxStyle.Information)

            conn2.Close()
            reset_form()
        Catch ex As Exception
            MessageBox.Show(ex.Message.ToString)
            transaction.Rollback()

        End Try
        If conn2.State Then conn2.Close()
    End Sub

    Private Function authentication() As Boolean
        Dim a As Boolean
        If txtNIK.Text = "" Then a = True
        If txtNama.Text = "" Then a = True

        authentication = a
    End Function

    Private Sub btnSimpan_Click(sender As Object, e As EventArgs) Handles btnSimpan.Click
        If authentication() = True Then
            MsgBox("Yang bertanda * harus diisi.", MsgBoxStyle.Information)
            Exit Sub
        End If
        Dim conn2 As New SqlConnection(strcon)
        Dim cmd As SqlCommand
        Dim reader As SqlDataReader = Nothing
        conn2.Open()
        transaction = conn2.BeginTransaction(IsolationLevel.ReadCommitted)
        Try
            cmd = New SqlCommand("delete from  ms_karyawan where nik='" & txtNIK.Text & "'", conn2, transaction)
            cmd.ExecuteNonQuery()
            cmd = New SqlCommand("delete from  ms_karyawan_tipe where nik='" & txtNIK.Text & "'", conn2, transaction)
            cmd.ExecuteNonQuery()
            cmd = New SqlCommand(add_dataheader, conn2, transaction)
            cmd.ExecuteNonQuery()
            cmd = New SqlCommand(add_datadivisi, conn2, transaction)
            cmd.ExecuteNonQuery()

            transaction.Commit()
            conn2.Close()
            If MsgBox("Data sudah tersimpan, masukkan data baru?", vbYesNo) = vbYes Then reset_form()


        Catch ex As Exception
            MessageBox.Show(ex.Message.ToString)
            transaction.Rollback()
            If reader IsNot Nothing Then reader.Close()

        End Try
        If conn2.State Then conn2.Close()
    End Sub

    Private Function add_dataheader() As String
        add_dataheader = ""
        Dim fields() As String
        Dim nilai() As String
        Dim table_name As String

        ReDim fields(4)
        ReDim nilai(4)

        table_name = "absensi.dbo.ms_karyawan"
        fields(0) = "NIK"
        fields(1) = "nama_depan"
        fields(2) = "finger_id"
        fields(3) = "aktif"

        nilai(0) = txtNIK.Text
        nilai(1) = txtNama.Text
        nilai(2) = txt_fingerID.Text
        nilai(3) = ChkAktif.Checked

        add_dataheader = query_tambah_data(table_name, fields, nilai)

    End Function

    Private Function add_datadivisi() As String
        Dim fields() As String
        Dim nilai() As String
        Dim table_name As String
        ReDim fields(6)
        ReDim nilai(6)
        table_name = " ms_karyawan_tipe"
        fields(0) = "nik"
        nilai(0) = txtNIK.Text
        fields(1) = "tipe"
        nilai(1) = cmbTipe.Text
        fields(2) = "area"
        nilai(2) = cmbArea.Text
        fields(3) = "divisi"
        nilai(3) = cmbDivisi.Text
        fields(4) = "groupkerja"
        nilai(4) = cmbGroupKerja.Text
        fields(5) = "kode_groupgaji"
        nilai(5) = cmbGroupGaji.Text
        add_datadivisi = query_tambah_data(table_name, fields, nilai)
    End Function

    Private Sub frmMasterKaryawan_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        reset_form()

    End Sub

    Private Sub txtLimitKredit_KeyPress(sender As Object, e As KeyPressEventArgs)
        NumericOnly(e)
    End Sub

    Private Sub txtGajiPokok_KeyPress(sender As Object, e As KeyPressEventArgs)
        NumericOnly(e)
    End Sub

    Private Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click
        Dim f As New frmSearch
        f.setCol(0)
        f.setTableName(" vw_karyawantipe")
        f.setColumns("*")
        f.setOrder("order by nik")
        If f.ShowDialog = System.Windows.Forms.DialogResult.OK Then
            txtNIK.Text = f.value
            loadMasterKaryawan()
        End If
        f.Dispose()
    End Sub

    Private Sub loadMasterKaryawan()
        Dim conn2 As New SqlConnection(strcon)
        Dim cmd As SqlCommand
        Dim reader As SqlDataReader = Nothing
        conn2.Open()
        Try
            cmd = New SqlCommand("select *,isnull(finger_id,'') fingerid from  vw_karyawantipe where NIK = '" & txtNIK.Text & "'", conn2)
            reader = cmd.ExecuteReader()
            If reader.Read() Then
                txtNama.Text = reader("nama_depan")
                If reader("aktif") = True Then ChkAktif.Checked = True Else ChkAktif.Checked = False
                txt_fingerID.Text = reader("fingerid")
                cmbTipe.Text = reader("tipe")
                cmbArea.Text = reader("area")
                cmbDivisi.Text = reader("divisi")
                cmbGroupKerja.Text = reader("groupkerja")
                cmbGroupGaji.Text = reader("kode_groupgaji")
            End If
            reader.Close()

        Catch ex As Exception
            MessageBox.Show(ex.Message.ToString)
        End Try
        conn2.Close()
    End Sub

    Private Sub txtNIK_Leave(sender As Object, e As System.EventArgs) Handles txtNIK.Leave
        loadMasterKaryawan()
    End Sub

    Private Sub btnNext_Click(sender As System.Object, e As System.EventArgs) Handles btnNext.Click
        Dim conn As New SqlConnection(strcon)
        Dim cmd As SqlCommand
        Dim dr As SqlDataReader
        conn.Open()
        'load next bill
        cmd = New SqlCommand("select nik from  ms_karyawan where nik>'" & txtNIK.Text & "' order by nik asc", conn)
        dr = cmd.ExecuteReader
        If dr.Read Then
            txtNIK.Text = dr.Item("nik")
            loadMasterKaryawan()
        End If
        conn.Close()
    End Sub

    Private Sub btnPrev_Click(sender As System.Object, e As System.EventArgs) Handles btnPrev.Click
        Dim conn As New SqlConnection(strcon)
        Dim cmd As SqlCommand
        Dim dr As SqlDataReader
        conn.Open()
        'load next bill
        cmd = New SqlCommand("select nik from ms_karyawan where nik<'" & txtNIK.Text & "' order by nik asc", conn)
        dr = cmd.ExecuteReader
        If dr.Read Then
            txtNIK.Text = dr.Item("nik")
            loadMasterKaryawan()
        End If
        conn.Close()
    End Sub

    Private Sub btnKeluar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnKeluar.Click
        Me.Close()
    End Sub

    Private Sub GroupBox1_Enter(sender As System.Object, e As System.EventArgs) Handles GroupBox1.Enter

    End Sub

    Private Sub btnGroupGaji_Click(sender As System.Object, e As System.EventArgs)

    End Sub

    Private Sub Button1_Click(sender As System.Object, e As System.EventArgs) Handles Button1.Click
        Dim f As New frmMasterGroupGaji
        f.txtKode.Text = txtNIK.Text
        f.txtNama.Text = txtNama.Text
        f.txtKode.Enabled = False
        f.btnSearch.Visible = False
        f.ShowDialog()
        cmbGroupGaji.Text = txtNIK.Text
    End Sub
End Class