﻿Imports System.Data.SqlClient
Imports System.Data.Sql
Imports System.IO
Public Class frmMasterKaryawan2

    Private Sub frmMasterKaryawan_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        reloadcombo()
        reset_form()
    End Sub
    Private Sub reloadcombo()
        Dim conn As New SqlConnection
        Dim cmd As New SqlCommand
        Dim dr As SqlDataReader
        conn.ConnectionString = strcon
        conn.Open()


        cmd = New SqlCommand("select * from var_tipekaryawan order by  tipe;select * from var_payrollarea order by  area;select * from var_agama order by agama;select * from var_bank order by bank;select * from var_divisi order by divisi;select * from ms_groupkerja order by nama_groupkerja;select * from ms_groupgaji order by kode_groupgaji", conn)
        dr = cmd.ExecuteReader
        cmbTipe.Items.Clear()
        While dr.Read
            cmbTipe.Items.Add(dr.Item(0))
        End While

        'dr.Close()
        'cmd = New SqlCommand("select * from var_payrollarea order by  area", conn)
        'dr = cmd.ExecuteReader
        dr.NextResult()
        cmbArea.Items.Clear()
        While dr.Read
            cmbArea.Items.Add(dr.Item(0))
        End While
        dr.NextResult()

        'cmd = New SqlCommand("select * from var_agama order by  agama", conn)
        'dr = cmd.ExecuteReader
        cmbAgama.Items.Clear()
        While dr.Read
            cmbAgama.Items.Add(dr.Item(0))
        End While
        dr.NextResult()
        'cmd = New SqlCommand("select * from var_bank order by  bank", conn)
        'dr = cmd.ExecuteReader
        cmbBank.Items.Clear()
        While dr.Read
            cmbBank.Items.Add(dr.Item(0))
        End While
        dr.NextResult()
        cmbDivisi.Items.Clear()
        While dr.Read
            cmbDivisi.Items.Add(dr.Item(0))
        End While
        dr.NextResult()
        cmbWorkGroup.Items.Clear()
        While dr.Read
            cmbWorkGroup.Items.Add(dr.Item(0))
        End While
        dr.NextResult()
        cmbGroupGaji.Items.Clear()
        While dr.Read
            cmbGroupGaji.Items.Add(dr.Item(0))
        End While
        conn.Close()
    End Sub

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        Me.Dispose()
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim conn As New SqlConnection
        Dim cmd As New SqlCommand
        Dim tr As SqlTransaction
        conn.ConnectionString = strcon
        conn.Open()

        tr = conn.BeginTransaction()
        Try
            cmd = New SqlCommand("delete from ms_karyawan where nik='" & txtNIK.Text & "'", conn, tr)
            cmd.ExecuteNonQuery()
            cmd = New SqlCommand("delete from ms_karyawan_tipe where nik='" & txtNIK.Text & "'", conn, tr)
            cmd.ExecuteNonQuery()
            cmd = New SqlCommand("delete from ms_karyawan_keluarga where nik='" & txtNIK.Text & "'", conn, tr)
            cmd.ExecuteNonQuery()
            cmd = New SqlCommand("delete from ms_karyawan_pendidikan where nik='" & txtNIK.Text & "'", conn, tr)
            cmd.ExecuteNonQuery()
            cmd = New SqlCommand(add_data, conn, tr)
            cmd.ExecuteNonQuery()
            cmd = New SqlCommand(add_datadivisi, conn, tr)
            cmd.ExecuteNonQuery()
            For i As Integer = 0 To DBGridKeluarga.Rows.Count - 1
                If DBGridKeluarga.Item(0, i).Value <> "" Then
                    cmd = New SqlCommand(add_datakeluarga(i), conn, tr)
                    cmd.ExecuteNonQuery()
                End If
            Next
            For i As Integer = 0 To DBGridPendidikan.Rows.Count - 1
                If DBGridPendidikan.Item(0, i).Value <> "" Then
                    cmd = New SqlCommand(add_datapendidikan(i), conn, tr)
                    cmd.ExecuteNonQuery()
                End If
            Next
            tr.Commit()
            MsgBox("Data sudah tersimpan")
        Catch ex As Exception
            MsgBox(ex.Message)
            tr.Rollback()
        Finally

        End Try


        conn.Close()
        Reset()
    End Sub
    Private Function add_data() As String
        Dim fields() As String
        Dim nilai() As String
        Dim table_name As String
        ReDim fields(30)
        ReDim nilai(30)
        table_name = "ms_karyawan"
        fields(0) = "nik"
        nilai(0) = txtNIK.Text
        fields(1) = "nama_depan"
        nilai(1) = txtNamaDepan.Text
        fields(2) = "nama_belakang"
        nilai(2) = txtNamaBelakang.Text
        fields(3) = "nama_panggilan"
        nilai(3) = txtNamaPanggilan.Text
        fields(4) = "alamat"
        nilai(4) = txtAlamat.Text
        fields(5) = "rt"
        nilai(5) = txtRT.Text
        fields(6) = "rw"
        nilai(6) = txtRW.Text
        fields(7) = "kelurahan"
        nilai(7) = txtKelurahan.Text
        fields(8) = "kecamatan"
        nilai(8) = txtKecamatan.Text
        fields(9) = "kota"
        nilai(9) = txtKota.Text
        fields(10) = "kodepos"
        nilai(10) = txtKodePos.Text
        fields(11) = "telp"
        nilai(11) = txtTelp.Text
        fields(12) = "hp"
        nilai(12) = txtHP.Text
        fields(13) = "email"
        nilai(13) = txtEmail.Text
        fields(14) = "suku"
        nilai(14) = txtSuku.Text
        fields(15) = "wni"
        nilai(15) = chkWNI.Checked
        fields(16) = "noktp"
        nilai(16) = txtNoKTP.Text
        fields(17) = "nosim"
        nilai(17) = txtNoSim.Text
        fields(18) = "nopasport"
        nilai(18) = txtNoPassport.Text
        fields(19) = "tempat_lahir"
        nilai(19) = txtTempatLahir.Text
        fields(20) = "tanggal_lahir"
        nilai(20) = Format(dtTanggalLahir.Value, "yyyy/MM/dd")
        fields(21) = "agama"
        nilai(21) = cmbAgama.Text
        fields(22) = "jenis_kelamin"
        nilai(22) = IIf(rdJenisKelamin1.Checked = True, "L", "P")
        fields(23) = "status_pernikahan"
        nilai(23) = cmbStatusNikah.Text
        fields(24) = "bank"
        nilai(24) = cmbBank.Text
        fields(25) = "no_rekening"
        nilai(25) = txtNoRekening.Text
        fields(26) = "nama_rekening"
        nilai(27) = txtAtasNama.Text
        fields(28) = "npwp"
        nilai(28) = txtNPWP.Text
        fields(29) = "jamsostek"
        nilai(29) = txtJamsostek.Text
        add_data = query_tambah_data(table_name, fields, nilai)
    End Function
    Private Function add_datadivisi() As String
        Dim fields() As String
        Dim nilai() As String
        Dim table_name As String
        ReDim fields(6)
        ReDim nilai(6)
        table_name = "ms_karyawan_tipe"
        fields(0) = "nik"
        nilai(0) = txtNIK.Text
        fields(1) = "tipe"
        nilai(1) = cmbTipe.Text
        fields(2) = "area"
        nilai(2) = cmbArea.Text
        fields(3) = "divisi"
        nilai(3) = cmbDivisi.Text
        fields(4) = "groupkerja"
        nilai(4) = cmbWorkGroup.Text
        fields(5) = "kode_groupgaji"
        nilai(5) = cmbGroupGaji.Text
        add_datadivisi = query_tambah_data(table_name, fields, nilai)
    End Function
    Private Function add_datakeluarga(ByVal baris As Integer) As String
        Dim fields() As String
        Dim nilai() As String
        Dim table_name As String
        ReDim fields(6)
        ReDim nilai(6)
        table_name = "ms_karyawan_keluarga"
        fields(0) = "nik"
        nilai(0) = txtNIK.Text
        fields(1) = "nama_keluarga"
        nilai(1) = DBGridKeluarga.Rows(baris).Cells(0).Value
        fields(2) = "hubungan_keluarga"
        nilai(2) = DBGridKeluarga.Rows(baris).Cells(1).Value
        fields(3) = "alamat_keluarga"
        nilai(3) = DBGridKeluarga.Rows(baris).Cells(2).Value
        fields(4) = "telp_keluarga"
        nilai(4) = DBGridKeluarga.Rows(baris).Cells(3).Value
        fields(5) = "urut"
        nilai(5) = baris


        add_datakeluarga = query_tambah_data(table_name, fields, nilai)
    End Function
    Private Function add_datapendidikan(ByVal baris As Integer) As String
        Dim fields() As String
        Dim nilai() As String
        Dim table_name As String
        ReDim fields(6)
        ReDim nilai(6)
        table_name = "ms_karyawan_pendidikan"
        fields(0) = "nik"
        nilai(0) = txtNIK.Text
        fields(1) = "nama_pendidikan"
        nilai(1) = DBGridPendidikan.Rows(baris).Cells(0).Value
        fields(2) = "jenjang_pendidikan"
        nilai(2) = DBGridPendidikan.Rows(baris).Cells(1).Value
        fields(3) = "tahun_awal_pendidikan"
        nilai(3) = DBGridPendidikan.Rows(baris).Cells(2).Value
        fields(4) = "tahun_selesai_pendidikan"
        nilai(4) = DBGridPendidikan.Rows(baris).Cells(3).Value
        fields(5) = "urut"
        nilai(5) = baris

        add_datapendidikan = query_tambah_data(table_name, fields, nilai)
    End Function
    Private Sub reset_form()

        txtNIK.Text = ""
        txtNamaDepan.Text = ""
        txtNamaBelakang.Text = ""
        txtNamaPanggilan.Text = ""
        txtAlamat.Text = ""
        txtRT.Text = ""
        txtRW.Text = ""
        txtKelurahan.Text = ""
        txtKecamatan.Text = ""
        txtKota.Text = ""
        txtKodePos.Text = ""
        txtTelp.Text = ""
        txtHP.Text = ""
        txtEmail.Text = ""
        txtSuku.Text = ""
        chkWNI.Checked = True
        txtNoKTP.Text = ""
        txtNoSim.Text = ""
        txtNoPassport.Text = ""
        txtTempatLahir.Text = ""
        dtTanggalLahir.Value = Now
        cmbAgama.SelectedIndex = 0
        cmbStatusNikah.SelectedIndex = 0
        cmbBank.Text = ""
        txtNoRekening.Text = ""
        txtAtasNama.Text = ""
        txtNPWP.Text = ""
        txtJamsostek.Text = ""
        On Error Resume Next
        txtNIK.Focus()
    End Sub
    Private Sub cari_data()
        Dim conn As New SqlConnection
        Dim cmd As New SqlCommand
        Dim dr As SqlDataReader
        conn.ConnectionString = strcon
        conn.Open()


        cmd = New SqlCommand("select * from ms_karyawan where nik='" & txtNIK.Text & "'", conn)
        dr = cmd.ExecuteReader

        If dr.Read Then
            txtNIK.Text = dr.Item("nik")
            txtNamaDepan.Text = dr.Item("nama_depan")
            txtNamaBelakang.Text = dr.Item("nama_belakang")
            txtNamaPanggilan.Text = dr.Item("nama_panggilan")
            txtAlamat.Text = dr.Item("alamat")
            txtRT.Text = dr.Item("rt")
            txtRW.Text = dr.Item("rw")
            txtKelurahan.Text = dr.Item("kelurahan")
            txtKecamatan.Text = dr.Item("kecamatan")
            txtKota.Text = dr.Item("kota")
            txtKodePos.Text = dr.Item("kodepos")
            txtTelp.Text = dr.Item("telp")
            txtHP.Text = dr.Item("hp")
            txtEmail.Text = dr.Item("email")
            txtSuku.Text = dr.Item("suku")
            If dr.Item("wni") Then chkWNI.Checked = True Else chkWNI.Checked = False
            txtNoKTP.Text = dr.Item("noktp")
            txtNoSim.Text = dr.Item("nosim")
            txtNoPassport.Text = dr.Item("nopasport")
            txtTempatLahir.Text = dr.Item("tempat_lahir")
            dtTanggalLahir.Value = dr.Item("tanggal_lahir")
            cmbAgama.Text = dr.Item("agama")
            cmbStatusNikah.Text = dr.Item("status_pernikahan")
            cmbBank.Text = dr.Item("bank")
            txtNoRekening.Text = dr.Item("no_rekening")
            txtAtasNama.Text = dr.Item("nama_rekening")
            txtNPWP.Text = dr.Item("npwp")
            txtJamsostek.Text = dr.Item("jamsostek")
        Else
            GoTo close
        End If
        dr.Close()
        DBGridKeluarga.Rows.Clear()
        DBGridPendidikan.Rows.Clear()
        cmd = New SqlCommand("select * from ms_karyawan_keluarga where nik='" & txtNIK.Text & "' order by urut", conn)
        dr = cmd.ExecuteReader
        While dr.Read
            With DBGridKeluarga
                .Rows.Add()
                .Rows(.Rows.Count - 1).Cells(0).Value = dr.Item("nama_keluarga")
                .Rows(.Rows.Count - 1).Cells(1).Value = dr.Item("hubungan_keluarga")
                .Rows(.Rows.Count - 1).Cells(2).Value = dr.Item("alamat_keluarga")
                .Rows(.Rows.Count - 1).Cells(3).Value = dr.Item("telp_keluarga")
            End With
        End While
        dr.Close()
        cmd = New SqlCommand("select * from ms_karyawan_pendidikan where nik='" & txtNIK.Text & "' order by urut", conn)
        dr = cmd.ExecuteReader
        While dr.Read
            With DBGridPendidikan
                .Rows.Add()
                .Rows(.Rows.Count - 1).Cells(0).Value = dr.Item("nama_pendidikan")
                .Rows(.Rows.Count - 1).Cells(1).Value = dr.Item("jenjang_pendidikan")
                .Rows(.Rows.Count - 1).Cells(2).Value = dr.Item("tahun_awal_pendidikan")
                .Rows(.Rows.Count - 1).Cells(3).Value = dr.Item("tahun_selesai_pendidikan")
            End With
        End While
        dr.Close()
        cmd = New SqlCommand("select * from ms_karyawan_tipe where nik='" & txtNIK.Text & "'", conn)
        dr = cmd.ExecuteReader

        If dr.Read Then
            cmbTipe.Text = dr.Item("tipe")
            cmbArea.Text = dr.Item("area")
            cmbDivisi.Text = dr.Item("divisi")
            cmbWorkGroup.Text = dr.Item("groupkerja")
            cmbGroupGaji.Text = dr.Item("kode_groupgaji")
        End If
        dr.Close()
close:

        conn.Close()
    End Sub

    Private Sub txtNIK_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtNIK.LostFocus
        cari_data()
    End Sub

    Private Sub btnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Dim f As New frmSearch
        f.setCol(0)
        f.setTableName("ms_karyawan")
        f.setColumns("nik,nama_depan,nama_belakang,nama_panggilan,alamat")

        If f.ShowDialog = System.Windows.Forms.DialogResult.OK Then
            txtNIK.Text = f.value
            cari_data()
        End If
        f.Dispose()
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrev.Click
        On Error GoTo err
        Dim kode As String
        Dim conn As New SqlConnection
        Dim cmd As New SqlCommand
        Dim dr As SqlDataReader
        conn.ConnectionString = strcon
        conn.Open()
        kode = txtNIK.Text

        cmd = New SqlCommand("select top 1 [nik] from ms_karyawan where [nik]<'" & kode & "' order by [nik] desc", conn)
        dr = cmd.ExecuteReader
        If dr.Read Then
            txtNIK.Text = dr.Item(0)
            GoTo search
        Else
            reset_form()
        End If
        dr.Close()
        conn.Close()
        Exit Sub
search:
        dr.Close()
        conn.Close()

        cari_data()
        Exit Sub
err:
        MsgBox(Err.Description)
        conn.Close()

    End Sub

    Private Sub btnNext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNext.Click
        On Error GoTo err
        Dim kode As String
        Dim conn As New SqlConnection
        Dim cmd As New SqlCommand
        Dim dr As SqlDataReader
        conn.ConnectionString = strcon
        conn.Open()
        kode = txtNIK.Text

        cmd = New SqlCommand("select top 1 [nik] from ms_karyawan where [nik]>'" & kode & "' order by [nik] asc", conn)
        dr = cmd.ExecuteReader
        If dr.Read Then
            txtNIK.Text = dr.Item(0)
            GoTo search
        Else
            reset_form()
        End If
        dr.Close()
        conn.Close()
        Exit Sub
search:
        dr.Close()
        conn.Close()

        cari_data()
        Exit Sub
err:
        MsgBox(Err.Description)
        conn.Close()
    End Sub

    Private Sub btnGroupGaji_Click(sender As System.Object, e As System.EventArgs) Handles btnGroupGaji.Click

    End Sub

    Private Sub btnGroupGaji_MouseDown(sender As Object, e As System.Windows.Forms.MouseEventArgs) Handles btnGroupGaji.MouseDown
        Dim frm As New frmMasterGroupGaji
        frm.txtKode.Text = cmbGroupGaji.Text
        frm.reload()
        frm.txtKode.Enabled = False
        frm.btnNext.Visible = False
        frm.btnPrev.Visible = False
        frm.btnSearch.Visible = False
        frm.ShowDialog()
    End Sub
End Class