﻿Imports System.Data.Sql
Imports System.Data.SqlClient

Module modProcedure
    Public Sub NumericOnly(ByVal e As KeyPressEventArgs)
        If IsNumeric(e.KeyChar) Or e.KeyChar = "," Or e.KeyChar = "." Or e.KeyChar = ControlChars.Back Then
            e.Handled = False
        Else
            e.Handled = True
        End If
    End Sub
    Public Function getfields(ByVal tablename As String, columns As String, conditions As String) As DataSet
        Dim cmd As SqlCommand
        Dim conn As New SqlConnection
        Dim str As String = ""

        cmd = New SqlCommand("select " & columns & " from " & tablename & " " & conditions & "", conn)
        Dim dataadaptor As New SqlDataAdapter(cmd)
        Dim dsDataSet As New DataSet
        Try
            conn.ConnectionString = strcon
            conn.Open()
            dataadaptor.Fill(dsDataSet, tablename)
        Catch ex As Exception
            MsgBox("Failed to Fill " + tablename + ": " + ex.ToString)
        Finally
            conn.Close()
        End Try
        Return dsDataSet


    End Function
    Public Sub loadComboBoxAll(ByVal cb As ComboBox, ByVal columns As String, ByVal tables As String, ByVal where As String, ByVal displayMember As String, ByVal valueMember As String)
        Dim conn2 As New SqlConnection(strcon)
        Try
            conn2.ConnectionString = strcon
            conn2.Open()

            Dim da As New SqlDataAdapter()
            Dim dt As New DataTable
            cmd = New SqlCommand("select " & columns & " from " & tables & " " & where & "", conn2)
            da.SelectCommand = cmd
            da.Fill(dt)
            Dim emptyrow As DataRow = dt.NewRow
            emptyrow(0) = ""

            dt.Rows.InsertAt(emptyrow, 0)
            cb.DataSource = dt
            cb.DisplayMember = displayMember
            cb.ValueMember = valueMember
            conn2.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message.ToString)
        End Try

    End Sub
    Public Sub printout(ByVal formula As String, ByVal reportname As String, Optional ByVal printername As String = "", Optional ByVal papersize As String = "")
        Dim f As New frmPrintOut
        f.printername = printername
        f.papersize = papersize
        f.filename = reportname
        f.formula = formula
        f.Show()

    End Sub
End Module
