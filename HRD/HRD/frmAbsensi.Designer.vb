﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAbsensi
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.txtCommand = New System.Windows.Forms.TextBox()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.lblStatus = New System.Windows.Forms.Label()
        Me.lblNamaBelakang = New System.Windows.Forms.Label()
        Me.lblNamaDepan = New System.Windows.Forms.Label()
        Me.lblTanggal = New System.Windows.Forms.Label()
        Me.lblJam = New System.Windows.Forms.Label()
        Me.btnDatang = New System.Windows.Forms.Button()
        Me.btnKeluar = New System.Windows.Forms.Button()
        Me.btnMasuk = New System.Windows.Forms.Button()
        Me.btnPulang = New System.Windows.Forms.Button()
        Me.lblJenisAbsen = New System.Windows.Forms.Label()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'txtCommand
        '
        Me.txtCommand.Font = New System.Drawing.Font("Arial", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCommand.Location = New System.Drawing.Point(12, 69)
        Me.txtCommand.Name = "txtCommand"
        Me.txtCommand.Size = New System.Drawing.Size(300, 32)
        Me.txtCommand.TabIndex = 0
        '
        'Timer1
        '
        Me.Timer1.Enabled = True
        Me.Timer1.Interval = 1000
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.lblStatus)
        Me.GroupBox1.Controls.Add(Me.lblNamaBelakang)
        Me.GroupBox1.Controls.Add(Me.lblNamaDepan)
        Me.GroupBox1.Location = New System.Drawing.Point(93, 177)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(544, 224)
        Me.GroupBox1.TabIndex = 1
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "GroupBox1"
        '
        'lblStatus
        '
        Me.lblStatus.AutoSize = True
        Me.lblStatus.Font = New System.Drawing.Font("Arial", 26.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStatus.ForeColor = System.Drawing.Color.Red
        Me.lblStatus.Location = New System.Drawing.Point(203, 151)
        Me.lblStatus.Name = "lblStatus"
        Me.lblStatus.Size = New System.Drawing.Size(117, 40)
        Me.lblStatus.TabIndex = 10
        Me.lblStatus.Text = "Label1"
        '
        'lblNamaBelakang
        '
        Me.lblNamaBelakang.AutoSize = True
        Me.lblNamaBelakang.Font = New System.Drawing.Font("Arial", 26.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNamaBelakang.Location = New System.Drawing.Point(19, 82)
        Me.lblNamaBelakang.Name = "lblNamaBelakang"
        Me.lblNamaBelakang.Size = New System.Drawing.Size(36, 40)
        Me.lblNamaBelakang.TabIndex = 9
        Me.lblNamaBelakang.Text = "n"
        '
        'lblNamaDepan
        '
        Me.lblNamaDepan.AutoSize = True
        Me.lblNamaDepan.Font = New System.Drawing.Font("Arial", 26.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNamaDepan.Location = New System.Drawing.Point(19, 29)
        Me.lblNamaDepan.Name = "lblNamaDepan"
        Me.lblNamaDepan.Size = New System.Drawing.Size(36, 40)
        Me.lblNamaDepan.TabIndex = 8
        Me.lblNamaDepan.Text = "n"
        '
        'lblTanggal
        '
        Me.lblTanggal.AutoSize = True
        Me.lblTanggal.Font = New System.Drawing.Font("Arial", 26.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTanggal.Location = New System.Drawing.Point(557, 35)
        Me.lblTanggal.Name = "lblTanggal"
        Me.lblTanggal.Size = New System.Drawing.Size(117, 40)
        Me.lblTanggal.TabIndex = 2
        Me.lblTanggal.Text = "Label1"
        '
        'lblJam
        '
        Me.lblJam.AutoSize = True
        Me.lblJam.Font = New System.Drawing.Font("Arial", 26.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblJam.Location = New System.Drawing.Point(557, 94)
        Me.lblJam.Name = "lblJam"
        Me.lblJam.Size = New System.Drawing.Size(117, 40)
        Me.lblJam.TabIndex = 3
        Me.lblJam.Text = "Label1"
        '
        'btnDatang
        '
        Me.btnDatang.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDatang.Location = New System.Drawing.Point(12, 12)
        Me.btnDatang.Name = "btnDatang"
        Me.btnDatang.Size = New System.Drawing.Size(75, 38)
        Me.btnDatang.TabIndex = 1
        Me.btnDatang.Text = "Datang"
        Me.btnDatang.UseVisualStyleBackColor = True
        Me.btnDatang.Visible = False
        '
        'btnKeluar
        '
        Me.btnKeluar.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnKeluar.Location = New System.Drawing.Point(93, 12)
        Me.btnKeluar.Name = "btnKeluar"
        Me.btnKeluar.Size = New System.Drawing.Size(75, 38)
        Me.btnKeluar.TabIndex = 2
        Me.btnKeluar.Text = "Keluar"
        Me.btnKeluar.UseVisualStyleBackColor = True
        Me.btnKeluar.Visible = False
        '
        'btnMasuk
        '
        Me.btnMasuk.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnMasuk.Location = New System.Drawing.Point(174, 12)
        Me.btnMasuk.Name = "btnMasuk"
        Me.btnMasuk.Size = New System.Drawing.Size(75, 38)
        Me.btnMasuk.TabIndex = 3
        Me.btnMasuk.Text = "Masuk"
        Me.btnMasuk.UseVisualStyleBackColor = True
        Me.btnMasuk.Visible = False
        '
        'btnPulang
        '
        Me.btnPulang.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPulang.Location = New System.Drawing.Point(255, 12)
        Me.btnPulang.Name = "btnPulang"
        Me.btnPulang.Size = New System.Drawing.Size(75, 38)
        Me.btnPulang.TabIndex = 4
        Me.btnPulang.Text = "Pulang"
        Me.btnPulang.UseVisualStyleBackColor = True
        Me.btnPulang.Visible = False
        '
        'lblJenisAbsen
        '
        Me.lblJenisAbsen.AutoSize = True
        Me.lblJenisAbsen.Font = New System.Drawing.Font("Arial", 26.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblJenisAbsen.Location = New System.Drawing.Point(12, 119)
        Me.lblJenisAbsen.Name = "lblJenisAbsen"
        Me.lblJenisAbsen.Size = New System.Drawing.Size(117, 40)
        Me.lblJenisAbsen.TabIndex = 7
        Me.lblJenisAbsen.Text = "Label1"
        '
        'frmAbsensi
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(864, 435)
        Me.Controls.Add(Me.lblJenisAbsen)
        Me.Controls.Add(Me.btnPulang)
        Me.Controls.Add(Me.btnMasuk)
        Me.Controls.Add(Me.btnKeluar)
        Me.Controls.Add(Me.btnDatang)
        Me.Controls.Add(Me.lblJam)
        Me.Controls.Add(Me.lblTanggal)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.txtCommand)
        Me.Name = "frmAbsensi"
        Me.Text = "frmAbsensi"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents txtCommand As System.Windows.Forms.TextBox
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents lblTanggal As System.Windows.Forms.Label
    Friend WithEvents lblJam As System.Windows.Forms.Label
    Friend WithEvents btnDatang As System.Windows.Forms.Button
    Friend WithEvents btnKeluar As System.Windows.Forms.Button
    Friend WithEvents btnMasuk As System.Windows.Forms.Button
    Friend WithEvents btnPulang As System.Windows.Forms.Button
    Friend WithEvents lblJenisAbsen As System.Windows.Forms.Label
    Friend WithEvents lblNamaDepan As System.Windows.Forms.Label
    Friend WithEvents lblNamaBelakang As System.Windows.Forms.Label
    Friend WithEvents lblStatus As System.Windows.Forms.Label
End Class
