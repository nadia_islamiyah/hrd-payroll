﻿Imports System.Data.Sql
Imports System.Data.SqlClient
Imports System.Data.OleDb
Imports System.IO
Imports System.Text

Public Class frmImportAtt
    Dim da As SqlCommand
    Dim dr As SqlDataReader
    Private commandTimeOut As Integer = 0
    Dim transaction As SqlTransaction
    Dim lineCount As Integer
    Dim awal, akhir As String
    Private Sub btnSave_Click(sender As System.Object, e As System.EventArgs) Handles btnSave.Click
        Dim sqlcon As New SqlConnection
        Dim olecon As New OleDbConnection
        Dim oledr, oledr1 As OleDbDataReader
        Dim olecmd, olecmd1 As OleDbCommand
        Dim insert As String = ""
        sqlcon.ConnectionString = strcon
        olecon.ConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & txtFile.Text & ";Persist Security Info=False;"
        olecon.Open()

        olecmd1 = New OleDbCommand("select count(userid)  from userinfo", olecon)
        oledr1 = olecmd1.ExecuteReader
        If oledr1.Read Then
            ProgressBar1.Maximum = oledr1.Item(0)
        End If
        ProgressBar1.Value = 0
        ProgressBar1.Visible = True

        olecmd = New OleDbCommand("select * from userinfo", olecon)
        oledr = olecmd.ExecuteReader
        While oledr.Read
            ProgressBar1.Value += 1
            insert &= "if not exists (select * from ms_karyawan where nik='" & oledr.Item("userid") & "') insert into ms_karyawan (nik,nama_depan) values ('" & oledr.Item("userid") & "','" & oledr.Item("name") & "');"
        End While
        ProgressBar1.Visible = False
        Dim sqlcmd As New SqlCommand(insert, sqlcon)
        sqlcon.Open()
        sqlcmd.ExecuteNonQuery()
        sqlcon.Close()
        olecon.Close()
        MsgBox("selesai")
    End Sub

    Private Sub Button2_Click(sender As System.Object, e As System.EventArgs) Handles Button2.Click
        Dim sqlcon As New SqlConnection
        Dim olecon As New OleDbConnection
        Dim oledr, oledr1 As OleDbDataReader
        Dim olecmd, olecmd1 As OleDbCommand
        Dim insert As String = ""
        Dim sqlcmd As SqlCommand
        Dim counter As Integer = 0
        sqlcon.ConnectionString = strcon
        olecon.ConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & txtFile.Text & ";Persist Security Info=False;"
        olecon.Open()
        'sqlcon.CommandTimeout = 0
        sqlcon.Open()
        olecmd1 = New OleDbCommand("select count(userid) as jml from checkinout  where checktime between #" & dtAwal.Value & "# and #" & dtAkhir.Value & "#", olecon)
        oledr1 = olecmd1.ExecuteReader
        If oledr1.Read Then
            ProgressBar1.Maximum = oledr1.Item(0)
        End If
        ProgressBar1.Value = 0
        ProgressBar1.Visible = True

        olecmd = New OleDbCommand("select * from checkinout where checktime between #" & dtAwal.Value & "# and #" & dtAkhir.Value & "#", olecon)
        oledr = olecmd.ExecuteReader
        While oledr.Read
            ProgressBar1.Value += 1
            insert &= "if not exists (select * from t_clockin where nik='" & oledr.Item("userid") & "' and replace(convert(varchar(19),waktu,120),'-','')='" & Format(oledr.Item("checktime"), "yyyyMMdd HH:mm:ss") & "') insert into t_clockin (nik,waktu,rekap,manual,userid) values ('" & oledr.Item("userid") & "','" & Format(oledr.Item("checktime"), "yyyy/MM/dd HH:mm:ss") & "',0,null,null);"
            counter += 1
            If counter = 100 Then
                sqlcmd = New SqlCommand(insert, sqlcon)
                sqlcmd.CommandTimeout = 0
                sqlcmd.ExecuteNonQuery()
                counter = 0
                insert = ""
            End If
        End While
        If insert <> "" Then
            sqlcmd = New SqlCommand(insert, sqlcon)
            sqlcmd.CommandTimeout = 0
            sqlcmd.ExecuteNonQuery()
        End If
        sqlcmd = New SqlCommand("exec sp_AutoRekapAbsensi '" & dtAwal.Value & "','" & dtAkhir.Value & "'", sqlcon)
        sqlcmd.CommandTimeout = 0
        sqlcmd.ExecuteNonQuery()
        sqlcmd = New SqlCommand("exec sp_autoshift1 '" & dtAwal.Value & "','" & dtAkhir.Value & "'", sqlcon)
        sqlcmd.CommandTimeout = 0
        sqlcmd.ExecuteNonQuery()
        ProgressBar1.Visible = False
        sqlcon.Close()
        olecon.Close()
        MsgBox("selesai")
    End Sub

    Private Sub Button1_Click(sender As System.Object, e As System.EventArgs) Handles Button1.Click
        Dim xm As New Ini(GetAppPath() & "\setting.ini")
        xm.WriteString("Import", "mdb", txtFile.Text)
    End Sub

    Private Sub frmImportAtt_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Dim xm As New Ini(GetAppPath() & "\setting.ini")
        txtFile.Text = xm.GetString("Import", "mdb", "")
        dtAwal.Value = Now
        dtAkhir.Value = Now
        TextBox1.Text = ""
    End Sub

    Private Sub btnSearchBarang_Click(sender As System.Object, e As System.EventArgs) Handles btnSearchBarang.Click
        Dim Ex As OpenFileDialog = New OpenFileDialog()
        'Ex.InitialDirectory = "c:\"
        Ex.Filter = "(*.mdb)|*.mdb"
        Ex.FilterIndex = 2
        Ex.RestoreDirectory = True
        If (Ex.ShowDialog() = Windows.Forms.DialogResult.OK) Then
            txtFile.Text = Ex.FileName
        End If
    End Sub

    Private Sub Button3_Click(sender As System.Object, e As System.EventArgs) Handles Button3.Click
        Dim Ex As OpenFileDialog = New OpenFileDialog()
        Ex.Filter = "(*.dat)|*.dat"
        Ex.FilterIndex = 2
        Ex.RestoreDirectory = True
        If (Ex.ShowDialog() = Windows.Forms.DialogResult.OK) Then
            TextBox1.Text = Ex.FileName
        End If
    End Sub
   
    Private Function import_dat() As Boolean
        import_dat = False
        Dim cmd As SqlCommand
        Dim conn As New SqlConnection
        conn.ConnectionString = strcon
        conn.Open()
        transaction = conn.BeginTransaction(IsolationLevel.ReadCommitted)
        Try
            cmd = New SqlCommand("delete from tmp_absensi", conn, transaction)
            cmd.ExecuteNonQuery()
            Dim fname As String = TextBox1.Text
            Dim colsexpected As Integer = 6
            'Progress bar
            Dim FileData As New StreamReader(fname, Encoding.Default)
            lineCount = 0
            Do Until FileData.ReadLine = Nothing
                lineCount += 1
            Loop
            Label4.Visible = True
            'save data
            Dim FileReader As New StreamReader(fname, Encoding.Default)
            Dim sLine As String = ""
            Dim sqldetail As String = Nothing
            Do
                sLine = FileReader.ReadLine
                If sLine Is Nothing Then Exit Do
                Dim words() As String = sLine.Split(vbTab)
                Dim nik As String = ""
                Dim tgl As DateTime
                For x As Integer = 0 To 1
                    If x = 0 Then nik = Trim(words(x))
                    If x = 1 Then tgl = words(x)
                Next
                sqldetail += add_data(nik, tgl) + ";"
            Loop
            FileReader.Close()
            If sqldetail <> Nothing Then
                cmd = New SqlCommand(sqldetail, conn, transaction)
                cmd.CommandTimeout = 0
                cmd.ExecuteNonQuery()
            End If
           
            transaction.Commit()
            conn.Close()

        Catch ex As Exception
            MessageBox.Show(ex.Message.ToString)
            If reader IsNot Nothing Then reader.Close()
            transaction.Rollback()
            If conn.State Then conn.Close()
        End Try
    End Function
    Private Function add_data(ByVal nik1 As String, ByVal tgl1 As DateTime) As String
        Dim fields() As String
        Dim nilai() As String
        Dim table_name As String
        ReDim fields(2)
        ReDim nilai(2)

        table_name = "tmp_absensi"
        fields(0) = "nik"
        fields(1) = "waktu"
        nilai(0) = nik1
        nilai(1) = tgl1.ToString("yyyy/MM/dd HH:mm:ss")

        add_data = query_tambah_data(table_name, fields, nilai)
    End Function
    Private Sub Button4_Click(sender As System.Object, e As System.EventArgs) Handles Button4.Click
        import_dat()
        generate()
        MsgBox("Import Selesai")
    End Sub

    Private Sub generate()
        Dim conn As New SqlConnection
        Dim cmd As New SqlCommand
        Dim dr As SqlDataReader
        conn.ConnectionString = strcon
        conn.Open()
        'insert clock in
        cmd = New SqlCommand("select MIN(waktu) awal,MAX(waktu) akhir from tmp_absensi", conn)
        dr = cmd.ExecuteReader
        If dr.Read Then
            awal = Format(dr("awal"), "yyyyMMdd")
            akhir = Format(dr("akhir"), "yyyyMMdd")
        End If
        dr.Close()
        cmd = New SqlCommand("insert into t_clockin  select *,0,null,null from tmp_absensi where nik+CONVERT(varchar(23),waktu,121) not in " & _
                "(select nik+CONVERT(varchar(23),waktu,121) from t_clockin where CONVERT(varchar(8),waktu,112) between '" & awal & "' and  '" & akhir & "' ) ", conn, transaction)
        cmd.CommandTimeout = 0
        cmd.ExecuteNonQuery()
        conn.Close()
    End Sub
    Private Sub generaterekap()
        Dim conn As New SqlConnection
        Dim cmd As New SqlCommand
        Dim dr As SqlDataReader
        conn.ConnectionString = strcon
        conn.Open()
       'insert to rekapabsesni
        cmd = New SqlCommand("exec sp_autoRekapAbsensi '" & awal & "','" & akhir & "'", conn, transaction)
        cmd.CommandTimeout = 0
        cmd.ExecuteNonQuery()
last:   conn.Close()

        MsgBox("Generate Rekap Selesai")
    End Sub
    Private Sub generateShift()
        Dim conn As New SqlConnection
        Dim cmd As New SqlCommand
        Dim dr As SqlDataReader
        conn.ConnectionString = strcon
        conn.Open()
        'generate shift
        cmd = New SqlCommand("exec sp_autoshift1 '" & awal & "','" & akhir & "'", conn, transaction)
        cmd.CommandTimeout = 0
        cmd.ExecuteNonQuery()
last:   conn.Close()

        MsgBox("Generate Shift Selesai")
    End Sub
    Private Sub GroupBox2_Enter(sender As System.Object, e As System.EventArgs) Handles GroupBox2.Enter

    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        generaterekap()
    End Sub

    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        generateShift()
    End Sub
End Class