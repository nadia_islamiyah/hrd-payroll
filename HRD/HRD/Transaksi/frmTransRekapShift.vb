﻿Imports System.Data.SqlClient
Imports System.Data.Sql
Imports System.IO
Public Class frmTransRekapShift

    Private Sub dtTanggalAkhir_ValueChanged(sender As System.Object, e As System.EventArgs) Handles dtTanggalAkhir.ValueChanged
        DBGridShift.Columns.Clear()
        DBGridShift.Rows.Clear()
        load_template()
    End Sub
    Private Sub Label1_Click(sender As System.Object, e As System.EventArgs) Handles Label1.Click

    End Sub
    Private Sub dtTanggalAwal_ValueChanged(sender As System.Object, e As System.EventArgs) Handles dtTanggalAwal.ValueChanged
        DBGridShift.Columns.Clear()
        DBGridShift.Rows.Clear()
        load_template()
    End Sub

    Private Sub txtKeyword_KeyDown(sender As Object, e As System.Windows.Forms.KeyEventArgs) Handles txtKeyword.KeyDown
        If e.KeyCode = Keys.Enter Then
            DBGridShift.Columns.Clear()
            DBGridShift.Rows.Clear()
            load_template()
        End If
    End Sub

    Private Sub frmTransRekapShift_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        DBGridShift.Columns.Clear()
        DBGridShift.Rows.Clear()
        load_template()
    End Sub
    Private Sub load_template()
        Dim conn As New SqlConnection
        Dim cmd As New SqlCommand
        Dim cmd2 As New SqlCommand
        Dim dr As SqlDataReader
        Dim dr2 As SqlDataReader
        Dim key, key2 As String
        conn.ConnectionString = strcon
        conn.Open()
        DBGridShift.Columns.Add("nik", "NIK")
        DBGridShift.Columns.Add("nama_depan", "Nama Depan")

        If txtKeyword.Text = "" Then
            key = ""
            key2 = ""
        Else
            key = "and ( k.nama_depan like'%" & txtKeyword.Text & "%' or k.nik like'%" & txtKeyword.Text & "%' )"
            key2 = "and ( nama_depan like'%" & txtKeyword.Text & "%' or nik like'%" & txtKeyword.Text & "%' )"
        End If
        cmd = New SqlCommand("select nik, nama_depan from ms_karyawan where not nik is null and not nama_depan is null  " & key2 & " order by nama_depan, nik", conn)
        dr = cmd.ExecuteReader
        Dim i As Integer = 0
        While dr.Read
            DBGridShift.Rows.Add()
            DBGridShift.Rows(i).Cells("nik").Value = dr.Item("nik")
            DBGridShift.Rows(i).Cells("nama_depan").Value = dr.Item("nama_depan")
            i += 1
        End While
        dr.Close()

        cmd = New SqlCommand("select*from ms_jamkerja", conn)
        dr = cmd.ExecuteReader
        While dr.Read
            DBGridShift.Columns.Add("Shift_" & dr.Item("nama_shift"), "Shift " & dr.Item("nama_shift"))
            cmd2 = New SqlCommand("select k.nik, k.nama_depan, '" & dr.Item("nama_shift") & "' as shift,isnull(count(j.nama_shift),0) as jumlah " & _
                    "from t_jadwalkerja j right join ms_karyawan k on j.NIK = k.nik left join ms_jamkerja m on m.nama_shift = j.nama_shift " & _
                    "where not k.nik is null and not k.nama_depan is null and (m.nama_shift ='" & dr.Item("nama_shift") & "') " & _
                    "and convert(varchar(8),j.tanggal,112) between '" & Format(dtTanggalAwal.Value, "yyyyMMdd") & "' and '" & Format(dtTanggalAkhir.Value, "yyyyMMdd") & "' " & key & " " & _
                    "group by k.nik, k.nama_depan , m.nama_shift " & _
                    "union all " & _
                    "select distinct nik, nama_depan, '" & dr.Item("nama_shift") & "' as shift ,0 as jumlah from ms_karyawan where nik not in ( " & _
                    "select k.nik from t_jadwalkerja j right join ms_karyawan k on j.NIK = k.nik left join ms_jamkerja m on m.nama_shift = j.nama_shift " & _
                    "where not k.nik is null and not k.nama_depan is null and (m.nama_shift ='" & dr.Item("nama_shift") & "') " & _
                    "and convert(varchar(8),j.tanggal,112) between '" & Format(dtTanggalAwal.Value, "yyyyMMdd") & "' and '" & Format(dtTanggalAkhir.Value, "yyyyMMdd") & "' group by k.nik, k.nama_depan , m.nama_shift) " & _
                    " " & key2 & "  order by nama_depan, nik", conn)
            dr2 = cmd2.ExecuteReader
            i = 0
            While dr2.Read
                If DBGridShift.Rows(i).Cells(0).Value = dr2.Item("NIK") Then DBGridShift.Rows(i).Cells("Shift_" & dr.Item("nama_shift")).Value = dr2.Item("jumlah")
                i += 1
            End While
        End While
        dr.Close()
        conn.Close()

    End Sub
End Class