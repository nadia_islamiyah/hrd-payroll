﻿Imports System.Data.SqlClient
Imports System.Data.Sql
Imports System.IO
Public Class frmTransJadwalKerja
    Dim da As SqlCommand
    Dim dr As SqlDataReader

    Private Sub frmTransJadwalKerja_KeyDown(sender As Object, e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        If e.KeyCode = Keys.Enter Then SendKeys.Send("{tab}")
        If e.KeyCode = Keys.Escape Then Me.Close()
    End Sub
    Private Sub frmTransAbsenHarian_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        dtTanggalAwal.Value = Now
        dtAutoAkhir.Value = Now
        dtAutoAwal.Value = Now
        dtPeriode.Value = Now
        reloadcombo()
        load_karyawan()
        loadComboBoxAll(cmbtipe, "tipe", "var_tipekaryawan", "", "tipe", "tipe")
    End Sub
    Private Sub load_karyawan()
        Dim conn As New SqlConnection
        Dim datatable1 As New DataTable
        Dim tipe As String
        On Error Resume Next
        conn.ConnectionString = strcon
        conn.Open()
        If cmbtipe.Text = "" Then
            tipe = ""
        Else
            tipe = " and t.tipe='" & cmbtipe.Text & "'"
        End If
        da = New SqlCommand("select m.nik,m.nama_depan +' '+ isnull(m.nama_belakang,'') as nama " & _
                                "from ms_karyawan m left join ms_karyawan_tipe t on m.nik=t.nik where aktif='True' " & tipe & "", conn)

        dr = da.ExecuteReader
        DBKaryawan.Rows.Clear()
        While dr.Read
            DBKaryawan.Rows.Add()
            DBKaryawan.Rows(DBKaryawan.Rows.Count - 1).Cells(0).Value = dr.Item(0)
            DBKaryawan.Rows(DBKaryawan.Rows.Count - 1).Cells(1).Value = dr.Item(1)
        End While

    End Sub

    Private Sub dtTanggal_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        'load_karyawan()
    End Sub

    Private Sub DBGrid_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DBGrid.CellContentClick
        If e.ColumnIndex = 6 Then
            Dim f As New frmTransAbsensi
            f.txtNIK.Text = DBGrid.Rows(DBGrid.CurrentRow.Index).Cells(0).Value
            f.dtTanggal.Value = dtTanggalAwal.Value
            f.cari_data()

            f.ShowDialog()
            f.Dispose()
            load_karyawan()
        End If
    End Sub

    Private Sub dtPeriode_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtPeriode.ValueChanged
        Dim tanggalawal As Date
        tanggalawal = CDate(Format(dtPeriode.Value, "MM/01/yyyy"))
        dtTanggalAwal.Value = tanggalawal
        dtTanggalAkhir.Value = DateAdd(DateInterval.Day, -1, DateAdd(DateInterval.Month, 1, tanggalawal))
        setcol()
    End Sub
    Private Sub setcol()

        Dim d As Date
        For i = DBGrid.Columns.Count - 1 To 2 Step -1
            DBGrid.Columns.RemoveAt(i)
        Next
        For i = 0 To DateDiff(DateInterval.Day, dtTanggalAwal.Value, dtTanggalAkhir.Value)
            Dim newcol As New DataGridViewComboBoxColumn
            d = DateAdd(DateInterval.Day, i, dtTanggalAwal.Value)
            newcol.HeaderText = Format(d, "d/M")

            newcol.Name = Format(d, "yyyyMMdd")
            newcol.DisplayStyle = DataGridViewComboBoxDisplayStyle.Nothing

            DBGrid.Columns.Add(newcol)
            DBGrid.Columns(DBGrid.Columns.Count - 1).Width = 50
        Next
    End Sub


    Private Sub DBKaryawan_CellContentClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DBKaryawan.CellContentClick
        DBKaryawan.EndEdit()
        cekvalue(e.RowIndex)
    End Sub

    Private Sub DBKaryawan_CellValueChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DBKaryawan.CellValueChanged
        DBKaryawan.EndEdit()
        cekvalue(e.RowIndex)
    End Sub
    Private Sub cekvalue(ByVal rowindex As Integer)
        If rowindex >= 0 Then
            If DBKaryawan.Rows(rowindex).Cells(2).Value = True Then
                add_karyawan(DBKaryawan.Rows(rowindex).Cells(0).Value, IIf(IsDBNull(DBKaryawan.Rows(rowindex).Cells(1).Value), "", DBKaryawan.Rows(rowindex).Cells(1).Value))
            Else
                remove_karyawan(DBKaryawan.Rows(rowindex).Cells(0).Value)
            End If
        End If
    End Sub
    Private Sub add_karyawan(ByVal nik As String, ByVal nama As String)
        Dim conn As New SqlConnection
        Dim dr As SqlDataReader
        Dim combolist As String

        Dim list() As String
        On Error Resume Next
        conn.ConnectionString = strcon
        conn.Open()
        da = New SqlCommand("select * from ms_jamkerja order by nama_shift", conn)
        dr = da.ExecuteReader
        combolist = ","

        While dr.Read
            combolist = combolist & dr.Item(0) & ","

        End While
        combolist = Mid(combolist, 1, Len(combolist) - 1)
        list = Split(combolist, ",")
        dr.Close()
        For i = 0 To DBGrid.Rows.Count - 1
            If DBGrid.Rows(i).Cells(0).Value = nik Then Exit Sub
        Next

        DBGrid.Rows.Add()
        DBGrid.Rows(DBGrid.Rows.Count - 1).Cells(0).Value = nik
        DBGrid.Rows(DBGrid.Rows.Count - 1).Cells(1).Value = nama
        For j = 2 To DBGrid.Columns.Count - 1
            Dim cmbclm As DataGridViewComboBoxCell
            cmbclm = DBGrid.Rows(DBGrid.Rows.Count - 1).Cells(j)
            cmbclm.Items.Clear()
            cmbclm.Items.AddRange(list)

        Next
        da = New SqlCommand("select convert(varchar(8),tanggal,112),nama_shift from t_jadwalkerja where nik='" & nik & "' ", conn)
        dr = da.ExecuteReader
        While dr.Read
            For j = 2 To DBGrid.Columns.Count - 1
                If DBGrid.Columns(j).Name = dr.Item(0) Then
                    DBGrid.Rows(DBGrid.Rows.Count - 1).Cells(j).Value = dr.Item("nama_shift")
                End If
            Next
        End While
        dr.Close()
    End Sub
    Private Sub remove_karyawan(ByVal nik As String)
        For i = DBGrid.Rows.Count - 1 To 0 Step -1
            If DBGrid.Rows(i).Cells(0).Value = nik Then DBGrid.Rows.RemoveAt(i)
        Next
    End Sub


    Private Sub btnAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAll.Click
        ProgressBar1.Maximum = DBKaryawan.Rows.Count
        ProgressBar1.Value = 0
        ProgressBar1.Visible = True
        For i = 0 To DBKaryawan.Rows.Count - 1
            DBKaryawan.Rows(i).Cells(2).Value = True
            ProgressBar1.Value += 1
        Next
        ProgressBar1.Visible = False
    End Sub

    Private Sub btnNone_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNone.Click
        ProgressBar1.Maximum = DBKaryawan.Rows.Count
        ProgressBar1.Value = 0
        ProgressBar1.Visible = True
        For i = DBKaryawan.Rows.Count - 1 To 0 Step -1
            DBKaryawan.Rows(i).Cells(2).Value = False
            ProgressBar1.Value += 1
        Next
        ProgressBar1.Visible = False
    End Sub
    Private Sub reloadcombo()
        Dim conn As New SqlConnection
        Dim cmd As New SqlCommand
        Dim dr As SqlDataReader
        conn.ConnectionString = strcon
        conn.Open()


        cmd = New SqlCommand("select * from ms_groupkerja order by nama_groupkerja", conn)
        dr = cmd.ExecuteReader
        cmbWorkgroup.Items.Clear()
        While dr.Read
            cmbWorkgroup.Items.Add(dr.Item(0))
        End While
        conn.Close()
    End Sub
    Private Sub getshift()
        Dim conn As New SqlConnection
        Dim cmd As New SqlCommand
        Dim dr As SqlDataReader
        conn.ConnectionString = strcon
        conn.Open()

        cmd = New SqlCommand("select * from ms_groupkerja where nama_groupkerja='" & cmbWorkgroup.Text & "'", conn)
        dr = cmd.ExecuteReader
        lblShift.Text = ""
        If dr.Read Then lblShift.Text = dr.Item(1)
        dr.Close()
        cmd = New SqlCommand("select * from ms_karyawan_tipe where groupkerja='" & cmbWorkgroup.Text & "' order by nik", conn)
        dr = cmd.ExecuteReader
        While dr.Read
            For i = 0 To DBKaryawan.Rows.Count - 1
                If DBKaryawan.Rows(i).Cells(0).Value = dr.Item("nik") Then DBKaryawan.Rows(i).Cells(2).Value = True
            Next
        End While
        dr.Close()

        conn.Close()
        setallshift()
    End Sub
    Private Sub setallshift()
        For i = 0 To DBGrid.Rows.Count - 1
            For j = 2 To DBGrid.Columns.Count - 1
                DBGrid.Rows(i).Cells(j).Value = lblShift.Text
            Next
        Next
    End Sub
    Private Sub cmbWorkgroup_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbWorkgroup.SelectedIndexChanged
        getshift()
    End Sub


    Private Sub btnSimpan_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSimpan.Click
        Dim conn As New SqlConnection
        Dim cmd As New SqlCommand
        Dim i As Integer
        Dim j As Integer
        Dim query As String
        conn.ConnectionString = strcon
        conn.Open()
        ProgressBar1.Visible = True
        ProgressBar1.Maximum = DBGrid.Rows.Count
        ProgressBar1.Value = 0
        cmd.ExecuteNonQuery()
        cmd = New SqlCommand("delete from t_jadwalkerja where year(tanggal)='" & Format(dtPeriode.Value, "yyyy") & "' and month(tanggal)='" & Month(dtPeriode.Value) & "'", conn)
        cmd.ExecuteNonQuery()
        For i = 0 To DBGrid.Rows.Count - 1
            query = ""
            For j = 2 To DBGrid.Columns.Count - 1
                If DBGrid.Rows(i).Cells(j).Value <> "" Then
                    query += ";insert into t_jadwalkerja (tanggal,nik,nama_shift) values ('" & DBGrid.Columns(j).Name & "','" & DBGrid.Rows(i).Cells(0).Value & "','" & DBGrid.Rows(i).Cells(j).Value & "')"
                End If
            Next
            If query <> "" Then
                cmd = New SqlCommand(query, conn)
                cmd.ExecuteNonQuery()
            End If
            ProgressBar1.Value += 1

        Next
       
        conn.Close()
        MsgBox("data sudah tersimpan")

        ProgressBar1.Visible = False
    End Sub


    Private Sub btnSetcol_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSetcol.Click

    End Sub

    Private Sub btnAutoShift_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAutoShift.Click
        Dim conn As New SqlConnection
        Dim cmd As New SqlCommand
        Dim i As Integer
        Dim j As Integer
        Dim tanggal As Date
        conn.ConnectionString = strcon
        conn.Open()
        tanggal = dtAutoAwal.Value
        ProgressBar1.Visible = True
        ProgressBar1.Maximum = DateDiff(DateInterval.Day, tanggal, dtAutoAkhir.Value)
        ProgressBar1.Value = 0

        While tanggal <= dtAutoAkhir.Value

            For j = 2 To DBGrid.Columns.Count - 1
                If DBGrid.Columns(j).Name = Format(tanggal, "yyyyMMdd") Then
                    cmd = New SqlCommand("select * from vw_closestshift where convert(varchar(20),tanggal,112) = '" & DBGrid.Columns(j).Name & "' and ranking=1 and nik is not null", conn)
                    dr = cmd.ExecuteReader()
                    While dr.Read
                        For i = 0 To DBGrid.Rows.Count - 1
                            If dr.Item("nik") = DBGrid.Rows(i).Cells(0).Value Then DBGrid.Rows(i).Cells(j).Value = dr.Item("nama_shift")
                        Next
                    End While
                    dr.Close()
                End If

            Next

            tanggal = DateAdd(DateInterval.Day, 1, tanggal)
            If tanggal < dtAutoAkhir.Value Then ProgressBar1.Value += 1
        End While
        conn.Close()


        ProgressBar1.Visible = False
    End Sub

    Private Sub cmbtipe_SelectionChangeCommitted(sender As Object, e As System.EventArgs) Handles cmbtipe.SelectionChangeCommitted
        load_karyawan()
    End Sub
End Class