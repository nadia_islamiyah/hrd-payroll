﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmTransRekapAbsenNIK
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.DBGrid = New System.Windows.Forms.DataGridView()
        Me.NIK = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Nama = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Tanggal = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Masuk = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Istirahat = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Kembali = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Pulang = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Total = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Telat = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PC = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Lembur = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Absen = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.TanggalMasuk = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TanggalIstirahat = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TanggalKembali = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TanggalPulang = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dtAwal = New System.Windows.Forms.DateTimePicker()
        Me.dtAkhir = New System.Windows.Forms.DateTimePicker()
        Me.txtSearch = New System.Windows.Forms.TextBox()
        CType(Me.DBGrid, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'DBGrid
        '
        Me.DBGrid.AllowUserToAddRows = False
        Me.DBGrid.AllowUserToDeleteRows = False
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DBGrid.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.DBGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DBGrid.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.NIK, Me.Nama, Me.Tanggal, Me.Masuk, Me.Istirahat, Me.Kembali, Me.Pulang, Me.Total, Me.Telat, Me.PC, Me.Lembur, Me.Absen, Me.TanggalMasuk, Me.TanggalIstirahat, Me.TanggalKembali, Me.TanggalPulang})
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DBGrid.DefaultCellStyle = DataGridViewCellStyle2
        Me.DBGrid.Location = New System.Drawing.Point(22, 49)
        Me.DBGrid.Name = "DBGrid"
        Me.DBGrid.Size = New System.Drawing.Size(883, 458)
        Me.DBGrid.TabIndex = 0
        '
        'NIK
        '
        Me.NIK.HeaderText = "NIK"
        Me.NIK.Name = "NIK"
        Me.NIK.ReadOnly = True
        Me.NIK.Width = 90
        '
        'Nama
        '
        Me.Nama.HeaderText = "Nama"
        Me.Nama.Name = "Nama"
        Me.Nama.ReadOnly = True
        Me.Nama.Width = 200
        '
        'Tanggal
        '
        Me.Tanggal.HeaderText = "Tanggal"
        Me.Tanggal.Name = "Tanggal"
        Me.Tanggal.ReadOnly = True
        '
        'Masuk
        '
        Me.Masuk.HeaderText = "Masuk"
        Me.Masuk.Name = "Masuk"
        Me.Masuk.ReadOnly = True
        Me.Masuk.Width = 60
        '
        'Istirahat
        '
        Me.Istirahat.HeaderText = "Istirahat"
        Me.Istirahat.Name = "Istirahat"
        Me.Istirahat.ReadOnly = True
        Me.Istirahat.Width = 60
        '
        'Kembali
        '
        Me.Kembali.HeaderText = "Kembali"
        Me.Kembali.Name = "Kembali"
        Me.Kembali.ReadOnly = True
        Me.Kembali.Width = 65
        '
        'Pulang
        '
        Me.Pulang.HeaderText = "Pulang"
        Me.Pulang.Name = "Pulang"
        Me.Pulang.ReadOnly = True
        Me.Pulang.Width = 60
        '
        'Total
        '
        Me.Total.HeaderText = "Total"
        Me.Total.Name = "Total"
        Me.Total.ReadOnly = True
        Me.Total.Width = 60
        '
        'Telat
        '
        Me.Telat.HeaderText = "Telat"
        Me.Telat.Name = "Telat"
        Me.Telat.ReadOnly = True
        Me.Telat.Width = 50
        '
        'PC
        '
        Me.PC.HeaderText = "PC"
        Me.PC.Name = "PC"
        Me.PC.ReadOnly = True
        Me.PC.Width = 50
        '
        'Lembur
        '
        Me.Lembur.HeaderText = "Lembur"
        Me.Lembur.Name = "Lembur"
        Me.Lembur.ReadOnly = True
        Me.Lembur.Width = 60
        '
        'Absen
        '
        Me.Absen.HeaderText = "Absen"
        Me.Absen.Name = "Absen"
        Me.Absen.ReadOnly = True
        Me.Absen.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Absen.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.Absen.Width = 50
        '
        'TanggalMasuk
        '
        Me.TanggalMasuk.HeaderText = "TanggalMasuk"
        Me.TanggalMasuk.Name = "TanggalMasuk"
        Me.TanggalMasuk.ReadOnly = True
        '
        'TanggalIstirahat
        '
        Me.TanggalIstirahat.HeaderText = "TanggalIstirahat"
        Me.TanggalIstirahat.Name = "TanggalIstirahat"
        Me.TanggalIstirahat.ReadOnly = True
        '
        'TanggalKembali
        '
        Me.TanggalKembali.HeaderText = "TanggalKembali"
        Me.TanggalKembali.Name = "TanggalKembali"
        Me.TanggalKembali.ReadOnly = True
        '
        'TanggalPulang
        '
        Me.TanggalPulang.HeaderText = "TanggalPulang"
        Me.TanggalPulang.Name = "TanggalPulang"
        Me.TanggalPulang.ReadOnly = True
        '
        'dtAwal
        '
        Me.dtAwal.CustomFormat = "dd/MM/yyyy"
        Me.dtAwal.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtAwal.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtAwal.Location = New System.Drawing.Point(22, 12)
        Me.dtAwal.Name = "dtAwal"
        Me.dtAwal.Size = New System.Drawing.Size(128, 23)
        Me.dtAwal.TabIndex = 23
        Me.dtAwal.Value = New Date(2013, 3, 2, 0, 0, 0, 0)
        '
        'dtAkhir
        '
        Me.dtAkhir.CustomFormat = "dd/MM/yyyy"
        Me.dtAkhir.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtAkhir.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtAkhir.Location = New System.Drawing.Point(156, 12)
        Me.dtAkhir.Name = "dtAkhir"
        Me.dtAkhir.Size = New System.Drawing.Size(128, 23)
        Me.dtAkhir.TabIndex = 24
        Me.dtAkhir.Value = New Date(2013, 3, 2, 0, 0, 0, 0)
        '
        'txtSearch
        '
        Me.txtSearch.Location = New System.Drawing.Point(304, 12)
        Me.txtSearch.Name = "txtSearch"
        Me.txtSearch.Size = New System.Drawing.Size(204, 20)
        Me.txtSearch.TabIndex = 25
        '
        'frmTransRekapAbsenNIK
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(949, 519)
        Me.Controls.Add(Me.txtSearch)
        Me.Controls.Add(Me.dtAkhir)
        Me.Controls.Add(Me.dtAwal)
        Me.Controls.Add(Me.DBGrid)
        Me.KeyPreview = True
        Me.Name = "frmTransRekapAbsenNIK"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Rekap Absensi Harian"
        CType(Me.DBGrid, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents DBGrid As System.Windows.Forms.DataGridView
    Friend WithEvents dtAwal As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtAkhir As System.Windows.Forms.DateTimePicker
    Friend WithEvents txtSearch As System.Windows.Forms.TextBox
    Friend WithEvents NIK As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Nama As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Tanggal As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Masuk As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Istirahat As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Kembali As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Pulang As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Total As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Telat As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PC As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Lembur As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Absen As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents TanggalMasuk As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TanggalIstirahat As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TanggalKembali As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TanggalPulang As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
