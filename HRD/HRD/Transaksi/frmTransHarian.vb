﻿Imports System.Data.OleDb
Imports System.Data.SqlClient
Public Class frmTransHarian
    Dim transaction As SqlTransaction
    Private Sub frmTransBorongan_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        If e.KeyCode = Keys.Enter Then SendKeys.Send("{TAB}")
        If e.KeyCode = Keys.F2 Then btnSimpan.PerformClick()
        If e.KeyCode = Keys.F5 Then btnSearchId.PerformClick()
        If e.Control And e.KeyCode = Keys.R Then btnReset.PerformClick()
        If e.KeyCode = Keys.Escape Then Me.Close()
    End Sub

    Private Sub frmTransBorongan_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim dtcol, dtcol1 As New CalendarColumn
        dtcol.Name = "Tanggal_Mulai"
        dtcol.HeaderText = "Tanggal Mulai"
        dgv1.Columns.Add(dtcol)
        dgv1.Columns("Tanggal_Mulai").DefaultCellStyle.Format = "dd/MM/yyyy"
        dgv1.Columns("Tanggal_Mulai").SortMode = DataGridViewColumnSortMode.Automatic

        dtcol1.Name = "Tanggal_Berakhir"
        dtcol1.HeaderText = "Tanggal Berakhir"
        dgv1.Columns.Add(dtcol1)
        dgv1.Columns("Tanggal_Berakhir").DefaultCellStyle.Format = "dd/MM/yyyy"
        dgv1.Columns("Tanggal_Berakhir").SortMode = DataGridViewColumnSortMode.Automatic

        reset_form()
        loadComboBoxAll(cmbJenis_Pekerjaan, "pekerjaan", "var_pekerjaan", "", "pekerjaan", "pekerjaan")
    End Sub

    Private Sub reset_form()
        txtNIK.Text = ""
        lblnama_karyawan.Text = ""
        lblnama_karyawan.Text = "-"
        lblNoTrans.Text = "-"
        DateTimePicker1.Value = Now
        dtStart.Value = Now
        dtFinish.Value = Now
        txtKeterangan.Text = ""
        dgv1.Rows.Clear()
    End Sub

    Private Sub btnSearchKaryawan_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSearchKaryawan.Click
        Dim f As New frmSearch
        f.setCol(0)
        f.setTableName("[absensi].[dbo].vw_karyawantipe") ' m inner join [absensi].[dbo].absensi a on m.nik=a.nik")
        f.setColumns("*")
        f.setWhere("tipe='Harian'")
        'f.setWhere("convert(varchar(20),a.tanggal,112) = '" & dtproduksi.Value.ToString("yyyyMMdd") & "' group by m.nik,m.nama")
        f.setOrder("order by nama_depan")
        If f.ShowDialog = System.Windows.Forms.DialogResult.OK Then
            txtNIK.Text = f.value
            cek_karyawan()
        End If
        f.Dispose()
    End Sub

    Private Sub cek_karyawan()
        lblnama_karyawan.Text = "-"
        Dim barang As DataTableReader
        barang = getfields("vw_karyawantipe", "*", " where nik = '" & txtNIK.Text & "' and tipe='Harian'").CreateDataReader ' and convert(varchar(20),a.tanggal,112) = '" & dtproduksi.Value.ToString("yyyyMMdd") & "'
        While barang.Read
            lblnama_karyawan.Text = barang("nama_depan")
        End While
    End Sub

    Private Sub btnTambahkan_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnTambahkan.Click
        Dim row As Integer
        If lblnama_karyawan.Text = "-" Then
            MsgBox("NIK tidak terdaftar!")
            Exit Sub
        End If
        dgv1.Rows.Add()
        row = dgv1.RowCount - 2
        dgv1.Item("nik", row).Value = UCase(txtNIK.Text)
        dgv1.Item("nama_karyawan", row).Value = lblnama_karyawan.Text
        dgv1.Item("jenis_pekerjaan", row).Value = cmbJenis_Pekerjaan.Text
        dgv1.Item(dgv1.Columns("Tanggal_Mulai").Index, row).Value = dtStart.Value
        dgv1.Item(dgv1.Columns("Tanggal_Berakhir").Index, row).Value = dtFinish.Value

        txtNIK.Text = ""
        lblnama_karyawan.Text = ""
        cmbJenis_Pekerjaan.Text = ""
        lblnama_karyawan.Text = "-"
        txtNIK.Focus()
        lblItem.Text = dgv1.RowCount - 1
    End Sub

    Private Sub btnSimpan_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSimpan.Click
        If dgv1.RowCount < 1 Then
            MsgBox("Masukkan data terlebih dahulu")
            dgv1.Focus()
            Exit Sub
        End If

        If simpan() Then
            reset_form()
        End If
    End Sub

    Private Function simpan() As Boolean
        simpan = False
        Dim cmd As SqlCommand
        Dim conn2 As New SqlConnection(strcon)
        Try
            conn2.Open()
            transaction = conn2.BeginTransaction(IsolationLevel.ReadCommitted)

            If lblNoTrans.Text <> "-" Then
                cmd = New SqlCommand("delete from t_harianh where nomer_harian = '" & lblNoTrans.Text & "'", conn2, transaction)
                cmd.ExecuteNonQuery()
                cmd = New SqlCommand("delete from t_hariand where nomer_harian = '" & lblNoTrans.Text & "'", conn2, transaction)
                cmd.ExecuteNonQuery()
            Else
                lblNoTrans.Text = newid("t_harianh", "nomer_harian", DateTimePicker1.Value, "HR")
            End If

            cmd = New SqlCommand(add_dataheader(), conn2, transaction)
            cmd.ExecuteNonQuery()

            Dim sqldetail As String = Nothing
            For row As Integer = 0 To dgv1.RowCount - 2
                sqldetail += add_datadetail(row) + ";"
            Next
            If sqldetail <> Nothing Then
                cmd = New SqlCommand(sqldetail, conn2, transaction)
                cmd.ExecuteNonQuery()
            End If

            transaction.Commit()
            conn2.Close()
            MsgBox("Data sudah tersimpan!")

        Catch ex As Exception
            MessageBox.Show(ex.Message.ToString)
            If reader IsNot Nothing Then reader.Close()
            transaction.Rollback()
            If conn2.State Then conn2.Close()
        End Try
    End Function

    Private Function add_dataheader()
        Dim fields() As String
        Dim nilai() As String
        Dim table_name As String

        ReDim fields(4)
        ReDim nilai(4)

        table_name = "t_harianh"
        fields(0) = "nomer_harian"
        fields(1) = "tanggal"
        fields(2) = "userid"
        fields(3) = "keterangan"

        nilai(0) = lblNoTrans.Text
        nilai(1) = DateTimePicker1.Value.ToString("yyyy/MM/dd HH:mm:ss")
        nilai(2) = username
        nilai(3) = txtKeterangan.Text

        add_dataheader = query_tambah_data(table_name, fields, nilai)
    End Function

    Private Function add_datadetail(ByVal row As Integer) As String
        Dim fields() As String
        Dim nilai() As String
        Dim table_name As String

        ReDim fields(6)
        ReDim nilai(6)

        table_name = "t_hariand"
        fields(0) = "nomer_harian"
        fields(1) = "nik"
        fields(2) = "jenis_pekerjaan"
        fields(3) = "tanggal_mulai"
        fields(4) = "tanggal_berakhir"
        fields(5) = "no_urut"

        nilai(0) = lblNoTrans.Text
        nilai(1) = dgv1.Item("nik", row).Value
        nilai(2) = dgv1.Item("jenis_pekerjaan", row).Value
        nilai(3) = CDate(dgv1.Item("tanggal_mulai", row).Value).ToString("yyyy/MM/dd HH:mm:ss")
        nilai(4) = CDate(dgv1.Item("tanggal_berakhir", row).Value).ToString("yyyy/MM/dd HH:mm:ss")
        nilai(5) = row

        add_datadetail = query_tambah_data(table_name, fields, nilai)
    End Function


    Private Sub btnSearchId_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSearchId.Click
        Dim f As New frmSearch
        f.setCol(0)
        f.setTableName("t_harianh")
        f.setColumns("*")
        f.setOrder("order by tanggal desc")
        If f.ShowDialog = System.Windows.Forms.DialogResult.OK Then
            lblNoTrans.Text = f.value
            load_trans()
        End If
        f.Dispose()
    End Sub

    Private Sub load_trans()
        Dim conn2 As New SqlConnection
        conn2.ConnectionString = strcon
        conn2.Open()
        Dim cmd As SqlCommand
        Dim reader As SqlDataReader = Nothing
        Dim da As New SqlDataAdapter()
        Dim ds As New DataSet()
        Dim row As Integer
        Try
            cmd = New SqlCommand("select * from t_harianh where nomer_harian = '" & lblNoTrans.Text & "'", conn2)
            reader = cmd.ExecuteReader()

            If reader.Read() Then
                DateTimePicker1.Value = reader("tanggal")
                txtKeterangan.Text = reader("keterangan")
            End If
            reader.Close()

            Dim detail As DataTableReader
            detail = getds("select d.*,isnull(m.nama_depan,'') as nama_karyawan from t_hariand d left join [absensi].[dbo].ms_karyawan m on d.nik = m.nik  where nomer_harian = '" & lblNoTrans.Text & "' order by no_urut", conn2).CreateDataReader
            dgv1.Rows.Clear()
            While detail.Read
                dgv1.Rows.Add()
                row = dgv1.Rows.Count - 2
                dgv1.Item("nik", row).Value = detail("nik")
                dgv1.Item("nama_karyawan", row).Value = detail("nama_karyawan")
                dgv1.Item("jenis_pekerjaan", row).Value = detail("jenis_pekerjaan")
                dgv1.Item("Tanggal_Mulai", row).Value = detail("tanggal_mulai")
                dgv1.Item("Tanggal_Berakhir", row).Value = detail("tanggal_berakhir")
            End While
            conn2.Close()
            lblItem.Text = dgv1.RowCount - 1
        Catch ex As Exception
            MessageBox.Show(ex.Message.ToString)
            reader.Close()
            conn2.Close()
        End Try
    End Sub

    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        reset_form()
    End Sub

    Private Sub txtNIK_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtNIK.KeyDown
        If e.KeyCode = Keys.F3 Then btnSearchKaryawan.PerformClick()
    End Sub

    Private Sub dgv1_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgv1.CellContentClick

    End Sub

    Private Sub dgv1_CellValueChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgv1.CellValueChanged
        If (e.ColumnIndex = 6 Or e.ColumnIndex = 8) And e.RowIndex >= 0 Then
            dgv1.Rows(e.RowIndex).Cells(9).Value = dgv1.Rows(e.RowIndex).Cells(6).Value * dgv1.Rows(e.RowIndex).Cells(8).Value
        End If
    End Sub

    Private Sub dgv1_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgv1.KeyDown
        If e.KeyCode = Keys.Delete Then
            Dim index As Integer
            If dgv1.RowCount > 1 Then
                If dgv1.Rows.Count <> dgv1.CurrentRow.Index + 1 Then
                    index = dgv1.CurrentRow.Index
                    dgv1.Rows.RemoveAt(index)
                End If
            End If
        End If
    End Sub

    Private Sub txtNIK_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtNIK.LostFocus
        If txtNIK.Text <> "" Then cek_karyawan()
    End Sub

    Private Sub btnKeluar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnKeluar.Click
        Me.Close()
    End Sub
End Class