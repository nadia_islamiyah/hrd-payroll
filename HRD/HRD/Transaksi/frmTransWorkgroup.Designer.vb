﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmTransJadwalKerja
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.DBGrid = New System.Windows.Forms.DataGridView()
        Me.NIK = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Nama = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DBKaryawan = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Check = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.btnAll = New System.Windows.Forms.Button()
        Me.btnNone = New System.Windows.Forms.Button()
        Me.cmbWorkgroup = New System.Windows.Forms.ComboBox()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.lblShift = New System.Windows.Forms.Label()
        Me.btnSimpan = New System.Windows.Forms.Button()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.dtTanggalAwal = New System.Windows.Forms.DateTimePicker()
        Me.dtTanggalAkhir = New System.Windows.Forms.DateTimePicker()
        Me.btnSetcol = New System.Windows.Forms.Button()
        Me.dtPeriode = New System.Windows.Forms.DateTimePicker()
        Me.ProgressBar1 = New System.Windows.Forms.ProgressBar()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.dtAutoAwal = New System.Windows.Forms.DateTimePicker()
        Me.dtAutoAkhir = New System.Windows.Forms.DateTimePicker()
        Me.btnAutoShift = New System.Windows.Forms.Button()
        Me.cmbtipe = New System.Windows.Forms.ComboBox()
        CType(Me.DBGrid, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DBKaryawan, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'DBGrid
        '
        Me.DBGrid.AllowUserToAddRows = False
        Me.DBGrid.AllowUserToDeleteRows = False
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DBGrid.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.DBGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DBGrid.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.NIK, Me.Nama})
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DBGrid.DefaultCellStyle = DataGridViewCellStyle2
        Me.DBGrid.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter
        Me.DBGrid.Location = New System.Drawing.Point(352, 72)
        Me.DBGrid.Name = "DBGrid"
        Me.DBGrid.RowHeadersVisible = False
        Me.DBGrid.Size = New System.Drawing.Size(688, 458)
        Me.DBGrid.TabIndex = 0
        '
        'NIK
        '
        Me.NIK.Frozen = True
        Me.NIK.HeaderText = "NIK"
        Me.NIK.Name = "NIK"
        Me.NIK.ReadOnly = True
        Me.NIK.Width = 80
        '
        'Nama
        '
        Me.Nama.Frozen = True
        Me.Nama.HeaderText = "Nama"
        Me.Nama.Name = "Nama"
        Me.Nama.ReadOnly = True
        Me.Nama.Width = 170
        '
        'DBKaryawan
        '
        Me.DBKaryawan.AllowUserToAddRows = False
        Me.DBKaryawan.AllowUserToDeleteRows = False
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DBKaryawan.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle3
        Me.DBKaryawan.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DBKaryawan.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn1, Me.DataGridViewTextBoxColumn2, Me.Check})
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DBKaryawan.DefaultCellStyle = DataGridViewCellStyle4
        Me.DBKaryawan.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnKeystroke
        Me.DBKaryawan.Location = New System.Drawing.Point(22, 72)
        Me.DBKaryawan.Name = "DBKaryawan"
        Me.DBKaryawan.RowHeadersVisible = False
        Me.DBKaryawan.Size = New System.Drawing.Size(314, 458)
        Me.DBKaryawan.TabIndex = 24
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.Frozen = True
        Me.DataGridViewTextBoxColumn1.HeaderText = "NIK"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.Width = 80
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.Frozen = True
        Me.DataGridViewTextBoxColumn2.HeaderText = "Nama"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.Width = 170
        '
        'Check
        '
        Me.Check.Frozen = True
        Me.Check.HeaderText = ""
        Me.Check.Name = "Check"
        Me.Check.Width = 30
        '
        'btnAll
        '
        Me.btnAll.Location = New System.Drawing.Point(22, 41)
        Me.btnAll.Name = "btnAll"
        Me.btnAll.Size = New System.Drawing.Size(75, 23)
        Me.btnAll.TabIndex = 81
        Me.btnAll.Text = "Select All"
        Me.btnAll.UseVisualStyleBackColor = True
        '
        'btnNone
        '
        Me.btnNone.Location = New System.Drawing.Point(103, 41)
        Me.btnNone.Name = "btnNone"
        Me.btnNone.Size = New System.Drawing.Size(75, 23)
        Me.btnNone.TabIndex = 82
        Me.btnNone.Text = "Select None"
        Me.btnNone.UseVisualStyleBackColor = True
        '
        'cmbWorkgroup
        '
        Me.cmbWorkgroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbWorkgroup.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbWorkgroup.FormattingEnabled = True
        Me.cmbWorkgroup.Location = New System.Drawing.Point(401, 46)
        Me.cmbWorkgroup.Name = "cmbWorkgroup"
        Me.cmbWorkgroup.Size = New System.Drawing.Size(150, 22)
        Me.cmbWorkgroup.TabIndex = 83
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.BackColor = System.Drawing.Color.Transparent
        Me.Label16.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.ForeColor = System.Drawing.Color.Black
        Me.Label16.Location = New System.Drawing.Point(355, 49)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(40, 14)
        Me.Label16.TabIndex = 84
        Me.Label16.Text = "Group"
        '
        'lblShift
        '
        Me.lblShift.AutoSize = True
        Me.lblShift.BackColor = System.Drawing.Color.Transparent
        Me.lblShift.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblShift.ForeColor = System.Drawing.Color.Black
        Me.lblShift.Location = New System.Drawing.Point(557, 50)
        Me.lblShift.Name = "lblShift"
        Me.lblShift.Size = New System.Drawing.Size(0, 14)
        Me.lblShift.TabIndex = 85
        '
        'btnSimpan
        '
        Me.btnSimpan.Location = New System.Drawing.Point(352, 541)
        Me.btnSimpan.Name = "btnSimpan"
        Me.btnSimpan.Size = New System.Drawing.Size(104, 29)
        Me.btnSimpan.TabIndex = 86
        Me.btnSimpan.Text = "Simpan"
        Me.btnSimpan.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.dtTanggalAwal)
        Me.GroupBox1.Controls.Add(Me.dtTanggalAkhir)
        Me.GroupBox1.Controls.Add(Me.btnSetcol)
        Me.GroupBox1.Location = New System.Drawing.Point(712, 44)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(328, 24)
        Me.GroupBox1.TabIndex = 87
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "GroupBox1"
        Me.GroupBox1.Visible = False
        '
        'dtTanggalAwal
        '
        Me.dtTanggalAwal.CustomFormat = "dd/MM/yyyy"
        Me.dtTanggalAwal.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtTanggalAwal.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtTanggalAwal.Location = New System.Drawing.Point(-13, 6)
        Me.dtTanggalAwal.Name = "dtTanggalAwal"
        Me.dtTanggalAwal.Size = New System.Drawing.Size(128, 23)
        Me.dtTanggalAwal.TabIndex = 81
        Me.dtTanggalAwal.Value = New Date(2013, 3, 2, 0, 0, 0, 0)
        '
        'dtTanggalAkhir
        '
        Me.dtTanggalAkhir.CustomFormat = "dd/MM/yyyy"
        Me.dtTanggalAkhir.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtTanggalAkhir.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtTanggalAkhir.Location = New System.Drawing.Point(143, 6)
        Me.dtTanggalAkhir.Name = "dtTanggalAkhir"
        Me.dtTanggalAkhir.Size = New System.Drawing.Size(128, 23)
        Me.dtTanggalAkhir.TabIndex = 82
        Me.dtTanggalAkhir.Value = New Date(2013, 3, 2, 0, 0, 0, 0)
        '
        'btnSetcol
        '
        Me.btnSetcol.Location = New System.Drawing.Point(277, 6)
        Me.btnSetcol.Name = "btnSetcol"
        Me.btnSetcol.Size = New System.Drawing.Size(75, 23)
        Me.btnSetcol.TabIndex = 83
        Me.btnSetcol.Text = "Set Column"
        Me.btnSetcol.UseVisualStyleBackColor = True
        '
        'dtPeriode
        '
        Me.dtPeriode.CustomFormat = "MMMM yyyy"
        Me.dtPeriode.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtPeriode.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtPeriode.Location = New System.Drawing.Point(22, 6)
        Me.dtPeriode.Name = "dtPeriode"
        Me.dtPeriode.ShowUpDown = True
        Me.dtPeriode.Size = New System.Drawing.Size(149, 23)
        Me.dtPeriode.TabIndex = 88
        Me.dtPeriode.Value = New Date(2013, 3, 2, 0, 0, 0, 0)
        '
        'ProgressBar1
        '
        Me.ProgressBar1.Location = New System.Drawing.Point(22, 544)
        Me.ProgressBar1.Name = "ProgressBar1"
        Me.ProgressBar1.Size = New System.Drawing.Size(314, 16)
        Me.ProgressBar1.Step = 1
        Me.ProgressBar1.Style = System.Windows.Forms.ProgressBarStyle.Continuous
        Me.ProgressBar1.TabIndex = 89
        Me.ProgressBar1.Value = 20
        Me.ProgressBar1.Visible = False
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.dtAutoAwal)
        Me.GroupBox2.Controls.Add(Me.dtAutoAkhir)
        Me.GroupBox2.Controls.Add(Me.btnAutoShift)
        Me.GroupBox2.Location = New System.Drawing.Point(352, 6)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(408, 35)
        Me.GroupBox2.TabIndex = 90
        Me.GroupBox2.TabStop = False
        '
        'dtAutoAwal
        '
        Me.dtAutoAwal.CustomFormat = "dd/MM/yyyy"
        Me.dtAutoAwal.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtAutoAwal.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtAutoAwal.Location = New System.Drawing.Point(7, 9)
        Me.dtAutoAwal.Name = "dtAutoAwal"
        Me.dtAutoAwal.Size = New System.Drawing.Size(110, 23)
        Me.dtAutoAwal.TabIndex = 81
        Me.dtAutoAwal.Value = New Date(2013, 3, 2, 0, 0, 0, 0)
        '
        'dtAutoAkhir
        '
        Me.dtAutoAkhir.CustomFormat = "dd/MM/yyyy"
        Me.dtAutoAkhir.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtAutoAkhir.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtAutoAkhir.Location = New System.Drawing.Point(163, 9)
        Me.dtAutoAkhir.Name = "dtAutoAkhir"
        Me.dtAutoAkhir.Size = New System.Drawing.Size(97, 23)
        Me.dtAutoAkhir.TabIndex = 82
        Me.dtAutoAkhir.Value = New Date(2013, 3, 2, 0, 0, 0, 0)
        '
        'btnAutoShift
        '
        Me.btnAutoShift.Location = New System.Drawing.Point(297, 9)
        Me.btnAutoShift.Name = "btnAutoShift"
        Me.btnAutoShift.Size = New System.Drawing.Size(75, 23)
        Me.btnAutoShift.TabIndex = 83
        Me.btnAutoShift.Text = "Auto Shift"
        Me.btnAutoShift.UseVisualStyleBackColor = True
        '
        'cmbtipe
        '
        Me.cmbtipe.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbtipe.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbtipe.FormattingEnabled = True
        Me.cmbtipe.Location = New System.Drawing.Point(184, 42)
        Me.cmbtipe.Name = "cmbtipe"
        Me.cmbtipe.Size = New System.Drawing.Size(119, 22)
        Me.cmbtipe.TabIndex = 91
        '
        'frmTransJadwalKerja
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1050, 579)
        Me.Controls.Add(Me.cmbtipe)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.ProgressBar1)
        Me.Controls.Add(Me.dtPeriode)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.btnSimpan)
        Me.Controls.Add(Me.lblShift)
        Me.Controls.Add(Me.cmbWorkgroup)
        Me.Controls.Add(Me.Label16)
        Me.Controls.Add(Me.btnNone)
        Me.Controls.Add(Me.btnAll)
        Me.Controls.Add(Me.DBKaryawan)
        Me.Controls.Add(Me.DBGrid)
        Me.KeyPreview = True
        Me.Name = "frmTransJadwalKerja"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Pengaturan Jadwal Group Kerja"
        CType(Me.DBGrid, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DBKaryawan, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox2.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents DBGrid As System.Windows.Forms.DataGridView
    Friend WithEvents DBKaryawan As System.Windows.Forms.DataGridView
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Check As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents btnAll As System.Windows.Forms.Button
    Friend WithEvents btnNone As System.Windows.Forms.Button
    Friend WithEvents cmbWorkgroup As System.Windows.Forms.ComboBox
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents lblShift As System.Windows.Forms.Label
    Friend WithEvents btnSimpan As System.Windows.Forms.Button
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents dtTanggalAwal As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtTanggalAkhir As System.Windows.Forms.DateTimePicker
    Friend WithEvents btnSetcol As System.Windows.Forms.Button
    Friend WithEvents dtPeriode As System.Windows.Forms.DateTimePicker
    Friend WithEvents NIK As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Nama As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ProgressBar1 As System.Windows.Forms.ProgressBar
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents dtAutoAwal As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtAutoAkhir As System.Windows.Forms.DateTimePicker
    Friend WithEvents btnAutoShift As System.Windows.Forms.Button
    Friend WithEvents cmbtipe As System.Windows.Forms.ComboBox
End Class
