﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmTransRekapAbsenHarian
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.DBGrid = New System.Windows.Forms.DataGridView()
        Me.NIK = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Nama = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Masuk = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Istirahat = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Kembali = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Pulang = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TanggalMasuk = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TanggalIstirahat = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TanggalKembali = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TanggalPulang = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Total = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Telat = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PC = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Lembur = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Absen = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.Shift = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dtTanggal = New System.Windows.Forms.DateTimePicker()
        Me.btnSimpan = New System.Windows.Forms.Button()
        Me.cmbTipe = New System.Windows.Forms.ComboBox()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.cmbdata = New System.Windows.Forms.ComboBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtKeyword = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.chkEdit = New System.Windows.Forms.CheckBox()
        Me.CheckBox2 = New System.Windows.Forms.CheckBox()
        Me.CheckBox3 = New System.Windows.Forms.CheckBox()
        CType(Me.DBGrid, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'DBGrid
        '
        Me.DBGrid.AllowUserToAddRows = False
        Me.DBGrid.AllowUserToDeleteRows = False
        Me.DBGrid.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DBGrid.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle3
        Me.DBGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DBGrid.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.NIK, Me.Nama, Me.Masuk, Me.Istirahat, Me.Kembali, Me.Pulang, Me.TanggalMasuk, Me.TanggalIstirahat, Me.TanggalKembali, Me.TanggalPulang, Me.Total, Me.Telat, Me.PC, Me.Lembur, Me.Absen, Me.Shift})
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DBGrid.DefaultCellStyle = DataGridViewCellStyle4
        Me.DBGrid.Location = New System.Drawing.Point(22, 103)
        Me.DBGrid.Name = "DBGrid"
        Me.DBGrid.Size = New System.Drawing.Size(1151, 498)
        Me.DBGrid.TabIndex = 0
        '
        'NIK
        '
        Me.NIK.HeaderText = "NIK"
        Me.NIK.Name = "NIK"
        Me.NIK.ReadOnly = True
        Me.NIK.Width = 80
        '
        'Nama
        '
        Me.Nama.HeaderText = "Nama"
        Me.Nama.Name = "Nama"
        Me.Nama.ReadOnly = True
        Me.Nama.Width = 160
        '
        'Masuk
        '
        Me.Masuk.HeaderText = "Masuk"
        Me.Masuk.Name = "Masuk"
        Me.Masuk.ReadOnly = True
        Me.Masuk.Visible = False
        Me.Masuk.Width = 60
        '
        'Istirahat
        '
        Me.Istirahat.HeaderText = "Istirahat"
        Me.Istirahat.Name = "Istirahat"
        Me.Istirahat.ReadOnly = True
        Me.Istirahat.Visible = False
        Me.Istirahat.Width = 60
        '
        'Kembali
        '
        Me.Kembali.HeaderText = "Kembali"
        Me.Kembali.Name = "Kembali"
        Me.Kembali.ReadOnly = True
        Me.Kembali.Visible = False
        Me.Kembali.Width = 65
        '
        'Pulang
        '
        Me.Pulang.HeaderText = "Pulang"
        Me.Pulang.Name = "Pulang"
        Me.Pulang.ReadOnly = True
        Me.Pulang.Visible = False
        Me.Pulang.Width = 60
        '
        'TanggalMasuk
        '
        Me.TanggalMasuk.HeaderText = "TanggalMasuk"
        Me.TanggalMasuk.Name = "TanggalMasuk"
        Me.TanggalMasuk.Width = 130
        '
        'TanggalIstirahat
        '
        Me.TanggalIstirahat.HeaderText = "TanggalIstirahat"
        Me.TanggalIstirahat.Name = "TanggalIstirahat"
        Me.TanggalIstirahat.Width = 130
        '
        'TanggalKembali
        '
        Me.TanggalKembali.HeaderText = "TanggalKembali"
        Me.TanggalKembali.Name = "TanggalKembali"
        Me.TanggalKembali.Width = 130
        '
        'TanggalPulang
        '
        Me.TanggalPulang.HeaderText = "TanggalPulang"
        Me.TanggalPulang.Name = "TanggalPulang"
        Me.TanggalPulang.Width = 130
        '
        'Total
        '
        Me.Total.HeaderText = "Total"
        Me.Total.Name = "Total"
        Me.Total.ReadOnly = True
        Me.Total.Width = 60
        '
        'Telat
        '
        Me.Telat.HeaderText = "Telat"
        Me.Telat.Name = "Telat"
        Me.Telat.ReadOnly = True
        Me.Telat.Width = 50
        '
        'PC
        '
        Me.PC.HeaderText = "PC"
        Me.PC.Name = "PC"
        Me.PC.ReadOnly = True
        Me.PC.Width = 50
        '
        'Lembur
        '
        Me.Lembur.HeaderText = "Lembur"
        Me.Lembur.Name = "Lembur"
        Me.Lembur.Width = 60
        '
        'Absen
        '
        Me.Absen.HeaderText = "Absen"
        Me.Absen.Name = "Absen"
        Me.Absen.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Absen.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.Absen.Width = 50
        '
        'Shift
        '
        Me.Shift.HeaderText = "Shift"
        Me.Shift.Name = "Shift"
        Me.Shift.Width = 50
        '
        'dtTanggal
        '
        Me.dtTanggal.CustomFormat = "dd/MM/yyyy"
        Me.dtTanggal.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtTanggal.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtTanggal.Location = New System.Drawing.Point(139, 73)
        Me.dtTanggal.Name = "dtTanggal"
        Me.dtTanggal.Size = New System.Drawing.Size(128, 23)
        Me.dtTanggal.TabIndex = 23
        Me.dtTanggal.Value = New Date(2013, 3, 2, 0, 0, 0, 0)
        '
        'btnSimpan
        '
        Me.btnSimpan.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnSimpan.Location = New System.Drawing.Point(22, 607)
        Me.btnSimpan.Name = "btnSimpan"
        Me.btnSimpan.Size = New System.Drawing.Size(104, 29)
        Me.btnSimpan.TabIndex = 87
        Me.btnSimpan.Text = "Simpan"
        Me.btnSimpan.UseVisualStyleBackColor = True
        '
        'cmbTipe
        '
        Me.cmbTipe.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbTipe.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbTipe.FormattingEnabled = True
        Me.cmbTipe.Location = New System.Drawing.Point(139, 43)
        Me.cmbTipe.Name = "cmbTipe"
        Me.cmbTipe.Size = New System.Drawing.Size(128, 22)
        Me.cmbTipe.TabIndex = 175
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.BackColor = System.Drawing.Color.Transparent
        Me.Label16.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.ForeColor = System.Drawing.Color.Black
        Me.Label16.Location = New System.Drawing.Point(27, 46)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(87, 14)
        Me.Label16.TabIndex = 176
        Me.Label16.Text = "Tipe Karyawan"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.Black
        Me.Label1.Location = New System.Drawing.Point(27, 79)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(50, 14)
        Me.Label1.TabIndex = 177
        Me.Label1.Text = "Tanggal"
        '
        'cmbdata
        '
        Me.cmbdata.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbdata.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbdata.FormattingEnabled = True
        Me.cmbdata.Items.AddRange(New Object() {"Ada Absen", "Semua"})
        Me.cmbdata.Location = New System.Drawing.Point(139, 12)
        Me.cmbdata.Name = "cmbdata"
        Me.cmbdata.Size = New System.Drawing.Size(128, 22)
        Me.cmbdata.TabIndex = 178
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.BackColor = System.Drawing.Color.Transparent
        Me.Label2.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.Black
        Me.Label2.Location = New System.Drawing.Point(27, 15)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(62, 14)
        Me.Label2.TabIndex = 179
        Me.Label2.Text = "Filter Data"
        '
        'txtKeyword
        '
        Me.txtKeyword.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtKeyword.Location = New System.Drawing.Point(410, 12)
        Me.txtKeyword.Name = "txtKeyword"
        Me.txtKeyword.Size = New System.Drawing.Size(200, 22)
        Me.txtKeyword.TabIndex = 180
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(342, 15)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(60, 16)
        Me.Label3.TabIndex = 182
        Me.Label3.Text = "Keyword"
        '
        'Label4
        '
        Me.Label4.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(1080, 613)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(15, 16)
        Me.Label4.TabIndex = 183
        Me.Label4.Text = "0"
        '
        'Label5
        '
        Me.Label5.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(1001, 613)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(59, 16)
        Me.Label5.TabIndex = 184
        Me.Label5.Text = "Record :"
        '
        'chkEdit
        '
        Me.chkEdit.AutoSize = True
        Me.chkEdit.Location = New System.Drawing.Point(1092, 80)
        Me.chkEdit.Name = "chkEdit"
        Me.chkEdit.Size = New System.Drawing.Size(71, 17)
        Me.chkEdit.TabIndex = 185
        Me.chkEdit.Text = "Lock Edit"
        Me.chkEdit.UseVisualStyleBackColor = True
        '
        'CheckBox2
        '
        Me.CheckBox2.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.CheckBox2.AutoSize = True
        Me.CheckBox2.BackColor = System.Drawing.Color.Khaki
        Me.CheckBox2.Checked = True
        Me.CheckBox2.CheckState = System.Windows.Forms.CheckState.Checked
        Me.CheckBox2.Enabled = False
        Me.CheckBox2.Location = New System.Drawing.Point(687, 614)
        Me.CheckBox2.Name = "CheckBox2"
        Me.CheckBox2.Size = New System.Drawing.Size(141, 17)
        Me.CheckBox2.TabIndex = 187
        Me.CheckBox2.Text = "Total Absen < Jam Kerja"
        Me.CheckBox2.UseVisualStyleBackColor = False
        '
        'CheckBox3
        '
        Me.CheckBox3.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.CheckBox3.AutoSize = True
        Me.CheckBox3.BackColor = System.Drawing.Color.PaleVioletRed
        Me.CheckBox3.Checked = True
        Me.CheckBox3.CheckState = System.Windows.Forms.CheckState.Checked
        Me.CheckBox3.Enabled = False
        Me.CheckBox3.Location = New System.Drawing.Point(834, 614)
        Me.CheckBox3.Name = "CheckBox3"
        Me.CheckBox3.Size = New System.Drawing.Size(147, 17)
        Me.CheckBox3.TabIndex = 188
        Me.CheckBox3.Text = "Jam Masuk = Jam Pulang"
        Me.CheckBox3.UseVisualStyleBackColor = False
        '
        'frmTransRekapAbsenHarian
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1195, 642)
        Me.Controls.Add(Me.CheckBox3)
        Me.Controls.Add(Me.CheckBox2)
        Me.Controls.Add(Me.chkEdit)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.txtKeyword)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.cmbdata)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.cmbTipe)
        Me.Controls.Add(Me.Label16)
        Me.Controls.Add(Me.btnSimpan)
        Me.Controls.Add(Me.dtTanggal)
        Me.Controls.Add(Me.DBGrid)
        Me.KeyPreview = True
        Me.Name = "frmTransRekapAbsenHarian"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Rekap Absensi Harian"
        CType(Me.DBGrid, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents DBGrid As System.Windows.Forms.DataGridView
    Friend WithEvents dtTanggal As System.Windows.Forms.DateTimePicker
    Friend WithEvents NIK As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Nama As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Masuk As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Istirahat As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Kembali As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Pulang As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TanggalMasuk As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TanggalIstirahat As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TanggalKembali As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TanggalPulang As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Total As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Telat As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PC As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Lembur As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Absen As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents Shift As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents btnSimpan As System.Windows.Forms.Button
    Friend WithEvents cmbTipe As System.Windows.Forms.ComboBox
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents cmbdata As System.Windows.Forms.ComboBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtKeyword As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents chkEdit As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox2 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox3 As System.Windows.Forms.CheckBox
End Class
