﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmTransAbsensi
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnSearch = New System.Windows.Forms.Button()
        Me.txtNama = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtNIK = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.jamMasuk = New System.Windows.Forms.DateTimePicker()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.dtTanggal = New System.Windows.Forms.DateTimePicker()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.jamKembali = New System.Windows.Forms.DateTimePicker()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.jamIstirahat = New System.Windows.Forms.DateTimePicker()
        Me.jamPulang = New System.Windows.Forms.DateTimePicker()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.btnSave = New System.Windows.Forms.Button()
        Me.chkMasuk = New System.Windows.Forms.CheckBox()
        Me.chkIstirahat = New System.Windows.Forms.CheckBox()
        Me.chkKembali = New System.Windows.Forms.CheckBox()
        Me.chkPulang = New System.Windows.Forms.CheckBox()
        Me.SuspendLayout()
        '
        'btnSearch
        '
        Me.btnSearch.Location = New System.Drawing.Point(219, 44)
        Me.btnSearch.Name = "btnSearch"
        Me.btnSearch.Size = New System.Drawing.Size(28, 23)
        Me.btnSearch.TabIndex = 64
        Me.btnSearch.Text = "F3"
        Me.btnSearch.UseVisualStyleBackColor = True
        '
        'txtNama
        '
        Me.txtNama.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNama.Location = New System.Drawing.Point(121, 74)
        Me.txtNama.Name = "txtNama"
        Me.txtNama.ReadOnly = True
        Me.txtNama.Size = New System.Drawing.Size(161, 22)
        Me.txtNama.TabIndex = 61
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.Black
        Me.Label1.Location = New System.Drawing.Point(10, 74)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(76, 14)
        Me.Label1.TabIndex = 63
        Me.Label1.Text = "Nama Depan"
        '
        'txtNIK
        '
        Me.txtNIK.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNIK.Location = New System.Drawing.Point(121, 45)
        Me.txtNIK.Name = "txtNIK"
        Me.txtNIK.Size = New System.Drawing.Size(92, 22)
        Me.txtNIK.TabIndex = 60
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.BackColor = System.Drawing.Color.Transparent
        Me.Label2.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.Black
        Me.Label2.Location = New System.Drawing.Point(10, 45)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(26, 14)
        Me.Label2.TabIndex = 62
        Me.Label2.Text = "NIK"
        '
        'jamMasuk
        '
        Me.jamMasuk.CustomFormat = "dd/MM/yyyy HH:mm:ss"
        Me.jamMasuk.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.jamMasuk.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.jamMasuk.Location = New System.Drawing.Point(120, 104)
        Me.jamMasuk.Name = "jamMasuk"
        Me.jamMasuk.ShowUpDown = True
        Me.jamMasuk.Size = New System.Drawing.Size(178, 23)
        Me.jamMasuk.TabIndex = 66
        Me.jamMasuk.Value = New Date(2013, 3, 2, 0, 0, 0, 0)
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.BackColor = System.Drawing.Color.Transparent
        Me.Label3.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.Black
        Me.Label3.Location = New System.Drawing.Point(12, 109)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(71, 16)
        Me.Label3.TabIndex = 65
        Me.Label3.Text = "Jam Masuk"
        '
        'dtTanggal
        '
        Me.dtTanggal.CustomFormat = "dd/MM/yyyy"
        Me.dtTanggal.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtTanggal.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtTanggal.Location = New System.Drawing.Point(119, 12)
        Me.dtTanggal.Name = "dtTanggal"
        Me.dtTanggal.Size = New System.Drawing.Size(128, 23)
        Me.dtTanggal.TabIndex = 67
        Me.dtTanggal.Value = New Date(2013, 3, 2, 0, 0, 0, 0)
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.BackColor = System.Drawing.Color.Transparent
        Me.Label13.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.ForeColor = System.Drawing.Color.Black
        Me.Label13.Location = New System.Drawing.Point(12, 167)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(80, 16)
        Me.Label13.TabIndex = 70
        Me.Label13.Text = "Jam Kembali"
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.BackColor = System.Drawing.Color.Transparent
        Me.Label25.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label25.ForeColor = System.Drawing.Color.Black
        Me.Label25.Location = New System.Drawing.Point(12, 167)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(31, 16)
        Me.Label25.TabIndex = 69
        Me.Label25.Text = "Jam"
        '
        'jamKembali
        '
        Me.jamKembali.CustomFormat = "dd/MM/yyyy HH:mm:ss"
        Me.jamKembali.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.jamKembali.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.jamKembali.Location = New System.Drawing.Point(120, 162)
        Me.jamKembali.Name = "jamKembali"
        Me.jamKembali.ShowUpDown = True
        Me.jamKembali.Size = New System.Drawing.Size(178, 23)
        Me.jamKembali.TabIndex = 73
        Me.jamKembali.Value = New Date(2013, 3, 2, 0, 0, 0, 0)
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.BackColor = System.Drawing.Color.Transparent
        Me.Label19.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label19.ForeColor = System.Drawing.Color.Black
        Me.Label19.Location = New System.Drawing.Point(12, 167)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(31, 16)
        Me.Label19.TabIndex = 68
        Me.Label19.Text = "Jam"
        '
        'jamIstirahat
        '
        Me.jamIstirahat.CustomFormat = "dd/MM/yyyy HH:mm:ss"
        Me.jamIstirahat.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.jamIstirahat.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.jamIstirahat.Location = New System.Drawing.Point(121, 131)
        Me.jamIstirahat.Name = "jamIstirahat"
        Me.jamIstirahat.ShowUpDown = True
        Me.jamIstirahat.Size = New System.Drawing.Size(178, 23)
        Me.jamIstirahat.TabIndex = 72
        Me.jamIstirahat.Value = New Date(2013, 3, 2, 0, 0, 0, 0)
        '
        'jamPulang
        '
        Me.jamPulang.CustomFormat = "dd/MM/yyyy HH:mm:ss"
        Me.jamPulang.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.jamPulang.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.jamPulang.Location = New System.Drawing.Point(120, 191)
        Me.jamPulang.Name = "jamPulang"
        Me.jamPulang.ShowUpDown = True
        Me.jamPulang.Size = New System.Drawing.Size(178, 23)
        Me.jamPulang.TabIndex = 71
        Me.jamPulang.Value = New Date(2013, 3, 2, 0, 0, 0, 0)
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.BackColor = System.Drawing.Color.Transparent
        Me.Label4.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.Black
        Me.Label4.Location = New System.Drawing.Point(12, 138)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(82, 16)
        Me.Label4.TabIndex = 74
        Me.Label4.Text = "Jam Istirahat"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.BackColor = System.Drawing.Color.Transparent
        Me.Label5.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.Color.Black
        Me.Label5.Location = New System.Drawing.Point(12, 196)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(73, 16)
        Me.Label5.TabIndex = 76
        Me.Label5.Text = "Jam Pulang"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.BackColor = System.Drawing.Color.Transparent
        Me.Label6.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.Color.Black
        Me.Label6.Location = New System.Drawing.Point(10, 18)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(50, 14)
        Me.Label6.TabIndex = 78
        Me.Label6.Text = "Tanggal"
        '
        'btnSave
        '
        Me.btnSave.Location = New System.Drawing.Point(112, 219)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(75, 23)
        Me.btnSave.TabIndex = 79
        Me.btnSave.Text = "Simpan"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'chkMasuk
        '
        Me.chkMasuk.AutoSize = True
        Me.chkMasuk.Location = New System.Drawing.Point(316, 111)
        Me.chkMasuk.Name = "chkMasuk"
        Me.chkMasuk.Size = New System.Drawing.Size(15, 14)
        Me.chkMasuk.TabIndex = 80
        Me.chkMasuk.UseVisualStyleBackColor = True
        '
        'chkIstirahat
        '
        Me.chkIstirahat.AutoSize = True
        Me.chkIstirahat.Location = New System.Drawing.Point(316, 138)
        Me.chkIstirahat.Name = "chkIstirahat"
        Me.chkIstirahat.Size = New System.Drawing.Size(15, 14)
        Me.chkIstirahat.TabIndex = 81
        Me.chkIstirahat.UseVisualStyleBackColor = True
        '
        'chkKembali
        '
        Me.chkKembali.AutoSize = True
        Me.chkKembali.Location = New System.Drawing.Point(316, 169)
        Me.chkKembali.Name = "chkKembali"
        Me.chkKembali.Size = New System.Drawing.Size(15, 14)
        Me.chkKembali.TabIndex = 82
        Me.chkKembali.UseVisualStyleBackColor = True
        '
        'chkPulang
        '
        Me.chkPulang.AutoSize = True
        Me.chkPulang.Location = New System.Drawing.Point(316, 196)
        Me.chkPulang.Name = "chkPulang"
        Me.chkPulang.Size = New System.Drawing.Size(15, 14)
        Me.chkPulang.TabIndex = 83
        Me.chkPulang.UseVisualStyleBackColor = True
        '
        'frmTransAbsensi
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(351, 266)
        Me.Controls.Add(Me.chkPulang)
        Me.Controls.Add(Me.chkKembali)
        Me.Controls.Add(Me.chkIstirahat)
        Me.Controls.Add(Me.chkMasuk)
        Me.Controls.Add(Me.btnSave)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label13)
        Me.Controls.Add(Me.Label25)
        Me.Controls.Add(Me.jamKembali)
        Me.Controls.Add(Me.Label19)
        Me.Controls.Add(Me.jamIstirahat)
        Me.Controls.Add(Me.jamPulang)
        Me.Controls.Add(Me.dtTanggal)
        Me.Controls.Add(Me.jamMasuk)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.btnSearch)
        Me.Controls.Add(Me.txtNama)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.txtNIK)
        Me.Controls.Add(Me.Label2)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmTransAbsensi"
        Me.Text = "frmTransAbsensi"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnSearch As System.Windows.Forms.Button
    Friend WithEvents txtNama As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtNIK As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents jamMasuk As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents dtTanggal As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents jamKembali As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents jamIstirahat As System.Windows.Forms.DateTimePicker
    Friend WithEvents jamPulang As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents btnSave As System.Windows.Forms.Button
    Friend WithEvents chkMasuk As System.Windows.Forms.CheckBox
    Friend WithEvents chkIstirahat As System.Windows.Forms.CheckBox
    Friend WithEvents chkKembali As System.Windows.Forms.CheckBox
    Friend WithEvents chkPulang As System.Windows.Forms.CheckBox
End Class
