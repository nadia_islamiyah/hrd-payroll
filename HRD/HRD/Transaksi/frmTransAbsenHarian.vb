﻿Imports System.Data.SqlClient
Imports System.Data.Sql
Imports System.IO
Public Class frmTransAbsenHarian
    Dim da As SqlCommand
    Dim dr As SqlDataReader

    Private Sub frmTransAbsenHarian_KeyDown(sender As Object, e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        If e.KeyCode = Keys.Enter Then SendKeys.Send("{tab}")
        If e.KeyCode = Keys.Escape Then Me.Close()
    End Sub
    Private Sub frmTransAbsenHarian_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        dtTanggal.Value = Now
        load_absensi()
    End Sub
    Private Sub load_absensi()
        Dim conn As New SqlConnection
        Dim datatable1 As New DataTable
        conn.ConnectionString = strcon
        conn.Open()
        da = New SqlCommand("select m.nik,m.nama_depan +' '+ m.nama_belakang as nama,datang as datang,istirahat_out as istirahat,istirahat_in as kembali,pulang as pulang,isnull(status_datang,0) as status_datang,isnull(status_istirahat_in,0) as status_istirahat_in,isnull(status_istirahat_out,0) as status_istirahat_out,isnull(status_pulang,0) as status_pulang " & _
                                "from ms_karyawan m left join t_rekapabsensi t on m.nik=t.nik  and convert(varchar(8),tanggal,112)='" & Format(dtTanggal.Value, "yyyyMMdd") & "' where aktif='True'", conn)

        dr = da.ExecuteReader
        DBGrid.Rows.Clear()
        While dr.Read
            DBGrid.Rows.Add()
            DBGrid.Rows(DBGrid.Rows.Count - 1).Cells(0).Value = dr.Item(0)
            DBGrid.Rows(DBGrid.Rows.Count - 1).Cells(1).Value = dr.Item(1)
            If dr.Item("status_datang") Then DBGrid.Rows(DBGrid.Rows.Count - 1).Cells(2).Value = Format(dr.Item(2), "dd/MM/yyyy HH:mm:ss") Else DBGrid.Rows(DBGrid.Rows.Count - 1).Cells(2).Value = ""
            If dr.Item("status_istirahat_out") Then DBGrid.Rows(DBGrid.Rows.Count - 1).Cells(3).Value = Format(dr.Item(3), "dd/MM/yyyy HH:mm:ss") Else DBGrid.Rows(DBGrid.Rows.Count - 1).Cells(3).Value = ""
            If dr.Item("status_istirahat_in") Then DBGrid.Rows(DBGrid.Rows.Count - 1).Cells(4).Value = Format(dr.Item(4), "dd/MM/yyyy HH:mm:ss") Else DBGrid.Rows(DBGrid.Rows.Count - 1).Cells(4).Value = ""
            If dr.Item("status_pulang") Then DBGrid.Rows(DBGrid.Rows.Count - 1).Cells(5).Value = Format(dr.Item(5), "dd/MM/yyyy HH:mm:ss") Else DBGrid.Rows(DBGrid.Rows.Count - 1).Cells(5).Value = ""
            If dr.Item("status_pulang") And dr.Item("status_datang") Then
                If DateDiff(DateInterval.Hour, dr.Item(2), dr.Item(5)) > 16 Then
                    DBGrid.Rows(DBGrid.Rows.Count - 1).DefaultCellStyle.BackColor = Color.PaleVioletRed
                End If
            End If
        End While

    End Sub

    Private Sub dtTanggal_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtTanggal.ValueChanged
        Dim cur As Integer
        Dim scroll As Integer
        cur = 0
        If DBGrid.Rows.Count > 0 Then
            If DBGrid.CurrentRow.Index > 0 Then cur = DBGrid.CurrentRow.Index
            scroll = DBGrid.FirstDisplayedScrollingRowIndex
        End If
        load_absensi()
        If cur > 0 Then
            DBGrid.CurrentCell = DBGrid.Item(0, cur)
            DBGrid.FirstDisplayedScrollingRowIndex = scroll
        End If


    End Sub

    Private Sub DBGrid_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DBGrid.CellContentClick
        If e.ColumnIndex = 6 Then
            Dim f As New frmTransAbsensi
            f.txtNIK.Text = DBGrid.Rows(DBGrid.CurrentRow.Index).Cells(0).Value
            f.dtTanggal.Value = dtTanggal.Value
            f.cari_data()

            f.ShowDialog()
            f.Dispose()
            load_absensi()
        End If
    End Sub

    Private Sub DBGrid_CellValueChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DBGrid.CellValueChanged

    End Sub
End Class