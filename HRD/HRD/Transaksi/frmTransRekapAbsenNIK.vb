﻿Imports System.Data.SqlClient
Imports System.Data.Sql
Imports System.IO
Public Class frmTransRekapAbsenNIK
    Dim da As SqlCommand
    Dim dr As SqlDataReader

    Private Sub frmTransRekapAbsenNIK_KeyDown(sender As Object, e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        If e.KeyCode = Keys.Enter Then SendKeys.Send("{tab}")
        If e.KeyCode = Keys.Escape Then Me.Close()
    End Sub
    Private Sub frmTransAbsenHarian_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        load_absensi()
    End Sub
    Private Sub load_absensi()
        Dim conn As New SqlConnection
        Dim datatable1 As New DataTable
        conn.ConnectionString = strcon
        conn.Open()
        da = New SqlCommand("select *,isnull(status_datang,0) as st_datang,isnull(status_istirahat_in,0) as st_istirahat_in,isnull(status_istirahat_out,0) as st_istirahat_out,isnull(status_pulang,0) as st_pulang  " & _
                                "from ms_karyawan m inner join t_rekapabsensi t on m.nik=t.nik  and convert(varchar(8),t.tanggal,112) between '" & Format(dtAwal.Value, "yyyyMMdd") & "' and '" & Format(dtAkhir.Value, "yyyyMMdd") & "' " & _
                                "left join t_jadwalkerja d on m.nik=d.nik and d.tanggal=t.tanggal" & _
                                " left join ms_jamkerja j on j.nama_shift=d.nama_shift where aktif='True' and m.nik ='" & txtSearch.Text & "'", conn)
        ',convert(varchar(8),datang,8) as datang,convert(varchar(8),istirahat_out,8) as istirahat,convert(varchar(8),istirahat_in,8) as kembali,convert(varchar(8),pulang,8) as pulang
        dr = da.ExecuteReader
        DBGrid.Rows.Clear()
        While dr.Read
            DBGrid.Rows.Add()
            DBGrid.Rows(DBGrid.Rows.Count - 1).Cells(0).Value = dr.Item(0)
            DBGrid.Rows(DBGrid.Rows.Count - 1).Cells(1).Value = dr.Item("nama_depan") & " " & dr.Item("nama_belakang")
            DBGrid.Rows(DBGrid.Rows.Count - 1).Cells("Tanggal").Value = dr.Item("tanggal")
            If Not IsDBNull(dr.Item("nama_shift")) Then
                Try
                    If dr.Item("st_datang") Then
                        If dr.Item("masuk_pembulatan") Then
                            If dr.Item("masuk_arahpembulatan") Then
                                DBGrid.Rows(DBGrid.Rows.Count - 1).Cells("masuk").Value = Format(timeroundup(dr.Item("datang"), dr.Item("masuk_toleransi_sebelum"), dr.Item("masuk_toleransi_sesudah")), "HH:mm:ss")
                                DBGrid.Rows(DBGrid.Rows.Count - 1).Cells("tanggalmasuk").Value = Format(timeroundup(dr.Item("datang"), dr.Item("masuk_toleransi_sebelum"), dr.Item("masuk_toleransi_sesudah")), "MM/dd/yyyy HH:mm:ss")
                            Else
                                DBGrid.Rows(DBGrid.Rows.Count - 1).Cells("masuk").Value = Format(timerounddown(dr.Item("datang"), dr.Item("masuk_toleransi_sebelum"), dr.Item("masuk_toleransi_sesudah")), "HH:mm:ss")
                                DBGrid.Rows(DBGrid.Rows.Count - 1).Cells("tanggalmasuk").Value = Format(timerounddown(dr.Item("datang"), dr.Item("masuk_toleransi_sebelum"), dr.Item("masuk_toleransi_sesudah")), "MM/dd/yyyy HH:mm:ss")
                            End If
                        End If
                    Else
                        DBGrid.Rows(DBGrid.Rows.Count - 1).Cells("masuk").Value = ""
                    End If
                    If dr.Item("st_istirahat_out") Then
                        If dr.Item("istirahat_pembulatan") Then
                            If dr.Item("istirahat_arahpembulatan") Then
                                DBGrid.Rows(DBGrid.Rows.Count - 1).Cells("istirahat").Value = Format(timeroundup(dr.Item("istirahat_out"), dr.Item("istirahat_toleransi_sebelum"), dr.Item("istirahat_toleransi_sesudah")), "HH:mm:ss")
                                DBGrid.Rows(DBGrid.Rows.Count - 1).Cells("tanggalistirahat").Value = Format(timeroundup(dr.Item("istirahat_out"), dr.Item("istirahat_toleransi_sebelum"), dr.Item("istirahat_toleransi_sesudah")), "MM/dd/yyyy HH:mm:ss")
                            Else
                                DBGrid.Rows(DBGrid.Rows.Count - 1).Cells("istirahat").Value = Format(timerounddown(dr.Item("istirahat_out"), dr.Item("istirahat_toleransi_sebelum"), dr.Item("istirahat_toleransi_sesudah")), "HH:mm:ss")
                                DBGrid.Rows(DBGrid.Rows.Count - 1).Cells("tanggalistirahat").Value = Format(timerounddown(dr.Item("istirahat_out"), dr.Item("istirahat_toleransi_sebelum"), dr.Item("istirahat_toleransi_sesudah")), "MM/dd/yyyy HH:mm:ss")
                            End If
                        End If
                    Else
                        DBGrid.Rows(DBGrid.Rows.Count - 1).Cells("istirahat").Value = ""
                    End If
                    If dr.Item("st_istirahat_in") Then
                        If dr.Item("kembali_pembulatan") Then
                            If dr.Item("kembali_arahpembulatan") Then
                                DBGrid.Rows(DBGrid.Rows.Count - 1).Cells("kembali").Value = Format(timeroundup(dr.Item("istirahat_in"), dr.Item("kembali_toleransi_sebelum"), dr.Item("kembali_toleransi_sesudah")), "HH:mm:ss")
                                DBGrid.Rows(DBGrid.Rows.Count - 1).Cells("tanggalkembali").Value = Format(timeroundup(dr.Item("istirahat_in"), dr.Item("kembali_toleransi_sebelum"), dr.Item("kembali_toleransi_sesudah")), "MM/dd/yyyy HH:mm:ss")
                            Else
                                DBGrid.Rows(DBGrid.Rows.Count - 1).Cells("kembali").Value = Format(timerounddown(dr.Item("istirahat_in"), dr.Item("kembali_toleransi_sebelum"), dr.Item("kembali_toleransi_sesudah")), "HH:mm:ss")
                                DBGrid.Rows(DBGrid.Rows.Count - 1).Cells("tanggalkembali").Value = Format(timerounddown(dr.Item("istirahat_in"), dr.Item("kembali_toleransi_sebelum"), dr.Item("kembali_toleransi_sesudah")), "MM/dd/yyyy HH:mm:ss")
                            End If
                        End If
                    Else
                        DBGrid.Rows(DBGrid.Rows.Count - 1).Cells(4).Value = ""
                    End If
                    If dr.Item("st_pulang") Then
                        If dr.Item("pulang_pembulatan") Then
                            If dr.Item("pulang_arahpembulatan") Then
                                DBGrid.Rows(DBGrid.Rows.Count - 1).Cells("pulang").Value = Format(timeroundup(dr.Item("pulang"), dr.Item("pulang_toleransi_sebelum"), dr.Item("pulang_toleransi_sesudah")), "HH:mm:ss")
                                DBGrid.Rows(DBGrid.Rows.Count - 1).Cells("tanggalpulang").Value = Format(timeroundup(dr.Item("pulang"), dr.Item("pulang_toleransi_sebelum"), dr.Item("pulang_toleransi_sesudah")), "MM/dd/yyyy HH:mm:ss")
                            Else
                                DBGrid.Rows(DBGrid.Rows.Count - 1).Cells("pulang").Value = Format(timerounddown(dr.Item("pulang"), dr.Item("pulang_toleransi_sebelum"), dr.Item("pulang_toleransi_sesudah")), "HH:mm:ss")
                                DBGrid.Rows(DBGrid.Rows.Count - 1).Cells("tanggalpulang").Value = Format(timerounddown(dr.Item("pulang"), dr.Item("pulang_toleransi_sebelum"), dr.Item("pulang_toleransi_sesudah")), "MM/dd/yyyy HH:mm:ss")
                            End If
                        End If
                    Else
                        DBGrid.Rows(DBGrid.Rows.Count - 1).Cells("pulang").Value = ""
                    End If
                    'DBGrid.Rows(DBGrid.Rows.Count - 1).Cells(6).Value = dr.Item("datang")
                    'DBGrid.Rows(DBGrid.Rows.Count - 1).Cells(7).Value = dr.Item("istirahat_out")
                    'DBGrid.Rows(DBGrid.Rows.Count - 1).Cells(8).Value = dr.Item("istirahat_in")
                    'DBGrid.Rows(DBGrid.Rows.Count - 1).Cells(9).Value = dr.Item("pulang")

                    If DBGrid.Rows(DBGrid.Rows.Count - 1).Cells("telat").Value <> "" Then
                        DBGrid.Rows(DBGrid.Rows.Count - 1).Cells("total").Value = DateDiff(DateInterval.Hour, CDate(Format(dr.Item("tanggal_masuk"), "yyyy/MM/dd HH:mm:ss")), DBGrid.Rows(DBGrid.Rows.Count - 1).Cells("tanggalmasuk").Value)
                    Else
                        DBGrid.Rows(DBGrid.Rows.Count - 1).Cells("total").Value = 0
                    End If
                    DBGrid.Rows(DBGrid.Rows.Count - 1).Cells("pc").Value = 0
                    If DBGrid.Rows(DBGrid.Rows.Count - 1).Cells("tanggalpulang").Value <> "" Then
                        If DBGrid.Rows(DBGrid.Rows.Count - 1).Cells("tanggalpulang").Value < CDate(Format(dr.Item("tanggal"), "yyyy/MM/dd") & " " & Format(dr.Item("pulang_jam"), "HH:mm:ss")) Then
                            DBGrid.Rows(DBGrid.Rows.Count - 1).Cells("pc").Value = DateDiff(DateInterval.Hour, DBGrid.Rows(DBGrid.Rows.Count - 1).Cells("tanggalpulang").Value, CDate(Format(dr.Item("tanggal"), "yyyy/MM/dd") & " " & Format(dr.Item("pulang_jam"), "HH:mm:ss")))
                        End If
                    End If
                    If Not dr.Item("st_datang") Or IsDBNull(dr.Item("st_datang")) Then DBGrid.Rows(DBGrid.Rows.Count - 1).Cells("Absen").Value = True Else DBGrid.Rows(DBGrid.Rows.Count - 1).Cells("Absen").Value = False
                    If DBGrid.Rows(DBGrid.Rows.Count - 1).Cells("pulang").Value <> "" And DBGrid.Rows(DBGrid.Rows.Count - 1).Cells("masuk").Value <> "" Then
                        DBGrid.Rows(DBGrid.Rows.Count - 1).Cells("total").Value = DateDiff(DateInterval.Hour, DBGrid.Rows(DBGrid.Rows.Count - 1).Cells("tanggalmasuk").Value, DBGrid.Rows(DBGrid.Rows.Count - 1).Cells("tanggalpulang").Value)

                    Else
                        DBGrid.Rows(DBGrid.Rows.Count - 1).Cells("total").Value = 0
                        DBGrid.Rows(DBGrid.Rows.Count - 1).Cells("lembur").Value = 0
                    End If
                    If IsNumeric(DBGrid.Rows(DBGrid.Rows.Count - 1).Cells("total").Value) Then
                        If DBGrid.Rows(DBGrid.Rows.Count - 1).Cells("total").Value > 12 Then
                            DBGrid.Rows(DBGrid.Rows.Count - 1).DefaultCellStyle.BackColor = Color.PaleVioletRed
                        End If
                    End If
                    If DBGrid.Rows(DBGrid.Rows.Count - 1).Cells("istirahat").Value <> "" And DBGrid.Rows(DBGrid.Rows.Count - 1).Cells("kembali").Value <> "" And DBGrid.Rows(DBGrid.Rows.Count - 1).Cells("total").Value > 0 Then
                        DBGrid.Rows(DBGrid.Rows.Count - 1).Cells("total").Value = DBGrid.Rows(DBGrid.Rows.Count - 1).Cells("total").Value - (DateDiff(DateInterval.Hour, DBGrid.Rows(DBGrid.Rows.Count - 1).Cells("tanggalistirahat").Value, DBGrid.Rows(DBGrid.Rows.Count - 1).Cells("tanggalkembali").Value))
                    End If
                    If DBGrid.Rows(DBGrid.Rows.Count - 1).Cells("total").Value > dr.Item("jumlah_jamkerja") Then DBGrid.Rows(DBGrid.Rows.Count - 1).Cells("lembur").Value = Format(DBGrid.Rows(DBGrid.Rows.Count - 1).Cells("total").Value - dr.Item("jumlah_jamkerja"), "#,##0")
                Catch ex As Exception
                End Try
            Else
                If Not IsDBNull(dr.Item("datang")) And dr.Item("st_datang") Then DBGrid.Rows(DBGrid.Rows.Count - 1).Cells("masuk").Value = Format(dr.Item("datang"), "HH:mm:ss")
                If Not IsDBNull(dr.Item("istirahat_out")) And dr.Item("st_istirahat_out") Then DBGrid.Rows(DBGrid.Rows.Count - 1).Cells("istirahat").Value = Format(dr.Item("istirahat_out"), "HH:mm:ss")
                If Not IsDBNull(dr.Item("istirahat_in")) And dr.Item("st_istirahat_in") Then DBGrid.Rows(DBGrid.Rows.Count - 1).Cells("kembali").Value = Format(dr.Item("istirahat_in"), "HH:mm:ss")
                If Not IsDBNull(dr.Item("pulang")) And dr.Item("st_pulang") Then DBGrid.Rows(DBGrid.Rows.Count - 1).Cells("pulang").Value = Format(dr.Item("pulang"), "HH:mm:ss")
                'If dr.Item("status_datang") Then DBGrid.Rows(DBGrid.Rows.Count - 1).Cells(2).Value = Format(dr.Item("datang"), "HH:mm:ss") Else DBGrid.Rows(DBGrid.Rows.Count - 1).Cells(2).Value = ""
                'If dr.Item("status_istirahat_out") Then DBGrid.Rows(DBGrid.Rows.Count - 1).Cells(3).Value = Format(dr.Item("istirahat"), "HH:mm:ss") Else DBGrid.Rows(DBGrid.Rows.Count - 1).Cells(3).Value = ""
                'If dr.Item("status_istirahat_in") Then DBGrid.Rows(DBGrid.Rows.Count - 1).Cells(4).Value = Format(dr.Item("kembali"), "HH:mm:ss") Else DBGrid.Rows(DBGrid.Rows.Count - 1).Cells(4).Value = ""
                'If dr.Item("status_pulang") Then DBGrid.Rows(DBGrid.Rows.Count - 1).Cells(5).Value = Format(dr.Item("pulang"), "HH:mm:ss") Else DBGrid.Rows(DBGrid.Rows.Count - 1).Cells(5).Value = ""

            End If

        End While

    End Sub

    Private Sub dtTanggal_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtAwal.ValueChanged
        refresh_grid()
    End Sub
    Private Sub refresh_grid()
        Dim cur As Integer
        Dim scroll As Integer
        cur = 0
        If DBGrid.Rows.Count > 0 Then
            If DBGrid.CurrentRow.Index > 0 Then cur = DBGrid.CurrentRow.Index
            scroll = DBGrid.FirstDisplayedScrollingRowIndex
        End If
        load_absensi()
        If cur > 0 Then
            DBGrid.CurrentCell = DBGrid.Item(0, cur)
            DBGrid.FirstDisplayedScrollingRowIndex = scroll
        End If
    End Sub

    Private Sub dtAkhir_ValueChanged(sender As System.Object, e As System.EventArgs) Handles dtAkhir.ValueChanged
        refresh_grid()
    End Sub

    Private Sub txtSearch_TextChanged(sender As System.Object, e As System.EventArgs) Handles txtSearch.TextChanged
        refresh_grid()
    End Sub
End Class