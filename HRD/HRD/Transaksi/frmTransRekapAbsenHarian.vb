﻿Imports System.Data.SqlClient
Imports System.Data.Sql
Imports System.IO
Imports System.Data.OleDb
Public Class frmTransRekapAbsenHarian
    Dim da As SqlCommand
    Dim dr As SqlDataReader
    Dim transaction As SqlTransaction
    Private Sub frmTransRekapAbsenHarian_KeyDown(sender As Object, e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        If e.KeyCode = Keys.Enter Then SendKeys.Send("{tab}")
        If e.KeyCode = Keys.Escape Then Me.Close()
    End Sub
    Private Sub frmTransAbsenHarian_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        dtTanggal.Value = Now
        loadComboBoxAll(cmbTipe, "tipe", " var_tipekaryawan", "", "tipe", "tipe")
        cmbdata.Text = "Semua"
        load_absensi()
        If default_editabsen Then
            chkEdit.Checked = True
            Me.DBGrid.Columns("TanggalMasuk").ReadOnly = True
            Me.DBGrid.Columns("TanggalPulang").ReadOnly = True
            btnSimpan.Enabled = False
        Else
            chkEdit.Checked = False
            Me.DBGrid.Columns("TanggalMasuk").ReadOnly = False
            Me.DBGrid.Columns("TanggalPulang").ReadOnly = False
            btnSimpan.Enabled = True
        End If

        If rekap_tglistirahat Then Me.DBGrid.Columns("TanggalIstirahat").Visible = False Else Me.DBGrid.Columns("TanggalIstirahat").Visible = True
        If rekap_tglkembali Then Me.DBGrid.Columns("TanggalKembali").Visible = False Else Me.DBGrid.Columns("TanggalKembali").Visible = True
        If rekap_total Then Me.DBGrid.Columns("Total").Visible = False Else Me.DBGrid.Columns("Total").Visible = True
        If rekap_PC Then Me.DBGrid.Columns("PC").Visible = False Else Me.DBGrid.Columns("PC").Visible = True
        If rekap_lembur Then Me.DBGrid.Columns("Lembur").Visible = False Else Me.DBGrid.Columns("Lembur").Visible = True
        If rekap_absen Then Me.DBGrid.Columns("Absen").Visible = False Else Me.DBGrid.Columns("Absen").Visible = True
        If rekap_shift Then Me.DBGrid.Columns("Shift").Visible = False Else Me.DBGrid.Columns("Shift").Visible = True

    End Sub
    Private Sub load_absensi()
        Dim conn As New SqlConnection
        Dim datatable1 As New DataTable
        conn.ConnectionString = strcon
        conn.Open()
        Dim tipe, query, data, key As String
        If cmbTipe.Text = "" Then
            tipe = ""
        Else
            tipe = " and t2.tipe='" & cmbTipe.Text & "'"
        End If
        If cmbdata.Text = "" Or cmbdata.Text = "Semua" Then
            data = ""
        Else
            data = " and t.status_datang='1'"
        End If
        If txtKeyword.Text = "" Then
            key = ""
        Else
            key = "and ( nama_depan like'%" & txtKeyword.Text & "%' or m.nik like'%" & txtKeyword.Text & "%' or finger_id like'%" & txtKeyword.Text & "%' )"
        End If
        query = "select *,isnull(status_datang,0) as st_datang,isnull(status_istirahat_in,0) as st_istirahat_in,isnull(status_istirahat_out,0) as st_istirahat_out,isnull(status_pulang,0) as st_pulang  " & _
                 "from ms_karyawan m left join t_rekapabsensi t on m.nik=t.nik  and convert(varchar(8),t.tanggal,112)='" & Format(dtTanggal.Value, "yyyyMMdd") & "' " & _
                 " left join ms_jamkerja j on j.nama_shift=t.nama_shift left join ms_karyawan_tipe t2 on m.nik=t2.nik where aktif='True' " & tipe & " " & data & " " & key & "  order by nama_depan"
        
        da = New SqlCommand(query, conn)
        dr = da.ExecuteReader
        DBGrid.Rows.Clear()
        While dr.Read
            DBGrid.Rows.Add()
            DBGrid.Columns("Total").ReadOnly = False
            DBGrid.Rows(DBGrid.Rows.Count - 1).Cells("NIK").Value = dr.Item(0)
            DBGrid.Rows(DBGrid.Rows.Count - 1).Cells("Nama").Value = dr.Item("nama_depan") & " " & dr.Item("nama_belakang")
            DBGrid.Rows(DBGrid.Rows.Count - 1).Cells("Shift").Value = IIf(IsDBNull(dr.Item("nama_shift")) = True, "", dr.Item("nama_shift"))
            If Not IsDBNull(dr.Item("nama_shift")) Then
                Try
                    If dr.Item("st_datang") Then
                        If dr.Item("masuk_pembulatan") Then
                            If dr.Item("masuk_arahpembulatan") Then
                                DBGrid.Rows(DBGrid.Rows.Count - 1).Cells("Masuk").Value = Format(timeroundup(dr.Item("datang"), dr.Item("masuk_toleransi_sebelum"), dr.Item("masuk_toleransi_sesudah")), "HH:mm:ss")
                                DBGrid.Rows(DBGrid.Rows.Count - 1).Cells("TanggalMasuk").Value = Format(timeroundup(dr.Item("datang"), dr.Item("masuk_toleransi_sebelum"), dr.Item("masuk_toleransi_sesudah")), "MM/dd/yyyy HH:mm:ss")
                            Else
                                DBGrid.Rows(DBGrid.Rows.Count - 1).Cells("Masuk").Value = Format(timerounddown(dr.Item("datang"), dr.Item("masuk_toleransi_sebelum"), dr.Item("masuk_toleransi_sesudah")), "HH:mm:ss")
                                DBGrid.Rows(DBGrid.Rows.Count - 1).Cells("TanggalMasuk").Value = Format(timerounddown(dr.Item("datang"), dr.Item("masuk_toleransi_sebelum"), dr.Item("masuk_toleransi_sesudah")), "MM/dd/yyyy HH:mm:ss")
                            End If
                        Else
                            DBGrid.Rows(DBGrid.Rows.Count - 1).Cells("Masuk").Value = Format(dr.Item("datang"), "HH:mm:ss")
                            DBGrid.Rows(DBGrid.Rows.Count - 1).Cells("TanggalMasuk").Value = Format(dr.Item("datang"), "MM/dd/yyyy HH:mm:ss")
                        End If
                    Else
                        DBGrid.Rows(DBGrid.Rows.Count - 1).Cells("Masuk").Value = ""
                    End If
                    If dr.Item("st_istirahat_out") Then
                        If dr.Item("istirahat_pembulatan") Then
                            If dr.Item("istirahat_arahpembulatan") Then
                                DBGrid.Rows(DBGrid.Rows.Count - 1).Cells("Istirahat").Value = Format(timeroundup(dr.Item("istirahat_out"), dr.Item("istirahat_toleransi_sebelum"), dr.Item("istirahat_toleransi_sesudah")), "HH:mm:ss")
                                DBGrid.Rows(DBGrid.Rows.Count - 1).Cells("TanggalIstirahat").Value = Format(timeroundup(dr.Item("istirahat_out"), dr.Item("istirahat_toleransi_sebelum"), dr.Item("istirahat_toleransi_sesudah")), "MM/dd/yyyy HH:mm:ss")
                            Else
                                DBGrid.Rows(DBGrid.Rows.Count - 1).Cells("Istirahat").Value = Format(timerounddown(dr.Item("istirahat_out"), dr.Item("istirahat_toleransi_sebelum"), dr.Item("istirahat_toleransi_sesudah")), "HH:mm:ss")
                                DBGrid.Rows(DBGrid.Rows.Count - 1).Cells("TanggalIstirahat").Value = Format(timerounddown(dr.Item("istirahat_out"), dr.Item("istirahat_toleransi_sebelum"), dr.Item("istirahat_toleransi_sesudah")), "MM/dd/yyyy HH:mm:ss")
                            End If
                        Else
                            DBGrid.Rows(DBGrid.Rows.Count - 1).Cells("Istirahat").Value = Format(dr.Item("istirahat_out"), "HH:mm:ss")
                            DBGrid.Rows(DBGrid.Rows.Count - 1).Cells("TanggalIstirahat").Value = Format(dr.Item("istirahat_out"), "MM/dd/yyyy HH:mm:ss")
                        End If
                    Else
                        DBGrid.Rows(DBGrid.Rows.Count - 1).Cells("Istirahat").Value = ""
                    End If
                    If dr.Item("st_istirahat_in") Then
                        If dr.Item("kembali_pembulatan") Then
                            If dr.Item("kembali_arahpembulatan") Then
                                DBGrid.Rows(DBGrid.Rows.Count - 1).Cells("Kembali").Value = Format(timeroundup(dr.Item("istirahat_in"), dr.Item("kembali_toleransi_sebelum"), dr.Item("kembali_toleransi_sesudah")), "HH:mm:ss")
                                DBGrid.Rows(DBGrid.Rows.Count - 1).Cells("TanggalKembali").Value = Format(timeroundup(dr.Item("istirahat_in"), dr.Item("kembali_toleransi_sebelum"), dr.Item("kembali_toleransi_sesudah")), "MM/dd/yyyy HH:mm:ss")
                            Else
                                DBGrid.Rows(DBGrid.Rows.Count - 1).Cells("Kembali").Value = Format(timerounddown(dr.Item("istirahat_in"), dr.Item("kembali_toleransi_sebelum"), dr.Item("kembali_toleransi_sesudah")), "HH:mm:ss")
                                DBGrid.Rows(DBGrid.Rows.Count - 1).Cells("TanggalKembali").Value = Format(timerounddown(dr.Item("istirahat_in"), dr.Item("kembali_toleransi_sebelum"), dr.Item("kembali_toleransi_sesudah")), "MM/dd/yyyy HH:mm:ss")
                            End If
                        Else
                            DBGrid.Rows(DBGrid.Rows.Count - 1).Cells("Kembali").Value = Format(dr.Item("istirahat_in"), "HH:mm:ss")
                            DBGrid.Rows(DBGrid.Rows.Count - 1).Cells("TanggalKembali").Value = Format(dr.Item("istirahat_in"), "MM/dd/yyyy HH:mm:ss")
                        End If
                    Else
                        DBGrid.Rows(DBGrid.Rows.Count - 1).Cells("Kembali").Value = ""
                    End If
                    If dr.Item("st_pulang") Then
                        If dr.Item("pulang_pembulatan") Then
                            If dr.Item("pulang_arahpembulatan") Then
                                DBGrid.Rows(DBGrid.Rows.Count - 1).Cells("Pulang").Value = Format(timeroundup(dr.Item("pulang"), dr.Item("pulang_toleransi_sebelum"), dr.Item("pulang_toleransi_sesudah")), "HH:mm:ss")
                                DBGrid.Rows(DBGrid.Rows.Count - 1).Cells("TanggalPulang").Value = Format(timeroundup(dr.Item("pulang"), dr.Item("pulang_toleransi_sebelum"), dr.Item("pulang_toleransi_sesudah")), "MM/dd/yyyy HH:mm:ss")
                            Else
                                DBGrid.Rows(DBGrid.Rows.Count - 1).Cells("Pulang").Value = Format(timerounddown(dr.Item("pulang"), dr.Item("pulang_toleransi_sebelum"), dr.Item("pulang_toleransi_sesudah")), "HH:mm:ss")
                                DBGrid.Rows(DBGrid.Rows.Count - 1).Cells("TanggalPulang").Value = Format(timerounddown(dr.Item("pulang"), dr.Item("pulang_toleransi_sebelum"), dr.Item("pulang_toleransi_sesudah")), "MM/dd/yyyy HH:mm:ss")
                            End If
                        Else
                            DBGrid.Rows(DBGrid.Rows.Count - 1).Cells("Pulang").Value = Format(dr.Item("pulang"), "HH:mm:ss")
                            DBGrid.Rows(DBGrid.Rows.Count - 1).Cells("TanggalPulang").Value = Format(dr.Item("pulang"), "MM/dd/yyyy HH:mm:ss")
                        End If
                    Else
                        DBGrid.Rows(DBGrid.Rows.Count - 1).Cells("Pulang").Value = ""
                    End If

                    If DBGrid.Rows(DBGrid.Rows.Count - 1).Cells("TanggalMasuk").Value <> "" Then
                        DBGrid.Rows(DBGrid.Rows.Count - 1).Cells("Telat").Value = DateDiff(DateInterval.Minute, CDate(Format(dtTanggal.Value, "yyyy/MM/dd") & " " & Format(dr.Item("masuk_jam"), "HH:mm:ss")), CDate(DBGrid.Rows(DBGrid.Rows.Count - 1).Cells("TanggalMasuk").Value))
                        If DBGrid.Rows(DBGrid.Rows.Count - 1).Cells("Telat").Value < 0 Then DBGrid.Rows(DBGrid.Rows.Count - 1).Cells("Telat").Value = 0
                        If Math.Abs(DBGrid.Rows(DBGrid.Rows.Count - 1).Cells("Telat").Value) / 60 > 1 And DBGrid.Rows(DBGrid.Rows.Count - 1).Cells("Telat").Value <> 0 Then
                            DBGrid.Rows(DBGrid.Rows.Count - 1).Cells("Telat").Value = CStr(Math.Floor(Math.Abs(DBGrid.Rows(DBGrid.Rows.Count - 1).Cells("Telat").Value) / 60)) + " h " + CStr(Math.Abs(DBGrid.Rows(DBGrid.Rows.Count - 1).Cells("Telat").Value) Mod 60) + " m"
                        ElseIf DBGrid.Rows(DBGrid.Rows.Count - 1).Cells("Telat").Value <> 0 Then
                            DBGrid.Rows(DBGrid.Rows.Count - 1).Cells("Telat").Value = CStr(Math.Abs(DBGrid.Rows(DBGrid.Rows.Count - 1).Cells("Telat").Value) Mod 60) + " m"
                        End If
                    End If
                    DBGrid.Rows(DBGrid.Rows.Count - 1).Cells("PC").Value = 0
                    If DBGrid.Rows(DBGrid.Rows.Count - 1).Cells("TanggalPulang").Value <> "" Then
                        DBGrid.Rows(DBGrid.Rows.Count - 1).Cells("PC").Value = DateDiff(DateInterval.Minute, DBGrid.Rows(DBGrid.Rows.Count - 1).Cells("TanggalPulang").Value, CDate(Format(dtTanggal.Value, "yyyy/MM/dd") & " " & Format(dr.Item("pulang_jam"), "HH:mm:ss")))
                        If DBGrid.Rows(DBGrid.Rows.Count - 1).Cells("PC").Value < 0 Then DBGrid.Rows(DBGrid.Rows.Count - 1).Cells("PC").Value = 0
                        If Math.Abs(DBGrid.Rows(DBGrid.Rows.Count - 1).Cells("PC").Value) / 60 > 1 And DBGrid.Rows(DBGrid.Rows.Count - 1).Cells("PC").Value <> 0 Then
                            DBGrid.Rows(DBGrid.Rows.Count - 1).Cells("PC").Value = CStr(Math.Floor(Math.Abs(DBGrid.Rows(DBGrid.Rows.Count - 1).Cells("PC").Value) / 60)) + " h " + CStr(Math.Abs(DBGrid.Rows(DBGrid.Rows.Count - 1).Cells("PC").Value) Mod 60) + " m"
                        ElseIf DBGrid.Rows(DBGrid.Rows.Count - 1).Cells("PC").Value <> 0 Then
                            DBGrid.Rows(DBGrid.Rows.Count - 1).Cells("PC").Value = CStr(Math.Abs(DBGrid.Rows(DBGrid.Rows.Count - 1).Cells("PC").Value) Mod 60) + " m"
                        End If
                    End If
                    If Not dr.Item("st_datang") Or IsDBNull(dr.Item("st_datang")) Then DBGrid.Rows(DBGrid.Rows.Count - 1).Cells(14).Value = True Else DBGrid.Rows(DBGrid.Rows.Count - 1).Cells(14).Value = False
                    If DBGrid.Rows(DBGrid.Rows.Count - 1).Cells("Pulang").Value <> "" And DBGrid.Rows(DBGrid.Rows.Count - 1).Cells("Masuk").Value <> "" Then
                        DBGrid.Rows(DBGrid.Rows.Count - 1).Cells("Total").Value = DateDiff(DateInterval.Hour, DBGrid.Rows(DBGrid.Rows.Count - 1).Cells("TanggalMasuk").Value, DBGrid.Rows(DBGrid.Rows.Count - 1).Cells("TanggalPulang").Value)
                    Else
                        DBGrid.Rows(DBGrid.Rows.Count - 1).Cells("Total").Value = 0
                        DBGrid.Rows(DBGrid.Rows.Count - 1).Cells("Lembur").Value = 0
                    End If
                    If IsNumeric(DBGrid.Rows(DBGrid.Rows.Count - 1).Cells("Total").Value) Then
                        If DBGrid.Rows(DBGrid.Rows.Count - 1).Cells("Total").Value < dr.Item("jumlah_jamkerja") Then
                            DBGrid.Rows(DBGrid.Rows.Count - 1).DefaultCellStyle.BackColor = Color.Khaki
                        End If
                    End If
                    If DBGrid.Rows(DBGrid.Rows.Count - 1).Cells("Masuk").Value = DBGrid.Rows(DBGrid.Rows.Count - 1).Cells("Pulang").Value Then
                        DBGrid.Rows(DBGrid.Rows.Count - 1).DefaultCellStyle.BackColor = Color.PaleVioletRed
                        DBGrid.Rows(DBGrid.Rows.Count - 1).Cells("Telat").Value = 0
                        DBGrid.Rows(DBGrid.Rows.Count - 1).Cells("PC").Value = 0
                    End If
                    If DBGrid.Rows(DBGrid.Rows.Count - 1).Cells("Istirahat").Value <> "" And DBGrid.Rows(DBGrid.Rows.Count - 1).Cells("Kembali").Value <> "" And DBGrid.Rows(DBGrid.Rows.Count - 1).Cells("Total").Value > 0 Then
                        DBGrid.Rows(DBGrid.Rows.Count - 1).Cells("Total").Value = DBGrid.Rows(DBGrid.Rows.Count - 1).Cells("Total").Value - (DateDiff(DateInterval.Hour, DBGrid.Rows(DBGrid.Rows.Count - 1).Cells("TanggalIstirahat").Value, DBGrid.Rows(DBGrid.Rows.Count - 1).Cells("TanggalKembali").Value))
                    End If
                    If DBGrid.Rows(DBGrid.Rows.Count - 1).Cells("Total").Value > dr.Item("jumlah_jamkerja") Then
                        DBGrid.Rows(DBGrid.Rows.Count - 1).Cells("Lembur").Value = Format(DBGrid.Rows(DBGrid.Rows.Count - 1).Cells("Total").Value - dr.Item("jumlah_jamkerja"), "#,##0")
                    Else
                        DBGrid.Rows(DBGrid.Rows.Count - 1).Cells("Lembur").Value = 0
                    End If
                    If Str(dr.Item("Total")) <> "Null" Then DBGrid.Rows(DBGrid.Rows.Count - 1).Cells("Total").Value = dr.Item("Total")
                    If Str(dr.Item("Lembur")) <> "Null" Then DBGrid.Rows(DBGrid.Rows.Count - 1).Cells("Lembur").Value = dr.Item("Lembur")

                Catch ex As Exception
                End Try
            Else
                If Not IsDBNull(dr.Item("datang")) And dr.Item("st_datang") Then
                    DBGrid.Rows(DBGrid.Rows.Count - 1).Cells("Masuk").Value = Format(dr.Item("datang"), "HH:mm:ss")
                    DBGrid.Rows(DBGrid.Rows.Count - 1).Cells("TanggalMasuk").Value = Format(dr.Item("datang"), "MM/dd/yyyy HH:mm:ss")
                End If
                If Not IsDBNull(dr.Item("istirahat_out")) And dr.Item("st_istirahat_out") Then
                    DBGrid.Rows(DBGrid.Rows.Count - 1).Cells("Istirahat").Value = Format(dr.Item("istirahat_out"), "HH:mm:ss")
                    DBGrid.Rows(DBGrid.Rows.Count - 1).Cells("TanggalIstitrahat").Value = Format(dr.Item("datang"), "MM/dd/yyyy HH:mm:ss")
                End If
                If Not IsDBNull(dr.Item("istirahat_in")) And dr.Item("st_istirahat_in") Then
                    DBGrid.Rows(DBGrid.Rows.Count - 1).Cells("Kembali").Value = Format(dr.Item("istirahat_in"), "HH:mm:ss")
                    DBGrid.Rows(DBGrid.Rows.Count - 1).Cells("TanggalKembali").Value = Format(dr.Item("datang"), "MM/dd/yyyy HH:mm:ss")
                End If
                If Not IsDBNull(dr.Item("pulang")) And dr.Item("st_pulang") Then
                    DBGrid.Rows(DBGrid.Rows.Count - 1).Cells("Pulang").Value = Format(dr.Item("pulang"), "HH:mm:ss")
                    DBGrid.Rows(DBGrid.Rows.Count - 1).Cells("TanggalPulang").Value = Format(dr.Item("datang"), "MM/dd/yyyy HH:mm:ss")
                End If
            End If

        End While
        Label4.Text = DBGrid.Rows.Count
    End Sub

    Private Sub dtTanggal_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtTanggal.ValueChanged
        Dim cur As Integer
        Dim scroll As Integer
        cur = 0
        If DBGrid.Rows.Count > 0 Then
            If DBGrid.CurrentRow.Index > 0 Then cur = DBGrid.CurrentRow.Index
            scroll = DBGrid.FirstDisplayedScrollingRowIndex
        End If
        load_absensi()
        If cur > 0 Then
            DBGrid.CurrentCell = DBGrid.Item(0, cur)
            DBGrid.FirstDisplayedScrollingRowIndex = scroll
        End If
    End Sub

    Private Sub btnSimpan_Click(sender As System.Object, e As System.EventArgs) Handles btnSimpan.Click
        simpan()
        load_absensi()
    End Sub

    Private Function simpan() As Boolean
        simpan = False
        Dim cmd As SqlCommand
        Dim conn As New SqlConnection
        Conn.ConnectionString = strcon
        conn.Open()
        transaction = conn.BeginTransaction(IsolationLevel.ReadCommitted)
        Try
            cmd = New SqlCommand("delete from t_rekapabsensi where convert(varchar(8),tanggal,112)='" & Format(dtTanggal.Value, "yyyyMMdd") & "'; " & _
                                 "delete from t_jadwalkerja where convert(varchar(8),tanggal,112)='" & Format(dtTanggal.Value, "yyyyMMdd") & "'; ", conn, transaction)
            cmd.ExecuteNonQuery()
            Dim sqldetail As String = Nothing
            For i As Integer = 0 To DBGrid.RowCount - 1
                If DBGrid.Item("TanggalMasuk", i).Value <> "" And DBGrid.Item("TanggalPulang", i).Value <> "" Then
                    sqldetail += "if not exists (select * from t_clockin where nik='" & DBGrid.Item("NIK", i).Value & "' and replace(convert(varchar(19),waktu,120),'-','')='" & Format(CDate(DBGrid.Item("TanggalMasuk", i).Value), "yyyyMMdd HH:mm:ss") & "') insert into t_clockin (nik,waktu,rekap,manual,userid) values ('" & DBGrid.Item("NIK", i).Value & "','" & Format(CDate(DBGrid.Item("TanggalMasuk", i).Value), "yyyy/MM/dd HH:mm:ss") & "',0,1,'" & username & "');"
                    sqldetail += "if not exists (select * from t_clockin where nik='" & DBGrid.Item("NIK", i).Value & "' and replace(convert(varchar(19),waktu,120),'-','')='" & Format(CDate(DBGrid.Item("TanggalPulang", i).Value), "yyyyMMdd HH:mm:ss") & "') insert into t_clockin (nik,waktu,rekap,manual,userid) values ('" & DBGrid.Item("NIK", i).Value & "','" & Format(CDate(DBGrid.Item("TanggalPulang", i).Value), "yyyy/MM/dd HH:mm:ss") & "',0,1,'" & username & "');"
                End If
            Next
            sqldetail += "update t_clockin set rekap=0 where convert(varchar(8),waktu,112)='" & Format(dtTanggal.Value, "yyyyMMdd") & "';"
            If sqldetail <> Nothing Then
                cmd = New SqlCommand(sqldetail, conn, transaction)
                cmd.CommandTimeout = 0
                cmd.ExecuteNonQuery()
            End If

            cmd = New SqlCommand("exec sp_AutoRekapAbsensi '" & Format(dtTanggal.Value, "yyyyMMdd") & "','" & Format(dtTanggal.Value, "yyyyMMdd") & "'", conn, transaction)
            cmd.CommandTimeout = 0
            cmd.ExecuteNonQuery()
            cmd = New SqlCommand("exec sp_autoshift1 '" & Format(dtTanggal.Value, "yyyyMMdd") & "','" & Format(dtTanggal.Value, "yyyyMMdd") & "'", conn, transaction)
            cmd.CommandTimeout = 0
            cmd.ExecuteNonQuery()

            transaction.Commit()
            conn.Close()
            MsgBox("Data sudah tersimpan")

        Catch ex As Exception
            MessageBox.Show(ex.Message.ToString)
            If reader IsNot Nothing Then reader.Close()
            transaction.Rollback()
            If conn.State Then conn.Close()
        End Try
    End Function

    Private Function add_dataabsen(ByVal row As Integer) As String
        Dim fields() As String
        Dim nilai() As String
        Dim table_name As String

        ReDim fields(11)
        ReDim nilai(11)

        table_name = "t_rekapabsensi"
        fields(0) = "nik"
        fields(1) = "tanggal"
        fields(2) = "datang"
        fields(3) = "istirahat_out"
        fields(4) = "istirahat_in"
        fields(5) = "pulang"
        fields(6) = "status_datang"
        fields(7) = "status_istirahat_in"
        fields(8) = "status_istirahat_out"
        fields(9) = "status_pulang"
        fields(10) = "nama_shift"

        nilai(0) = DBGrid.Item("NIK", row).Value
        nilai(1) = dtTanggal.Value.ToString("yyyy/MM/dd")
        nilai(2) = Format(CDate(DBGrid.Item("TanggalMasuk", row).Value), "yyyy/MM/dd HH:mm:ss")
        nilai(3) = IIf(DBGrid.Item("TanggalIstirahat", row).Value = "", nilai(1), Format(CDate(DBGrid.Item("TanggalIstirahat", row).Value), "yyyy/MM/dd HH:mm:ss"))
        nilai(4) = IIf(DBGrid.Item("TanggalKembali", row).Value = "", nilai(1), Format(CDate(DBGrid.Item("TanggalKembali", row).Value), "yyyy/MM/dd HH:mm:ss"))
        nilai(5) = Format(CDate(DBGrid.Item("TanggalPulang", row).Value), "yyyy/MM/dd HH:mm:ss")
        nilai(6) = IIf(DBGrid.Item("TanggalMasuk", row).Value <> "", 1, 0)
        nilai(7) = IIf(DBGrid.Item("TanggalIstirahat", row).Value <> "", 1, 0)
        nilai(8) = IIf(DBGrid.Item("TanggalKembali", row).Value <> "", 1, 0)
        nilai(9) = IIf(DBGrid.Item("TanggalPulang", row).Value <> "", 1, 0)
        nilai(10) = DBGrid.Item("Shift", row).Value

        add_dataabsen = query_tambah_data(table_name, fields, nilai)
    End Function

    Private Sub cmbTipe_SelectionChangeCommitted(sender As Object, e As System.EventArgs) Handles cmbTipe.SelectionChangeCommitted
        load_absensi()
    End Sub

    Private Sub cmbdata_SelectionChangeCommitted(sender As Object, e As System.EventArgs) Handles cmbdata.SelectionChangeCommitted
        load_absensi()
    End Sub

    Private Sub txtKeyword_KeyDown(sender As Object, e As System.Windows.Forms.KeyEventArgs) Handles txtKeyword.KeyDown
        If e.KeyCode = Keys.Enter Then
            load_absensi()
        End If
    End Sub

    Private Sub unlock
        If chkEdit.Checked = False Then
            Dim ch As New frmChallengeLogin
            ch.namagroup = ""
            ch.koordinator = True
            ch.keterangan = "Edit Absensi"
            ch.ShowDialog()
            If ch.LoginSucceeded = True Then
                userchallenge = ""
                Me.DBGrid.Columns("TanggalMasuk").ReadOnly = False
                Me.DBGrid.Columns("TanggalPulang").ReadOnly = False
                btnSimpan.Enabled = True
            Else
                chkEdit.Checked = True
            End If
        Else
            Me.DBGrid.Columns("TanggalMasuk").ReadOnly = False
            Me.DBGrid.Columns("TanggalPulang").ReadOnly = False
            btnSimpan.Enabled = True
        End If
    End Sub

    Private Sub chkEdit_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles chkEdit.CheckedChanged
        If default_editabsen And chkEdit.Checked = False Then unlock()
        If chkEdit.Checked = True Then
            Me.DBGrid.Columns("TanggalMasuk").ReadOnly = True
            Me.DBGrid.Columns("TanggalPulang").ReadOnly = True
            btnSimpan.Enabled = False
        End If
    End Sub
End Class