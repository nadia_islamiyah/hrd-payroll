﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmTransPayroll
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.cmbTemplate = New System.Windows.Forms.ComboBox()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.btnSearch = New System.Windows.Forms.Button()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.lblNoTrans = New System.Windows.Forms.Label()
        Me.dtAwal = New System.Windows.Forms.DateTimePicker()
        Me.dtAkhir = New System.Windows.Forms.DateTimePicker()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.DBGridKomponen = New System.Windows.Forms.DataGridView()
        Me.KodeKomponen = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Komponen = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.C = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.Harian = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.Jam = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.Lembur = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.MasukPenuh = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.Manual = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.Libur = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.Custom = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.Custom_name = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.normal = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.DBGrid = New System.Windows.Forms.DataGridView()
        Me.NIK = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Nama = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.cmbGroupKerja = New System.Windows.Forms.ComboBox()
        Me.Label36 = New System.Windows.Forms.Label()
        Me.cmbDivisi = New System.Windows.Forms.ComboBox()
        Me.Label35 = New System.Windows.Forms.Label()
        Me.cmbArea = New System.Windows.Forms.ComboBox()
        Me.cmbTipe = New System.Windows.Forms.ComboBox()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.btnProcess = New System.Windows.Forms.Button()
        Me.DBGridAbsensi = New System.Windows.Forms.DataGridView()
        Me.btnExit = New System.Windows.Forms.Button()
        Me.btnHapus = New System.Windows.Forms.Button()
        Me.btnSave = New System.Windows.Forms.Button()
        Me.lblTotal = New System.Windows.Forms.Label()
        Me.lbl100000 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.lbl50000 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.lbl20000 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.lbl10000 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.lbl5000 = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.lbl2000 = New System.Windows.Forms.Label()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.lbl1000 = New System.Windows.Forms.Label()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.lbl500 = New System.Windows.Forms.Label()
        Me.btnPrint = New System.Windows.Forms.Button()
        Me.btnPrintTTD = New System.Windows.Forms.Button()
        Me.btnPrintAbsensi = New System.Windows.Forms.Button()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.lbl100 = New System.Windows.Forms.Label()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.lbl200 = New System.Windows.Forms.Label()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.btnPrintGaji = New System.Windows.Forms.Button()
        Me.txtKeterangan = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Masuk = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Jamkerja = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Total = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Istirahat = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Kerja = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.harilembur = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.jamlembur = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.jumlahlibur = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.JamLibur = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.jmltelat = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.jmlPC = New System.Windows.Forms.DataGridViewTextBoxColumn()
        CType(Me.DBGridKomponen, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DBGrid, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DBGridAbsensi, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.Black
        Me.Label1.Location = New System.Drawing.Point(17, 41)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(51, 16)
        Me.Label1.TabIndex = 95
        Me.Label1.Text = "Periode"
        '
        'cmbTemplate
        '
        Me.cmbTemplate.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbTemplate.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbTemplate.FormattingEnabled = True
        Me.cmbTemplate.Items.AddRange(New Object() {"Pendapatan", "Potongan"})
        Me.cmbTemplate.Location = New System.Drawing.Point(96, 70)
        Me.cmbTemplate.Name = "cmbTemplate"
        Me.cmbTemplate.Size = New System.Drawing.Size(150, 22)
        Me.cmbTemplate.TabIndex = 91
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        Me.Label26.BackColor = System.Drawing.Color.Transparent
        Me.Label26.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label26.ForeColor = System.Drawing.Color.Black
        Me.Label26.Location = New System.Drawing.Point(17, 72)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(36, 16)
        Me.Label26.TabIndex = 94
        Me.Label26.Text = "Jenis"
        '
        'btnSearch
        '
        Me.btnSearch.Location = New System.Drawing.Point(228, 12)
        Me.btnSearch.Name = "btnSearch"
        Me.btnSearch.Size = New System.Drawing.Size(37, 23)
        Me.btnSearch.TabIndex = 93
        Me.btnSearch.Text = "F5"
        Me.btnSearch.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.BackColor = System.Drawing.Color.Transparent
        Me.Label2.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.Black
        Me.Label2.Location = New System.Drawing.Point(17, 12)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(65, 16)
        Me.Label2.TabIndex = 92
        Me.Label2.Text = "No Payroll"
        '
        'lblNoTrans
        '
        Me.lblNoTrans.AutoSize = True
        Me.lblNoTrans.BackColor = System.Drawing.Color.Transparent
        Me.lblNoTrans.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNoTrans.ForeColor = System.Drawing.Color.Black
        Me.lblNoTrans.Location = New System.Drawing.Point(96, 12)
        Me.lblNoTrans.Name = "lblNoTrans"
        Me.lblNoTrans.Size = New System.Drawing.Size(0, 16)
        Me.lblNoTrans.TabIndex = 96
        '
        'dtAwal
        '
        Me.dtAwal.CustomFormat = "dd/MM/yyyy"
        Me.dtAwal.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtAwal.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtAwal.Location = New System.Drawing.Point(96, 38)
        Me.dtAwal.Name = "dtAwal"
        Me.dtAwal.Size = New System.Drawing.Size(94, 23)
        Me.dtAwal.TabIndex = 97
        Me.dtAwal.Value = New Date(2013, 3, 2, 0, 0, 0, 0)
        '
        'dtAkhir
        '
        Me.dtAkhir.CustomFormat = "dd/MM/yyyy"
        Me.dtAkhir.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtAkhir.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtAkhir.Location = New System.Drawing.Point(209, 38)
        Me.dtAkhir.Name = "dtAkhir"
        Me.dtAkhir.Size = New System.Drawing.Size(89, 23)
        Me.dtAkhir.TabIndex = 98
        Me.dtAkhir.Value = New Date(2013, 3, 2, 0, 0, 0, 0)
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.BackColor = System.Drawing.Color.Transparent
        Me.Label3.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.Black
        Me.Label3.Location = New System.Drawing.Point(190, 41)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(13, 16)
        Me.Label3.TabIndex = 99
        Me.Label3.Text = "-"
        '
        'DBGridKomponen
        '
        Me.DBGridKomponen.AllowUserToAddRows = False
        Me.DBGridKomponen.AllowUserToDeleteRows = False
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DBGridKomponen.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.DBGridKomponen.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DBGridKomponen.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.KodeKomponen, Me.Komponen, Me.C, Me.Harian, Me.Jam, Me.Lembur, Me.MasukPenuh, Me.Manual, Me.Libur, Me.Custom, Me.Custom_name, Me.normal})
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DBGridKomponen.DefaultCellStyle = DataGridViewCellStyle2
        Me.DBGridKomponen.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnKeystroke
        Me.DBGridKomponen.Location = New System.Drawing.Point(554, 4)
        Me.DBGridKomponen.Name = "DBGridKomponen"
        Me.DBGridKomponen.RowHeadersVisible = False
        Me.DBGridKomponen.Size = New System.Drawing.Size(407, 150)
        Me.DBGridKomponen.TabIndex = 104
        '
        'KodeKomponen
        '
        Me.KodeKomponen.HeaderText = "Kode"
        Me.KodeKomponen.Name = "KodeKomponen"
        Me.KodeKomponen.Visible = False
        '
        'Komponen
        '
        Me.Komponen.HeaderText = "Komponen"
        Me.Komponen.Name = "Komponen"
        Me.Komponen.ReadOnly = True
        Me.Komponen.Width = 120
        '
        'C
        '
        Me.C.HeaderText = ""
        Me.C.Name = "C"
        Me.C.Width = 40
        '
        'Harian
        '
        Me.Harian.HeaderText = "Harian"
        Me.Harian.Name = "Harian"
        Me.Harian.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Harian.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.Harian.Width = 60
        '
        'Jam
        '
        Me.Jam.HeaderText = "Jam"
        Me.Jam.Name = "Jam"
        Me.Jam.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Jam.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.Jam.Width = 60
        '
        'Lembur
        '
        Me.Lembur.HeaderText = "Lembur"
        Me.Lembur.Name = "Lembur"
        Me.Lembur.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Lembur.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.Lembur.Visible = False
        Me.Lembur.Width = 60
        '
        'MasukPenuh
        '
        Me.MasukPenuh.HeaderText = "MasukPenuh"
        Me.MasukPenuh.Name = "MasukPenuh"
        Me.MasukPenuh.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.MasukPenuh.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.MasukPenuh.Visible = False
        '
        'Manual
        '
        Me.Manual.HeaderText = "Manual"
        Me.Manual.Name = "Manual"
        Me.Manual.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Manual.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.Manual.Visible = False
        '
        'Libur
        '
        Me.Libur.HeaderText = "Libur"
        Me.Libur.Name = "Libur"
        Me.Libur.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Libur.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.Libur.Width = 60
        '
        'Custom
        '
        Me.Custom.HeaderText = "Custom"
        Me.Custom.Name = "Custom"
        Me.Custom.Visible = False
        '
        'Custom_name
        '
        Me.Custom_name.HeaderText = "Custom_name"
        Me.Custom_name.Name = "Custom_name"
        Me.Custom_name.Visible = False
        '
        'normal
        '
        Me.normal.HeaderText = "Normal"
        Me.normal.Name = "normal"
        Me.normal.Width = 60
        '
        'DBGrid
        '
        Me.DBGrid.AllowUserToAddRows = False
        Me.DBGrid.AllowUserToDeleteRows = False
        Me.DBGrid.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DBGrid.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle3
        Me.DBGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DBGrid.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.NIK, Me.Nama})
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DBGrid.DefaultCellStyle = DataGridViewCellStyle4
        Me.DBGrid.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnKeystroke
        Me.DBGrid.Location = New System.Drawing.Point(20, 160)
        Me.DBGrid.Name = "DBGrid"
        Me.DBGrid.RowHeadersVisible = False
        Me.DBGrid.Size = New System.Drawing.Size(941, 364)
        Me.DBGrid.TabIndex = 105
        '
        'NIK
        '
        Me.NIK.Frozen = True
        Me.NIK.HeaderText = "NIK"
        Me.NIK.Name = "NIK"
        Me.NIK.ReadOnly = True
        '
        'Nama
        '
        Me.Nama.Frozen = True
        Me.Nama.HeaderText = "Nama"
        Me.Nama.Name = "Nama"
        Me.Nama.ReadOnly = True
        Me.Nama.Width = 180
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.BackColor = System.Drawing.Color.Transparent
        Me.Label4.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.Black
        Me.Label4.Location = New System.Drawing.Point(17, 102)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(73, 16)
        Me.Label4.TabIndex = 107
        Me.Label4.Text = "Keterangan"
        '
        'cmbGroupKerja
        '
        Me.cmbGroupKerja.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbGroupKerja.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbGroupKerja.FormattingEnabled = True
        Me.cmbGroupKerja.Location = New System.Drawing.Point(416, 98)
        Me.cmbGroupKerja.Name = "cmbGroupKerja"
        Me.cmbGroupKerja.Size = New System.Drawing.Size(125, 22)
        Me.cmbGroupKerja.TabIndex = 114
        '
        'Label36
        '
        Me.Label36.AutoSize = True
        Me.Label36.BackColor = System.Drawing.Color.Transparent
        Me.Label36.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label36.ForeColor = System.Drawing.Color.Black
        Me.Label36.Location = New System.Drawing.Point(318, 101)
        Me.Label36.Name = "Label36"
        Me.Label36.Size = New System.Drawing.Size(68, 14)
        Me.Label36.TabIndex = 115
        Me.Label36.Text = "Workgroup"
        '
        'cmbDivisi
        '
        Me.cmbDivisi.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbDivisi.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbDivisi.FormattingEnabled = True
        Me.cmbDivisi.Location = New System.Drawing.Point(416, 68)
        Me.cmbDivisi.Name = "cmbDivisi"
        Me.cmbDivisi.Size = New System.Drawing.Size(125, 22)
        Me.cmbDivisi.TabIndex = 112
        '
        'Label35
        '
        Me.Label35.AutoSize = True
        Me.Label35.BackColor = System.Drawing.Color.Transparent
        Me.Label35.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label35.ForeColor = System.Drawing.Color.Black
        Me.Label35.Location = New System.Drawing.Point(318, 71)
        Me.Label35.Name = "Label35"
        Me.Label35.Size = New System.Drawing.Size(32, 14)
        Me.Label35.TabIndex = 113
        Me.Label35.Text = "Divisi"
        '
        'cmbArea
        '
        Me.cmbArea.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbArea.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbArea.FormattingEnabled = True
        Me.cmbArea.Location = New System.Drawing.Point(416, 39)
        Me.cmbArea.Name = "cmbArea"
        Me.cmbArea.Size = New System.Drawing.Size(125, 22)
        Me.cmbArea.TabIndex = 109
        '
        'cmbTipe
        '
        Me.cmbTipe.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbTipe.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbTipe.FormattingEnabled = True
        Me.cmbTipe.Location = New System.Drawing.Point(416, 10)
        Me.cmbTipe.Name = "cmbTipe"
        Me.cmbTipe.Size = New System.Drawing.Size(125, 22)
        Me.cmbTipe.TabIndex = 108
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.BackColor = System.Drawing.Color.Transparent
        Me.Label17.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label17.ForeColor = System.Drawing.Color.Black
        Me.Label17.Location = New System.Drawing.Point(318, 42)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(70, 14)
        Me.Label17.TabIndex = 111
        Me.Label17.Text = "Payroll Area"
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.BackColor = System.Drawing.Color.Transparent
        Me.Label16.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.ForeColor = System.Drawing.Color.Black
        Me.Label16.Location = New System.Drawing.Point(318, 15)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(87, 14)
        Me.Label16.TabIndex = 110
        Me.Label16.Text = "Tipe Karyawan"
        '
        'btnProcess
        '
        Me.btnProcess.Location = New System.Drawing.Point(416, 126)
        Me.btnProcess.Name = "btnProcess"
        Me.btnProcess.Size = New System.Drawing.Size(103, 25)
        Me.btnProcess.TabIndex = 116
        Me.btnProcess.Text = "Process"
        Me.btnProcess.UseVisualStyleBackColor = True
        '
        'DBGridAbsensi
        '
        Me.DBGridAbsensi.AllowUserToAddRows = False
        Me.DBGridAbsensi.AllowUserToDeleteRows = False
        Me.DBGridAbsensi.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle5.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DBGridAbsensi.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle5
        Me.DBGridAbsensi.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DBGridAbsensi.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn1, Me.Masuk, Me.Jamkerja, Me.Total, Me.Istirahat, Me.Kerja, Me.harilembur, Me.jamlembur, Me.jumlahlibur, Me.JamLibur, Me.jmltelat, Me.jmlPC})
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle6.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DBGridAbsensi.DefaultCellStyle = DataGridViewCellStyle6
        Me.DBGridAbsensi.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnKeystroke
        Me.DBGridAbsensi.Location = New System.Drawing.Point(967, 4)
        Me.DBGridAbsensi.Name = "DBGridAbsensi"
        Me.DBGridAbsensi.ReadOnly = True
        Me.DBGridAbsensi.RowHeadersVisible = False
        Me.DBGridAbsensi.Size = New System.Drawing.Size(335, 364)
        Me.DBGridAbsensi.TabIndex = 117
        '
        'btnExit
        '
        Me.btnExit.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnExit.Location = New System.Drawing.Point(229, 533)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(92, 34)
        Me.btnExit.TabIndex = 120
        Me.btnExit.Text = "Keluar"
        Me.btnExit.UseVisualStyleBackColor = True
        '
        'btnHapus
        '
        Me.btnHapus.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnHapus.Location = New System.Drawing.Point(131, 533)
        Me.btnHapus.Name = "btnHapus"
        Me.btnHapus.Size = New System.Drawing.Size(92, 34)
        Me.btnHapus.TabIndex = 119
        Me.btnHapus.Text = "Hapus"
        Me.btnHapus.UseVisualStyleBackColor = True
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnSave.Location = New System.Drawing.Point(33, 533)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(92, 34)
        Me.btnSave.TabIndex = 118
        Me.btnSave.Text = "Simpan"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'lblTotal
        '
        Me.lblTotal.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.lblTotal.AutoSize = True
        Me.lblTotal.BackColor = System.Drawing.Color.Transparent
        Me.lblTotal.Font = New System.Drawing.Font("Tahoma", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTotal.ForeColor = System.Drawing.Color.Black
        Me.lblTotal.Location = New System.Drawing.Point(858, 537)
        Me.lblTotal.Name = "lblTotal"
        Me.lblTotal.Size = New System.Drawing.Size(25, 25)
        Me.lblTotal.TabIndex = 121
        Me.lblTotal.Text = "0"
        Me.lblTotal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lbl100000
        '
        Me.lbl100000.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lbl100000.AutoSize = True
        Me.lbl100000.BackColor = System.Drawing.Color.Transparent
        Me.lbl100000.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl100000.ForeColor = System.Drawing.Color.Black
        Me.lbl100000.Location = New System.Drawing.Point(1098, 371)
        Me.lbl100000.Name = "lbl100000"
        Me.lbl100000.Size = New System.Drawing.Size(20, 23)
        Me.lbl100000.TabIndex = 122
        Me.lbl100000.Text = "0"
        Me.lbl100000.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label8
        '
        Me.Label8.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label8.AutoSize = True
        Me.Label8.BackColor = System.Drawing.Color.Transparent
        Me.Label8.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.ForeColor = System.Drawing.Color.Black
        Me.Label8.Location = New System.Drawing.Point(983, 371)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(58, 23)
        Me.Label8.TabIndex = 125
        Me.Label8.Text = "100rb"
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label6
        '
        Me.Label6.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label6.AutoSize = True
        Me.Label6.BackColor = System.Drawing.Color.Transparent
        Me.Label6.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.Color.Black
        Me.Label6.Location = New System.Drawing.Point(983, 394)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(48, 23)
        Me.Label6.TabIndex = 127
        Me.Label6.Text = "50rb"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lbl50000
        '
        Me.lbl50000.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lbl50000.AutoSize = True
        Me.lbl50000.BackColor = System.Drawing.Color.Transparent
        Me.lbl50000.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl50000.ForeColor = System.Drawing.Color.Black
        Me.lbl50000.Location = New System.Drawing.Point(1098, 394)
        Me.lbl50000.Name = "lbl50000"
        Me.lbl50000.Size = New System.Drawing.Size(20, 23)
        Me.lbl50000.TabIndex = 126
        Me.lbl50000.Text = "0"
        Me.lbl50000.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label9
        '
        Me.Label9.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label9.AutoSize = True
        Me.Label9.BackColor = System.Drawing.Color.Transparent
        Me.Label9.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.ForeColor = System.Drawing.Color.Black
        Me.Label9.Location = New System.Drawing.Point(983, 417)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(48, 23)
        Me.Label9.TabIndex = 129
        Me.Label9.Text = "20rb"
        Me.Label9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lbl20000
        '
        Me.lbl20000.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lbl20000.AutoSize = True
        Me.lbl20000.BackColor = System.Drawing.Color.Transparent
        Me.lbl20000.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl20000.ForeColor = System.Drawing.Color.Black
        Me.lbl20000.Location = New System.Drawing.Point(1098, 417)
        Me.lbl20000.Name = "lbl20000"
        Me.lbl20000.Size = New System.Drawing.Size(20, 23)
        Me.lbl20000.TabIndex = 128
        Me.lbl20000.Text = "0"
        Me.lbl20000.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label11
        '
        Me.Label11.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label11.AutoSize = True
        Me.Label11.BackColor = System.Drawing.Color.Transparent
        Me.Label11.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.ForeColor = System.Drawing.Color.Black
        Me.Label11.Location = New System.Drawing.Point(983, 440)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(48, 23)
        Me.Label11.TabIndex = 131
        Me.Label11.Text = "10rb"
        Me.Label11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lbl10000
        '
        Me.lbl10000.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lbl10000.AutoSize = True
        Me.lbl10000.BackColor = System.Drawing.Color.Transparent
        Me.lbl10000.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl10000.ForeColor = System.Drawing.Color.Black
        Me.lbl10000.Location = New System.Drawing.Point(1098, 440)
        Me.lbl10000.Name = "lbl10000"
        Me.lbl10000.Size = New System.Drawing.Size(20, 23)
        Me.lbl10000.TabIndex = 130
        Me.lbl10000.Text = "0"
        Me.lbl10000.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label13
        '
        Me.Label13.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label13.AutoSize = True
        Me.Label13.BackColor = System.Drawing.Color.Transparent
        Me.Label13.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.ForeColor = System.Drawing.Color.Black
        Me.Label13.Location = New System.Drawing.Point(983, 463)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(38, 23)
        Me.Label13.TabIndex = 133
        Me.Label13.Text = "5rb"
        Me.Label13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lbl5000
        '
        Me.lbl5000.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lbl5000.AutoSize = True
        Me.lbl5000.BackColor = System.Drawing.Color.Transparent
        Me.lbl5000.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl5000.ForeColor = System.Drawing.Color.Black
        Me.lbl5000.Location = New System.Drawing.Point(1098, 463)
        Me.lbl5000.Name = "lbl5000"
        Me.lbl5000.Size = New System.Drawing.Size(20, 23)
        Me.lbl5000.TabIndex = 132
        Me.lbl5000.Text = "0"
        Me.lbl5000.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label15
        '
        Me.Label15.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label15.AutoSize = True
        Me.Label15.BackColor = System.Drawing.Color.Transparent
        Me.Label15.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.ForeColor = System.Drawing.Color.Black
        Me.Label15.Location = New System.Drawing.Point(983, 487)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(38, 23)
        Me.Label15.TabIndex = 135
        Me.Label15.Text = "2rb"
        Me.Label15.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lbl2000
        '
        Me.lbl2000.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lbl2000.AutoSize = True
        Me.lbl2000.BackColor = System.Drawing.Color.Transparent
        Me.lbl2000.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl2000.ForeColor = System.Drawing.Color.Black
        Me.lbl2000.Location = New System.Drawing.Point(1098, 487)
        Me.lbl2000.Name = "lbl2000"
        Me.lbl2000.Size = New System.Drawing.Size(20, 23)
        Me.lbl2000.TabIndex = 134
        Me.lbl2000.Text = "0"
        Me.lbl2000.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label19
        '
        Me.Label19.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label19.AutoSize = True
        Me.Label19.BackColor = System.Drawing.Color.Transparent
        Me.Label19.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label19.ForeColor = System.Drawing.Color.Black
        Me.Label19.Location = New System.Drawing.Point(983, 510)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(38, 23)
        Me.Label19.TabIndex = 137
        Me.Label19.Text = "1rb"
        Me.Label19.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lbl1000
        '
        Me.lbl1000.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lbl1000.AutoSize = True
        Me.lbl1000.BackColor = System.Drawing.Color.Transparent
        Me.lbl1000.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl1000.ForeColor = System.Drawing.Color.Black
        Me.lbl1000.Location = New System.Drawing.Point(1098, 510)
        Me.lbl1000.Name = "lbl1000"
        Me.lbl1000.Size = New System.Drawing.Size(20, 23)
        Me.lbl1000.TabIndex = 136
        Me.lbl1000.Text = "0"
        Me.lbl1000.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label21
        '
        Me.Label21.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label21.AutoSize = True
        Me.Label21.BackColor = System.Drawing.Color.Transparent
        Me.Label21.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label21.ForeColor = System.Drawing.Color.Black
        Me.Label21.Location = New System.Drawing.Point(983, 533)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(40, 23)
        Me.Label21.TabIndex = 139
        Me.Label21.Text = "500"
        Me.Label21.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lbl500
        '
        Me.lbl500.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lbl500.AutoSize = True
        Me.lbl500.BackColor = System.Drawing.Color.Transparent
        Me.lbl500.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl500.ForeColor = System.Drawing.Color.Black
        Me.lbl500.Location = New System.Drawing.Point(1098, 533)
        Me.lbl500.Name = "lbl500"
        Me.lbl500.Size = New System.Drawing.Size(20, 23)
        Me.lbl500.TabIndex = 138
        Me.lbl500.Text = "0"
        Me.lbl500.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'btnPrint
        '
        Me.btnPrint.Location = New System.Drawing.Point(187, 592)
        Me.btnPrint.Name = "btnPrint"
        Me.btnPrint.Size = New System.Drawing.Size(99, 23)
        Me.btnPrint.TabIndex = 140
        Me.btnPrint.Text = "Print Slip Gaji"
        Me.btnPrint.UseVisualStyleBackColor = True
        Me.btnPrint.Visible = False
        '
        'btnPrintTTD
        '
        Me.btnPrintTTD.Location = New System.Drawing.Point(292, 589)
        Me.btnPrintTTD.Name = "btnPrintTTD"
        Me.btnPrintTTD.Size = New System.Drawing.Size(99, 23)
        Me.btnPrintTTD.TabIndex = 141
        Me.btnPrintTTD.Text = "Print ttd Gaji"
        Me.btnPrintTTD.UseVisualStyleBackColor = True
        Me.btnPrintTTD.Visible = False
        '
        'btnPrintAbsensi
        '
        Me.btnPrintAbsensi.Location = New System.Drawing.Point(397, 589)
        Me.btnPrintAbsensi.Name = "btnPrintAbsensi"
        Me.btnPrintAbsensi.Size = New System.Drawing.Size(99, 23)
        Me.btnPrintAbsensi.TabIndex = 143
        Me.btnPrintAbsensi.Text = "Print Absensi"
        Me.btnPrintAbsensi.UseVisualStyleBackColor = True
        Me.btnPrintAbsensi.Visible = False
        '
        'Label23
        '
        Me.Label23.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label23.AutoSize = True
        Me.Label23.BackColor = System.Drawing.Color.Transparent
        Me.Label23.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label23.ForeColor = System.Drawing.Color.Black
        Me.Label23.Location = New System.Drawing.Point(983, 577)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(40, 23)
        Me.Label23.TabIndex = 147
        Me.Label23.Text = "100"
        Me.Label23.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lbl100
        '
        Me.lbl100.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lbl100.AutoSize = True
        Me.lbl100.BackColor = System.Drawing.Color.Transparent
        Me.lbl100.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl100.ForeColor = System.Drawing.Color.Black
        Me.lbl100.Location = New System.Drawing.Point(1098, 577)
        Me.lbl100.Name = "lbl100"
        Me.lbl100.Size = New System.Drawing.Size(20, 23)
        Me.lbl100.TabIndex = 146
        Me.lbl100.Text = "0"
        Me.lbl100.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label25
        '
        Me.Label25.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label25.AutoSize = True
        Me.Label25.BackColor = System.Drawing.Color.Transparent
        Me.Label25.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label25.ForeColor = System.Drawing.Color.Black
        Me.Label25.Location = New System.Drawing.Point(983, 554)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(40, 23)
        Me.Label25.TabIndex = 145
        Me.Label25.Text = "200"
        Me.Label25.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lbl200
        '
        Me.lbl200.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lbl200.AutoSize = True
        Me.lbl200.BackColor = System.Drawing.Color.Transparent
        Me.lbl200.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl200.ForeColor = System.Drawing.Color.Black
        Me.lbl200.Location = New System.Drawing.Point(1098, 554)
        Me.lbl200.Name = "lbl200"
        Me.lbl200.Size = New System.Drawing.Size(20, 23)
        Me.lbl200.TabIndex = 144
        Me.lbl200.Text = "0"
        Me.lbl200.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Button2
        '
        Me.Button2.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Button2.BackColor = System.Drawing.Color.Transparent
        Me.Button2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Button2.FlatAppearance.BorderSize = 0
        Me.Button2.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DeepSkyBlue
        Me.Button2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.ForeColor = System.Drawing.Color.Black
        Me.Button2.Location = New System.Drawing.Point(327, 533)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(96, 34)
        Me.Button2.TabIndex = 433
        Me.Button2.Text = "Preview Slip Gaji"
        Me.Button2.UseVisualStyleBackColor = False
        '
        'Button1
        '
        Me.Button1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Button1.BackColor = System.Drawing.Color.Transparent
        Me.Button1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Button1.FlatAppearance.BorderSize = 0
        Me.Button1.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DeepSkyBlue
        Me.Button1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.ForeColor = System.Drawing.Color.Black
        Me.Button1.Location = New System.Drawing.Point(429, 533)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(86, 34)
        Me.Button1.TabIndex = 432
        Me.Button1.Text = "Print Slip Gaji"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'btnPrintGaji
        '
        Me.btnPrintGaji.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnPrintGaji.BackColor = System.Drawing.Color.Transparent
        Me.btnPrintGaji.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnPrintGaji.FlatAppearance.BorderSize = 0
        Me.btnPrintGaji.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DeepSkyBlue
        Me.btnPrintGaji.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPrintGaji.ForeColor = System.Drawing.Color.Black
        Me.btnPrintGaji.Location = New System.Drawing.Point(521, 533)
        Me.btnPrintGaji.Name = "btnPrintGaji"
        Me.btnPrintGaji.Size = New System.Drawing.Size(92, 34)
        Me.btnPrintGaji.TabIndex = 431
        Me.btnPrintGaji.Text = "Print Pengajuan"
        Me.btnPrintGaji.UseVisualStyleBackColor = False
        '
        'txtKeterangan
        '
        Me.txtKeterangan.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtKeterangan.ForeColor = System.Drawing.Color.Black
        Me.txtKeterangan.Location = New System.Drawing.Point(96, 102)
        Me.txtKeterangan.Multiline = True
        Me.txtKeterangan.Name = "txtKeterangan"
        Me.txtKeterangan.ScrollBars = System.Windows.Forms.ScrollBars.Both
        Me.txtKeterangan.Size = New System.Drawing.Size(202, 49)
        Me.txtKeterangan.TabIndex = 434
        '
        'Label5
        '
        Me.Label5.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(859, 577)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(59, 16)
        Me.Label5.TabIndex = 436
        Me.Label5.Text = "Record :"
        '
        'Label7
        '
        Me.Label7.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(919, 577)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(15, 16)
        Me.Label7.TabIndex = 435
        Me.Label7.Text = "0"
        '
        'Label10
        '
        Me.Label10.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(1205, 371)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(59, 16)
        Me.Label10.TabIndex = 438
        Me.Label10.Text = "Record :"
        '
        'Label12
        '
        Me.Label12.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.Location = New System.Drawing.Point(1263, 371)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(15, 16)
        Me.Label12.TabIndex = 437
        Me.Label12.Text = "0"
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.HeaderText = "NIK"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.Width = 80
        '
        'Masuk
        '
        Me.Masuk.HeaderText = "Masuk Normal"
        Me.Masuk.Name = "Masuk"
        Me.Masuk.ReadOnly = True
        Me.Masuk.Width = 50
        '
        'Jamkerja
        '
        Me.Jamkerja.HeaderText = "Jam Kerja"
        Me.Jamkerja.Name = "Jamkerja"
        Me.Jamkerja.ReadOnly = True
        Me.Jamkerja.Width = 50
        '
        'Total
        '
        Me.Total.HeaderText = "Total"
        Me.Total.Name = "Total"
        Me.Total.ReadOnly = True
        Me.Total.Width = 50
        '
        'Istirahat
        '
        Me.Istirahat.HeaderText = "Istirahat"
        Me.Istirahat.Name = "Istirahat"
        Me.Istirahat.ReadOnly = True
        Me.Istirahat.Width = 50
        '
        'Kerja
        '
        Me.Kerja.HeaderText = "Kerja"
        Me.Kerja.Name = "Kerja"
        Me.Kerja.ReadOnly = True
        Me.Kerja.Width = 50
        '
        'harilembur
        '
        Me.harilembur.HeaderText = "Jml Lembur"
        Me.harilembur.Name = "harilembur"
        Me.harilembur.ReadOnly = True
        Me.harilembur.Width = 50
        '
        'jamlembur
        '
        Me.jamlembur.HeaderText = "Jam Lembur"
        Me.jamlembur.Name = "jamlembur"
        Me.jamlembur.ReadOnly = True
        Me.jamlembur.Width = 50
        '
        'jumlahlibur
        '
        Me.jumlahlibur.HeaderText = "Masuk Libur"
        Me.jumlahlibur.Name = "jumlahlibur"
        Me.jumlahlibur.ReadOnly = True
        Me.jumlahlibur.Width = 50
        '
        'JamLibur
        '
        Me.JamLibur.HeaderText = "Jam Libur"
        Me.JamLibur.Name = "JamLibur"
        Me.JamLibur.ReadOnly = True
        Me.JamLibur.Width = 50
        '
        'jmltelat
        '
        Me.jmltelat.HeaderText = "Jml Telat"
        Me.jmltelat.Name = "jmltelat"
        Me.jmltelat.ReadOnly = True
        Me.jmltelat.Width = 50
        '
        'jmlPC
        '
        Me.jmlPC.HeaderText = "Jml PC"
        Me.jmlPC.Name = "jmlPC"
        Me.jmlPC.ReadOnly = True
        Me.jmlPC.Width = 50
        '
        'frmTransPayroll
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1299, 598)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.Label12)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.txtKeterangan)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.btnPrintGaji)
        Me.Controls.Add(Me.Label23)
        Me.Controls.Add(Me.lbl100)
        Me.Controls.Add(Me.Label25)
        Me.Controls.Add(Me.lbl200)
        Me.Controls.Add(Me.btnPrintAbsensi)
        Me.Controls.Add(Me.btnPrintTTD)
        Me.Controls.Add(Me.btnPrint)
        Me.Controls.Add(Me.Label21)
        Me.Controls.Add(Me.lbl500)
        Me.Controls.Add(Me.Label19)
        Me.Controls.Add(Me.lbl1000)
        Me.Controls.Add(Me.Label15)
        Me.Controls.Add(Me.lbl2000)
        Me.Controls.Add(Me.Label13)
        Me.Controls.Add(Me.lbl5000)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.lbl10000)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.lbl20000)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.lbl50000)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.lbl100000)
        Me.Controls.Add(Me.lblTotal)
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.btnHapus)
        Me.Controls.Add(Me.btnSave)
        Me.Controls.Add(Me.DBGridAbsensi)
        Me.Controls.Add(Me.btnProcess)
        Me.Controls.Add(Me.cmbGroupKerja)
        Me.Controls.Add(Me.Label36)
        Me.Controls.Add(Me.cmbDivisi)
        Me.Controls.Add(Me.Label35)
        Me.Controls.Add(Me.cmbArea)
        Me.Controls.Add(Me.cmbTipe)
        Me.Controls.Add(Me.Label17)
        Me.Controls.Add(Me.Label16)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.DBGrid)
        Me.Controls.Add(Me.DBGridKomponen)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.dtAwal)
        Me.Controls.Add(Me.dtAkhir)
        Me.Controls.Add(Me.lblNoTrans)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.cmbTemplate)
        Me.Controls.Add(Me.Label26)
        Me.Controls.Add(Me.btnSearch)
        Me.Controls.Add(Me.Label2)
        Me.KeyPreview = True
        Me.Name = "frmTransPayroll"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = " "
        Me.TopMost = True
        CType(Me.DBGridKomponen, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DBGrid, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DBGridAbsensi, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents cmbTemplate As System.Windows.Forms.ComboBox
    Friend WithEvents Label26 As System.Windows.Forms.Label
    Friend WithEvents btnSearch As System.Windows.Forms.Button
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents lblNoTrans As System.Windows.Forms.Label
    Friend WithEvents dtAwal As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtAkhir As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents DBGridKomponen As System.Windows.Forms.DataGridView
    Friend WithEvents DBGrid As System.Windows.Forms.DataGridView
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents cmbGroupKerja As System.Windows.Forms.ComboBox
    Friend WithEvents Label36 As System.Windows.Forms.Label
    Friend WithEvents cmbDivisi As System.Windows.Forms.ComboBox
    Friend WithEvents Label35 As System.Windows.Forms.Label
    Friend WithEvents cmbArea As System.Windows.Forms.ComboBox
    Friend WithEvents cmbTipe As System.Windows.Forms.ComboBox
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents btnProcess As System.Windows.Forms.Button
    Friend WithEvents DBGridAbsensi As System.Windows.Forms.DataGridView
    Friend WithEvents btnExit As System.Windows.Forms.Button
    Friend WithEvents btnHapus As System.Windows.Forms.Button
    Friend WithEvents btnSave As System.Windows.Forms.Button
    Friend WithEvents lblTotal As System.Windows.Forms.Label
    Friend WithEvents lbl100000 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents lbl50000 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents lbl20000 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents lbl10000 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents lbl5000 As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents lbl2000 As System.Windows.Forms.Label
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents lbl1000 As System.Windows.Forms.Label
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents lbl500 As System.Windows.Forms.Label
    Friend WithEvents btnPrint As System.Windows.Forms.Button
    Friend WithEvents btnPrintTTD As System.Windows.Forms.Button
    Friend WithEvents btnPrintAbsensi As System.Windows.Forms.Button
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents lbl100 As System.Windows.Forms.Label
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents lbl200 As System.Windows.Forms.Label
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents btnPrintGaji As System.Windows.Forms.Button
    Friend WithEvents txtKeterangan As System.Windows.Forms.TextBox
    Friend WithEvents KodeKomponen As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Komponen As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents C As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents Harian As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents Jam As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents Lembur As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents MasukPenuh As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents Manual As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents Libur As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents Custom As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents Custom_name As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents normal As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents NIK As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Nama As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Masuk As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Jamkerja As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Total As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Istirahat As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Kerja As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents harilembur As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents jamlembur As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents jumlahlibur As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents JamLibur As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents jmltelat As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents jmlPC As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
