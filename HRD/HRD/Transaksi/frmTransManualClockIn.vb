﻿Imports System.Data.OleDb
Imports System.Data.SqlClient

Public Class frmTransManualClockIn
    Dim transaction As SqlTransaction = Nothing
    Private Sub btnSearch_Click(sender As System.Object, e As System.EventArgs) Handles btnSearch.Click
        Dim f As New frmSearch
        f.setCol(0)
        f.setTableName(" vw_karyawantipe")
        f.setColumns("*")
        f.setOrder("order by nik")
        If f.ShowDialog = System.Windows.Forms.DialogResult.OK Then
            txtNIK.Text = f.value
            loadMasterKaryawan()
        End If
        f.Dispose()
    End Sub

    Private Sub loadMasterKaryawan()
        Dim conn2 As New SqlConnection(strcon)
        Dim cmd As SqlCommand
        Dim reader As SqlDataReader = Nothing
        conn2.Open()
        Try
            cmd = New SqlCommand("select * from  vw_karyawantipe where NIK = '" & txtNIK.Text & "'", conn2)
            reader = cmd.ExecuteReader()
            If reader.Read() Then
                txtNama.Text = reader("nama_depan")
            End If
            reader.Close()

        Catch ex As Exception
            MessageBox.Show(ex.Message.ToString)
        End Try
        conn2.Close()
    End Sub

    Private Sub frmTransManualClockIn_KeyDown(sender As Object, e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        If e.KeyCode = Keys.F2 Then btnSimpan.PerformClick()
        If e.KeyCode = Keys.F5 Then btnSearch.PerformClick()
        If e.KeyCode = Keys.Enter Then SendKeys.Send("{TAB}")
        If e.KeyCode = Keys.Escape Then Me.Close()
    End Sub

    Private Sub frmTransManualClockIn_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        reset_form()
    End Sub
    Private Sub reset_form()
        txtNama.Text = ""
        txtNIK.Text = ""
        dttgl.Value = Now
    End Sub

    Private Sub btnSimpan_Click(sender As System.Object, e As System.EventArgs) Handles btnSimpan.Click
        Dim conn2 As New SqlConnection(strcon)
        Dim cmd As New SqlCommand
        conn2.Open()
        transaction = conn2.BeginTransaction(IsolationLevel.ReadCommitted)
        Try
            cmd = New SqlCommand("insert into t_clockin values ('" & txtNIK.Text & "','" & jamMasuk.Value & "',0,1,'" & username & "');" & _
                                 "insert into t_clockin values ('" & txtNIK.Text & "','" & jamPulang.Value & "',0,1,'" & username & "')", conn2, transaction)
            cmd.ExecuteNonQuery()
            transaction.Commit()
            conn2.Close()

        Catch ex As Exception
            MessageBox.Show(ex.Message.ToString)
            transaction.Rollback()
        End Try
        If conn2.State Then conn2.Close()
        generate()
        MsgBox("Data berhasil disimpan . . .", MsgBoxStyle.Information)
        Me.Close()
    End Sub
    Private Sub generate()
        Dim conn As New SqlConnection
        Dim cmd As New SqlCommand
        conn.ConnectionString = strcon
        conn.Open()
        'insert to rekapabsesni
        cmd = New SqlCommand("exec sp_autoshiftdat '" & Format(dttgl.Value, "yyyyMMdd") & "','" & Format(dttgl.Value, "yyyyMMdd") & "'", conn, transaction)
        cmd.CommandTimeout = 0
        cmd.ExecuteNonQuery()
        'generate shift
        cmd = New SqlCommand("exec sp_autoshift1 '" & Format(dttgl.Value, "yyyyMMdd") & "','" & Format(dttgl.Value, "yyyyMMdd") & "'", conn, transaction)
        cmd.CommandTimeout = 0
        cmd.ExecuteNonQuery()
        conn.Close()
    End Sub
    Private Sub btnKeluar_Click(sender As System.Object, e As System.EventArgs) Handles btnKeluar.Click
        Me.Close()
    End Sub

    Private Sub dttgl_ValueChanged(sender As System.Object, e As System.EventArgs) Handles dttgl.ValueChanged
        jamMasuk.Value = dttgl.Value '.ToString("dd/MM/yyyy")
        jamPulang.Value = dttgl.Value '.ToString("dd/MM/yyyy")
    End Sub
End Class