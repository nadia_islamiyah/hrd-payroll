﻿Imports System.Data.SqlClient
Imports System.Data.Sql
Imports System.IO
Public Class frmProcessBonusShift

    Private Sub btnProccess_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnProccess.Click
        Dim conn As New SqlConnection
        Dim cmd As New SqlCommand
        Dim cmd2 As New SqlCommand
        Dim dr As SqlDataReader
        conn.ConnectionString = strcon

        conn.Open()

        cmd2 = New SqlCommand("delete from t_custompayroll where jenis='Bonus Shift' and convert(varchar(20),tanggal,112)='" & Format(dtTanggal.Value, "yyyyMMdd") & "'", conn)
        cmd2.ExecuteNonQuery()
        cmd = New SqlCommand("select vw_absensi.*,ms_bonusshift.jumlah_jamkerja as jamkerja,lembur,bonus from vw_absensi left join ms_bonusshift on vw_absensi.nama_shift=ms_bonusshift.nama_shift " & _
                             " where convert(varchar(20),tanggal,112)='" & Format(dtTanggal.Value, "yyyyMMdd") & "'", conn)
        dr = cmd.ExecuteReader
        DBGrid.Rows.Clear()
        
        While dr.Read
            DBGrid.Rows.Add()
            DBGrid.Rows(DBGrid.Rows.Count - 1).Cells(0).Value = dr.Item("nik")
            DBGrid.Rows(DBGrid.Rows.Count - 1).Cells(1).Value = dr.Item("nama_depan")
            DBGrid.Rows(DBGrid.Rows.Count - 1).Cells(2).Value = dr.Item("total") - dr.Item("istirahat")
            If dr.Item("total") - dr.Item("istirahat") > dr.Item("jumlah_jamkerja") Then
                DBGrid.Rows(DBGrid.Rows.Count - 1).Cells(3).Value = dr.Item("total") - dr.Item("istirahat") - dr.Item("jumlah_jamkerja")
            Else
                DBGrid.Rows(DBGrid.Rows.Count - 1).Cells(3).Value = 0
            End If
            If dr.Item("jamkerja") <= DBGrid.Rows(DBGrid.Rows.Count - 1).Cells(2).Value And
                dr.Item("lembur") <= DBGrid.Rows(DBGrid.Rows.Count - 1).Cells(3).Value Then
                DBGrid.Rows(DBGrid.Rows.Count - 1).Cells(4).Value = dr.Item("bonus")
            Else
                DBGrid.Rows(DBGrid.Rows.Count - 1).Cells(4).Value = 0
            End If
            cmd2 = New SqlCommand("insert into t_custompayroll (nik,tanggal,jenis,jumlah) values ('" & DBGrid.Rows(DBGrid.Rows.Count - 1).Cells(0).Value & "','" & Format(dtTanggal.Value, "yyyyMMdd") & "','Bonus Shift','" & DBGrid.Rows(DBGrid.Rows.Count - 1).Cells(4).Value & "')", conn)
            cmd2.ExecuteNonQuery()
        End While
        conn.Close()
    End Sub

    Private Sub frmProcessBonusShift_KeyDown(sender As Object, e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        If e.KeyCode = Keys.Enter Then SendKeys.Send("{tab}")
        If e.KeyCode = Keys.Escape Then Me.Close()
    End Sub

    Private Sub frmProcessBonusShift_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        dtTanggal.Value = Now
    End Sub
End Class