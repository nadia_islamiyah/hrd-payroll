﻿Imports System.Data.SqlClient
Imports System.Data.Sql
Imports System.IO
Public Class frmTransPayroll
    Dim jumlah_harilibur As Integer
    Dim matauang As New Dictionary(Of Long, Integer)
    Private Sub load_komponen()
        Dim conn As New SqlConnection
        Dim cmd As New SqlCommand
        Dim dr As SqlDataReader

        conn.ConnectionString = strcon
        conn.Open()

        cmd = New SqlCommand("select distinct nama from ms_templategaji order by nama", conn)
        dr = cmd.ExecuteReader
        cmbTemplate.Items.Clear()
        While dr.Read
            cmbTemplate.Items.Add(dr.Item(0))
        End While
        dr.Close()

        cmd = New SqlCommand("select * from ms_komponengaji order by kode_komponengaji", conn)
        dr = cmd.ExecuteReader
        DBGridKomponen.Rows.Clear()
        While dr.Read
            DBGridKomponen.Rows.Add()
            DBGridKomponen.Rows(DBGridKomponen.Rows.Count - 1).Cells(0).Value = dr.Item(0)
            DBGridKomponen.Rows(DBGridKomponen.Rows.Count - 1).Cells(1).Value = dr.Item(1)
            DBGridKomponen.Rows(DBGridKomponen.Rows.Count - 1).Cells(2).Value = 0
            DBGridKomponen.Rows(DBGridKomponen.Rows.Count - 1).Cells(3).Value = dr.Item("harian")
            DBGridKomponen.Rows(DBGridKomponen.Rows.Count - 1).Cells(4).Value = dr.Item("jam")
            DBGridKomponen.Rows(DBGridKomponen.Rows.Count - 1).Cells(5).Value = dr.Item("lembur")
            DBGridKomponen.Rows(DBGridKomponen.Rows.Count - 1).Cells(6).Value = dr.Item("masukpenuh")
            DBGridKomponen.Rows(DBGridKomponen.Rows.Count - 1).Cells(7).Value = dr.Item("manual")
            DBGridKomponen.Rows(DBGridKomponen.Rows.Count - 1).Cells(8).Value = dr.Item("libur")
            DBGridKomponen.Rows(DBGridKomponen.Rows.Count - 1).Cells(11).Value = dr.Item("normal")
            DBGridKomponen.Rows(DBGridKomponen.Rows.Count - 1).Cells(9).Value = dr.Item("custom")
            DBGridKomponen.Rows(DBGridKomponen.Rows.Count - 1).Cells(10).Value = dr.Item("custom_name")
        End While
        conn.Close()
    End Sub
    Private Sub reloadcombo()
        loadComboBoxAll(cmbTipe, "tipe", "var_tipekaryawan", "", "tipe", "tipe")
        loadComboBoxAll(cmbArea, "area", "var_PayrollArea", "", "area", "area")
        loadComboBoxAll(cmbGroupKerja, "nama_groupkerja", "ms_groupkerja", "", "nama_groupkerja", "nama_groupkerja")
        loadComboBoxAll(cmbDivisi, "divisi", "var_divisi", "", "divisi", "divisi")
    End Sub

    Private Sub cek_harilibur()
        Dim conn As New SqlConnection
        Dim cmd As New SqlCommand
        Dim dr As SqlDataReader
        conn.ConnectionString = strcon
        conn.Open()
        cmd = New SqlCommand("select count(*) from t_libur where convert(varchar(20),tanggal,112) between '" & Format(dtAwal.Value, "yyyyMMdd") & "' and '" & Format(dtAkhir.Value, "yyyyMMdd") & "'", conn)
        dr = cmd.ExecuteReader
        If dr.Read Then
            jumlah_harilibur = dr.Item(0)
        End If
        conn.Close()
    End Sub
    Private Sub load_template()
        Dim conn As New SqlConnection
        Dim cmd As New SqlCommand
        Dim dr As SqlDataReader
        conn.ConnectionString = strcon
        conn.Open()
        cmd = New SqlCommand("select * from ms_templategaji where nama='" & cmbTemplate.Text & "'", conn)
        dr = cmd.ExecuteReader
        For i = 0 To DBGridKomponen.Rows.Count - 1
            DBGridKomponen.Rows(i).Cells(2).Value = False
        Next
        For i = DBGrid.ColumnCount - 1 To 2 Step -1
            DBGrid.Columns.Remove(DBGrid.Columns(i).Name)
        Next
        While dr.Read
            For i = 0 To DBGridKomponen.Rows.Count - 1
                If DBGridKomponen.Rows(i).Cells(0).Value = dr.Item(1) Then
                    DBGridKomponen.Rows(i).Cells(2).Value = True
                    DBGrid.Columns.Add(dr.Item(1), DBGridKomponen.Rows(i).Cells(1).Value)
                    If DBGridKomponen.Rows(i).Cells(7).Value Then
                        DBGrid.Columns(DBGrid.Columns.Count - 1).ReadOnly = False
                    Else
                        DBGrid.Columns(DBGrid.Columns.Count - 1).ReadOnly = True
                    End If
                End If
            Next
        End While
        DBGrid.Columns.Add("Total", "Total")
        DBGrid.Columns(DBGrid.Columns.Count - 1).ReadOnly = True
        dr.Close()
        conn.Close()

    End Sub
    Private Sub load_karyawan()
        Dim conn As New SqlConnection
        Dim cmd As New SqlCommand
        Dim dr As SqlDataReader
        Dim where As String
        conn.ConnectionString = strcon
        conn.Open()

        where = ""
        If cmbArea.Text <> "" Then
            where = IIf(where = "", "", where & " and ")
            where = where & "area='" & cmbArea.Text & "'"
        End If
        If cmbTipe.Text <> "" Then
            where = IIf(where = "", "", where & " and ")
            where = where & "tipe='" & cmbTipe.Text & "'"
        End If
        If cmbDivisi.Text <> "" Then
            where = IIf(where = "", "", where & " and ")
            where = where & "divisi='" & cmbDivisi.Text & "'"
        End If
        If cmbGroupKerja.Text <> "" Then
            where = IIf(where = "", "", where & " and ")
            where = where & "groupkerja='" & cmbGroupKerja.Text & "'"
        End If
        If where <> "" Then where = " where " & where
        cmd = New SqlCommand("select m.nik,m.nama_depan from ms_karyawan m inner join ms_karyawan_tipe t on m.nik=t.nik " & where & " order by m.nik", conn)
        dr = cmd.ExecuteReader
        DBGrid.Rows.Clear()
        While dr.Read
            DBGrid.Rows.Add()
            DBGrid.Rows(DBGrid.Rows.Count - 1).Cells(0).Value = dr.Item(0)
            DBGrid.Rows(DBGrid.Rows.Count - 1).Cells(1).Value = dr.Item(1)
            For i = 2 To DBGrid.Columns.Count - 1
                DBGrid.Rows(DBGrid.Rows.Count - 1).Cells(i).Value = 0
            Next
        End While
        dr.Close()

        Dim a As String = " select m.nik,isnull(count(masuk),0) as masuk,isnull(sum(total),0) as total,isnull(sum(istirahat),0) as istirahat," & _
            " isnull(sum(jumlah_jamkerja),0) as jumlah_jamkerja, " & _
            " isnull(sum(case when total>jumlah_jamkerja then jumlah_jamkerja else total end),0) as kerja,isnull(sum(case when libur=0 and lembur>=lembur_minimal then (case when lembur_kelipatan>0 then floor(lembur/lembur_kelipatan)*lembur_kelipatan  else lembur end) else 0 end),0) as jamlembur ,   " & _
            " isnull(sum(case when libur=0 and lembur>=lembur_minimal then 1 else 0 end) ,0) as lembur,  " & _
            " isnull((select sum(libur) from vw_absensi where nik=m.nik  and convert(varchar(20),tanggal,112) between '" & Format(dtAwal.Value, "yyyyMMdd") & "' and '" & Format(dtAkhir.Value, "yyyyMMdd") & "' and libur=1 ),0) as libur,  " & _
            " isnull((select sum(total-istirahat) from vw_absensi where nik=m.nik  and convert(varchar(20),tanggal,112) between '" & Format(dtAwal.Value, "yyyyMMdd") & "' and '" & Format(dtAkhir.Value, "yyyyMMdd") & "' and libur=1 ),0) as jamlibur,  " & _
            " isnull((select count(telat) from vw_absensi where nik=m.nik  and convert(varchar(20),tanggal,112) between '" & Format(dtAwal.Value, "yyyyMMdd") & "' and '" & Format(dtAkhir.Value, "yyyyMMdd") & "' and telat>0),0) as jmltelat,  " & _
             " isnull((select count(pc) from vw_absensi where nik=m.nik  and convert(varchar(20),tanggal,112) between '" & Format(dtAwal.Value, "yyyyMMdd") & "' and '" & Format(dtAkhir.Value, "yyyyMMdd") & "' and pc>0),0) as jmlpc  " & _
            " from ms_karyawan m " & _
            " left join vw_absensi v on m.nik=v.nik and convert(varchar(20),tanggal,112) between '" & Format(dtAwal.Value, "yyyyMMdd") & "' and '" & Format(dtAkhir.Value, "yyyyMMdd") & "'  and libur=0 " & _
            "  " & where & " group by m.nik  order by m.nik"

        cmd = New SqlCommand(a, conn)
        dr = cmd.ExecuteReader
        DBGridAbsensi.Rows.Clear()
        While dr.Read
            DBGridAbsensi.Rows.Add()
            With DBGridAbsensi
                .Rows(DBGridAbsensi.Rows.Count - 1).Cells(0).Value = dr.Item("nik") 'NIK
                .Rows(DBGridAbsensi.Rows.Count - 1).Cells(1).Value = Format(dr.Item("masuk"), "#,##0") 'masuk
                .Rows(DBGridAbsensi.Rows.Count - 1).Cells(2).Value = Format(dr.Item("jumlah_jamkerja"), "#,##0") 'jamkerja
                .Rows(DBGridAbsensi.Rows.Count - 1).Cells(3).Value = Format(dr.Item("total"), "#,##0") 'total
                .Rows(DBGridAbsensi.Rows.Count - 1).Cells(4).Value = Format(dr.Item("istirahat"), "#,##0") 'istirahat
                .Rows(DBGridAbsensi.Rows.Count - 1).Cells(5).Value = Format(dr.Item("kerja"), "#,##0") 'kerja
                .Rows(DBGridAbsensi.Rows.Count - 1).Cells("harilembur").Value = Format(dr.Item("lembur"), "#,##0") 'Hari lembur 
                .Rows(DBGridAbsensi.Rows.Count - 1).Cells("jamlembur").Value = Format(dr.Item("jamlembur"), "#,##0") 'jamlembur 
                .Rows(DBGridAbsensi.Rows.Count - 1).Cells("jumlahlibur").Value = Format(dr.Item("libur"), "#,##0") ' libur
                .Rows(DBGridAbsensi.Rows.Count - 1).Cells("JamLibur").Value = Format(dr.Item("jamlibur"), "#,##0") ' jam libur
                .Rows(DBGridAbsensi.Rows.Count - 1).Cells("jmltelat").Value = Format(dr.Item("jmltelat"), "#,##0") ' telat
                .Rows(DBGridAbsensi.Rows.Count - 1).Cells("jmlpc").Value = Format(dr.Item("jmlpc"), "#,##0") ' telat
            End With
        End While
        dr.Close()
        If where <> "" Then where = where.Replace("where", "and ")
        cmd = New SqlCommand("delete from tmp_countshift; Insert into tmp_countshift select nik,kode_groupgaji,nama_shift, COUNT(nama_shift) as jmlshift,SUM(total-lembur) as jamshift, " & _
                            "sum(case when libur=0 and lembur>=lembur_minimal then 1 else 0 end) jmllembur,sum(case when libur=0 and lembur>=lembur_minimal then (case when lembur_kelipatan>0 then floor(lembur/lembur_kelipatan)*lembur_kelipatan else lembur end) else 0 end) jamlembur,sum(case when telat>0 then 1 else 0 end) jmltelat, " & _
                            "sum(case when libur=1 and lembur>=lembur_minimal then 1 else 0 end) jmllembur_libur,sum(case when libur=1 and lembur>=lembur_minimal then (case when lembur_kelipatan>0 then floor(lembur/lembur_kelipatan)*lembur_kelipatan else lembur end) else 0 end) jamlembur_libur,sum(case when pc>0 then 1 else 0 end) jmlpc,sum(case when libur=1  then 1 else 0 end) jmlmasuk_libur " & _
                            "from vw_absensi  where convert(varchar(20),tanggal,112) between '" & Format(dtAwal.Value, "yyyyMMdd") & "' and '" & Format(dtAkhir.Value, "yyyyMMdd") & "' " & _
                            " " & where & " group by  nik,kode_groupgaji,nama_shift", conn)
        cmd.ExecuteNonQuery()

        conn.Close()

    End Sub
    Private Sub process_payroll()
        Dim conn As New SqlConnection
        Dim cmd As New SqlCommand
        Dim dr As SqlDataReader
        conn.ConnectionString = strcon
        conn.Open()
        'generate payroll
        cmd = New SqlCommand(" exec sp_ProsesPayroll '" & cmbTemplate.Text & "'", conn)
        cmd.ExecuteNonQuery()

        'Load Nilai
        cmd = New SqlCommand("select nik,kode_komponen,SUM(nilai) nilai from tmp_payroll group by nik,kode_komponen order by nik,kode_komponen", conn)
        dr = cmd.ExecuteReader
        While dr.Read
            For i = 0 To DBGrid.Rows.Count - 1
                For x = 2 To DBGrid.Columns.Count - 1
                    If DBGrid.Rows(i).Cells(0).Value = dr.Item(0) And DBGrid.Columns(x).Name = dr.Item(1) Then
                        DBGrid.Rows(i).Cells(x).Value = Format(dr.Item(2), "#,##0")
                    End If
                Next
            Next
        End While
        dr.Close()

    End Sub

    Private Sub process_payroll1()
        Dim conn As New SqlConnection
        Dim cmd As New SqlCommand
        Dim dr As SqlDataReader
        Dim komponenindex As Integer
        conn.ConnectionString = strcon
        conn.Open()

        For i = 0 To DBGrid.Rows.Count - 1
            i = i
            For j = 2 To DBGrid.Columns.Count - 2

                With DBGrid
                    For h = 0 To DBGridKomponen.Rows.Count - 1
                        If .Columns(j).Name = DBGridKomponen.Rows(h).Cells(0).Value Then
                            komponenindex = h
                        End If
                    Next

                    If DBGridKomponen.Rows(komponenindex).Cells(9).Value = True Then
                        cmd = New SqlCommand("select isnull(sum(jumlah),0) from t_custompayroll where (convert(varchar(20),tanggal,112) between '" & Format(dtAwal.Value, "yyyyMMdd") & "' and '" & Format(dtAkhir.Value, "yyyyMMdd") & "') and nik='" & .Rows(i).Cells(0).Value & "'", conn)
                        dr = cmd.ExecuteReader
                        If dr.Read Then DBGrid.Rows(i).Cells(j).Value = dr.Item(0)

                    Else
                        cmd = New SqlCommand("select nilai from ms_karyawan_tipe m inner join ms_groupgajid d on m.kode_groupgaji=d.kode_groupgaji  and d.kode_komponen='" & .Columns(j).Name & "' left join ms_karyawan n on m.NIK = n.nik where n.nik='" & .Rows(i).Cells(0).Value & "'", conn)
                        dr = cmd.ExecuteReader
                        If dr.Read Then
                            If DBGridKomponen.Rows(komponenindex).Cells("Libur").Value = True And DBGridKomponen.Rows(komponenindex).Cells("normal").Value = False Then ' libur
                                If DBGridKomponen.Rows(komponenindex).Cells(3).Value Then ' harian
                                    DBGrid.Rows(i).Cells(j).Value = dr.Item(0) * DBGridAbsensi.Rows(i).Cells("jumlahlibur").Value
                                ElseIf DBGridKomponen.Rows(komponenindex).Cells(4).Value Then ' jam
                                    DBGrid.Rows(i).Cells(j).Value = dr.Item(0) * DBGridAbsensi.Rows(i).Cells("jamlibur").Value
                                End If
                            ElseIf DBGridKomponen.Rows(komponenindex).Cells("Lembur").Value = True Then ' lembur
                                If DBGridKomponen.Rows(komponenindex).Cells("normal").Value = True And DBGridKomponen.Rows(komponenindex).Cells("libur").Value = False Then
                                    If DBGridKomponen.Rows(komponenindex).Cells(3).Value Then ' harian
                                        DBGrid.Rows(i).Cells(j).Value = dr.Item(0) * DBGridAbsensi.Rows(i).Cells("harilembur").Value
                                    ElseIf DBGridKomponen.Rows(komponenindex).Cells(4).Value Then ' jam
                                        DBGrid.Rows(i).Cells(j).Value = dr.Item(0) * DBGridAbsensi.Rows(i).Cells("jamlembur").Value
                                    Else
                                        DBGrid.Rows(i).Cells(j).Value = dr.Item(0)
                                    End If
                                Else
                                    If DBGridKomponen.Rows(komponenindex).Cells(3).Value Then ' harian
                                        DBGrid.Rows(i).Cells(j).Value = dr.Item(0) * (CDbl(DBGridAbsensi.Rows(i).Cells("harilembur").Value) + CDbl(DBGridAbsensi.Rows(i).Cells("jumlahlibur").Value))
                                    ElseIf DBGridKomponen.Rows(komponenindex).Cells(4).Value Then ' jam
                                        DBGrid.Rows(i).Cells(j).Value = dr.Item(0) * (CDbl(DBGridAbsensi.Rows(i).Cells("jamlembur").Value) + CDbl(DBGridAbsensi.Rows(i).Cells("jamlibur").Value))
                                    Else
                                        DBGrid.Rows(i).Cells(j).Value = dr.Item(0)
                                    End If
                                End If
                            Else
                                If DBGridKomponen.Rows(komponenindex).Cells("normal").Value And DBGridKomponen.Rows(komponenindex).Cells("libur").Value Then
                                    If DBGridKomponen.Rows(komponenindex).Cells(3).Value Then ' harian
                                        DBGrid.Rows(i).Cells(j).Value = dr.Item(0) * (CDbl(DBGridAbsensi.Rows(i).Cells("Masuk").Value) + CDbl(DBGridAbsensi.Rows(i).Cells("jumlahlibur").Value))
                                    ElseIf DBGridKomponen.Rows(komponenindex).Cells(4).Value Then ' jam
                                        DBGrid.Rows(i).Cells(j).Value = dr.Item(0) * (CDbl(DBGridAbsensi.Rows(i).Cells("kerja").Value) + CDbl(DBGridAbsensi.Rows(i).Cells("jamlibur").Value))
                                    Else
                                        DBGrid.Rows(i).Cells(j).Value = dr.Item(0)
                                    End If
                                Else
                                    If DBGridKomponen.Rows(komponenindex).Cells(3).Value Then ' harian
                                        DBGrid.Rows(i).Cells(j).Value = dr.Item(0) * (CDbl(DBGridAbsensi.Rows(i).Cells("Masuk").Value))
                                    ElseIf DBGridKomponen.Rows(komponenindex).Cells(4).Value Then ' jam
                                        DBGrid.Rows(i).Cells(j).Value = dr.Item(0) * (CDbl(DBGridAbsensi.Rows(i).Cells("kerja").Value))
                                    Else
                                        DBGrid.Rows(i).Cells(j).Value = dr.Item(0)
                                    End If
                                End If
                            End If
                        Else
                            DBGrid.Rows(i).Cells(j).Value = 0
                        End If
                        dr.Close()
                    End If
                    DBGrid.Rows(i).Cells(j).Value = Format(DBGrid.Rows(i).Cells(j).Value, "#,##0")
                    If DBGridKomponen.Rows(komponenindex).Cells(6).Value And DBGridAbsensi.Rows(i).Cells(1).Value < (DateDiff(DateInterval.Day, dtAwal.Value, dtAkhir.Value) + 1 - jumlah_harilibur) Then
                        DBGrid.Rows(i).Cells(j).Value = 0
                    End If
                End With
            Next
        Next
        conn.Close()

    End Sub
    Private Sub hitung_ulang()
        Dim rowtotal As Double = 0
        Dim total As Double = 0
        Dim pecah As Double = 0
        Dim selisih As Double = 0
        Dim totaluang As Double = 0
        lblTotal.Text = 0
        Dim lst As New List(Of KeyValuePair(Of Long, Integer))(matauang)
        Dim key As KeyValuePair(Of Long, Integer)
        'key = matauang.keys.
        For Each key In lst
            matauang(key.Key) = 0
        Next
        total = 0
        selisih = 0
        totaluang = 0
        For i = 0 To DBGrid.Rows.Count - 1
            rowtotal = 0
            For j = 2 To DBGrid.Columns.Count - 2
                rowtotal += DBGrid.Rows(i).Cells(j).Value
            Next
            pecah = rowtotal
            'While pecah > 100
            For Each key In lst
                If key.Key <= pecah Then
                    matauang(key.Key) += (pecah \ key.Key)
                    pecah -= (pecah \ key.Key) * key.Key
                End If
            Next
            'End While
            If pecah > 0 Then
                Debug.Print(pecah)
                selisih += pecah
            End If
            pecah = 0
            DBGrid.Rows(i).Cells(DBGrid.Columns.Count - 1).Value = Format(rowtotal, "#,##0")
            total += rowtotal
        Next
        lblTotal.Text = Format(total, "#,##0")
        For Each key In lst
            Controls("lbl" & key.Key).Text = Format(matauang(key.Key), "#,##0")
            totaluang += key.Key * matauang(key.Key)
        Next
        Debug.Print(Format(totaluang, "#,##0") & "+" & Format(selisih, "#,##0"))
    End Sub
    Private Sub simpan()
        Dim conn As New SqlConnection
        Dim cmd As New SqlCommand
        Dim tr As SqlTransaction
        Dim id As String
        Dim i As Integer, j As Integer
        id = lblNoTrans.Text

        conn.ConnectionString = strcon
        conn.Open()

        Try
            tr = conn.BeginTransaction()
            If lblNoTrans.Text = "" Then
                lblNoTrans.Text = newid("t_payrollh", "nomer_payroll", dtAkhir.Value, "PR")
            Else
                cmd = New SqlCommand("delete from t_payrollh where nomer_payroll='" & id & "'", conn, tr)
                cmd = New SqlCommand("delete from t_payrolld where nomer_payroll='" & id & "'", conn, tr)
                cmd = New SqlCommand("delete from t_payroll_komponen where nomer_payroll='" & id & "'", conn, tr)
                cmd.ExecuteNonQuery()
            End If

            cmd = New SqlCommand(add_dataheader(), conn, tr)
            cmd.ExecuteNonQuery()
            For i = 0 To DBGridAbsensi.Rows.Count - 1
                cmd = New SqlCommand(add_datadetail(i), conn, tr)
                cmd.ExecuteNonQuery()
                For j = 2 To DBGrid.Columns.Count - 2
                    cmd = New SqlCommand(add_nilai(i, j), conn, tr)
                    cmd.ExecuteNonQuery()
                Next
            Next
            tr.Commit()
            conn.Close()

            MsgBox("Data tersimpan")
        Catch ex As Exception
            lblNoTrans.Text = id
            tr.Rollback()
            MsgBox(ex.Message)
            If conn.State Then conn.Close()
        End Try
    End Sub
    Private Function add_datadetail(ByVal row As Integer) As String
        Dim fields() As String
        Dim nilai() As String
        Dim table_name As String
        ReDim fields(11)
        ReDim nilai(11)
        table_name = "t_payrolld"
        fields(0) = "nomer_payroll"
        nilai(0) = lblNoTrans.Text
        fields(1) = "NIK"
        nilai(1) = DBGridAbsensi.Rows(row).Cells(0).Value
        fields(2) = "masuk"
        nilai(2) = DBGridAbsensi.Rows(row).Cells(1).Value
        fields(3) = "total"
        nilai(3) = CDbl(DBGrid.Rows(row).Cells(DBGrid.Columns.Count - 1).Value)
        fields(4) = "jumlah_jamkerja"
        nilai(4) = CDbl(DBGridAbsensi.Rows(row).Cells(2).Value)
        fields(5) = "jumlah_kerja"
        nilai(5) = CDbl(DBGridAbsensi.Rows(row).Cells(5).Value)
        fields(6) = "jumlah_istirahat"
        nilai(6) = CDbl(DBGridAbsensi.Rows(row).Cells(4).Value)
        fields(7) = "jumlah_lembur"
        nilai(7) = CDbl(DBGridAbsensi.Rows(row).Cells(6).Value)
        fields(8) = "no_urut"
        nilai(8) = row
        fields(9) = "libur"
        nilai(9) = CDbl(DBGridAbsensi.Rows(row).Cells(7).Value)
        fields(10) = "jumlah_jamlibur"
        nilai(10) = CDbl(DBGridAbsensi.Rows(row).Cells(7).Value)

        add_datadetail = query_tambah_data(table_name, fields, nilai)
    End Function
    Private Function add_nilai(ByVal row As Integer, ByVal col As Integer) As String
        Dim fields() As String
        Dim nilai() As String
        Dim table_name As String
        ReDim fields(5)
        ReDim nilai(5)
        table_name = "t_payroll_komponen"
        fields(0) = "nomer_payroll"
        nilai(0) = lblNoTrans.Text
        fields(1) = "NIK"
        nilai(1) = DBGridAbsensi.Rows(row).Cells(0).Value
        fields(2) = "kode_komponen"
        nilai(2) = DBGrid.Columns(col).Name
        fields(3) = "nilai"
        nilai(3) = CDbl(DBGrid.Rows(row).Cells(col).Value)
        fields(4) = "no_urut"
        nilai(4) = col

        add_nilai = query_tambah_data(table_name, fields, nilai)
    End Function
    Private Function add_dataheader() As String
        Dim fields() As String
        Dim nilai() As String
        Dim table_name As String
        ReDim fields(13)
        ReDim nilai(13)
        table_name = "t_payrollh"
        fields(0) = "nomer_payroll"
        nilai(0) = lblNoTrans.Text
        fields(1) = "tanggal"
        nilai(1) = Format(dtAkhir.Value, "yyyy/MM/dd hh:mm:ss")
        fields(2) = "periode_awal"
        nilai(2) = Format(dtAwal.Value, "yyyy/MM/dd")
        fields(3) = "periode_akhir"
        nilai(3) = Format(dtAkhir.Value, "yyyy/MM/dd")
        fields(4) = "userid"
        nilai(4) = username
        fields(5) = "keterangan"
        nilai(5) = txtKeterangan.Text
        fields(6) = "total"
        nilai(6) = CDbl(lblTotal.Text)
        fields(7) = "jenis"
        nilai(7) = cmbTemplate.Text
        fields(8) = "jumlah_harikerja"
        nilai(8) = DateDiff(DateInterval.Day, dtAwal.Value, dtAkhir.Value)
        fields(9) = "karyawan_tipe"
        nilai(9) = cmbTipe.Text
        fields(10) = "karyawan_area"
        nilai(10) = cmbArea.Text
        fields(11) = "karyawan_divisi"
        nilai(11) = cmbDivisi.Text
        fields(12) = "karyawan_groupkerja"
        nilai(12) = cmbGroupKerja.Text
        add_dataheader = query_tambah_data(table_name, fields, nilai)
    End Function
    Private Sub cmbTemplate_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbTemplate.SelectedIndexChanged
        load_template()
    End Sub

    Private Sub frmTransPayroll_KeyDown(sender As Object, e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        If e.KeyCode = Keys.Enter Then SendKeys.Send("{tab}")
        If e.KeyCode = Keys.Escape Then Me.Close()
        If e.KeyCode = Keys.F5 Then btnSearch.PerformClick()
    End Sub

    Private Sub frmTransPayroll_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        dtAwal.Value = Now
        dtAkhir.Value = Now
        matauang.Add(100000, 0)
        matauang.Add(50000, 0)
        matauang.Add(20000, 0)
        matauang.Add(10000, 0)
        matauang.Add(5000, 0)
        matauang.Add(2000, 0)
        matauang.Add(1000, 0)
        matauang.Add(500, 0)
        matauang.Add(200, 0)
        matauang.Add(100, 0)
        load_komponen()
        reloadcombo()
        If Grid_komponengaji Then
            DBGridKomponen.Visible = False
        Else
            DBGridKomponen.Visible = True
        End If
        If display_total Then
            Me.DBGridAbsensi.Columns("Total").Visible = False
        Else
            Me.DBGridAbsensi.Columns("Total").Visible = True
        End If
        If display_jumlahjamkerja Then
            Me.DBGridAbsensi.Columns("Jamkerja").Visible = False
        Else
            Me.DBGridAbsensi.Columns("Jamkerja").Visible = True
        End If
        If display_istirahat Then
            Me.DBGridAbsensi.Columns("Istirahat").Visible = False
        Else
            Me.DBGridAbsensi.Columns("Istirahat").Visible = True
        End If
        If display_kerja Then
            Me.DBGridAbsensi.Columns("Kerja").Visible = False
        Else
            Me.DBGridAbsensi.Columns("Kerja").Visible = True
        End If
        If display_telat Then
            Me.DBGridAbsensi.Columns("jmltelat").Visible = False
        Else
            Me.DBGridAbsensi.Columns("jmltelat").Visible = True
        End If
    End Sub

    Private Sub btnProcess_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnProcess.Click
        load_karyawan()
        Label12.Text = DBGridAbsensi.Rows.Count
        Label7.Text = DBGrid.Rows.Count
        process_payroll()
        hitung_ulang()
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        simpan()

    End Sub
    Private Sub cari_data()
        Dim conn As New SqlConnection
        Dim cmd As New SqlCommand
        Dim dr As SqlDataReader
        conn.ConnectionString = strcon
        conn.Open()
        DBGrid.Rows.Clear()
        DBGridAbsensi.Rows.Clear()

        cmd = New SqlCommand("select * from t_payrollh where nomer_payroll='" & lblNoTrans.Text & "'", conn)
        dr = cmd.ExecuteReader

        If dr.Read Then
            dtAwal.Value = dr.Item("periode_awal")
            dtAkhir.Value = dr.Item("periode_akhir")
            cmbTemplate.Text = dr.Item("jenis")
            cmbArea.Text = dr.Item("karyawan_area")
            cmbDivisi.Text = dr.Item("karyawan_divisi")
            cmbTipe.Text = dr.Item("karyawan_tipe")
            cmbGroupKerja.Text = dr.Item("karyawan_groupkerja")
            lblTotal.Text = Format(dr.Item("total"), "#,##0")
        Else
            GoTo close
        End If
        dr.Close()
        load_komponen()
        cmd = New SqlCommand("select d.*,m.nama_depan from t_payrolld d left join ms_karyawan m on d.nik=m.nik where nomer_payroll='" & lblNoTrans.Text & "' order by no_urut", conn)
        dr = cmd.ExecuteReader
        While dr.Read
            With DBGrid
                .Rows.Add()
                .Rows(.Rows.Count - 1).Cells(0).Value = dr.Item("nik")
                .Rows(.Rows.Count - 1).Cells(1).Value = dr.Item("nama_depan")
            End With
            With DBGridAbsensi
                .Rows.Add()
                .Rows(.Rows.Count - 1).Cells(0).Value = dr.Item("nik")
                .Rows(.Rows.Count - 1).Cells(1).Value = dr.Item("masuk")
                .Rows(.Rows.Count - 1).Cells(2).Value = dr.Item("jumlah_jamkerja")
                .Rows(.Rows.Count - 1).Cells(3).Value = dr.Item("jumlah_lembur")
                .Rows(.Rows.Count - 1).Cells(4).Value = dr.Item("jumlah_istirahat")
                .Rows(.Rows.Count - 1).Cells(5).Value = dr.Item("jumlah_kerja")
                .Rows(.Rows.Count - 1).Cells(6).Value = dr.Item("jumlah_lembur")
                .Rows(.Rows.Count - 1).Cells(7).Value = dr.Item("libur")
                .Rows(.Rows.Count - 1).Cells(8).Value = dr.Item("jumlah_jamlibur")
            End With
        End While
        dr.Close()
        For i = 0 To DBGridAbsensi.Rows.Count - 1
            For j = 2 To DBGrid.Columns.Count - 2
                cmd = New SqlCommand("select * from t_payroll_komponen where nomer_payroll='" & lblNoTrans.Text & "' and nik='" & DBGrid.Rows(i).Cells(0).Value & "' and kode_komponen='" & DBGrid.Columns(j).Name & "'", conn)
                dr = cmd.ExecuteReader
                If dr.Read Then
                    DBGrid.Rows(i).Cells(j).Value = Format(dr.Item("nilai"), "#,##0")
                End If
                dr.Close()
            Next
        Next
close:

        conn.Close()
    End Sub
    Private Sub btnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Dim f As New frmSearch
        f.setCol(0)
        f.setTableName("t_payrollh")
        f.setColumns("*")
        f.setOrder("order by tanggal desc")
        If f.ShowDialog = System.Windows.Forms.DialogResult.OK Then
            lblNoTrans.Text = f.value
            cari_data()
        End If
        f.Dispose()
        hitung_ulang()
    End Sub

    Private Sub btnPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrint.Click
        frmPrint.reportname = "print out gaji karyawan.rpt"
        frmPrint.filter = "{t_payrollh.nomer_payroll}='" & lblNoTrans.Text & "'"
        'frmPrint.Show()
        frmPrint.load_report()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrintTTD.Click
        frmPrint.reportname = "print out gaji ttd.rpt"
        frmPrint.filter = "{t_payrollh.nomer_payroll}='" & lblNoTrans.Text & "'"
        frmPrint.load_report()
    End Sub

    Private Sub btnPrintAbsensi_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrintAbsensi.Click
        frmPrint.reportname = "print out gaji karyawan.rpt"
        frmPrint.filter = "{t_payrollh.nomer_payroll}='" & lblNoTrans.Text & "'"
        frmPrint.load_report()
    End Sub

    Private Sub DBGrid_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DBGrid.CellContentClick

    End Sub

    Private Sub dtTanggalAkhir_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtAkhir.ValueChanged
        cek_harilibur()
    End Sub

    Private Sub dtTanggalAwal_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtAwal.ValueChanged
        cek_harilibur()
    End Sub

    Private Sub DBGridAbsensi_CellDoubleClick(sender As Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DBGridAbsensi.CellDoubleClick
        frmTransRekapAbsenNIK.dtAwal.Value = dtAwal.Value
        frmTransRekapAbsenNIK.dtAkhir.Value = dtAkhir.Value
        frmTransRekapAbsenNIK.txtSearch.Text = DBGridAbsensi.Item(0, DBGridAbsensi.CurrentRow.Index).Value
        frmTransRekapAbsenNIK.Show()
    End Sub

    Private Sub btnExit_Click(sender As System.Object, e As System.EventArgs) Handles btnExit.Click
        Me.Close()
    End Sub

    Private Sub btnPrintGaji_Click(sender As System.Object, e As System.EventArgs) Handles btnPrintGaji.Click
        printout("{t_payrollh.nomer_payroll}='" & lblNoTrans.Text & "'", "\Printout pengajuan_gaji.rpt")
    End Sub

    Private Sub Button2_Click(sender As System.Object, e As System.EventArgs) Handles Button2.Click
        Dim conn As New SqlConnection
        Dim cmd As New SqlCommand
        conn.ConnectionString = strcon
        conn.Open()
        cmd = New SqlCommand("exec sp_countpekerjaan '" & dtAwal.Value & "','" & dtAkhir.Value & "'", conn)
        cmd.ExecuteNonQuery()
        conn.Close()

        printout("{t_payrollh.nomer_payroll}='" & lblNoTrans.Text & "'", "\Printout Slip Gaji.rpt")
    End Sub

    Private Sub Button1_Click_1(sender As System.Object, e As System.EventArgs) Handles Button1.Click
        Dim conn As New SqlConnection
        Dim cmd As New SqlCommand
        conn.ConnectionString = strcon
        conn.Open()
        cmd = New SqlCommand(" exec  sp_countpekerjaan '" & dtAwal.Value & "','" & dtAkhir.Value & "'", conn)
        cmd.ExecuteNonQuery()
        conn.Close()

        printout("{t_payrollh.nomer_payroll}='" & lblNoTrans.Text & "'", "\Printout Slip Gaji.rpt", printer1, "1/2 A4")
    End Sub
End Class