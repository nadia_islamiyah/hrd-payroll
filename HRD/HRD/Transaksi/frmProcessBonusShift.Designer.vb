﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmProcessBonusShift
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.dtTanggal = New System.Windows.Forms.DateTimePicker()
        Me.DBGrid = New System.Windows.Forms.DataGridView()
        Me.NIK = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Nama = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Total = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Lembur = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Bonus = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.btnProccess = New System.Windows.Forms.Button()
        CType(Me.DBGrid, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'dtTanggal
        '
        Me.dtTanggal.CustomFormat = "dd/MM/yyyy"
        Me.dtTanggal.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtTanggal.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtTanggal.Location = New System.Drawing.Point(12, 9)
        Me.dtTanggal.Name = "dtTanggal"
        Me.dtTanggal.Size = New System.Drawing.Size(128, 23)
        Me.dtTanggal.TabIndex = 25
        Me.dtTanggal.Value = New Date(2013, 3, 2, 0, 0, 0, 0)
        '
        'DBGrid
        '
        Me.DBGrid.AllowUserToAddRows = False
        Me.DBGrid.AllowUserToDeleteRows = False
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DBGrid.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.DBGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DBGrid.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.NIK, Me.Nama, Me.Total, Me.Lembur, Me.Bonus})
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DBGrid.DefaultCellStyle = DataGridViewCellStyle2
        Me.DBGrid.Location = New System.Drawing.Point(12, 46)
        Me.DBGrid.Name = "DBGrid"
        Me.DBGrid.Size = New System.Drawing.Size(841, 458)
        Me.DBGrid.TabIndex = 24
        '
        'NIK
        '
        Me.NIK.HeaderText = "NIK"
        Me.NIK.Name = "NIK"
        Me.NIK.ReadOnly = True
        Me.NIK.Width = 90
        '
        'Nama
        '
        Me.Nama.HeaderText = "Nama"
        Me.Nama.Name = "Nama"
        Me.Nama.ReadOnly = True
        Me.Nama.Width = 200
        '
        'Total
        '
        Me.Total.HeaderText = "Total"
        Me.Total.Name = "Total"
        Me.Total.ReadOnly = True
        Me.Total.Width = 60
        '
        'Lembur
        '
        Me.Lembur.HeaderText = "Lembur"
        Me.Lembur.Name = "Lembur"
        Me.Lembur.ReadOnly = True
        Me.Lembur.Width = 60
        '
        'Bonus
        '
        Me.Bonus.HeaderText = "Bonus"
        Me.Bonus.Name = "Bonus"
        '
        'btnProccess
        '
        Me.btnProccess.Location = New System.Drawing.Point(146, 9)
        Me.btnProccess.Name = "btnProccess"
        Me.btnProccess.Size = New System.Drawing.Size(75, 23)
        Me.btnProccess.TabIndex = 26
        Me.btnProccess.Text = "proses"
        Me.btnProccess.UseVisualStyleBackColor = True
        '
        'frmProcessBonusShift
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(865, 516)
        Me.Controls.Add(Me.btnProccess)
        Me.Controls.Add(Me.dtTanggal)
        Me.Controls.Add(Me.DBGrid)
        Me.KeyPreview = True
        Me.Name = "frmProcessBonusShift"
        Me.Text = "frmProcessBonusShift"
        CType(Me.DBGrid, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents dtTanggal As System.Windows.Forms.DateTimePicker
    Friend WithEvents DBGrid As System.Windows.Forms.DataGridView
    Friend WithEvents NIK As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Nama As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Total As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Lembur As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Bonus As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents btnProccess As System.Windows.Forms.Button
End Class
