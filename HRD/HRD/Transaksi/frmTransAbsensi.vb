﻿Imports System.Data.SqlClient
Imports System.Data.Sql
Imports System.IO
Public Class frmTransAbsensi

    Private Sub btnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Dim f As New frmSearch
        f.setCol(0)
        f.setTableName("ms_karyawan")
        f.setColumns("nik,nama_depan,nama_belakang,nama_panggilan,alamat")

        If f.ShowDialog = System.Windows.Forms.DialogResult.OK Then
            txtNIK.Text = f.value
            cari_data()
        End If
        f.Dispose()
    End Sub
    Public Sub cari_data()
        Dim conn As New SqlConnection
        Dim cmd As New SqlCommand
        Dim dr As SqlDataReader
        conn.ConnectionString = strcon
        conn.Open()
        jamIstirahat.Value = Format(dtTanggal.Value, "yyyy/MM/dd 00:00:00")
        jamMasuk.Value = Format(dtTanggal.Value, "yyyy/MM/dd 00:00:00")
        jamKembali.Value = Format(dtTanggal.Value, "yyyy/MM/dd 00:00:00")
        jamPulang.Value = Format(dtTanggal.Value, "yyyy/MM/dd 00:00:00")

        cmd = New SqlCommand("select * from ms_karyawan where nik='" & txtNIK.Text & "'", conn)
        dr = cmd.ExecuteReader

        If dr.Read Then
            txtNIK.Text = dr.Item("nik")
            txtNama.Text = dr.Item("nama_depan") & " " & dr.Item("nama_belakang")

        End If
        dr.Close()
        cmd = New SqlCommand("select * from t_rekapabsensi where nik='" & txtNIK.Text & "'  and convert(varchar(8),tanggal,112)='" & Format(dtTanggal.Value, "yyyyMMdd") & "'", conn)
        dr = cmd.ExecuteReader
        chkMasuk.Checked = False
        chkIstirahat.Checked = False
        chkKembali.Checked = False
        chkPulang.Checked = False
        If dr.Read Then
            chkMasuk.Checked = dr.Item("status_datang")
            chkIstirahat.Checked = dr.Item("status_istirahat_out")
            chkKembali.Checked = dr.Item("status_istirahat_in")
            chkPulang.Checked = dr.Item("status_pulang")
            If dr.Item("status_datang") Then jamMasuk.Value = dr.Item("datang")
            If dr.Item("status_istirahat_out") Then jamIstirahat.Value = dr.Item("istirahat_out")

            If dr.Item("status_istirahat_in") Then jamKembali.Value = dr.Item("istirahat_in")
            If dr.Item("status_pulang") Then jamPulang.Value = dr.Item("pulang")
        End If
        dr.Close()
    End Sub

    Private Sub txtNIK_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtNIK.LostFocus
        If txtNIK.Text <> "" Then cari_data()
    End Sub

    Private Sub txtNIK_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtNIK.TextChanged

    End Sub

    Private Sub dtTanggal_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtTanggal.ValueChanged
        If txtNIK.Text <> "" Then cari_data()
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim conn As New SqlConnection
        Dim cmd As New SqlCommand
        conn.ConnectionString = strcon
        conn.Open()
        cmd = New SqlCommand("delete from t_rekapabsensi where nik='" & txtNIK.Text & "'  and convert(varchar(8),tanggal,112)='" & Format(dtTanggal.Value, "yyyyMMdd") & "'", conn)
        cmd.ExecuteNonQuery()
        cmd = New SqlCommand("insert into t_rekapabsensi (nik,tanggal,status_datang,status_istirahat_out,status_istirahat_in,status_pulang,datang,istirahat_out,istirahat_in,pulang) values " & _
                             "('" & txtNIK.Text & "','" & Format(dtTanggal.Value, "yyyyMMdd") & "'," & _
                             "'" & chkMasuk.Checked & "', " & _
                             "'" & chkIstirahat.Checked & "', " & _
                             "'" & chkKembali.Checked & "', " & _
                             "'" & chkPulang.Checked & "', " & _
                             "'" & Format(jamMasuk.Value, "yyyy/MM/dd HH:mm:ss") & "', " & _
                             "'" & Format(jamIstirahat.Value, "yyyy/MM/dd HH:mm:ss") & "', " & _
                             "'" & Format(jamKembali.Value, "yyyy/MM/dd HH:mm:ss") & "', " & _
                             "'" & Format(jamPulang.Value, "yyyy/MM/dd HH:mm:ss") & "' " & _
                             " ) " _
                             , conn)
        cmd.ExecuteNonQuery()
        conn.Close()
        Me.Dispose()
    End Sub

    Private Sub frmTransAbsensi_KeyDown(sender As Object, e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        If e.KeyCode = Keys.Enter Then SendKeys.Send("{tab}")
        If e.KeyCode = Keys.Escape Then Me.Close()
    End Sub

    Private Sub frmTransAbsensi_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load

    End Sub
End Class