﻿Imports System.Data.SqlClient
Imports System.IO

Public Class frmLaporanPenjualan
    Public formula As String
    Public filtertanggal As String
    Public filtertanggal2 As String
    Public filterKategori As String
    Public filterBarang As String, filterPenempatan As String, filterBagian As String
    Public filterMeja As String
    Public filterUser As String
    Public filterTipe As String
    Public filterCarabayar As String
    Public filename As String
    Public strformula As String
    Public barangtable As String = "vw_inventory"
    Public barangwhere As String = ""
    Public barangcolumn As String = "*"
    Public querymeja As String = "select kode_meja,kode_meja as kode1 from ms_meja order by kode_meja"
    Public querykategori As String = "select * from ms_kategori order by nama"
    Private Sub btnExit_Click(sender As System.Object, e As System.EventArgs) Handles btnExit.Click
        Close()
    End Sub
    Private Sub btnPrint_Click(sender As System.Object, e As System.EventArgs) Handles btnPrint.Click
        formula = strformula
        If GroupTanggal.Visible = True And filtertanggal <> "" Then
            If formula <> "" Then formula = formula & " and "
            If dtSampai.Visible = True Then
                formula = formula & filtertanggal & ">=#" & dtDari.Value.ToString("MM/dd/yyyy") & "# and " & filtertanggal & "<#" & DateAdd(DateInterval.Day, 1, dtSampai.Value).ToString("MM/dd/yyyy") & "#"
            Else
                formula = formula & filtertanggal & "=#" & dtSampai.Value.ToString("MM/dd/yyyy") & "#"
            End If
        End If
        If filename = "\Laporan Absensi per NIK.rpt" then
            Dim conn As New SqlConnection
            Dim cmd As New SqlCommand
            conn.ConnectionString = strcon
            conn.Open()
            cmd = New SqlCommand("delete from tmp_countshift; Insert into tmp_countshift select nik,kode_groupgaji,nama_shift, COUNT(nama_shift) as jmlshift,SUM(total-lembur) as jamshift, " & _
                                "sum(case when libur=0 and lembur>=lembur_minimal then 1 else 0 end) jmllembur,sum(case when libur=0 and lembur>=lembur_minimal then (case when lembur_kelipatan>0 then floor(lembur/lembur_kelipatan)*lembur_kelipatan else lembur end) else 0 end) jamlembur,sum(case when telat>0 then 1 else 0 end) jmltelat, " & _
                                "sum(case when libur=1 and lembur>=lembur_minimal then 1 else 0 end) jmllembur_libur,sum(case when libur=1 and lembur>=lembur_minimal then (case when lembur_kelipatan>0 then floor(lembur/lembur_kelipatan)*lembur_kelipatan else lembur end) else 0 end) jamlembur_libur,sum(case when pc>0 then 1 else 0 end) jmlpc,sum(case when libur=1  then 1 else 0 end) jmlmasuk_libur  " & _
                                "from vw_absensi  where convert(varchar(20),tanggal,112) between '" & Format(dtDari.Value, "yyyyMMdd") & "' and '" & Format(dtSampai.Value, "yyyyMMdd") & "' " & _
                                " group by  nik,kode_groupgaji,nama_shift", conn)
            cmd.ExecuteNonQuery()
            conn.Close()
        End If
        If GroupKaryawan.Visible = True And filterBarang <> "" And txtNIK.Text <> "" Then
            If formula <> "" Then formula = formula & " and "
            formula += filterBarang & "='" & txtNIK.Text & "'"
        End If
        If filename = "\Laporan Total Jam Kerja.rpt" Then
            executeSQL("exec sp_generate_totalkerja '" & dtDari.Value.ToString("yyyyMMdd") & "','" & dtSampai.Value.ToString("yyyyMMdd") & "' ")
        End If

        If Grouptipe.Visible = True And cmbtipe.Text <> "" Then
            If formula <> "" Then formula = formula & " and "
            formula += filterTipe & "='" & cmbtipe.Text & "'"
        End If
        If GroupPenempatan.Visible = True And filterPenempatan <> "" And cmbPenempatan.Text <> "" Then
            If formula <> "" Then formula = formula & " and "
            formula += filterPenempatan & "='" & cmbPenempatan.Text & "'"
        End If
        If GroupBagian.Visible = True And filterBagian <> "" And CmbBagian.Text <> "" Then
            If formula <> "" Then formula = formula & " and "
            formula += filterBagian & "='" & CmbBagian.Text & "'"
        End If
        Dim f As New frmReportView
        With f
            .filename = filename
            .formula = formula
            .param(1) = CStr(dtDari.Value.ToString("dd/MM/yyyy"))
            .param(2) = CStr(dtSampai.Value.ToString("dd/MM/yyyy"))
            .param(0) = username
            .Show()
        End With
    End Sub

    Private Sub frmLaporanPenjualan_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Load
        loadComboBoxAll(CmbBagian, "distinct groupkerja ", "ms_karyawan_tipe", "", "groupkerja", "groupkerja")
        loadComboBoxAll(cmbPenempatan, "distinct divisi ", "ms_karyawan_tipe", "", "divisi", "divisi")
        loadComboBoxAll(cmbtipe, "distinct tipe ", "ms_karyawan_tipe", "", "tipe", "tipe")
        dtDari.Value = Now
        dtSampai.Value = Now
    End Sub




    Private Sub chkTanggal_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkTanggal.CheckedChanged
        If chkTanggal.Checked = False Then
            GroupTanggal.Visible = False
        Else
            GroupTanggal.Visible = True
        End If
    End Sub

    Private Sub dtDari_ValueChanged(sender As System.Object, e As System.EventArgs) Handles dtDari.ValueChanged
        If Panel1.Visible = False Then
            dtSampai.Value = dtDari.Value
        End If
    End Sub

    Private Sub btnSearchBarang_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSearchBarang.Click
        Dim f As New frmSearch

        f.setCol(0)
        f.setTableName("ms_karyawan")
        f.setColumns("*")
        If barangwhere <> "" Then
            f.setWhere(barangwhere)
        End If
        If f.ShowDialog = System.Windows.Forms.DialogResult.OK Then
            txtNIK.Text = f.value
        End If
    End Sub
End Class