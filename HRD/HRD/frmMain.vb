﻿Imports System.Data.SqlClient
Public Class frmMain

    Private Sub AgamaToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

        frmVarAgama.MdiParent = Me
        frmVarAgama.Show()
    End Sub

    Private Sub frmMain_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        End
    End Sub

    Private Sub frmMain_KeyDown(sender As Object, e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        If e.KeyCode = Keys.F6 Then If Panel1.Visible = True Then Panel1.Visible = False Else Panel1.Visible = True
    End Sub

    Private Sub frmMain_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    End Sub

    Private Sub BankToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        frmVarBank.ShowDialog()
    End Sub

    Private Sub TipeKaryawanToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TipeKaryawanToolStripMenuItem.Click
        frmVarTipeKaryawan.ShowDialog()
    End Sub

    Private Sub PayrollAreaToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PayrollAreaToolStripMenuItem.Click
        frmVarPayrollArea.ShowDialog()
    End Sub

    Private Sub MasterKaryawanToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MasterKaryawanToolStripMenuItem.Click
        Dim MDIChild As New frmMasterKaryawan
        MDIChild.MdiParent = Me
        MDIChild.Show()
        MDIChild.Location = New System.Drawing.Point(0, 0)
    End Sub

    Private Sub DivisiToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DivisiToolStripMenuItem.Click
        frmVarDivisi.ShowDialog()
    End Sub

    Private Sub AbsensiHarianToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim MDIChild As New frmTransAbsenHarian
        MDIChild.MdiParent = Me
        MDIChild.Show()
        MDIChild.Location = New System.Drawing.Point(0, 0)
    End Sub

    Private Sub PengaturanShiftToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PengaturanShiftToolStripMenuItem.Click
        Dim MDIChild As New frmTransJadwalKerja
        MDIChild.MdiParent = Me
        MDIChild.Show()
        MDIChild.Location = New System.Drawing.Point(0, 0)
    End Sub

    Private Sub RekapAbsensiToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RekapAbsensiToolStripMenuItem.Click
        Dim MDIChild As New frmTransRekapAbsenHarian
        MDIChild.MdiParent = Me
        MDIChild.Show()
        MDIChild.Location = New System.Drawing.Point(0, 0)
    End Sub

    Private Sub KelompokGajiToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles KelompokGajiToolStripMenuItem.Click
        Dim MDIChild As New frmMasterGroupGaji
        MDIChild.MdiParent = Me
        MDIChild.Show()
        MDIChild.Location = New System.Drawing.Point(0, 0)
    End Sub

    Private Sub TemplateGajiToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TemplateGajiToolStripMenuItem.Click
        Dim MDIChild As New frmTemplate
        MDIChild.MdiParent = Me
        MDIChild.Show()
        MDIChild.Location = New System.Drawing.Point(0, 0)
    End Sub

    Private Sub PengajuanGajiToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PengajuanGajiToolStripMenuItem.Click
        Dim MDIChild As New frmTransPayroll
        MDIChild.MdiParent = Me
        MDIChild.Show()
        MDIChild.Opacity = 0
        MDIChild.Location = New System.Drawing.Point(0, 0)
    End Sub

    Private Sub BonusShiftToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BonusShiftToolStripMenuItem.Click
        Dim MDIChild As New frmSettingBonusShift
        MDIChild.MdiParent = Me
        MDIChild.Show()
        MDIChild.Location = New System.Drawing.Point(0, 0)
    End Sub

    Private Sub ProsesBonusShiftToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ProsesBonusShiftToolStripMenuItem.Click
        Dim MDIChild As New frmProcessBonusShift
        MDIChild.MdiParent = Me
        MDIChild.Show()
        MDIChild.Location = New System.Drawing.Point(0, 0)
    End Sub

    Private Sub PengaturanHariLiburToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PengaturanHariLiburToolStripMenuItem.Click
        frmSettingHoliday.ShowDialog()
    End Sub

    Private Sub WindowsToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles WindowsToolStripMenuItem.Click

    End Sub

    Private Sub HapusDataAbsensiToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles HapusDataAbsensiToolStripMenuItem.Click
        frmDeleteAbsensi.ShowDialog()
    End Sub

    Private Sub ImportFromAttToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles ImportFromAttToolStripMenuItem.Click
        frmImportAtt.ShowDialog()
    End Sub

    Private Sub GantiPasswordToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles GantiPasswordToolStripMenuItem.Click
        Dim MDIChild As New frmGantiPassword
        MDIChild.MdiParent = Me
        MDIChild.Show()
        MDIChild.Location = New System.Drawing.Point(0, 0)
        MDIChild.Text = sender.ToString
    End Sub

    Private Sub UserToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles UserToolStripMenuItem.Click
        Dim MDIChild As New frmSettingUser
        MDIChild.MdiParent = Me
        MDIChild.Show()
        MDIChild.Location = New System.Drawing.Point(0, 0)
        MDIChild.Text = sender.ToString
    End Sub

    Private Sub SecurityManagerToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles SecurityManagerToolStripMenuItem.Click
        Dim MDIChild As New frmSecurityperGroup
        MDIChild.MdiParent = Me
        MDIChild.Show()
        MDIChild.Location = New System.Drawing.Point(0, 0)
        MDIChild.Text = sender.ToString
    End Sub

    Private Sub MasterKomponenGajiToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles MasterKomponenGajiToolStripMenuItem.Click
        Dim MDIChild As New frmMasterKomponenGaji
        MDIChild.MdiParent = Me
        MDIChild.Show()
        MDIChild.Location = New System.Drawing.Point(0, 0)
        MDIChild.Text = sender.ToString
    End Sub

    Private Sub PrintDataKaryawwanToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles PrintDataKaryawwanToolStripMenuItem.Click
        Dim f As New frmLaporanPenjualan
        With f
            .Grouptipe.Visible = True
            .Text = sender.ToString
            .GroupTanggal.Visible = False
            .chkTanggal.Visible = False
            .GroupKaryawan.Visible = True
            .GroupBagian.Visible = True
            .GroupPenempatan.Visible = True
            .filterBarang = "{ms_karyawan.NIK}"
            .filterTipe = "{ms_karyawan_tipe.tipe}"
            .filterBagian = "{ms_karyawan_tipe.groupkerja}"
            .filterPenempatan = "{ms_karyawan_tipe.divisi}"
            .filename = "\karyawan.rpt"
            .ShowDialog()

        End With
    End Sub

    Private Sub TotalJamKerjaToolStripMenuItem1_Click(sender As System.Object, e As System.EventArgs) Handles TotalJamKerjaToolStripMenuItem1.Click
        Dim f As New frmLaporanPenjualan
        With f
            .Grouptipe.Visible = True
            .Text = sender.ToString
            .GroupKaryawan.Visible = True
            .filterBarang = "{vw_totalkerja.NIK}"
            .filterTipe = "{vw_totalkerja.tipe}"
            .filename = "\Laporan Total Jam Kerja.rpt"
            .ShowDialog()
        End With
    End Sub

    Private Sub DetailAbsensiToolStripMenuItem1_Click(sender As System.Object, e As System.EventArgs) Handles DetailAbsensiToolStripMenuItem1.Click
        Dim f As New frmLaporanPenjualan
        With f
            .Grouptipe.Visible = True
            .Text = sender.ToString
            .filtertanggal = "Date({absensi.tanggal})"
            .GroupKaryawan.Visible = True
            .filterBarang = "{absensi.NIK}"
            .filterTipe = "{ms_karyawan_tipe.tipe}"
            .filename = "\Detail Absensi.rpt"
            .ShowDialog()
        End With
    End Sub

    Private Sub LaporanAbsensiToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles LaporanAbsensiToolStripMenuItem.Click
        Dim f As New frmLaporanPenjualan
        With f
            .Grouptipe.Visible = True
            .Text = sender.ToString
            .filtertanggal = "Date({absensi.tanggal})"
            .GroupKaryawan.Visible = True
            .filterBarang = "{absensi.NIK}"
            .filterTipe = "{ms_karyawan_tipe.tipe}"
            .filename = "\Laporan Absensi.rpt"
            .ShowDialog()
        End With
    End Sub

    Private Sub LaporanAbsensiPerKaryawanToolStripMenuItem1_Click(sender As System.Object, e As System.EventArgs) Handles LaporanAbsensiPerKaryawanToolStripMenuItem1.Click
        Dim f As New frmLaporanPenjualan
        With f

            .Text = sender.ToString
            .filtertanggal = "Date({absensi.tanggal})"
            .GroupKaryawan.Visible = True
            .filterBarang = "{absensi.NIK}"

            .filename = "\Laporan Absensi per NIK.rpt"
            .ShowDialog()

        End With
    End Sub

    Private Sub MasterShiftToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles MasterShiftToolStripMenuItem.Click
        Dim MDIChild As New frmMasterJamKerja
        MDIChild.MdiParent = Me
        MDIChild.Show()
        MDIChild.Location = New System.Drawing.Point(0, 0)
    End Sub

    Private Sub MasterWorkgroupToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles MasterWorkgroupToolStripMenuItem.Click
        Dim MDIChild As New frmMasterWorkgroup
        MDIChild.MdiParent = Me
        MDIChild.Show()
        MDIChild.Location = New System.Drawing.Point(0, 0)
    End Sub

    Private Sub InputPekerjaanToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles InputPekerjaanToolStripMenuItem.Click
        Dim MDIChild As New frmTransHarian
        MDIChild.MdiParent = Me
        MDIChild.Show()
        MDIChild.Location = New System.Drawing.Point(0, 0)
    End Sub

    Private Sub PekerjaanToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles PekerjaanToolStripMenuItem.Click
        frmVarPekerjaan.ShowDialog()
    End Sub

    Private Sub RekapShiftToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles RekapShiftToolStripMenuItem.Click
        Dim MDIChild As New frmTransRekapShift
        MDIChild.MdiParent = Me
        MDIChild.Show()
        MDIChild.Location = New System.Drawing.Point(0, 0)
    End Sub

    Private Sub ManualClockInToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles ManualClockInToolStripMenuItem.Click
        Dim MDIChild As New frmTransManualClockIn
        MDIChild.MdiParent = Me
        MDIChild.Show()
        MDIChild.Location = New System.Drawing.Point(0, 0)
    End Sub

    Private Sub ConfigToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles ConfigToolStripMenuItem.Click
        Dim MDIChild As New frmConfig
        MDIChild.MdiParent = Me
        MDIChild.Show()
        MDIChild.Location = New System.Drawing.Point(0, 0)
        MDIChild.Text = sender.ToString
    End Sub

    Private Sub generaterekapabsensi()
        Dim conn As New SqlConnection
        Dim cmd As New SqlCommand
        Dim dr As SqlDataReader
        conn.ConnectionString = strcon
        conn.Open()
        cmd = New SqlCommand("select top 1 tanggal from t_rekapabsensi  order by tanggal desc", conn)
        dr = cmd.ExecuteReader
        Dim tgl As Date = Now
        If dr.Read Then
            If Format(dr("tanggal"), "yyyyMMdd") <> Format(tgl, "yyyyMMdd") Then
                cmd = New SqlCommand("exec [sp_AutoRekapAbsensi] '" & Format(dr("tanggal"), "yyyyMMdd") & "','" & Format(tgl, "yyyyMMdd") & "'", conn)
                cmd.CommandTimeout = 0
                cmd.ExecuteNonQuery()
                cmd = New SqlCommand("exec [sp_autoshift1] '" & Format(dr("tanggal"), "yyyyMMdd") & "','" & Format(tgl, "yyyyMMdd") & "'", conn)
                cmd.CommandTimeout = 0
                cmd.ExecuteNonQuery()
            End If
        End If
        conn.Close()
    End Sub

    Private Sub Button1_Click(sender As System.Object, e As System.EventArgs) Handles Button1.Click
        If LCase(dbname) = "absensi" Then
            generaterekapabsensi()
            MsgBox("Selesai")
        End If
    End Sub

    Private Sub Timer2_Tick(sender As System.Object, e As System.EventArgs) Handles Timer2.Tick
        Dim TimeEnd As DateTime
        Dim span As System.TimeSpan

        TimeEnd = dtJam1.Value
        span = TimeEnd.TimeOfDay - DateTime.Now.TimeOfDay
        If span.TotalMinutes > -1 And span.TotalMinutes < 1 Then
            Button1.PerformClick()
        End If

        TimeEnd = dtJam2.Value
        span = TimeEnd.TimeOfDay - DateTime.Now.TimeOfDay
        If span.TotalMinutes > -1 And span.TotalMinutes < 1 Then
            Button1.PerformClick()
        End If

        TimeEnd = dtJam3.Value
        span = TimeEnd.TimeOfDay - DateTime.Now.TimeOfDay
        If span.TotalMinutes > -1 And span.TotalMinutes < 1 Then
            Button1.PerformClick()
        End If
    End Sub
End Class