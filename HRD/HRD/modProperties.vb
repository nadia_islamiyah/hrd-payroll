﻿Imports System.Data.SqlClient
Module modProperties
    Public Const dbname1 As String = "3pm-elporindo"
    Public servnameasli As String
    Public Const ServerType As String = "mssql"
    Public Const katasandi As String = "ikan"

    Public servname As String
    Public group, gudang As String
    Public db_user As String, db_password As String
    Public printername As String
    Public right_stock, right_hpp, right_alert, right_harga As Boolean
    Public right_hargaManual As Boolean
    Public right_TipeHarga As String
    Public right_export As String

    Public Grid_komponengaji, display_total, display_jumlahjamkerja, display_istirahat, display_kerja, display_telat As Boolean
    Public rekap_tglistirahat, rekap_tglkembali, rekap_total, rekap_PC, rekap_lembur, rekap_absen, rekap_shift As Boolean
    Public Const cUsername As String = "toko"
    Public dir As String
    Public printer1 As String, printer2 As String, printer3 As String, printer4 As String
    Public printerkasir As String, printeradmin As String
    Public default_editabsen, userchallenge As String
    Public default_namaperusahaan As String
    Public default_alamatperusahaan As String
    Public default_coverFinished As String
    
    Public Sub load_settings()
        Grid_komponengaji = load_setting("Grid_komponengaji")
        default_editabsen = load_setting("Edit Absensi")
        display_total = load_setting("Display total")
        display_jumlahjamkerja = load_setting("Display Jumlah Jam Kerja")
        display_istirahat = load_setting("Display Istirahat")
        display_kerja = load_setting("Display Kerja")
        display_telat = load_setting("Display Telat")
        rekap_tglistirahat = load_setting("rekap_tglistirahat")
        rekap_tglkembali = load_setting("rekap_tglkembali")
        rekap_total = load_setting("rekap_total")
        rekap_PC = load_setting("rekap_PC")
        rekap_lembur = load_setting("rekap_lembur")
        rekap_absen = load_setting("rekap_absen")
        rekap_shift = load_setting("rekap_shift")
    End Sub
    Public Function load_setting(keterangan As String) As String
        Dim conn As New SqlConnection(strcon)
        Dim cmd As SqlCommand
        Dim reader As SqlDataReader

        load_setting = ""
        conn.Open()
        cmd = New SqlCommand("select * from setting where keterangan='" & keterangan & "'", conn)
        reader = cmd.ExecuteReader()
        If reader.Read() Then
            load_setting = reader.Item(1)
        End If
        reader.Close()
        conn.Close()
    End Function
    Public Function getds(ByVal query As String, ByVal conn As SqlConnection) As DataSet
        Dim cmd As SqlCommand
        Dim str As String = ""

        cmd = New SqlCommand(query, conn)
        Dim dataadaptor As New SqlDataAdapter(cmd)
        Dim dsDataSet As New DataSet
        Try
            dataadaptor.Fill(dsDataSet, "a")
        Catch ex As Exception
            MsgBox("Failed to Fill " + +": " + ex.ToString)
        Finally
        End Try
        Return dsDataSet
    End Function
    Public Sub load_divisi(ByVal combo As ComboBox)
        
        loadComboBoxAll(combo, "divisi", "userid_divisi", "where userid = '" & userid & "'", "divisi", "divisi")
       
    End Sub
End Module
