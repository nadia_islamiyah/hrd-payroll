﻿Imports System.Data.Sql
Imports System.Data.SqlClient

Module Module1
   Public servername As String
    Public dbname As String
    Public username As String
    Public password As String
    Public reportLocation As String
    Public msgboxTitle As String = "3PM-Solutions"
    Public userid As String
    Public Function GetAppPath() As String
        Dim i As Integer
        Dim strAppPath As String
        strAppPath = System.Reflection.Assembly.GetExecutingAssembly.Location()
        i = strAppPath.Length - 1
        Do Until strAppPath.Substring(i, 1) = "\"
            i = i - 1
        Loop
        strAppPath = strAppPath.Substring(0, i)
        Return strAppPath
    End Function
    Public Class CalendarColumn
        Inherits DataGridViewColumn

        Public Sub New()
            MyBase.New(New CalendarCell())
        End Sub

        Public Overrides Property CellTemplate() As DataGridViewCell
            Get
                Return MyBase.CellTemplate
            End Get
            Set(ByVal value As DataGridViewCell)

                ' Ensure that the cell used for the template is a CalendarCell. 
                If (value IsNot Nothing) AndAlso _
                    Not value.GetType().IsAssignableFrom(GetType(CalendarCell)) _
                    Then
                    Throw New InvalidCastException("Must be a CalendarCell")
                End If
                MyBase.CellTemplate = value

            End Set
        End Property

    End Class

    Public Class CalendarCell
        Inherits DataGridViewTextBoxCell

        Public Sub New()
            ' Use the short date format. 
            Me.Style.Format = "dd/MM/yyyy"
        End Sub

        Public Overrides Sub InitializeEditingControl(ByVal rowIndex As Integer, _
            ByVal initialFormattedValue As Object, _
            ByVal dataGridViewCellStyle As DataGridViewCellStyle)

            ' Set the value of the editing control to the current cell value. 
            MyBase.InitializeEditingControl(rowIndex, initialFormattedValue, _
                dataGridViewCellStyle)

            Dim ctl As CalendarEditingControl = _
                CType(DataGridView.EditingControl, CalendarEditingControl)

            ' Use the default row value when Value property is null. 
            If (Me.Value Is Nothing) Then
                ctl.Value = CType(Me.DefaultNewRowValue, DateTime)
            Else
                ctl.Value = CType(Me.Value, DateTime)
            End If
        End Sub

        Public Overrides ReadOnly Property EditType() As Type
            Get
                ' Return the type of the editing control that CalendarCell uses. 
                Return GetType(CalendarEditingControl)
            End Get
        End Property

        Public Overrides ReadOnly Property ValueType() As Type
            Get
                ' Return the type of the value that CalendarCell contains. 
                Return GetType(DateTime)
            End Get
        End Property

        Public Overrides ReadOnly Property DefaultNewRowValue() As Object
            Get
                ' Use the current date and time as the default value. 
                Return DateTime.Now
            End Get
        End Property

    End Class

    Class CalendarEditingControl
        Inherits DateTimePicker
        Implements IDataGridViewEditingControl

        Private dataGridViewControl As DataGridView
        Private valueIsChanged As Boolean = False
        Private rowIndexNum As Integer

        Public Sub New()
            Me.Format = DateTimePickerFormat.Short
        End Sub

        Public Property EditingControlFormattedValue() As Object _
            Implements IDataGridViewEditingControl.EditingControlFormattedValue

            Get
                Return Me.Value.ToShortDateString()
            End Get

            Set(ByVal value As Object)
                Try
                    ' This will throw an exception of the string is  
                    ' null, empty, or not in the format of a date. 
                    Me.Value = DateTime.Parse(CStr(value))
                Catch
                    ' In the case of an exception, just use the default 
                    ' value so we're not left with a null value. 
                    Me.Value = DateTime.Now
                End Try
            End Set

        End Property

        Public Function GetEditingControlFormattedValue(ByVal context _
            As DataGridViewDataErrorContexts) As Object _
            Implements IDataGridViewEditingControl.GetEditingControlFormattedValue

            Return Me.Value.ToShortDateString()

        End Function

        Public Sub ApplyCellStyleToEditingControl(ByVal dataGridViewCellStyle As  _
            DataGridViewCellStyle) _
            Implements IDataGridViewEditingControl.ApplyCellStyleToEditingControl

            Me.Font = dataGridViewCellStyle.Font
            Me.CalendarForeColor = dataGridViewCellStyle.ForeColor
            Me.CalendarMonthBackground = dataGridViewCellStyle.BackColor
            Me.Format = DateTimePickerFormat.Custom
            Me.CustomFormat = "dd/MM/yyyy"

        End Sub

        Public Property EditingControlRowIndex() As Integer _
            Implements IDataGridViewEditingControl.EditingControlRowIndex

            Get
                Return rowIndexNum
            End Get
            Set(ByVal value As Integer)
                rowIndexNum = value
            End Set

        End Property

        Public Function EditingControlWantsInputKey(ByVal key As Keys, _
            ByVal dataGridViewWantsInputKey As Boolean) As Boolean _
            Implements IDataGridViewEditingControl.EditingControlWantsInputKey

            ' Let the DateTimePicker handle the keys listed. 
            Select Case key And Keys.KeyCode
                Case Keys.Left, Keys.Up, Keys.Down, Keys.Right, _
                    Keys.Home, Keys.End, Keys.PageDown, Keys.PageUp
                    Return True
                Case Else
                    Return Not dataGridViewWantsInputKey
            End Select
        End Function

        Public Sub PrepareEditingControlForEdit(ByVal selectAll As Boolean) _
            Implements IDataGridViewEditingControl.PrepareEditingControlForEdit
            ' No preparation needs to be done. 
        End Sub

        Public ReadOnly Property RepositionEditingControlOnValueChange() _
            As Boolean Implements _
            IDataGridViewEditingControl.RepositionEditingControlOnValueChange
            Get
                Return False
            End Get
        End Property

        Public Property EditingControlDataGridView() As DataGridView _
            Implements IDataGridViewEditingControl.EditingControlDataGridView
            Get
                Return dataGridViewControl
            End Get
            Set(ByVal value As DataGridView)
                dataGridViewControl = value
            End Set
        End Property

        Public Property EditingControlValueChanged() As Boolean _
            Implements IDataGridViewEditingControl.EditingControlValueChanged
            Get
                Return valueIsChanged
            End Get
            Set(ByVal value As Boolean)
                valueIsChanged = value
            End Set
        End Property

        Public ReadOnly Property EditingControlCursor() As Cursor _
            Implements IDataGridViewEditingControl.EditingPanelCursor
            Get
                Return MyBase.Cursor
            End Get
        End Property

        Protected Overrides Sub OnValueChanged(ByVal eventargs As EventArgs)
            ' Notify the DataGridView that the contents of the cell have changed.
            valueIsChanged = True
            Me.EditingControlDataGridView.NotifyCurrentCellDirty(True)
            MyBase.OnValueChanged(eventargs)
        End Sub

    End Class
    Public Function newid(ByVal tabel As String, ByVal nama_id As String, ByVal tanggal As Date, ByVal prefix As String) As String
        Dim conn As New SqlConnection
        Dim cmd As SqlCommand
        Dim dr As SqlDataReader
        Dim query As String
        Dim start As Byte
        conn.ConnectionString = strcon
        conn.Open()


        start = Len(prefix) + 1
        query = "select top 1 [nama_id] from [tabel] where substring([nama_id],[start],2)='" & Format(tanggal, "yy") & "' and substring([nama_id],[start]+2,2)='" & Format(tanggal, "MM") & "'  and left([nama_id],[start]-1)='" & prefix & "' order by [nama_id] desc"
        query = Replace(query, "[tabel]", tabel)
        query = Replace(query, "[nama_id]", nama_id)
        query = Replace(query, "[start]", start)
        cmd = New SqlCommand(query, conn)
        dr = cmd.ExecuteReader
        If dr.Read Then
            newid = prefix & Format(tanggal, "yy") & Format(tanggal, "MM") & Format((CLng(Right(dr.Item(0), 4)) + 1), "0000")
        Else
            newid = prefix & Format(tanggal, "yy") & Format(tanggal, "MM") & Format(1, "0000")
        End If
        dr.Close()
        conn.Close()
    End Function
    
    Public Function App_Path() As String
        Return System.AppDomain.CurrentDomain.BaseDirectory()
    End Function
    Public Function getPaperIndex(ByVal printername As String, ByVal papername As String) As Integer
        Dim printDoc As New System.Drawing.Printing.PrintDocument
        Dim paperindex As Integer
        Dim i As Byte
        paperindex = 0
        printDoc.PrinterSettings.PrinterName = printername
        For i = 0 To printDoc.PrinterSettings.PaperSizes.Count
            If printDoc.PrinterSettings.PaperSizes(i).PaperName = papername Then
                paperindex = CInt(printDoc.PrinterSettings.PaperSizes(i).GetType().GetField("kind", Reflection.BindingFlags.Instance Or Reflection.BindingFlags.NonPublic).GetValue(printDoc.PrinterSettings.PaperSizes(i)))
                Exit For
            End If


        Next
        getPaperIndex = paperindex
    End Function
    Public Function setcombotextleft(ByRef cmb As ComboBox, ByVal text As String) As Boolean
        setcombotextleft = False
        For i As Integer = 0 To cmb.Items.Count - 1
            If Mid(cmb.Items(i), 1, Len(text)) = text Then cmb.Text = cmb.Items(i)
        Next
        setcombotextleft = True
    End Function
    Public Function setcombotextright(ByRef cmb As ComboBox, ByVal text As String) As Boolean
        setcombotextright = False
        For i As Integer = 0 To cmb.Items.Count - 1
            If Right(cmb.Items(i), Len(text)) = text Then cmb.Text = cmb.Items(i)
        Next
        setcombotextright = True

    End Function

    Public Function query_tambah_data(ByVal table_name As String, ByVal field() As String, ByVal nilai() As String) As String
        Dim query As String
        Dim i As Integer
        query = "insert into " & table_name & " ("
        For i = 0 To UBound(field) - 1
            If field(i) <> "" Then query = query & field(i) & ","
        Next
        query = Left(query, Len(query) - 1) & ") values ("
        For i = 0 To UBound(nilai) - 1
            If field(i) <> "" Then
                query = query & "'" & nilai(i) & "',"
            End If
        Next
        query = Left(query, Len(query) - 1) & ")"
        query_tambah_data = query

    End Function
    Public Function timeroundup(ByVal jam As DateTime, ByVal toleransi_sebelum As Integer, ByVal toleransi_sesudah As Integer) As DateTime
        Dim tgl = jam.Date
        Dim min = jam.TimeOfDay
        If (min.TotalMinutes() Mod 60) <= toleransi_sesudah Then min = TimeSpan.FromMinutes(min.TotalMinutes() - toleransi_sesudah)
        min = TimeSpan.FromMinutes(Math.Ceiling(min.TotalMinutes() / 60) * 60)
        timeroundup = tgl.Add(min)
    End Function
    Public Function timerounddown(ByVal jam As DateTime, ByVal toleransi_sebelum As Integer, ByVal toleransi_sesudah As Integer) As DateTime
        Dim tgl = jam.Date
        Dim min = jam.TimeOfDay
        If 60 - (min.TotalMinutes() Mod 60) <= toleransi_sebelum Then min = TimeSpan.FromMinutes(min.TotalMinutes() + toleransi_sebelum)
        min = TimeSpan.FromMinutes(Math.Floor(min.TotalMinutes() / 60) * 60)
        timerounddown = tgl.Add(min)
    End Function
    Public Sub executeSQL(ByVal query As String)
        Dim conn As New SqlConnection(strcon)
        Dim cmd As New SqlCommand(query, conn)
        Dim da As New SqlDataAdapter(cmd)
        Dim ds As New DataSet
        Try
            conn.Open()
            cmd.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox(ex.Message)
        Finally
            conn.Close()
        End Try

    End Sub
    Public Sub executeSQL(ByVal query As String, ByRef conn As SqlConnection, ByRef trans As SqlTransaction)

        Dim cmd As New SqlCommand(query, conn, trans)

        cmd.ExecuteNonQuery()
    End Sub
End Module
