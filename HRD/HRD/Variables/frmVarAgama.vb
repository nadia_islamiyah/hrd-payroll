﻿Imports System.Data.SqlClient
Imports System.Data.Sql
Imports System.IO
Public Class frmVarAgama
    Dim da As SqlDataAdapter
    Dim ds As New DataSet

    Private Sub frmVarAgama_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown

        If e.KeyCode = Keys.Escape Then Me.Dispose()
        If e.KeyCode = Keys.Enter Then SendKeys.Send("{tab}")
    End Sub

    Private Sub frmVarAgama_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress

    End Sub
    Private Sub frmVarAgama_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        reset()
    End Sub
    Private Sub reload()
        Dim conn As New SqlConnection
        conn.ConnectionString = strcon
        conn.Open()
        da = New SqlDataAdapter("select * from var_agama", conn)
        ds.Clear()
        da.Fill(ds, "var_agama")
        dbGrid.DataSource = ds
        dbGrid.DataMember = "var_agama"
    End Sub
    Private Sub reset()
        txtAgama.Text = ""
        reload()
        txtAgama.Focus()
    End Sub
    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim conn As New SqlConnection
        Dim cmd As New SqlCommand
        conn.ConnectionString = strcon
        conn.Open()

        cmd = New SqlCommand("delete from var_agama where agama='" & txtAgama.Text & "'", conn)
        cmd.ExecuteNonQuery()
        cmd = New SqlCommand("insert into var_agama values ('" & txtAgama.Text & "')", conn)
        cmd.ExecuteNonQuery()
        conn.Close()
        reset()
    End Sub

    Private Sub dbGrid_UserDeletingRow(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewRowCancelEventArgs) Handles dbGrid.UserDeletingRow
        If MsgBox("Apakah anda yakin hendak menghapus " & e.Row.Cells(0).Value & " ?", vbYesNo) = MsgBoxResult.Yes Then
            Dim conn As New SqlConnection
            Dim cmd As New SqlCommand
            conn.ConnectionString = strcon
            conn.Open()

            cmd = New SqlCommand("delete from var_agama where agama='" & e.Row.Cells(0).Value & "'", conn)
            cmd.ExecuteNonQuery()

            conn.Close()
        Else
            e.Cancel = True
        End If
    End Sub
End Class