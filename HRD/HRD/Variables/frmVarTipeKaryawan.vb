﻿Imports System.Data.SqlClient
Imports System.Data.Sql
Imports System.IO
Public Class frmVarTipeKaryawan
    Dim da As SqlDataAdapter
    Dim ds As New DataSet

    Private Sub frmtipe_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown

        If e.KeyCode = Keys.Escape Then Me.Dispose()
        If e.KeyCode = Keys.Enter Then SendKeys.Send("{tab}")
    End Sub

    Private Sub frmtipe_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress

    End Sub
    Private Sub frmtipe_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        reset()
    End Sub
    Private Sub reload()
        Dim conn As New SqlConnection
        Dim cmd As New SqlCommand
        Dim dr As SqlDataReader
        conn.ConnectionString = strcon
        conn.Open()


        cmd = New SqlCommand("select * from var_tipekaryawan where tipe='" & txtTipe.Text & "'", conn)
        dr = cmd.ExecuteReader
        dr.Read()
        If dr.HasRows Then
            chkKontrak.Checked = dr.Item("kontrak")
            txtLama.Text = dr.Item("lamakontrak")
        End If
        conn.Close()

    End Sub
    Private Sub reset()
        txtTipe.Text = ""
        chkKontrak.Checked = False
        txtTipe.Focus()
    End Sub
    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim conn As New SqlConnection
        Dim cmd As New SqlCommand
        conn.ConnectionString = strcon
        conn.Open()

        cmd = New SqlCommand("delete from var_tipekaryawan where tipe='" & txtTipe.Text & "'", conn)
        cmd.ExecuteNonQuery()
        cmd = New SqlCommand("insert into var_tipekaryawan (tipe,kontrak,lamakontrak) values ('" & txtTipe.Text & "','" & chkKontrak.Checked & "','" & txtlama.text & "')", conn)
        cmd.ExecuteNonQuery()
        conn.Close()
        reset()
    End Sub

    Private Sub btnHapus_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnHapus.Click
        If MsgBox("Apakah anda yakin hendak menghapus " & txtTipe.Text & " ?", vbYesNo) = MsgBoxResult.Yes Then
            Dim conn As New SqlConnection
            Dim cmd As New SqlCommand
            conn.ConnectionString = strcon
            conn.Open()

            cmd = New SqlCommand("delete from var_tipekaryawan where tipe='" & txtTipe.Text & "'", conn)
            cmd.ExecuteNonQuery()

            conn.Close()

        End If
    End Sub

    Private Sub txtTipe_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtTipe.LostFocus
        reload()
    End Sub


    Private Sub chkKontrak_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkKontrak.CheckedChanged
        If chkKontrak.Checked Then grpKontrak.Visible = True Else grpKontrak.Visible = False
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Dim f As New frmSearch
        f.setCol(0)
        f.setTableName("var_tipekaryawan")
        f.setColumns("tipe,kontrak,lamakontrak")

        If f.ShowDialog = System.Windows.Forms.DialogResult.OK Then
            txtTipe.Text = f.value
            reload()
        End If
        f.Dispose()
    End Sub
End Class