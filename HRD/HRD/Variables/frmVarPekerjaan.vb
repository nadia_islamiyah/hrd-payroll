﻿Imports System.Data.SqlClient
Imports System.Data.Sql
Imports System.IO
Public Class frmVarPekerjaan
    Dim da As SqlDataAdapter
    Dim ds As New DataSet

    Private Sub frmVarPekerjaan_KeyDown(sender As Object, e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        If e.KeyCode = Keys.Escape Then Me.Dispose()
        If e.KeyCode = Keys.Enter Then SendKeys.Send("{tab}")
    End Sub
    Private Sub frmVarPekerjaan_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Reset()
    End Sub
    Private Sub reload()
        Dim conn As New SqlConnection
        conn.ConnectionString = strcon
        conn.Open()
        da = New SqlDataAdapter("select * from var_pekerjaan", conn)
        ds.Clear()
        da.Fill(ds, "var_pekerjaan")
        dbGrid.DataSource = ds
        dbGrid.DataMember = "var_pekerjaan"
    End Sub
    Private Sub reset()
        txtpekerjaan.Text = ""
        reload()
        txtpekerjaan.Focus()
    End Sub

    Private Sub btnSave_Click(sender As System.Object, e As System.EventArgs) Handles btnSave.Click
        Dim conn As New SqlConnection
        Dim cmd As New SqlCommand
        conn.ConnectionString = strcon
        conn.Open()

        cmd = New SqlCommand("delete from var_pekerjaan where pekerjaan='" & txtpekerjaan.Text & "'", conn)
        cmd.ExecuteNonQuery()
        cmd = New SqlCommand("insert into var_pekerjaan values ('" & txtpekerjaan.Text & "')", conn)
        cmd.ExecuteNonQuery()
        conn.Close()
        reset()
    End Sub

    Private Sub dbGrid_UserDeletingRow(sender As Object, e As System.Windows.Forms.DataGridViewRowCancelEventArgs) Handles dbGrid.UserDeletingRow
        If MsgBox("Apakah anda yakin hendak menghapus " & e.Row.Cells(0).Value & " ?", vbYesNo) = MsgBoxResult.Yes Then
            Dim conn As New SqlConnection
            Dim cmd As New SqlCommand
            conn.ConnectionString = strcon
            conn.Open()

            cmd = New SqlCommand("delete from var_pekerjaan where pekerjaan='" & e.Row.Cells(0).Value & "'", conn)
            cmd.ExecuteNonQuery()

            conn.Close()
        Else
            e.Cancel = True
        End If
    End Sub
End Class