﻿Imports System.Data.SqlClient
Imports System.Data.Sql
Imports System.IO
Public Class frmVarBank
    Dim da As SqlDataAdapter
    Dim ds As New DataSet

    Private Sub frmbank_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown

        If e.KeyCode = Keys.Escape Then Me.Dispose()
        If e.KeyCode = Keys.Enter Then SendKeys.Send("{tab}")
    End Sub

    Private Sub frmbank_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress

    End Sub
    Private Sub frmbank_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        reset()
    End Sub
    Private Sub reload()
        Dim conn As New SqlConnection
        conn.ConnectionString = strcon
        conn.Open()
        da = New SqlDataAdapter("select * from var_bank", conn)
        ds.Clear()
        da.Fill(ds, "var_bank")
        dbGrid.DataSource = ds
        dbGrid.DataMember = "var_bank"
    End Sub
    Private Sub reset()
        txtBank.Text = ""
        reload()
        txtBank.Focus()
    End Sub
    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim conn As New SqlConnection
        Dim cmd As New SqlCommand
        conn.ConnectionString = strcon
        conn.Open()

        cmd = New SqlCommand("delete from var_bank where bank='" & txtBank.Text & "'", conn)
        cmd.ExecuteNonQuery()
        cmd = New SqlCommand("insert into var_bank values ('" & txtBank.Text & "')", conn)
        cmd.ExecuteNonQuery()
        conn.Close()
        reset()
    End Sub

    Private Sub dbGrid_UserDeletingRow(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewRowCancelEventArgs) Handles dbGrid.UserDeletingRow
        If MsgBox("Apakah anda yakin hendak menghapus " & e.Row.Cells(0).Value & " ?", vbYesNo) = MsgBoxResult.Yes Then
            Dim conn As New SqlConnection
            Dim cmd As New SqlCommand
            conn.ConnectionString = strcon
            conn.Open()

            cmd = New SqlCommand("delete from var_bank where bank='" & e.Row.Cells(0).Value & "'", conn)
            cmd.ExecuteNonQuery()

            conn.Close()
        Else
            e.Cancel = True
        End If
    End Sub
End Class