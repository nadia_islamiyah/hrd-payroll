﻿Imports System.Data.SqlClient
Public Class frmLogin
    Private Sub frmLogin_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown ', txtServerName.KeyDown, txtUserName.KeyDown, txtPassword.KeyDown
        If e.KeyCode = Keys.Escape Then Me.Close()
        If e.KeyCode = Keys.Enter Then SendKeys.Send("{TAB}")
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLogin.Click
        Dim conn2 As New SqlConnection()
        Try
            setconnectionstring(servername, dbname)
            conn2.ConnectionString = strcon
            conn2.Open()
            If authenticate(txtUserName.Text, txtPassword.Text) Then
                username = txtUserName.Text
                password = txtPassword.Text
                loadUserRight()
                load_settings()
                '  load_defaultprinter()
                conn2.Close()

                frmMain.Show()
                Me.Close()
                
            Else
                conn2.Close()
            End If
        Catch ex As Exception
            MsgBox("login failed, " & ex.Message)
            If conn2.State Then conn2.Close()
        End Try
    End Sub
    

    Private Sub loadUserRight()

        Dim conn2 As New SqlConnection(strcon)
        conn2.Open()
        Try
            cmd = New SqlCommand("select * from user_group where userid='" & txtUserName.Text & "' ;select * from user_groupmenu gm left join user_group g on gm.nama_group = g.nama_group where g.userid = '" & txtUserName.Text & "' ", conn2)
            reader = cmd.ExecuteReader()
    
            reader.NextResult()
            While reader.Read()
                For Each ti In frmMain.MenuStrip1.Items
                    If ti.Name = reader("key") Then
                        ti.visible = CBool(reader("value"))
                        Exit For
                    End If
                    If enablemenu(ti, reader("key"), reader("value")) Then Exit For

                Next
            End While
            reader.Close()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
        If conn2.State Then conn2.Close()
    End Sub
    Private Function enablemenu(ts As ToolStripMenuItem, namamenu As String, value As Boolean) As Boolean
        Dim ti As Object
        Dim res As Boolean
        res = False
        Try
            For Each ti In ts.DropDownItems
                If TypeOf (ti) Is ToolStripMenuItem Then
                    If ti.Name = namamenu Then
                        ti.Visible = value
                        res = True
                        Return res
                        'Exit Function
                    End If

                    If TryCast(ti, ToolStripMenuItem).HasDropDownItems Then
                        res = enablemenu(ti, namamenu, value)
                        If res Then
                            Return res
                            'Exit Function
                        End If
                    End If
                End If
                'loadmenu( ts)
            Next
        Catch ex As Exception

        End Try
        Return res
    End Function
   
    Private Sub frmLogin_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp, txtPassword.KeyUp, txtUserName.KeyUp
        If e.KeyCode = Keys.Enter Then e.SuppressKeyPress = True
    End Sub

    Private Sub frmLogin_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim xm As New Ini(GetAppPath() & "\setting.ini")
        servername = xm.GetString("Server", "Server name", "")
        dbname = xm.GetString("Server", "Database name", "")
        reportLocation = xm.GetString("Report", "Location", "")

    End Sub

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        Me.Close()
    End Sub


    Private Sub btnSetting_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSetting.Click
        FrmSettingDB.ShowDialog()
    End Sub
End Class
