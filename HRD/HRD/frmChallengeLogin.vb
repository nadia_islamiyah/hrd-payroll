﻿Imports System.IO
Imports System.Data.SqlClient
Public Class frmChallengeLogin
    Public LoginSucceeded As Boolean
    Public namagroup As String
    Public keterangan As String
    Public koordinator As Boolean
    Public nomorbill As String
    Public koneksi As String
    Public login1, login2 As Boolean
    Private Sub OK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OK.Click

        If txtUserName1.Text = "" Then
            MsgBox("User tidak berhak melakukan operasi ini")
        Else
            UserLogin1(txtUserName1.Text, generate_connstring1(txtUserName1.Text, txtPassword1.Text))
            authorize(txtUserName1.Text)
        End If
        Me.Dispose()

    End Sub
    Private Sub authorize(ByVal username1 As String)
        If login1 Then
            LoginSucceeded = True
            executeSQL("insert into t_authorize (tanggal,username,keterangan,nomer_bill) values ('" & Format(Now, "yyyy/MM/dd hh:mm:ss") & "','" & username1 & "','" & keterangan & "','" & nomorbill & "')")

            Me.Hide()
        End If
        Me.Dispose()
    End Sub

    Private Function generate_connstring1(ByVal user1 As String, ByVal pass1 As String) As String
        generate_connstring1 = "Persist Security Info=False;Data Source=" & servername & ";Initial Catalog=" & dbname & ";User ID=" & user1 & ";Password=" & pass1 & ";"
    End Function
    Private Sub UserLogin1(ByVal userlog1 As String, ByVal koneksi As String)
        Dim conn As New SqlConnection(koneksi)
        Dim cmd As SqlCommand
        Dim dr As SqlDataReader
        Dim condition As String = ""

        If koordinator = True Then
            If condition <> "" Then condition &= " and "
            condition &= " koordinator='" & koordinator & "'"
        Else
            condition = "1=1"
        End If

        Try
            conn.Open()
            cmd = New SqlCommand("select * from user_group where userid='" & userlog1 & "' and " & condition, conn)
            dr = cmd.ExecuteReader
            If dr.Read Then
                login1 = True
                userchallenge = txtUserName1.Text
            Else
                LoginSucceeded = False
                MsgBox("User tidak berhak melakukan operasi ini")
            End If
            dr.Close()
        Catch ex As Exception
            LoginSucceeded = False
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub Cancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Cancel.Click
        Me.Close()
    End Sub
    Private Function Connstring_FINGER() As String
        Connstring_FINGER = "Provider=SQLOLEDB;Data Source=" & servername & ";Initial Catalog=" & dbname & ";User ID=" & username & ";Password=" & password & ";"
    End Function

    Private Sub frmChallenge_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.FormClosing
        
    End Sub

    Private Sub frmChallengeLogin_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        If e.KeyCode = Keys.Enter Then SendKeys.Send("{TAB}")
        If e.KeyCode = Keys.Escape Then Me.Close()
    End Sub

    Private Sub frmChallenge_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Load
        LoginSucceeded = False
        'txtUserName1.Text = ""
    End Sub

End Class
