﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmSettingHoliday
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Mc1 = New System.Windows.Forms.MonthCalendar()
        Me.chkLibur = New System.Windows.Forms.CheckBox()
        Me.txtKeterangan = New System.Windows.Forms.TextBox()
        Me.btnSimpan = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'Mc1
        '
        Me.Mc1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Mc1.Location = New System.Drawing.Point(18, 18)
        Me.Mc1.MaxSelectionCount = 1
        Me.Mc1.Name = "Mc1"
        Me.Mc1.TabIndex = 0
        '
        'chkLibur
        '
        Me.chkLibur.AutoSize = True
        Me.chkLibur.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkLibur.Location = New System.Drawing.Point(257, 18)
        Me.chkLibur.Name = "chkLibur"
        Me.chkLibur.Size = New System.Drawing.Size(52, 18)
        Me.chkLibur.TabIndex = 1
        Me.chkLibur.Text = "Libur"
        Me.chkLibur.UseVisualStyleBackColor = True
        '
        'txtKeterangan
        '
        Me.txtKeterangan.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtKeterangan.Location = New System.Drawing.Point(257, 41)
        Me.txtKeterangan.Name = "txtKeterangan"
        Me.txtKeterangan.Size = New System.Drawing.Size(281, 22)
        Me.txtKeterangan.TabIndex = 2
        '
        'btnSimpan
        '
        Me.btnSimpan.BackColor = System.Drawing.Color.Transparent
        Me.btnSimpan.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnSimpan.FlatAppearance.BorderSize = 0
        Me.btnSimpan.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DeepSkyBlue
        Me.btnSimpan.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.btnSimpan.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSimpan.ForeColor = System.Drawing.Color.Black
        Me.btnSimpan.Location = New System.Drawing.Point(257, 78)
        Me.btnSimpan.Name = "btnSimpan"
        Me.btnSimpan.Size = New System.Drawing.Size(156, 32)
        Me.btnSimpan.TabIndex = 6
        Me.btnSimpan.TabStop = False
        Me.btnSimpan.Text = "Proses Ulang Shift"
        Me.btnSimpan.UseVisualStyleBackColor = False
        '
        'frmSettingHoliday
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(628, 204)
        Me.Controls.Add(Me.btnSimpan)
        Me.Controls.Add(Me.txtKeterangan)
        Me.Controls.Add(Me.chkLibur)
        Me.Controls.Add(Me.Mc1)
        Me.Name = "frmSettingHoliday"
        Me.Text = "frmSettingHoliday"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Mc1 As System.Windows.Forms.MonthCalendar
    Friend WithEvents chkLibur As System.Windows.Forms.CheckBox
    Friend WithEvents txtKeterangan As System.Windows.Forms.TextBox
    Friend WithEvents btnSimpan As System.Windows.Forms.Button
End Class
