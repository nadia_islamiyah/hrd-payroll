﻿Imports System.Drawing.Printing
Imports System.Data.SqlClient

Public Class frmConfig
    Dim transaction As SqlTransaction

    Private Sub reset_form()
        load_setting()
    End Sub

    Private Sub btnOk_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOk.Click
        Dim conn2 As New SqlConnection(strcon)
        Dim sqlstr As String = ""
        conn2.ConnectionString = strcon
        conn2.Open()
        transaction = conn2.BeginTransaction(IsolationLevel.ReadCommitted)

        Try
            sqlstr += ";delete from setting where keterangan='Grid_komponengaji';insert into setting values ('Grid_komponengaji','" & chkKomponen_gaji.Checked & "')"
            sqlstr += ";delete from setting where keterangan='Display total';insert into setting values ('Display total','" & chkpayroll_total.Checked & "')"
            sqlstr += ";delete from setting where keterangan='Display Jumlah Jam Kerja';insert into setting values ('Display Jumlah Jam Kerja','" & chkpayroll_JamKerja.Checked & "')"
            sqlstr += ";delete from setting where keterangan='Display Telat';insert into setting values ('Display Telat','" & chkpayroll_telat.Checked & "')"
            sqlstr += ";delete from setting where keterangan='Display Istirahat';insert into setting values ('Display Istirahat','" & chkpayroll_istirahat.Checked & "')"
            sqlstr += ";delete from setting where keterangan='Display Kerja';insert into setting values ('Display Kerja','" & chkpayroll_kerja.Checked & "')"
            sqlstr += ";delete from setting where keterangan='Edit Absensi';insert into setting values ('Edit Absensi','" & chkEditAbsensi.Checked & "')"

            sqlstr += ";delete from setting where keterangan='rekap_tglistirahat';insert into setting values ('rekap_tglistirahat','" & chkrekap_tglistirahat.Checked & "')"
            sqlstr += ";delete from setting where keterangan='rekap_tglkembali';insert into setting values ('rekap_tglkembali','" & chkrekap_tglkembali.Checked & "')"
            sqlstr += ";delete from setting where keterangan='rekap_total';insert into setting values ('rekap_total','" & chkrekap_total.Checked & "')"
            sqlstr += ";delete from setting where keterangan='rekap_PC';insert into setting values ('rekap_PC','" & chkrekap_PC.Checked & "')"
            sqlstr += ";delete from setting where keterangan='rekap_lembur';insert into setting values ('rekap_lembur','" & chkrekap_lembur.Checked & "')"
            sqlstr += ";delete from setting where keterangan='rekap_absen';insert into setting values ('rekap_absen','" & chkrekap_absen.Checked & "')"
            sqlstr += ";delete from setting where keterangan='rekap_shift';insert into setting values ('rekap_shift','" & chkrekap_shift.Checked & "')"

            cmd = New SqlCommand(sqlstr, conn2, transaction)
            cmd.ExecuteNonQuery()
            transaction.Commit()
            conn2.Close()
            MsgBox("Data sudah tersimpan")
            load_settings()
        Catch ex As Exception
            MessageBox.Show(ex.Message.ToString)
            transaction.Rollback()
            If conn2.State Then conn2.Close()

        Finally
            If reader IsNot Nothing Then reader.Close()
        End Try
    End Sub

    Private Sub frmConfig_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        reset_form()

    End Sub

    Private Sub load_setting()
        If Grid_komponengaji = True Then
            chkKomponen_gaji.Checked = True
        Else
            chkKomponen_gaji.Checked = False
        End If
        If display_total = True Then
            chkpayroll_total.Checked = True
        Else
            chkpayroll_total.Checked = False
        End If
        If display_jumlahjamkerja = True Then
            chkpayroll_JamKerja.Checked = True
        Else
            chkpayroll_JamKerja.Checked = False
        End If
        If display_istirahat = True Then
            chkpayroll_istirahat.Checked = True
        Else
            chkpayroll_istirahat.Checked = False
        End If
        If display_kerja = True Then
            chkpayroll_kerja.Checked = True
        Else
            chkpayroll_kerja.Checked = False
        End If
        If display_telat = True Then
            chkpayroll_telat.Checked = True
        Else
            chkpayroll_telat.Checked = False
        End If
        If default_editabsen = True Then
            chkEditAbsensi.Checked = True
        Else
            chkEditAbsensi.Checked = False
        End If

        If rekap_tglistirahat = True Then chkrekap_tglistirahat.Checked = True Else chkpayroll_total.Checked = False
        If rekap_tglkembali = True Then chkrekap_tglkembali.Checked = True Else chkrekap_tglkembali.Checked = False
        If rekap_total = True Then chkrekap_total.Checked = True Else chkrekap_total.Checked = False
        If rekap_PC = True Then chkrekap_PC.Checked = True Else chkrekap_PC.Checked = False
        If rekap_lembur = True Then chkrekap_lembur.Checked = True Else chkrekap_lembur.Checked = False
        If rekap_absen = True Then chkrekap_absen.Checked = True Else chkrekap_absen.Checked = False
        If rekap_shift = True Then chkrekap_shift.Checked = True Else chkrekap_shift.Checked = False
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub

    Private Sub frmConfig_KeyDown(sender As Object, e As KeyEventArgs) Handles MyBase.KeyDown
        If e.KeyCode = Keys.F2 Then btnOk.PerformClick()
        If e.KeyCode = Keys.Escape Then Me.Close()
        If e.KeyCode = Keys.Enter Then SendKeys.Send("{TAB}")
    End Sub

End Class