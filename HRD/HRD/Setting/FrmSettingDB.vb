﻿Public Class FrmSettingDB

    Private Sub btnOk_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOk.Click
        Dim xm As New Ini(GetAppPath() & "\setting.ini")
        xm.WriteString("Server", "Server name", txtServerName.Text)
        xm.WriteString("Server", "Database name", txtDBName.Text)
        xm.WriteString("Report", "Location", txtReportLocation.Text)
        reportLocation = txtReportLocation.Text
        servername = txtServerName.Text
        dbname = txtDBName.Text
        Me.Dispose()
    End Sub

    Private Sub FrmSettingDB_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        txtServerName.Text = servername
        txtDBName.Text = dbname
        txtReportLocation.Text = reportLocation

    End Sub
End Class