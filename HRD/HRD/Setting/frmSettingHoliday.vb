﻿Imports System.Data.SqlClient
Imports System.Data.Sql
Imports System.IO
Public Class frmSettingHoliday
    Dim bulan As Integer
    Dim tahun As Integer
    Dim tanggal As Date
    Dim cmd As SqlCommand
    Dim dr As SqlDataReader
    Dim transaction As SqlTransaction
    Private Sub Mc1_DateChanged(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DateRangeEventArgs) Handles Mc1.DateChanged
        savedate(tanggal)
        If bulan <> Month(Mc1.SelectionStart.Date) Or tahun <> tahun = Year(Mc1.SelectionStart.Date) Then
            bulan = Month(Mc1.SelectionStart.Date)
            tahun = Year(Mc1.SelectionStart.Date)
            reload_month()
        End If
        bulan = Month(Mc1.SelectionStart.Date)
        tahun = Year(Mc1.SelectionStart.Date)
        tanggal = Mc1.SelectionStart.Date
        load_date()
    End Sub
    Private Sub savedate(ByVal tgl As Date)
        Dim conn As New SqlConnection
        Dim datatable1 As New DataTable
        conn.ConnectionString = strcon
        conn.Open()
        cmd = New SqlCommand("delete from t_libur where convert(varchar(20),tanggal,112)='" & Format(tgl, "yyyyMMdd") & "'", conn)
        cmd.ExecuteNonQuery()
        If chkLibur.Checked Then
            cmd = New SqlCommand("insert into t_libur values ('" & Format(tgl, "yyyy/MM/dd") & "','True','" & txtKeterangan.Text & "')", conn)
            cmd.ExecuteNonQuery()
            Mc1.AddMonthlyBoldedDate(tgl)

        Else
            Mc1.RemoveMonthlyBoldedDate(tgl)
        End If
        Mc1.UpdateBoldedDates()
        Mc1.Refresh()
        conn.Close()

    End Sub

    Private Sub reload_month()
        Dim conn As New SqlConnection
        Dim datatable1 As New DataTable
        conn.ConnectionString = strcon


        conn.Open()
        cmd = New SqlCommand("select * from t_libur where month(tanggal)='" & bulan & "' and year(tanggal)='" & tahun & "'", conn)
        Mc1.RemoveAllMonthlyBoldedDates()
        dr = cmd.ExecuteReader
        While dr.Read
            Mc1.AddMonthlyBoldedDate(dr.Item(0))
        End While
        Mc1.UpdateBoldedDates()
    End Sub
    Private Sub load_date()
        Dim conn As New SqlConnection
        Dim datatable1 As New DataTable
        conn.ConnectionString = strcon
        conn.Open()
        cmd = New SqlCommand("select * from t_libur where convert(varchar(12),tanggal,112)='" & Format(Mc1.SelectionStart.Date, "yyyyMMdd") & "'", conn)
        dr = cmd.ExecuteReader
        txtKeterangan.Text = ""
        chkLibur.Checked = False
        If dr.Read Then
            chkLibur.Checked = True
            txtKeterangan.Text = dr.Item("keterangan")
        End If
        Mc1.Refresh()
    End Sub
    Private Sub frmSettingHoliday_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Mc1.TodayDate = Now
        bulan = Month(Now)
        tahun = Year(Now)
        reload_month()
    End Sub

    Private Sub btnSimpan_Click(sender As System.Object, e As System.EventArgs) Handles btnSimpan.Click

    End Sub

    Private Function simpan() As Boolean
        simpan = False
        Dim cmd As SqlCommand
        Dim conn As New SqlConnection
        Conn.ConnectionString = strcon
        conn.Open()
        transaction = conn.BeginTransaction(IsolationLevel.ReadCommitted)
        Try
            cmd = New SqlCommand("delete from t_jadwalkerja where convert(varchar(8),tanggal,112)='" & Format(Mc1.SelectionStart.Date, "yyyyMMdd") & "'; ", conn, transaction)
            cmd.ExecuteNonQuery()
            
            cmd = New SqlCommand("exec sp_autoshift1 '" & Format(Mc1.SelectionStart.Date, "yyyyMMdd") & "','" & Format(Mc1.SelectionStart.Date, "yyyyMMdd") & "'", conn, transaction)
            cmd.CommandTimeout = 0
            cmd.ExecuteNonQuery()

            transaction.Commit()
            conn.Close()
            MsgBox("Proses Selesai")

        Catch ex As Exception
            MessageBox.Show(ex.Message.ToString)
            If reader IsNot Nothing Then reader.Close()
            transaction.Rollback()
            If conn.State Then conn.Close()
        End Try
    End Function

End Class