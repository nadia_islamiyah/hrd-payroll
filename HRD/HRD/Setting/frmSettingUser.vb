﻿Imports System.Data.SqlClient
Imports System.Data.OleDb

Public Class frmSettingUser
    Dim transaction As SqlTransaction

    Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Dim result As String = ""
        If txtUserid.Text = "" Then
            MsgBox("Username tidak boleh kosong")
            txtUserid.Focus()
            Exit Sub
        End If

        If cmbGroup.Text = "" Then
            MsgBox("group tidak boleh kosong")
            cmbGroup.Focus()
            Exit Sub
        End If

        Dim reader As SqlDataReader = Nothing
        Dim conn2 As New SqlConnection(strcon)
        conn2.ConnectionString = strcon
        conn2.Open()

        transaction = conn2.BeginTransaction(IsolationLevel.ReadCommitted)
        Try
            cmd = New SqlCommand("select * from userid where userid='" & txtUserid.Text & "'", conn2, transaction)
            reader = cmd.ExecuteReader()

            If Not reader.HasRows() Then
                cmd = New SqlCommand("insert into [userid] values ('" & txtUserid.Text & "',encryptbypassphrase ('3PM','" & txtUserid.Text & "'))", conn2, transaction)
                cmd.ExecuteNonQuery()
            End If
            reader.Close()

            cmd = New SqlCommand("delete from user_group where userid='" & txtUserid.Text & "'", conn2, transaction)
            cmd.ExecuteNonQuery()

            cmd = New SqlCommand("insert into user_group (userid,nama_group) values ('" & txtUserid.Text & "','" & cmbGroup.Text & "')", conn2, transaction)
            cmd.ExecuteNonQuery()

            transaction.Commit()
            conn2.Close()
            If result = "1" Or result = "" Then
                MsgBox("New User Added")
                reset_form()
                refresh_grid()
            Else
                MsgBox(result)
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message.ToString)

            If conn2.State Then
                'If reader.IsClosed = False Then
                reader.Close()
                'End If
                cmd = New SqlCommand("SELECT @@TRANCOUNT", conn2, transaction)
                reader = cmd.ExecuteReader()
                If reader.HasRows Then
                    reader.Read()
                    If reader.Item(0) > 0 Then
                        reader.Close()
                        transaction.Rollback()
                    End If

                End If
                conn2.Close()
            End If
        Finally
            If reader IsNot Nothing Then reader.Close()
        End Try
    End Sub

    Private Sub frmSettingUser_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        If e.KeyCode = Keys.Escape Then Me.Close()

        If e.KeyCode = Keys.Enter Then SendKeys.Send("{TAB}")

    End Sub

    Private Sub frmSettingUser_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        reset_form()
    End Sub

    Private Sub reset_form()
        'dgv1.Rows.Clear()
        refresh_grid()
        txtUserid.Text = ""
        loadComboBoxAll(cmbGroup, "distinct nama_group", "user_groupmenu", "where '' = ''", "nama_group", "nama_group")
        cmbGroup.Text = ""

    End Sub

    Private Sub refresh_grid()
        Dim da As New SqlDataAdapter()
        Dim ds As New DataSet()
        Dim ds1 As New DataSet()
        Dim dt As DataTable
        Dim conn2 As New SqlConnection(strcon)
        conn2.Open()

        cmd = New SqlCommand("select * from user_group order by userid", conn2)
        da.SelectCommand = cmd
        da.Fill(ds, "user_group")
        dt = ds.Tables("user_group")
        dgv1.DataSource = dt
        
        conn2.Close()
    End Sub

    Private Sub dgv1_CellDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgv1.CellDoubleClick
        txtUserid.Text = dgv1.Item(0, dgv1.CurrentCell.RowIndex).Value
        cmbGroup.Text = dgv1.Item(1, dgv1.CurrentCell.RowIndex).Value

    End Sub

    Private Sub dgv1_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgv1.KeyDown
        If e.KeyCode = Keys.Enter Then
            txtUserid.Text = dgv1.Item(0, dgv1.CurrentCell.RowIndex).Value
            cmbGroup.Text = dgv1.Item(1, dgv1.CurrentCell.RowIndex).Value
        End If
    End Sub

    Private Sub btnHapus_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnHapus.Click
        If txtUserid.Text = "" Then
            MsgBox("Username tidak boleh kosong, pilih(double klik) username yang akan dihapus, di tabel yang disediakan")
            txtUserid.Focus()
            Exit Sub
        End If
        Dim conn2 As New SqlConnection(strcon)
        Dim query As String
        conn2.Open()
        transaction = conn2.BeginTransaction(IsolationLevel.ReadCommitted)
        Try
            query = "delete from user_group where nama_group ='" & cmbGroup.Text & "' and userid='" & txtUserid.Text & "'"
            query += "delete from userid where userid ='" & txtUserid.Text & "'"
            cmd = New SqlCommand(query, conn2, transaction)
            cmd.ExecuteNonQuery()
            
            transaction.Commit()
            conn2.Close()
            MsgBox("Login sudah dihapus")
            reset_form()
        Catch ex As Exception
            MessageBox.Show(ex.Message.ToString)
            transaction.Rollback()
            
        Finally
            If reader IsNot Nothing Then reader.Close()
        End Try
        If conn2.State Then conn2.Close()
    End Sub

    Private Sub btnKeluar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnKeluar.Click
        Me.Close()
    End Sub

    Private Sub btnAdd_MouseHover(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.MouseHover
        btnAdd.Font = New Font(btnAdd.Text, 10, FontStyle.Underline)
    End Sub

    Private Sub btnAdd_MouseLeave(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.MouseLeave
        btnAdd.Font = New Font(btnAdd.Text, 8, FontStyle.Regular)
    End Sub

    Private Sub btnHapus_MouseHover(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnHapus.MouseHover
        btnHapus.Font = New Font(btnHapus.Text, 10, FontStyle.Underline)
    End Sub

    Private Sub btnHapus_MouseLeave(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnHapus.MouseLeave
        btnHapus.Font = New Font(btnHapus.Text, 8, FontStyle.Regular)
    End Sub

    Private Sub btnKeluar_MouseHover(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnKeluar.MouseHover
        btnKeluar.Font = New Font(btnKeluar.Text, 10, FontStyle.Underline)
    End Sub

    Private Sub btnKeluar_MouseLeave(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnKeluar.MouseLeave
        btnKeluar.Font = New Font(btnKeluar.Text, 8, FontStyle.Regular)
    End Sub

End Class