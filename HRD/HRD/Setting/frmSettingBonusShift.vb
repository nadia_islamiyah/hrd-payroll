﻿Imports System.Data.SqlClient
Imports System.Data.Sql
Imports System.IO
Public Class frmSettingBonusShift
    Dim da As SqlDataAdapter
    Dim ds As New DataSet
    Private Sub reloadcombo()
        Dim conn As New SqlConnection
        Dim cmd As New SqlCommand
        Dim dr As SqlDataReader
        conn.ConnectionString = strcon
        conn.Open()


        cmd = New SqlCommand("select * from ms_jamkerja order by nama_shift", conn)
        dr = cmd.ExecuteReader
        cmbNama.Items.Clear()
        While dr.Read
            cmbNama.Items.Add(dr.Item(0))
        End While
        conn.Close()
    End Sub
    Private Sub frmMasterJamKerja_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown

        If e.KeyCode = Keys.Escape Then Me.Dispose()
        If e.KeyCode = Keys.Enter Then SendKeys.Send("{tab}")
    End Sub

    Private Sub frmMasterJamKerja_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress

    End Sub
    Private Sub frmMasterJamKerja_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        reloadcombo()
        reset()
    End Sub
    Private Sub reload()
        Dim conn As New SqlConnection
        Dim cmd As New SqlCommand
        Dim dr As SqlDataReader
        conn.ConnectionString = strcon
        conn.Open()


        cmd = New SqlCommand("select * from ms_bonusshift where nama_shift='" & cmbNama.Text & "'", conn)
        dr = cmd.ExecuteReader
        dr.Read()
        If dr.HasRows Then

            txtJamKerja.Text = CDbl(dr.Item("jumlah_jamkerja"))
            txtLembur.Text = dr.Item("lembur")
            txtbonus.Text = dr.Item("bonus")
        End If
        conn.Close()

    End Sub
    Private Sub reset()
        txtJamKerja.Text = "0"
        txtLembur.Text = "0"
        txtBonus.Text = "0"
        cmbNama.Text = ""

        cmbNama.Focus()
    End Sub
    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim conn As New SqlConnection
        Dim cmd As New SqlCommand
        conn.ConnectionString = strcon
        conn.Open()

        cmd = New SqlCommand("delete from ms_bonusshift where nama_shift='" & cmbNama.Text & "'", conn)
        cmd.ExecuteNonQuery()
        cmd = New SqlCommand(add_data, conn)
        cmd.ExecuteNonQuery()
        conn.Close()
        reset()
    End Sub
    Private Function add_data() As String
        Dim fields() As String
        Dim nilai() As String
        Dim table_name As String
        ReDim fields(22)
        ReDim nilai(22)
        table_name = "ms_bonusshift"
        fields(0) = "nama_shift"
        nilai(0) = cmbNama.Text
        fields(1) = "jumlah_jamkerja"
        nilai(1) = txtJamKerja.Text
        fields(2) = "lembur"
        nilai(2) = txtLembur.Text
        fields(3) = "bonus"
        nilai(3) = txtBonus.Text


        add_data = query_tambah_data(table_name, fields, nilai)
    End Function
    Private Sub btnHapus_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnHapus.Click
        If MsgBox("Apakah anda yakin hendak menghapus " & cmbNama.Text & " ?", vbYesNo) = MsgBoxResult.Yes Then
            Dim conn As New SqlConnection
            Dim cmd As New SqlCommand
            conn.ConnectionString = strcon
            conn.Open()

            cmd = New SqlCommand("delete from ms_bonusshift where nama_shift='" & cmbNama.Text & "'", conn)
            cmd.ExecuteNonQuery()

            conn.Close()

        End If
    End Sub

    Private Sub cmbNama_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbNama.LostFocus
        reload()
    End Sub


    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        Me.Dispose()
    End Sub

    Private Sub cmbNama_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbNama.SelectedIndexChanged
        reload()
    End Sub
End Class