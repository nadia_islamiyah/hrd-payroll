﻿Imports System.Data.SqlClient
Imports System.Data.Sql
Imports System.IO

Public Class frmTemplate

    Private Sub reloadgrid()
        Dim conn As New SqlConnection
        Dim cmd As New SqlCommand
        Dim dr As SqlDataReader
        conn.ConnectionString = strcon
        conn.Open()

        cmd = New SqlCommand("select distinct nama from ms_templategaji order by nama", conn)
        dr = cmd.ExecuteReader
        ListTemplate.Items.Clear()
        While dr.Read
            ListTemplate.Items.Add(dr.Item(0))
        End While
        dr.Close()

        cmd = New SqlCommand("select * from ms_komponengaji order by kode_komponengaji", conn)
        dr = cmd.ExecuteReader
        DBGrid.Rows.Clear()
        While dr.Read
            DBGrid.Rows.Add()
            DBGrid.Rows(DBGrid.Rows.Count - 1).Cells(0).Value = dr.Item(0)
            DBGrid.Rows(DBGrid.Rows.Count - 1).Cells(1).Value = dr.Item(1)
            DBGrid.Rows(DBGrid.Rows.Count - 1).Cells(2).Value = 0
        End While
        conn.Close()
    End Sub
    Private Sub reload()
        Dim conn As New SqlConnection
        Dim cmd As New SqlCommand
        Dim dr As SqlDataReader
        conn.ConnectionString = strcon
        conn.Open()


        cmd = New SqlCommand("select * from ms_templategaji where nama='" & txtKode.Text & "'", conn)


        dr = cmd.ExecuteReader

        For i = 0 To DBGrid.Rows.Count - 1
            DBGrid.Rows(i).Cells(2).Value = False
        Next
        While dr.Read
            For i = 0 To DBGrid.Rows.Count - 1
                If DBGrid.Rows(i).Cells(0).Value = dr.Item(1) Then
                    DBGrid.Rows(i).Cells(2).Value = True
                End If
            Next

        End While
        dr.Close()
        conn.Close()

    End Sub
    Private Sub reset_form()
        reloadgrid()
        txtKode.Text = ""

    End Sub
    Private Sub save_data()
        Dim conn As New SqlConnection
        Dim cmd As New SqlCommand
        Dim tr As SqlTransaction
        conn.ConnectionString = strcon
        conn.Open()
        tr = conn.BeginTransaction

        Try
            cmd = New SqlCommand("delete from ms_templategaji where nama='" & txtKode.Text & "'", conn, tr)
            cmd.ExecuteNonQuery()


            For i = 0 To DBGrid.Rows.Count - 1
                If DBGrid.Rows(i).Cells(2).Value Then
                    cmd = New SqlCommand(add_datadetail(i), conn, tr)
                    cmd.ExecuteNonQuery()
                End If
            Next
            tr.Commit()
            MsgBox("Data sudah tersimpan")
            reset_form()
        Catch ex As Exception
            MsgBox(ex.Message)
            tr.Rollback()
        End Try


        conn.Close()
        reset_form()
    End Sub

    Private Function add_datadetail(ByVal row As Integer) As String
        Dim fields() As String
        Dim nilai() As String
        Dim table_name As String
        ReDim fields(2)
        ReDim nilai(2)
        table_name = "ms_templategaji"
        fields(0) = "nama"
        nilai(0) = txtKode.Text
        fields(1) = "kode_komponen"
        nilai(1) = DBGrid.Rows(row).Cells(0).Value


        add_datadetail = query_tambah_data(table_name, fields, nilai)
    End Function

    Private Sub frmTemplate_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        If e.KeyCode = Keys.Enter Then SendKeys.Send("{tab}")
        If e.KeyCode = Keys.Escape Then Me.Dispose()
    End Sub


    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If txtKode.Text = "" Then
            MsgBox("Isi kode terlebih dahulu")
            Exit Sub
        End If

        save_data()
    End Sub


    Private Sub btnHapus_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnHapus.Click
        Dim conn As New SqlConnection
        Dim cmd As New SqlCommand
        conn.ConnectionString = strcon
        conn.Open()

        cmd = New SqlCommand("delete from ms_templategaji where nama='" & txtKode.Text & "'", conn)
        cmd.ExecuteNonQuery()

        conn.Close()
    End Sub

    
    Private Sub frmTemplate_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        reset_form()

    End Sub

    Private Sub ListTemplate_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles ListTemplate.DoubleClick
        txtKode.Text = ListTemplate.Items(ListTemplate.SelectedIndex)
        reload()
    End Sub

    Private Sub ListTemplate_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ListTemplate.SelectedIndexChanged

    End Sub
End Class