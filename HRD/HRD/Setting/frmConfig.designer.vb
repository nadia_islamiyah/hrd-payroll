﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmConfig
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnCancel = New System.Windows.Forms.Button()
        Me.btnOk = New System.Windows.Forms.Button()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.TabPage4 = New System.Windows.Forms.TabPage()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.chkrekap_shift = New System.Windows.Forms.CheckBox()
        Me.chkrekap_absen = New System.Windows.Forms.CheckBox()
        Me.chkrekap_lembur = New System.Windows.Forms.CheckBox()
        Me.chkrekap_PC = New System.Windows.Forms.CheckBox()
        Me.chkrekap_total = New System.Windows.Forms.CheckBox()
        Me.chkrekap_tglkembali = New System.Windows.Forms.CheckBox()
        Me.chkrekap_tglistirahat = New System.Windows.Forms.CheckBox()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.chkpayroll_telat = New System.Windows.Forms.CheckBox()
        Me.chkpayroll_kerja = New System.Windows.Forms.CheckBox()
        Me.chkpayroll_istirahat = New System.Windows.Forms.CheckBox()
        Me.chkpayroll_JamKerja = New System.Windows.Forms.CheckBox()
        Me.chkpayroll_total = New System.Windows.Forms.CheckBox()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.chkEditAbsensi = New System.Windows.Forms.CheckBox()
        Me.chkKomponen_gaji = New System.Windows.Forms.CheckBox()
        Me.TabControl1.SuspendLayout()
        Me.TabPage4.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnCancel
        '
        Me.btnCancel.BackColor = System.Drawing.Color.Transparent
        Me.btnCancel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnCancel.FlatAppearance.BorderSize = 0
        Me.btnCancel.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DeepSkyBlue
        Me.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.btnCancel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.btnCancel.Location = New System.Drawing.Point(257, 269)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(98, 31)
        Me.btnCancel.TabIndex = 72
        Me.btnCancel.Text = "Cancel"
        Me.btnCancel.UseVisualStyleBackColor = False
        '
        'btnOk
        '
        Me.btnOk.BackColor = System.Drawing.Color.Transparent
        Me.btnOk.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnOk.FlatAppearance.BorderSize = 0
        Me.btnOk.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DeepSkyBlue
        Me.btnOk.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.btnOk.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOk.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.btnOk.Location = New System.Drawing.Point(153, 269)
        Me.btnOk.Name = "btnOk"
        Me.btnOk.Size = New System.Drawing.Size(98, 31)
        Me.btnOk.TabIndex = 71
        Me.btnOk.Text = "OK"
        Me.btnOk.UseVisualStyleBackColor = False
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.TabPage4)
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Location = New System.Drawing.Point(2, 12)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(607, 251)
        Me.TabControl1.TabIndex = 90
        '
        'TabPage4
        '
        Me.TabPage4.Controls.Add(Me.GroupBox2)
        Me.TabPage4.Controls.Add(Me.GroupBox1)
        Me.TabPage4.Location = New System.Drawing.Point(4, 22)
        Me.TabPage4.Name = "TabPage4"
        Me.TabPage4.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage4.Size = New System.Drawing.Size(599, 225)
        Me.TabPage4.TabIndex = 3
        Me.TabPage4.Text = "Display"
        Me.TabPage4.UseVisualStyleBackColor = True
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.chkrekap_shift)
        Me.GroupBox2.Controls.Add(Me.chkrekap_absen)
        Me.GroupBox2.Controls.Add(Me.chkrekap_lembur)
        Me.GroupBox2.Controls.Add(Me.chkrekap_PC)
        Me.GroupBox2.Controls.Add(Me.chkrekap_total)
        Me.GroupBox2.Controls.Add(Me.chkrekap_tglkembali)
        Me.GroupBox2.Controls.Add(Me.chkrekap_tglistirahat)
        Me.GroupBox2.Location = New System.Drawing.Point(207, 9)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(175, 197)
        Me.GroupBox2.TabIndex = 77
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "From RekapAbsensi"
        '
        'chkrekap_shift
        '
        Me.chkrekap_shift.AutoSize = True
        Me.chkrekap_shift.Location = New System.Drawing.Point(16, 162)
        Me.chkrekap_shift.Name = "chkrekap_shift"
        Me.chkrekap_shift.Size = New System.Drawing.Size(47, 17)
        Me.chkrekap_shift.TabIndex = 77
        Me.chkrekap_shift.Text = "Shift"
        Me.chkrekap_shift.UseVisualStyleBackColor = True
        '
        'chkrekap_absen
        '
        Me.chkrekap_absen.AutoSize = True
        Me.chkrekap_absen.Location = New System.Drawing.Point(16, 139)
        Me.chkrekap_absen.Name = "chkrekap_absen"
        Me.chkrekap_absen.Size = New System.Drawing.Size(56, 17)
        Me.chkrekap_absen.TabIndex = 76
        Me.chkrekap_absen.Text = "Absen"
        Me.chkrekap_absen.UseVisualStyleBackColor = True
        '
        'chkrekap_lembur
        '
        Me.chkrekap_lembur.AutoSize = True
        Me.chkrekap_lembur.Location = New System.Drawing.Point(16, 116)
        Me.chkrekap_lembur.Name = "chkrekap_lembur"
        Me.chkrekap_lembur.Size = New System.Drawing.Size(61, 17)
        Me.chkrekap_lembur.TabIndex = 75
        Me.chkrekap_lembur.Text = "Lembur"
        Me.chkrekap_lembur.UseVisualStyleBackColor = True
        '
        'chkrekap_PC
        '
        Me.chkrekap_PC.AutoSize = True
        Me.chkrekap_PC.Location = New System.Drawing.Point(16, 93)
        Me.chkrekap_PC.Name = "chkrekap_PC"
        Me.chkrekap_PC.Size = New System.Drawing.Size(40, 17)
        Me.chkrekap_PC.TabIndex = 74
        Me.chkrekap_PC.Text = "PC"
        Me.chkrekap_PC.UseVisualStyleBackColor = True
        '
        'chkrekap_total
        '
        Me.chkrekap_total.AutoSize = True
        Me.chkrekap_total.Location = New System.Drawing.Point(16, 70)
        Me.chkrekap_total.Name = "chkrekap_total"
        Me.chkrekap_total.Size = New System.Drawing.Size(50, 17)
        Me.chkrekap_total.TabIndex = 73
        Me.chkrekap_total.Text = "Total"
        Me.chkrekap_total.UseVisualStyleBackColor = True
        '
        'chkrekap_tglkembali
        '
        Me.chkrekap_tglkembali.AutoSize = True
        Me.chkrekap_tglkembali.Location = New System.Drawing.Point(16, 47)
        Me.chkrekap_tglkembali.Name = "chkrekap_tglkembali"
        Me.chkrekap_tglkembali.Size = New System.Drawing.Size(105, 17)
        Me.chkrekap_tglkembali.TabIndex = 72
        Me.chkrekap_tglkembali.Text = "Tanggal Kembali"
        Me.chkrekap_tglkembali.UseVisualStyleBackColor = True
        '
        'chkrekap_tglistirahat
        '
        Me.chkrekap_tglistirahat.AutoSize = True
        Me.chkrekap_tglistirahat.Location = New System.Drawing.Point(16, 24)
        Me.chkrekap_tglistirahat.Name = "chkrekap_tglistirahat"
        Me.chkrekap_tglistirahat.Size = New System.Drawing.Size(105, 17)
        Me.chkrekap_tglistirahat.TabIndex = 71
        Me.chkrekap_tglistirahat.Text = "Tanggal Istirahat"
        Me.chkrekap_tglistirahat.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.chkKomponen_gaji)
        Me.GroupBox1.Controls.Add(Me.chkpayroll_telat)
        Me.GroupBox1.Controls.Add(Me.chkpayroll_kerja)
        Me.GroupBox1.Controls.Add(Me.chkpayroll_istirahat)
        Me.GroupBox1.Controls.Add(Me.chkpayroll_JamKerja)
        Me.GroupBox1.Controls.Add(Me.chkpayroll_total)
        Me.GroupBox1.Location = New System.Drawing.Point(13, 9)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(188, 197)
        Me.GroupBox1.TabIndex = 76
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "From Payroll"
        '
        'chkpayroll_telat
        '
        Me.chkpayroll_telat.AutoSize = True
        Me.chkpayroll_telat.Location = New System.Drawing.Point(16, 116)
        Me.chkpayroll_telat.Name = "chkpayroll_telat"
        Me.chkpayroll_telat.Size = New System.Drawing.Size(112, 17)
        Me.chkpayroll_telat.TabIndex = 75
        Me.chkpayroll_telat.Text = "Grid Absensi Telat"
        Me.chkpayroll_telat.UseVisualStyleBackColor = True
        '
        'chkpayroll_kerja
        '
        Me.chkpayroll_kerja.AutoSize = True
        Me.chkpayroll_kerja.Location = New System.Drawing.Point(16, 93)
        Me.chkpayroll_kerja.Name = "chkpayroll_kerja"
        Me.chkpayroll_kerja.Size = New System.Drawing.Size(112, 17)
        Me.chkpayroll_kerja.TabIndex = 74
        Me.chkpayroll_kerja.Text = "Grid Absensi Kerja"
        Me.chkpayroll_kerja.UseVisualStyleBackColor = True
        '
        'chkpayroll_istirahat
        '
        Me.chkpayroll_istirahat.AutoSize = True
        Me.chkpayroll_istirahat.Location = New System.Drawing.Point(16, 70)
        Me.chkpayroll_istirahat.Name = "chkpayroll_istirahat"
        Me.chkpayroll_istirahat.Size = New System.Drawing.Size(125, 17)
        Me.chkpayroll_istirahat.TabIndex = 73
        Me.chkpayroll_istirahat.Text = "Grid Absensi Istirahat"
        Me.chkpayroll_istirahat.UseVisualStyleBackColor = True
        '
        'chkpayroll_JamKerja
        '
        Me.chkpayroll_JamKerja.AutoSize = True
        Me.chkpayroll_JamKerja.Location = New System.Drawing.Point(16, 47)
        Me.chkpayroll_JamKerja.Name = "chkpayroll_JamKerja"
        Me.chkpayroll_JamKerja.Size = New System.Drawing.Size(170, 17)
        Me.chkpayroll_JamKerja.TabIndex = 72
        Me.chkpayroll_JamKerja.Text = "Grid Absensi Jumlah Jam Kerja"
        Me.chkpayroll_JamKerja.UseVisualStyleBackColor = True
        '
        'chkpayroll_total
        '
        Me.chkpayroll_total.AutoSize = True
        Me.chkpayroll_total.Location = New System.Drawing.Point(16, 24)
        Me.chkpayroll_total.Name = "chkpayroll_total"
        Me.chkpayroll_total.Size = New System.Drawing.Size(112, 17)
        Me.chkpayroll_total.TabIndex = 71
        Me.chkpayroll_total.Text = "Grid Absensi Total"
        Me.chkpayroll_total.UseVisualStyleBackColor = True
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.chkEditAbsensi)
        Me.TabPage1.Location = New System.Drawing.Point(4, 22)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(599, 225)
        Me.TabPage1.TabIndex = 4
        Me.TabPage1.Text = "Absensi"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'chkEditAbsensi
        '
        Me.chkEditAbsensi.AutoSize = True
        Me.chkEditAbsensi.Location = New System.Drawing.Point(19, 18)
        Me.chkEditAbsensi.Name = "chkEditAbsensi"
        Me.chkEditAbsensi.Size = New System.Drawing.Size(169, 17)
        Me.chkEditAbsensi.TabIndex = 72
        Me.chkEditAbsensi.Text = "Edit Absesni Dengan Pasword"
        Me.chkEditAbsensi.UseVisualStyleBackColor = True
        '
        'chkKomponen_gaji
        '
        Me.chkKomponen_gaji.AutoSize = True
        Me.chkKomponen_gaji.Location = New System.Drawing.Point(16, 139)
        Me.chkKomponen_gaji.Name = "chkKomponen_gaji"
        Me.chkKomponen_gaji.Size = New System.Drawing.Size(120, 17)
        Me.chkKomponen_gaji.TabIndex = 76
        Me.chkKomponen_gaji.Text = "Grid Komponen Gaji"
        Me.chkKomponen_gaji.UseVisualStyleBackColor = True
        '
        'frmConfig
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(640, 312)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.btnOk)
        Me.Controls.Add(Me.TabControl1)
        Me.KeyPreview = True
        Me.Name = "frmConfig"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Config"
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage4.ResumeLayout(False)
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btnCancel As System.Windows.Forms.Button
    Friend WithEvents btnOk As System.Windows.Forms.Button
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents TabPage4 As System.Windows.Forms.TabPage
    Friend WithEvents chkpayroll_total As System.Windows.Forms.CheckBox
    Friend WithEvents chkpayroll_JamKerja As System.Windows.Forms.CheckBox
    Friend WithEvents chkpayroll_istirahat As System.Windows.Forms.CheckBox
    Friend WithEvents chkpayroll_kerja As System.Windows.Forms.CheckBox
    Friend WithEvents chkpayroll_telat As System.Windows.Forms.CheckBox
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents chkEditAbsensi As System.Windows.Forms.CheckBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents chkrekap_lembur As System.Windows.Forms.CheckBox
    Friend WithEvents chkrekap_PC As System.Windows.Forms.CheckBox
    Friend WithEvents chkrekap_total As System.Windows.Forms.CheckBox
    Friend WithEvents chkrekap_tglkembali As System.Windows.Forms.CheckBox
    Friend WithEvents chkrekap_tglistirahat As System.Windows.Forms.CheckBox
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents chkrekap_absen As System.Windows.Forms.CheckBox
    Friend WithEvents chkrekap_shift As System.Windows.Forms.CheckBox
    Friend WithEvents chkKomponen_gaji As System.Windows.Forms.CheckBox
End Class
