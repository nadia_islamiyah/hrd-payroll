﻿Imports System.Data.SqlClient
Imports System.Data.OleDb

Public Class frmSecurityperGroup
    Public Event AfterCheck As TreeViewEventHandler
    Dim transaction As SqlTransaction = Nothing
    Dim cmd As SqlCommand
    Dim reader As SqlDataReader
    Private Sub frmSecurityperGroup_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        If e.KeyCode = Keys.F2 Then btnSimpan.PerformClick()
        If e.KeyCode = Keys.F3 Then btnSearchGroup.PerformClick()
        If e.KeyCode = Keys.Control And e.KeyCode = Keys.R Then btnReset.PerformClick()
        If e.KeyCode = Keys.Escape Then Me.Close()
    End Sub
    Private Sub frmSecurityperGroup_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        reset_form()
    End Sub

    Private Sub reset_form()
        tvwMain.Nodes.Clear()
        For Each ti As ToolStripMenuItem In frmMain.MenuStrip1.Items
            tvwMain.Nodes.Add(ti.Name, ti.Text)
            loadmenu(ti, tvwMain.Nodes(ti.Name))
        Next
        txtGroup.Text = ""
    End Sub

    Private Sub loadmenu(ByVal ti As ToolStripMenuItem, ByRef tn As TreeNode)
        Dim tm As ToolStripMenuItem
        For Each ts As ToolStripItem In ti.DropDownItems
            If TypeOf ts Is ToolStripMenuItem Then
                tm = TryCast(ts, ToolStripMenuItem)
                tn.Nodes.Add(ts.Name, ts.Text)
                If tm.HasDropDownItems Then
                    loadmenu(tm, tn.Nodes(tm.Name))
                End If

            End If
        Next
    End Sub

    Private Sub CheckAllChildNodes(ByVal treeNode As TreeNode, ByVal nodeChecked As Boolean)
        Dim node As TreeNode
        For Each node In treeNode.Nodes
            node.Checked = nodeChecked
            If node.Nodes.Count > 0 Then
                Me.CheckAllChildNodes(node, nodeChecked)
            End If
        Next node
    End Sub
    Private Sub node_AfterCheck(ByVal sender As Object, ByVal e As TreeViewEventArgs) Handles tvwMain.AfterCheck
        If e.Action <> TreeViewAction.Unknown Then
            If e.Node.Nodes.Count > 0 Then
                Me.CheckAllChildNodes(e.Node, e.Node.Checked)
            End If
        End If
    End Sub

    Function GetChildren(ByVal parentNode As TreeNode) As List(Of String)
        Dim nodes As List(Of String) = New List(Of String)
        GetAllChildren(parentNode, nodes)
        Return nodes

    End Function

    Sub GetAllChildren(ByVal parentNode As TreeNode, ByVal nodes As List(Of String))
        For Each childNode As TreeNode In parentNode.Nodes
            nodes.Add(childNode.Text)
            GetAllChildren(childNode, nodes)
        Next
    End Sub

    Private Sub btnSimpan_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSimpan.Click
        Dim pesan = MessageBox.Show("Apa Anda yakin melanjutkan penyimpanan?", "Perhatian", MessageBoxButtons.OKCancel, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2)
        If pesan = MsgBoxResult.Cancel Then

        ElseIf pesan = MsgBoxResult.Ok Then
            Dim transaction As SqlTransaction = Nothing
            Dim conn2 As New SqlConnection(strcon)
            Try
                conn2.Open()
                transaction = conn2.BeginTransaction(IsolationLevel.ReadCommitted)
                cmd = New SqlCommand("delete from user_groupmenu where nama_group ='" & txtGroup.Text & "'", conn2, transaction)
                cmd.ExecuteNonQuery()
                For J As Integer = 0 To tvwMain.Nodes.Count - 1
                    cmd = New SqlCommand("insert into user_groupmenu (nama_group,nama_menu,[key],parent,idx,value) values " & _
                                         "('" & txtGroup.Text & "', '" & tvwMain.Nodes.Item(J).Text & "','" & tvwMain.Nodes.Item(J).Name & "', '-', " & tvwMain.Nodes.Item(J).Index & ",'" & IIf(tvwMain.Nodes.Item(J).Checked, "1", "0") & "')", conn2, transaction)
                    cmd.ExecuteNonQuery()
                    Dim tn As TreeNode
                    tn = tvwMain.Nodes.Item(J)
                    cmd = New SqlCommand(getgroup(tn), conn2, transaction)
                    cmd.ExecuteNonQuery()
                    
                    
                Next
                MsgBox("Data berhasil disimpan. . .", MsgBoxStyle.Information, "Informasi!")
                transaction.Commit()
                conn2.Close()
            Catch ex As Exception
                MessageBox.Show(ex.Message.ToString)
                If conn2.State Then
                    cmd = New SqlCommand("SELECT @@TRANCOUNT", conn2, transaction)
                    reader = cmd.ExecuteReader()
                    If reader.HasRows Then
                        reader.Read()
                        If reader.Item(0) > 0 Then
                            reader.Close()
                            transaction.Rollback()
                        End If

                    End If
                    conn2.Close()
                End If
            Finally
                If reader IsNot Nothing Then reader.Close()
            End Try
            reset_form()
        End If
    End Sub

    Private Function getGroup(tn As TreeNode) As String
        Dim res As String = ""
        Dim t As TreeNode
        For i As Integer = 0 To tn.Nodes.Count - 1
            res += "insert into user_groupmenu (nama_group,nama_menu,[key],parent,idx,value) values " & _
            "('" & txtGroup.Text & "','" & tn.Nodes.Item(i).Text & "','" & tn.Nodes.Item(i).Name & "', '" & tn.Name & "'," & tn.Nodes.Item(i).Index & ",'" & IIf(tn.Nodes.Item(i).Checked, "1", "0") & "');"
            t = tn.Nodes.Item(i)
            If t.Nodes.Count > 0 Then res += getGroup(t)

        Next
        Return res
    End Function

    Private Sub tvwMain_NodeMouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.TreeNodeMouseClickEventArgs) Handles tvwMain.NodeMouseClick

        'Dim nama As List(Of String)
        'nama = GetChildren(e.Node).ToList

        'For i As Integer = 0 To e.Node.Nodes.Count - 1
        '    MsgBox(e.Node.Nodes.Item(i).Text)
        'Next
    End Sub

    Private Sub btnSearchGroup_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSearchGroup.Click
        Dim f As New frmSearch
        f.setCol(0)
        f.setTableName("user_groupmenu")
        f.setColumns("distinct nama_group")


        If f.ShowDialog = System.Windows.Forms.DialogResult.OK Then
            txtGroup.Text = f.value
        End If
        f.Dispose()
        loadgroup()
    End Sub
    Private Sub setnodecheckedstate(key As String, value As Boolean, ByRef tn As TreeNode)
        For Each nd As TreeNode In tn.Nodes
            If nd.Name = key Then nd.Checked = value
            If nd.Nodes.Count > 0 Then setnodecheckedstate(key, value, nd)
        Next
    End Sub
    Private Sub loadgroup()
        Dim conn2 As New SqlConnection(strcon)
        Try
            conn2.Open()
            cmd = New SqlCommand("select * from user_groupmenu where nama_group ='" & txtGroup.Text & "'", conn2) ' and value = '1' and parent = '-' order by idx
            reader = cmd.ExecuteReader()


            While reader.Read()
                For Each nd As TreeNode In tvwMain.Nodes
                    If nd.Name = reader("key") Then nd.Checked = reader("value")
                    If nd.Nodes.Count > 0 Then setnodecheckedstate(reader("key"), reader("value"), nd)
                Next
                'tvwMain.Nodes.Item().Checked = CBool()
                'cmd = New SqlCommand("select * from user_groupmenu where nama_group = '" & txtGroup.Text & "' and parent = '" & reader("key") & "' ", conn2)
                'reader1 = cmd.ExecuteReader()

                'While reader1.Read()
                '    tvwMain.Nodes.Item(reader("idx")).Nodes.Item(reader1("idx")).Checked = CBool(reader1("value"))
                'End While


                'reader1.Close()
            End While

            tvwMain.ExpandAll()
            reader.Close()
            conn2.Close()

        Catch ex As Exception
            MessageBox.Show(ex.Message.ToString)
            If Not reader.IsClosed Then reader.Close()
            conn2.Close()
        End Try
    End Sub

    Private Sub btnSimpan_MouseHover(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSimpan.MouseHover
        btnSimpan.Font = New Font(btnSimpan.Text, 10, FontStyle.Underline)
    End Sub

    Private Sub btnSimpan_MouseLeave(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSimpan.MouseLeave
        btnSimpan.Font = New Font(btnSimpan.Text, 8, FontStyle.Regular)
    End Sub

    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        txtGroup.Text = ""
    End Sub

    Private Sub btnReset_MouseHover(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.MouseHover
        btnReset.Font = New Font(btnReset.Text, 10, FontStyle.Underline)
    End Sub

    Private Sub btnReset_MouseLeave(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.MouseLeave
        btnReset.Font = New Font(btnReset.Text, 8, FontStyle.Regular)
    End Sub

    Private Sub btnKeluar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnKeluar.Click
        Me.Close()
    End Sub

    Private Sub btnKeluar_MouseHover(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnKeluar.MouseHover
        btnKeluar.Font = New Font(btnKeluar.Text, 10, FontStyle.Underline)
    End Sub

    Private Sub btnKeluar_MouseLeave(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnKeluar.MouseLeave
        btnKeluar.Font = New Font(btnKeluar.Text, 8, FontStyle.Regular)
    End Sub

    Private Sub txtGroup_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtGroup.LostFocus
        loadgroup()
    End Sub
End Class