﻿Imports System.Data.SqlClient
Imports System.Data.Sql
Imports System.IO

Public Class frmDeleteAbsensi

    Private Sub btnHapus_Click(sender As System.Object, e As System.EventArgs) Handles btnHapus.Click
        If MsgBox("Apakah anda yakin hendak menghapus data absensi mulai tanggal " & Format(dtAwal.Value, "dd/MM/yyyy") & " sampai tanggal " & Format(dtAkhir.Value, "dd/MM/yyyy") & " ?", vbYesNo) = MsgBoxResult.Yes Then
            Dim conn As New SqlConnection
            Dim cmd As New SqlCommand
            Try
                conn.ConnectionString = strcon
                conn.Open()

                cmd = New SqlCommand("delete from t_rekapabsensi where tanggal between '" & Format(dtAwal.Value, "yyyy/MM/dd") & "' and  '" & Format(dtAkhir.Value, "yyyy/MM/dd") & "'", conn)
                cmd.ExecuteNonQuery()
                cmd = New SqlCommand("delete from t_clockin where waktu between '" & Format(dtAwal.Value, "yyyy/MM/dd") & "' and  '" & Format(dtAkhir.Value, "yyyy/MM/dd") & "'", conn)
                cmd.ExecuteNonQuery()
                cmd = New SqlCommand("delete from t_jadwalkerja where tanggal between '" & Format(dtAwal.Value, "yyyy/MM/dd") & "' and  '" & Format(dtAkhir.Value, "yyyy/MM/dd") & "'", conn)
                cmd.ExecuteNonQuery()
            Catch ex As Exception
                MsgBox(ex.Message)
            End Try
            conn.Close()
            MsgBox("selesai")
        End If
    End Sub
End Class