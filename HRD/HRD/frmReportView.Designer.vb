﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmReportView
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.crV = New CrystalDecisions.Windows.Forms.CrystalReportViewer()
        Me.SuspendLayout()
        '
        'crV
        '
        Me.crV.ActiveViewIndex = -1
        Me.crV.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.crV.Cursor = System.Windows.Forms.Cursors.Default
        Me.crV.Dock = System.Windows.Forms.DockStyle.Fill
        Me.crV.Location = New System.Drawing.Point(0, 0)
        Me.crV.Name = "crV"
        Me.crV.Size = New System.Drawing.Size(610, 355)
        Me.crV.TabIndex = 0
        '
        'frmReportView
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(610, 355)
        Me.Controls.Add(Me.crV)
        Me.Name = "frmReportView"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "frmReportView"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.ResumeLayout(False)

    End Sub
    Private WithEvents crV As CrystalDecisions.Windows.Forms.CrystalReportViewer
End Class
