﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.MasterToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TipeKaryawanToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PayrollAreaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DivisiToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PekerjaanToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MasterToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.MasterKaryawanToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MasterKomponenGajiToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator3 = New System.Windows.Forms.ToolStripSeparator()
        Me.MasterShiftToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MasterWorkgroupToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AbsensiToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PengaturanHariLiburToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PengaturanShiftToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator4 = New System.Windows.Forms.ToolStripSeparator()
        Me.RekapAbsensiToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RekapShiftToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator8 = New System.Windows.Forms.ToolStripSeparator()
        Me.HapusDataAbsensiToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ImportFromAttToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator5 = New System.Windows.Forms.ToolStripSeparator()
        Me.ManualClockInToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.InputPekerjaanToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator9 = New System.Windows.Forms.ToolStripSeparator()
        Me.GajiToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.KelompokGajiToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TemplateGajiToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator6 = New System.Windows.Forms.ToolStripSeparator()
        Me.SettingGajiLainLainToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.BonusShiftToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ProsesBonusShiftToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator7 = New System.Windows.Forms.ToolStripSeparator()
        Me.PengajuanGajiToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ReportToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.PrintDataKaryawwanToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.LaporanAbsensiToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.LaporanAbsensiPerKaryawanToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.TotalJamKerjaToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.DetailAbsensiToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.WindowsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.VariableAndMaintenanceToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.GantiPasswordToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.UserToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SecurityManagerToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ConfigToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Timer2 = New System.Windows.Forms.Timer(Me.components)
        Me.dtJam3 = New System.Windows.Forms.DateTimePicker()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.dtJam2 = New System.Windows.Forms.DateTimePicker()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.dtJam1 = New System.Windows.Forms.DateTimePicker()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.MenuStrip1.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Font = New System.Drawing.Font("Segoe UI", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.MasterToolStripMenuItem, Me.MasterToolStripMenuItem1, Me.AbsensiToolStripMenuItem, Me.GajiToolStripMenuItem, Me.ReportToolStripMenuItem1, Me.WindowsToolStripMenuItem, Me.VariableAndMaintenanceToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.MdiWindowListItem = Me.WindowsToolStripMenuItem
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(895, 28)
        Me.MenuStrip1.TabIndex = 1
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'MasterToolStripMenuItem
        '
        Me.MasterToolStripMenuItem.BackColor = System.Drawing.SystemColors.Control
        Me.MasterToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.TipeKaryawanToolStripMenuItem, Me.PayrollAreaToolStripMenuItem, Me.DivisiToolStripMenuItem, Me.PekerjaanToolStripMenuItem})
        Me.MasterToolStripMenuItem.Name = "MasterToolStripMenuItem"
        Me.MasterToolStripMenuItem.Size = New System.Drawing.Size(75, 24)
        Me.MasterToolStripMenuItem.Text = "Variabel"
        '
        'TipeKaryawanToolStripMenuItem
        '
        Me.TipeKaryawanToolStripMenuItem.Name = "TipeKaryawanToolStripMenuItem"
        Me.TipeKaryawanToolStripMenuItem.Size = New System.Drawing.Size(175, 24)
        Me.TipeKaryawanToolStripMenuItem.Text = "Tipe Karyawan"
        '
        'PayrollAreaToolStripMenuItem
        '
        Me.PayrollAreaToolStripMenuItem.Name = "PayrollAreaToolStripMenuItem"
        Me.PayrollAreaToolStripMenuItem.Size = New System.Drawing.Size(175, 24)
        Me.PayrollAreaToolStripMenuItem.Text = "Payroll Area"
        '
        'DivisiToolStripMenuItem
        '
        Me.DivisiToolStripMenuItem.Name = "DivisiToolStripMenuItem"
        Me.DivisiToolStripMenuItem.Size = New System.Drawing.Size(175, 24)
        Me.DivisiToolStripMenuItem.Text = "Divisi"
        '
        'PekerjaanToolStripMenuItem
        '
        Me.PekerjaanToolStripMenuItem.Name = "PekerjaanToolStripMenuItem"
        Me.PekerjaanToolStripMenuItem.Size = New System.Drawing.Size(175, 24)
        Me.PekerjaanToolStripMenuItem.Text = "Pekerjaan"
        '
        'MasterToolStripMenuItem1
        '
        Me.MasterToolStripMenuItem1.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.MasterKaryawanToolStripMenuItem, Me.MasterKomponenGajiToolStripMenuItem, Me.ToolStripSeparator3, Me.MasterShiftToolStripMenuItem, Me.MasterWorkgroupToolStripMenuItem})
        Me.MasterToolStripMenuItem1.Name = "MasterToolStripMenuItem1"
        Me.MasterToolStripMenuItem1.Size = New System.Drawing.Size(66, 24)
        Me.MasterToolStripMenuItem1.Text = "Master"
        '
        'MasterKaryawanToolStripMenuItem
        '
        Me.MasterKaryawanToolStripMenuItem.Name = "MasterKaryawanToolStripMenuItem"
        Me.MasterKaryawanToolStripMenuItem.Size = New System.Drawing.Size(230, 24)
        Me.MasterKaryawanToolStripMenuItem.Text = "Master Karyawan"
        '
        'MasterKomponenGajiToolStripMenuItem
        '
        Me.MasterKomponenGajiToolStripMenuItem.Name = "MasterKomponenGajiToolStripMenuItem"
        Me.MasterKomponenGajiToolStripMenuItem.Size = New System.Drawing.Size(230, 24)
        Me.MasterKomponenGajiToolStripMenuItem.Text = "Master Komponen Gaji"
        '
        'ToolStripSeparator3
        '
        Me.ToolStripSeparator3.Name = "ToolStripSeparator3"
        Me.ToolStripSeparator3.Size = New System.Drawing.Size(227, 6)
        '
        'MasterShiftToolStripMenuItem
        '
        Me.MasterShiftToolStripMenuItem.Name = "MasterShiftToolStripMenuItem"
        Me.MasterShiftToolStripMenuItem.Size = New System.Drawing.Size(230, 24)
        Me.MasterShiftToolStripMenuItem.Text = "Master Shift"
        '
        'MasterWorkgroupToolStripMenuItem
        '
        Me.MasterWorkgroupToolStripMenuItem.Name = "MasterWorkgroupToolStripMenuItem"
        Me.MasterWorkgroupToolStripMenuItem.Size = New System.Drawing.Size(230, 24)
        Me.MasterWorkgroupToolStripMenuItem.Text = "Master Workgroup"
        '
        'AbsensiToolStripMenuItem
        '
        Me.AbsensiToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.PengaturanHariLiburToolStripMenuItem, Me.PengaturanShiftToolStripMenuItem, Me.ToolStripSeparator4, Me.RekapAbsensiToolStripMenuItem, Me.RekapShiftToolStripMenuItem, Me.ToolStripSeparator8, Me.HapusDataAbsensiToolStripMenuItem, Me.ImportFromAttToolStripMenuItem, Me.ToolStripSeparator5, Me.ManualClockInToolStripMenuItem, Me.InputPekerjaanToolStripMenuItem, Me.ToolStripSeparator9})
        Me.AbsensiToolStripMenuItem.Name = "AbsensiToolStripMenuItem"
        Me.AbsensiToolStripMenuItem.Size = New System.Drawing.Size(72, 24)
        Me.AbsensiToolStripMenuItem.Text = "Absensi"
        '
        'PengaturanHariLiburToolStripMenuItem
        '
        Me.PengaturanHariLiburToolStripMenuItem.Name = "PengaturanHariLiburToolStripMenuItem"
        Me.PengaturanHariLiburToolStripMenuItem.Size = New System.Drawing.Size(218, 24)
        Me.PengaturanHariLiburToolStripMenuItem.Text = "Pengaturan hari Libur"
        '
        'PengaturanShiftToolStripMenuItem
        '
        Me.PengaturanShiftToolStripMenuItem.Name = "PengaturanShiftToolStripMenuItem"
        Me.PengaturanShiftToolStripMenuItem.Size = New System.Drawing.Size(218, 24)
        Me.PengaturanShiftToolStripMenuItem.Text = "Pengaturan Shift"
        '
        'ToolStripSeparator4
        '
        Me.ToolStripSeparator4.Name = "ToolStripSeparator4"
        Me.ToolStripSeparator4.Size = New System.Drawing.Size(215, 6)
        '
        'RekapAbsensiToolStripMenuItem
        '
        Me.RekapAbsensiToolStripMenuItem.Name = "RekapAbsensiToolStripMenuItem"
        Me.RekapAbsensiToolStripMenuItem.Size = New System.Drawing.Size(218, 24)
        Me.RekapAbsensiToolStripMenuItem.Text = "Rekap Absensi"
        '
        'RekapShiftToolStripMenuItem
        '
        Me.RekapShiftToolStripMenuItem.Name = "RekapShiftToolStripMenuItem"
        Me.RekapShiftToolStripMenuItem.Size = New System.Drawing.Size(218, 24)
        Me.RekapShiftToolStripMenuItem.Text = "Rekap Shift"
        '
        'ToolStripSeparator8
        '
        Me.ToolStripSeparator8.Name = "ToolStripSeparator8"
        Me.ToolStripSeparator8.Size = New System.Drawing.Size(215, 6)
        '
        'HapusDataAbsensiToolStripMenuItem
        '
        Me.HapusDataAbsensiToolStripMenuItem.Name = "HapusDataAbsensiToolStripMenuItem"
        Me.HapusDataAbsensiToolStripMenuItem.Size = New System.Drawing.Size(218, 24)
        Me.HapusDataAbsensiToolStripMenuItem.Text = "Hapus data absensi"
        '
        'ImportFromAttToolStripMenuItem
        '
        Me.ImportFromAttToolStripMenuItem.Name = "ImportFromAttToolStripMenuItem"
        Me.ImportFromAttToolStripMenuItem.Size = New System.Drawing.Size(218, 24)
        Me.ImportFromAttToolStripMenuItem.Text = "Import from Att"
        '
        'ToolStripSeparator5
        '
        Me.ToolStripSeparator5.Name = "ToolStripSeparator5"
        Me.ToolStripSeparator5.Size = New System.Drawing.Size(215, 6)
        '
        'ManualClockInToolStripMenuItem
        '
        Me.ManualClockInToolStripMenuItem.Name = "ManualClockInToolStripMenuItem"
        Me.ManualClockInToolStripMenuItem.Size = New System.Drawing.Size(218, 24)
        Me.ManualClockInToolStripMenuItem.Text = "Manual Clock In"
        '
        'InputPekerjaanToolStripMenuItem
        '
        Me.InputPekerjaanToolStripMenuItem.Name = "InputPekerjaanToolStripMenuItem"
        Me.InputPekerjaanToolStripMenuItem.Size = New System.Drawing.Size(218, 24)
        Me.InputPekerjaanToolStripMenuItem.Text = "Input pekerjaan"
        '
        'ToolStripSeparator9
        '
        Me.ToolStripSeparator9.Name = "ToolStripSeparator9"
        Me.ToolStripSeparator9.Size = New System.Drawing.Size(215, 6)
        '
        'GajiToolStripMenuItem
        '
        Me.GajiToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.KelompokGajiToolStripMenuItem, Me.TemplateGajiToolStripMenuItem, Me.ToolStripSeparator6, Me.SettingGajiLainLainToolStripMenuItem, Me.ProsesBonusShiftToolStripMenuItem, Me.ToolStripSeparator7, Me.PengajuanGajiToolStripMenuItem})
        Me.GajiToolStripMenuItem.Name = "GajiToolStripMenuItem"
        Me.GajiToolStripMenuItem.Size = New System.Drawing.Size(47, 24)
        Me.GajiToolStripMenuItem.Text = "Gaji"
        '
        'KelompokGajiToolStripMenuItem
        '
        Me.KelompokGajiToolStripMenuItem.Name = "KelompokGajiToolStripMenuItem"
        Me.KelompokGajiToolStripMenuItem.Size = New System.Drawing.Size(214, 24)
        Me.KelompokGajiToolStripMenuItem.Text = "Kelompok gaji"
        '
        'TemplateGajiToolStripMenuItem
        '
        Me.TemplateGajiToolStripMenuItem.Name = "TemplateGajiToolStripMenuItem"
        Me.TemplateGajiToolStripMenuItem.Size = New System.Drawing.Size(214, 24)
        Me.TemplateGajiToolStripMenuItem.Text = "Template Gaji"
        '
        'ToolStripSeparator6
        '
        Me.ToolStripSeparator6.Name = "ToolStripSeparator6"
        Me.ToolStripSeparator6.Size = New System.Drawing.Size(211, 6)
        '
        'SettingGajiLainLainToolStripMenuItem
        '
        Me.SettingGajiLainLainToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BonusShiftToolStripMenuItem})
        Me.SettingGajiLainLainToolStripMenuItem.Name = "SettingGajiLainLainToolStripMenuItem"
        Me.SettingGajiLainLainToolStripMenuItem.Size = New System.Drawing.Size(214, 24)
        Me.SettingGajiLainLainToolStripMenuItem.Text = "Setting Gaji Lain lain"
        '
        'BonusShiftToolStripMenuItem
        '
        Me.BonusShiftToolStripMenuItem.Name = "BonusShiftToolStripMenuItem"
        Me.BonusShiftToolStripMenuItem.Size = New System.Drawing.Size(152, 24)
        Me.BonusShiftToolStripMenuItem.Text = "Bonus Shift"
        '
        'ProsesBonusShiftToolStripMenuItem
        '
        Me.ProsesBonusShiftToolStripMenuItem.Name = "ProsesBonusShiftToolStripMenuItem"
        Me.ProsesBonusShiftToolStripMenuItem.Size = New System.Drawing.Size(214, 24)
        Me.ProsesBonusShiftToolStripMenuItem.Text = "Proses Bonus Shift"
        Me.ProsesBonusShiftToolStripMenuItem.Visible = False
        '
        'ToolStripSeparator7
        '
        Me.ToolStripSeparator7.Name = "ToolStripSeparator7"
        Me.ToolStripSeparator7.Size = New System.Drawing.Size(211, 6)
        '
        'PengajuanGajiToolStripMenuItem
        '
        Me.PengajuanGajiToolStripMenuItem.Name = "PengajuanGajiToolStripMenuItem"
        Me.PengajuanGajiToolStripMenuItem.Size = New System.Drawing.Size(214, 24)
        Me.PengajuanGajiToolStripMenuItem.Text = "Pengajuan Gaji"
        '
        'ReportToolStripMenuItem1
        '
        Me.ReportToolStripMenuItem1.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.PrintDataKaryawwanToolStripMenuItem, Me.ToolStripSeparator2, Me.LaporanAbsensiToolStripMenuItem, Me.LaporanAbsensiPerKaryawanToolStripMenuItem1, Me.ToolStripSeparator1, Me.TotalJamKerjaToolStripMenuItem1, Me.DetailAbsensiToolStripMenuItem1})
        Me.ReportToolStripMenuItem1.Name = "ReportToolStripMenuItem1"
        Me.ReportToolStripMenuItem1.Size = New System.Drawing.Size(66, 24)
        Me.ReportToolStripMenuItem1.Text = "Report"
        '
        'PrintDataKaryawwanToolStripMenuItem
        '
        Me.PrintDataKaryawwanToolStripMenuItem.Name = "PrintDataKaryawwanToolStripMenuItem"
        Me.PrintDataKaryawwanToolStripMenuItem.Size = New System.Drawing.Size(279, 24)
        Me.PrintDataKaryawwanToolStripMenuItem.Text = "Print Data Karyawan"
        '
        'ToolStripSeparator2
        '
        Me.ToolStripSeparator2.Name = "ToolStripSeparator2"
        Me.ToolStripSeparator2.Size = New System.Drawing.Size(276, 6)
        '
        'LaporanAbsensiToolStripMenuItem
        '
        Me.LaporanAbsensiToolStripMenuItem.Name = "LaporanAbsensiToolStripMenuItem"
        Me.LaporanAbsensiToolStripMenuItem.Size = New System.Drawing.Size(279, 24)
        Me.LaporanAbsensiToolStripMenuItem.Text = "Laporan Absensi"
        '
        'LaporanAbsensiPerKaryawanToolStripMenuItem1
        '
        Me.LaporanAbsensiPerKaryawanToolStripMenuItem1.Name = "LaporanAbsensiPerKaryawanToolStripMenuItem1"
        Me.LaporanAbsensiPerKaryawanToolStripMenuItem1.Size = New System.Drawing.Size(279, 24)
        Me.LaporanAbsensiPerKaryawanToolStripMenuItem1.Text = "Laporan Absensi Per Karyawan"
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(276, 6)
        '
        'TotalJamKerjaToolStripMenuItem1
        '
        Me.TotalJamKerjaToolStripMenuItem1.Name = "TotalJamKerjaToolStripMenuItem1"
        Me.TotalJamKerjaToolStripMenuItem1.Size = New System.Drawing.Size(279, 24)
        Me.TotalJamKerjaToolStripMenuItem1.Text = "Total Jam Kerja"
        '
        'DetailAbsensiToolStripMenuItem1
        '
        Me.DetailAbsensiToolStripMenuItem1.Name = "DetailAbsensiToolStripMenuItem1"
        Me.DetailAbsensiToolStripMenuItem1.Size = New System.Drawing.Size(279, 24)
        Me.DetailAbsensiToolStripMenuItem1.Text = "Detail Absensi"
        '
        'WindowsToolStripMenuItem
        '
        Me.WindowsToolStripMenuItem.Name = "WindowsToolStripMenuItem"
        Me.WindowsToolStripMenuItem.Size = New System.Drawing.Size(82, 24)
        Me.WindowsToolStripMenuItem.Text = "Windows"
        '
        'VariableAndMaintenanceToolStripMenuItem
        '
        Me.VariableAndMaintenanceToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.GantiPasswordToolStripMenuItem, Me.UserToolStripMenuItem, Me.SecurityManagerToolStripMenuItem, Me.ConfigToolStripMenuItem})
        Me.VariableAndMaintenanceToolStripMenuItem.Name = "VariableAndMaintenanceToolStripMenuItem"
        Me.VariableAndMaintenanceToolStripMenuItem.Size = New System.Drawing.Size(193, 24)
        Me.VariableAndMaintenanceToolStripMenuItem.Text = "Variable and Maintenance"
        '
        'GantiPasswordToolStripMenuItem
        '
        Me.GantiPasswordToolStripMenuItem.Name = "GantiPasswordToolStripMenuItem"
        Me.GantiPasswordToolStripMenuItem.Size = New System.Drawing.Size(193, 24)
        Me.GantiPasswordToolStripMenuItem.Text = "Ganti Password"
        '
        'UserToolStripMenuItem
        '
        Me.UserToolStripMenuItem.Name = "UserToolStripMenuItem"
        Me.UserToolStripMenuItem.Size = New System.Drawing.Size(193, 24)
        Me.UserToolStripMenuItem.Text = "User"
        '
        'SecurityManagerToolStripMenuItem
        '
        Me.SecurityManagerToolStripMenuItem.Name = "SecurityManagerToolStripMenuItem"
        Me.SecurityManagerToolStripMenuItem.Size = New System.Drawing.Size(193, 24)
        Me.SecurityManagerToolStripMenuItem.Text = "Security Manager"
        '
        'ConfigToolStripMenuItem
        '
        Me.ConfigToolStripMenuItem.Name = "ConfigToolStripMenuItem"
        Me.ConfigToolStripMenuItem.Size = New System.Drawing.Size(193, 24)
        Me.ConfigToolStripMenuItem.Text = "Config"
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(18, 28)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(105, 47)
        Me.Button1.TabIndex = 3
        Me.Button1.Text = "generarte rekap absensi"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Timer2
        '
        Me.Timer2.Enabled = True
        Me.Timer2.Interval = 120000
        '
        'dtJam3
        '
        Me.dtJam3.CustomFormat = "HH:mm"
        Me.dtJam3.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtJam3.Location = New System.Drawing.Point(63, 160)
        Me.dtJam3.Name = "dtJam3"
        Me.dtJam3.ShowUpDown = True
        Me.dtJam3.Size = New System.Drawing.Size(60, 20)
        Me.dtJam3.TabIndex = 19
        Me.dtJam3.Value = New Date(2020, 3, 23, 19, 45, 0, 0)
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(22, 163)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(35, 13)
        Me.Label12.TabIndex = 18
        Me.Label12.Text = "Jam 3"
        '
        'dtJam2
        '
        Me.dtJam2.CustomFormat = "HH:mm"
        Me.dtJam2.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtJam2.Location = New System.Drawing.Point(63, 125)
        Me.dtJam2.Name = "dtJam2"
        Me.dtJam2.ShowUpDown = True
        Me.dtJam2.Size = New System.Drawing.Size(60, 20)
        Me.dtJam2.TabIndex = 17
        Me.dtJam2.Value = New Date(2020, 3, 23, 15, 45, 0, 0)
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(22, 128)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(35, 13)
        Me.Label11.TabIndex = 16
        Me.Label11.Text = "Jam 2"
        '
        'dtJam1
        '
        Me.dtJam1.CustomFormat = "HH:mm"
        Me.dtJam1.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtJam1.Location = New System.Drawing.Point(63, 89)
        Me.dtJam1.Name = "dtJam1"
        Me.dtJam1.ShowUpDown = True
        Me.dtJam1.Size = New System.Drawing.Size(60, 20)
        Me.dtJam1.TabIndex = 15
        Me.dtJam1.Value = New Date(2020, 3, 23, 8, 45, 0, 0)
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(22, 92)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(35, 13)
        Me.Label8.TabIndex = 14
        Me.Label8.Text = "Jam 1"
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.SystemColors.ActiveBorder
        Me.Panel1.Controls.Add(Me.dtJam3)
        Me.Panel1.Controls.Add(Me.Label12)
        Me.Panel1.Controls.Add(Me.dtJam2)
        Me.Panel1.Controls.Add(Me.Label11)
        Me.Panel1.Controls.Add(Me.dtJam1)
        Me.Panel1.Controls.Add(Me.Label8)
        Me.Panel1.Controls.Add(Me.Button1)
        Me.Panel1.Location = New System.Drawing.Point(18, 43)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(156, 206)
        Me.Panel1.TabIndex = 20
        Me.Panel1.Visible = False
        '
        'frmMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoSize = True
        Me.ClientSize = New System.Drawing.Size(895, 471)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.MenuStrip1)
        Me.IsMdiContainer = True
        Me.KeyPreview = True
        Me.MainMenuStrip = Me.MenuStrip1
        Me.MaximizeBox = False
        Me.Name = "frmMain"
        Me.Text = "HRD V.0.1"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents MasterToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MasterToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TipeKaryawanToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PayrollAreaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MasterKaryawanToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DivisiToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AbsensiToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PengaturanShiftToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents RekapAbsensiToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents GajiToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents KelompokGajiToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TemplateGajiToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PengajuanGajiToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents WindowsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SettingGajiLainLainToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents BonusShiftToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ProsesBonusShiftToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PengaturanHariLiburToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents HapusDataAbsensiToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ImportFromAttToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents VariableAndMaintenanceToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents GantiPasswordToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents UserToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SecurityManagerToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MasterKomponenGajiToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ReportToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PrintDataKaryawwanToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TotalJamKerjaToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DetailAbsensiToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents LaporanAbsensiToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents LaporanAbsensiPerKaryawanToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents MasterShiftToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MasterWorkgroupToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator3 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripSeparator4 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripSeparator5 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripSeparator6 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripSeparator7 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripSeparator8 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents InputPekerjaanToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PekerjaanToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents RekapShiftToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator9 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ManualClockInToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ConfigToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Timer2 As System.Windows.Forms.Timer
    Friend WithEvents dtJam3 As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents dtJam2 As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents dtJam1 As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
End Class
