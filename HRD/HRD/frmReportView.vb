﻿Imports System.Data.SqlClient
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports System.Data

Public Class frmReportView

    Public param(3) As String
    Public filter As String
    Public filename As String
    Public formula As String
    Private Sub frmReportViewer_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim Report As New ReportDocument
        Dim Report1 As New ReportDocument
        Dim li As New TableLogOnInfo
        Dim tbs As Tables
        Dim tb As Table
        Try

            Report.Load(reportLocation & filename)

            li.ConnectionInfo.DatabaseName = dbname
            li.ConnectionInfo.UserID = db_user
            li.ConnectionInfo.Password = db_password
            li.ConnectionInfo.ServerName = servername

            tbs = Report.Database.Tables
            For Each tb In Report.Database.Tables
                tb.ApplyLogOnInfo(li)
            Next
            If Report.Subreports.Count > 0 Then
                For Each Report1 In Report.Subreports
                    For Each tb In Report1.Database.Tables
                        tb.ApplyLogOnInfo(li)
                    Next
                    If filename = "\Laporan Penjualan Per Barang HPP.rpt" Then Report1.RecordSelectionFormula += " and " + formula
                Next

            End If
            'Report.SetDatabaseLogon(user, pwd, servername, dbname)

            Report.RecordSelectionFormula = formula
            Report.Refresh()
            Report.SetParameterValue(0, param(0))
            Report.SetParameterValue(1, param(1))
            Report.SetParameterValue(2, param(2))
           
            crV.ReportSource = Report
            crV.Show()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
End Class