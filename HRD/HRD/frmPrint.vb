﻿Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports System.Data.SqlClient

Public Class frmPrint
    Public reportname As String
    Public filter As String

    Public Sub load_report()
        Dim Report As New ReportDocument
        Dim li As New TableLogOnInfo
        Dim tbs As Tables
        Dim tb As Table
        Report.Load(App_Path() & "\" & reportname)
        Report.RecordSelectionFormula = filter
        li.ConnectionInfo.DatabaseName = dbname
        li.ConnectionInfo.UserID = db_user
        li.ConnectionInfo.Password = db_password
        tbs = Report.Database.Tables
        For Each tb In Report.Database.Tables
            tb.ApplyLogOnInfo(li)
        Next
        'Report.PrintToPrinter(1, True, 0, 0)
        CRview.ReportSource = Report
        'CRview.Show()
        CRview.PrintReport()

    End Sub
End Class