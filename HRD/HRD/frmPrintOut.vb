﻿Imports System.Data.SqlClient
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports System.Data
Imports System.Drawing.Printing
Public Class frmPrintOut
    Public papersize, printername As String
    Public param As String
    Public filter As String
    Public filename As String
    Public formula As String
    Private Sub frmPrintOut_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim Report As New ReportDocument
        Dim Report1 As New ReportDocument
        Dim li As New TableLogOnInfo
        Dim tbs As Tables
        Dim tb As Table
        Dim printproperties As Boolean = False
        Dim prm() As String = Split(param, ",")
        Report.Load(reportLocation & filename)

        li.ConnectionInfo.DatabaseName = dbname
        li.ConnectionInfo.UserID = db_user
        li.ConnectionInfo.Password = db_password
        li.ConnectionInfo.ServerName = servername

        tbs = Report.Database.Tables
        For Each tb In Report.Database.Tables
            tb.ApplyLogOnInfo(li)
        Next
        If Report.Subreports.Count > 0 Then
            For Each Report1 In Report.Subreports
                For Each tb In Report1.Database.Tables
                    tb.ApplyLogOnInfo(li)
                Next
           Next
        End If
        If printername <> "" Then
            Report.PrintOptions.PrinterName = printername
        End If
        Report.RecordSelectionFormula = formula
        Report.Refresh()
        For i = 0 To UBound(prm) - 1
            Report.SetParameterValue(i, param(i))
            Report.SetParameterValue(i, param(i))
            Report.SetParameterValue(i, param(i))
        Next
        crV.ReportSource = Report
        If printername <> "" And papersize <> "" Then
            Report.PrintOptions.PaperSize = getPaperIndex(printername, papersize)
            Report.PrintToPrinter(1, True, 0, 0)
            Me.Close()
        Else
            crV.Show()
        End If
    End Sub
End Class