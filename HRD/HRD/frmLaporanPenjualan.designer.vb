﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmLaporanPenjualan
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.chkTanggal = New System.Windows.Forms.CheckBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.dtSampai = New System.Windows.Forms.DateTimePicker()
        Me.dtDari = New System.Windows.Forms.DateTimePicker()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.GroupTanggal = New System.Windows.Forms.GroupBox()
        Me.lblNamaBarang = New System.Windows.Forms.Label()
        Me.btnSearchBarang = New System.Windows.Forms.Button()
        Me.txtNIK = New System.Windows.Forms.TextBox()
        Me.LabelBarang = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.GroupKaryawan = New System.Windows.Forms.GroupBox()
        Me.btnExit = New System.Windows.Forms.Button()
        Me.btnPrint = New System.Windows.Forms.Button()
        Me.GroupPenempatan = New System.Windows.Forms.GroupBox()
        Me.cmbPenempatan = New System.Windows.Forms.ComboBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.GroupBagian = New System.Windows.Forms.GroupBox()
        Me.CmbBagian = New System.Windows.Forms.ComboBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Grouptipe = New System.Windows.Forms.GroupBox()
        Me.cmbtipe = New System.Windows.Forms.ComboBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Panel1.SuspendLayout()
        Me.GroupTanggal.SuspendLayout()
        Me.GroupKaryawan.SuspendLayout()
        Me.GroupPenempatan.SuspendLayout()
        Me.GroupBagian.SuspendLayout()
        Me.Grouptipe.SuspendLayout()
        Me.SuspendLayout()
        '
        'chkTanggal
        '
        Me.chkTanggal.AutoSize = True
        Me.chkTanggal.Checked = True
        Me.chkTanggal.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkTanggal.Location = New System.Drawing.Point(12, 18)
        Me.chkTanggal.Name = "chkTanggal"
        Me.chkTanggal.Size = New System.Drawing.Size(15, 14)
        Me.chkTanggal.TabIndex = 134
        Me.chkTanggal.UseVisualStyleBackColor = True
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.BackColor = System.Drawing.Color.Transparent
        Me.Label4.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label4.Location = New System.Drawing.Point(6, 16)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(45, 13)
        Me.Label4.TabIndex = 57
        Me.Label4.Text = "Tanggal"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.BackColor = System.Drawing.Color.Transparent
        Me.Label3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label3.Location = New System.Drawing.Point(78, 16)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(11, 13)
        Me.Label3.TabIndex = 58
        Me.Label3.Text = ":"
        '
        'dtSampai
        '
        Me.dtSampai.CustomFormat = "dd/MM/yyyy"
        Me.dtSampai.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtSampai.Location = New System.Drawing.Point(34, 1)
        Me.dtSampai.Name = "dtSampai"
        Me.dtSampai.Size = New System.Drawing.Size(111, 20)
        Me.dtSampai.TabIndex = 1
        '
        'dtDari
        '
        Me.dtDari.CustomFormat = "dd/MM/yyyy"
        Me.dtDari.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtDari.Location = New System.Drawing.Point(95, 12)
        Me.dtDari.Name = "dtDari"
        Me.dtDari.Size = New System.Drawing.Size(121, 20)
        Me.dtDari.TabIndex = 0
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label1.Location = New System.Drawing.Point(6, 5)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(22, 13)
        Me.Label1.TabIndex = 60
        Me.Label1.Text = "s/d"
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.dtSampai)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Location = New System.Drawing.Point(222, 12)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(159, 23)
        Me.Panel1.TabIndex = 62
        '
        'GroupTanggal
        '
        Me.GroupTanggal.BackColor = System.Drawing.Color.Transparent
        Me.GroupTanggal.Controls.Add(Me.Panel1)
        Me.GroupTanggal.Controls.Add(Me.dtDari)
        Me.GroupTanggal.Controls.Add(Me.Label4)
        Me.GroupTanggal.Controls.Add(Me.Label3)
        Me.GroupTanggal.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.GroupTanggal.Location = New System.Drawing.Point(35, 0)
        Me.GroupTanggal.Name = "GroupTanggal"
        Me.GroupTanggal.Size = New System.Drawing.Size(387, 38)
        Me.GroupTanggal.TabIndex = 135
        Me.GroupTanggal.TabStop = False
        '
        'lblNamaBarang
        '
        Me.lblNamaBarang.AutoSize = True
        Me.lblNamaBarang.BackColor = System.Drawing.Color.Transparent
        Me.lblNamaBarang.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNamaBarang.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.lblNamaBarang.Location = New System.Drawing.Point(260, 16)
        Me.lblNamaBarang.Name = "lblNamaBarang"
        Me.lblNamaBarang.Size = New System.Drawing.Size(11, 13)
        Me.lblNamaBarang.TabIndex = 118
        Me.lblNamaBarang.Text = "-"
        '
        'btnSearchBarang
        '
        Me.btnSearchBarang.Location = New System.Drawing.Point(222, 11)
        Me.btnSearchBarang.Name = "btnSearchBarang"
        Me.btnSearchBarang.Size = New System.Drawing.Size(32, 23)
        Me.btnSearchBarang.TabIndex = 117
        Me.btnSearchBarang.Text = "F3"
        Me.btnSearchBarang.UseVisualStyleBackColor = True
        '
        'txtNIK
        '
        Me.txtNIK.Location = New System.Drawing.Point(95, 13)
        Me.txtNIK.Name = "txtNIK"
        Me.txtNIK.Size = New System.Drawing.Size(121, 20)
        Me.txtNIK.TabIndex = 4
        '
        'LabelBarang
        '
        Me.LabelBarang.AutoSize = True
        Me.LabelBarang.BackColor = System.Drawing.Color.Transparent
        Me.LabelBarang.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelBarang.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.LabelBarang.Location = New System.Drawing.Point(6, 16)
        Me.LabelBarang.Name = "LabelBarang"
        Me.LabelBarang.Size = New System.Drawing.Size(24, 13)
        Me.LabelBarang.TabIndex = 59
        Me.LabelBarang.Text = "NIK"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.BackColor = System.Drawing.Color.Transparent
        Me.Label15.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label15.Location = New System.Drawing.Point(78, 16)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(11, 13)
        Me.Label15.TabIndex = 60
        Me.Label15.Text = ":"
        '
        'GroupKaryawan
        '
        Me.GroupKaryawan.BackColor = System.Drawing.Color.Transparent
        Me.GroupKaryawan.Controls.Add(Me.lblNamaBarang)
        Me.GroupKaryawan.Controls.Add(Me.btnSearchBarang)
        Me.GroupKaryawan.Controls.Add(Me.txtNIK)
        Me.GroupKaryawan.Controls.Add(Me.LabelBarang)
        Me.GroupKaryawan.Controls.Add(Me.Label15)
        Me.GroupKaryawan.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.GroupKaryawan.Location = New System.Drawing.Point(35, 44)
        Me.GroupKaryawan.Name = "GroupKaryawan"
        Me.GroupKaryawan.Size = New System.Drawing.Size(438, 39)
        Me.GroupKaryawan.TabIndex = 136
        Me.GroupKaryawan.TabStop = False
        '
        'btnExit
        '
        Me.btnExit.Location = New System.Drawing.Point(379, 124)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(75, 28)
        Me.btnExit.TabIndex = 133
        Me.btnExit.Text = "Exit"
        Me.btnExit.UseVisualStyleBackColor = True
        '
        'btnPrint
        '
        Me.btnPrint.Location = New System.Drawing.Point(298, 124)
        Me.btnPrint.Name = "btnPrint"
        Me.btnPrint.Size = New System.Drawing.Size(75, 28)
        Me.btnPrint.TabIndex = 132
        Me.btnPrint.Text = "Print"
        Me.btnPrint.UseVisualStyleBackColor = True
        '
        'GroupPenempatan
        '
        Me.GroupPenempatan.BackColor = System.Drawing.Color.Transparent
        Me.GroupPenempatan.Controls.Add(Me.cmbPenempatan)
        Me.GroupPenempatan.Controls.Add(Me.Label12)
        Me.GroupPenempatan.Controls.Add(Me.Label13)
        Me.GroupPenempatan.ForeColor = System.Drawing.Color.Black
        Me.GroupPenempatan.Location = New System.Drawing.Point(35, 115)
        Me.GroupPenempatan.Name = "GroupPenempatan"
        Me.GroupPenempatan.Size = New System.Drawing.Size(242, 39)
        Me.GroupPenempatan.TabIndex = 138
        Me.GroupPenempatan.TabStop = False
        Me.GroupPenempatan.Visible = False
        '
        'cmbPenempatan
        '
        Me.cmbPenempatan.ForeColor = System.Drawing.Color.Black
        Me.cmbPenempatan.FormattingEnabled = True
        Me.cmbPenempatan.Location = New System.Drawing.Point(95, 12)
        Me.cmbPenempatan.Name = "cmbPenempatan"
        Me.cmbPenempatan.Size = New System.Drawing.Size(133, 21)
        Me.cmbPenempatan.TabIndex = 5
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.BackColor = System.Drawing.Color.Transparent
        Me.Label12.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.ForeColor = System.Drawing.Color.Black
        Me.Label12.Location = New System.Drawing.Point(6, 16)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(67, 13)
        Me.Label12.TabIndex = 59
        Me.Label12.Text = "Penempatan"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.BackColor = System.Drawing.Color.Transparent
        Me.Label13.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.ForeColor = System.Drawing.Color.Black
        Me.Label13.Location = New System.Drawing.Point(78, 16)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(11, 13)
        Me.Label13.TabIndex = 60
        Me.Label13.Text = ":"
        '
        'GroupBagian
        '
        Me.GroupBagian.BackColor = System.Drawing.Color.Transparent
        Me.GroupBagian.Controls.Add(Me.CmbBagian)
        Me.GroupBagian.Controls.Add(Me.Label2)
        Me.GroupBagian.Controls.Add(Me.Label5)
        Me.GroupBagian.ForeColor = System.Drawing.Color.Black
        Me.GroupBagian.Location = New System.Drawing.Point(35, 152)
        Me.GroupBagian.Name = "GroupBagian"
        Me.GroupBagian.Size = New System.Drawing.Size(242, 39)
        Me.GroupBagian.TabIndex = 139
        Me.GroupBagian.TabStop = False
        Me.GroupBagian.Visible = False
        '
        'CmbBagian
        '
        Me.CmbBagian.ForeColor = System.Drawing.Color.Black
        Me.CmbBagian.FormattingEnabled = True
        Me.CmbBagian.Location = New System.Drawing.Point(95, 12)
        Me.CmbBagian.Name = "CmbBagian"
        Me.CmbBagian.Size = New System.Drawing.Size(133, 21)
        Me.CmbBagian.TabIndex = 5
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.BackColor = System.Drawing.Color.Transparent
        Me.Label2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.Black
        Me.Label2.Location = New System.Drawing.Point(6, 16)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(39, 13)
        Me.Label2.TabIndex = 59
        Me.Label2.Text = "Bagian"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.BackColor = System.Drawing.Color.Transparent
        Me.Label5.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.Color.Black
        Me.Label5.Location = New System.Drawing.Point(78, 16)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(11, 13)
        Me.Label5.TabIndex = 60
        Me.Label5.Text = ":"
        '
        'Grouptipe
        '
        Me.Grouptipe.BackColor = System.Drawing.Color.Transparent
        Me.Grouptipe.Controls.Add(Me.cmbtipe)
        Me.Grouptipe.Controls.Add(Me.Label6)
        Me.Grouptipe.Controls.Add(Me.Label7)
        Me.Grouptipe.ForeColor = System.Drawing.Color.Black
        Me.Grouptipe.Location = New System.Drawing.Point(35, 80)
        Me.Grouptipe.Name = "Grouptipe"
        Me.Grouptipe.Size = New System.Drawing.Size(242, 39)
        Me.Grouptipe.TabIndex = 139
        Me.Grouptipe.TabStop = False
        Me.Grouptipe.Visible = False
        '
        'cmbtipe
        '
        Me.cmbtipe.ForeColor = System.Drawing.Color.Black
        Me.cmbtipe.FormattingEnabled = True
        Me.cmbtipe.Location = New System.Drawing.Point(95, 12)
        Me.cmbtipe.Name = "cmbtipe"
        Me.cmbtipe.Size = New System.Drawing.Size(133, 21)
        Me.cmbtipe.TabIndex = 5
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.BackColor = System.Drawing.Color.Transparent
        Me.Label6.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.Color.Black
        Me.Label6.Location = New System.Drawing.Point(6, 16)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(27, 13)
        Me.Label6.TabIndex = 59
        Me.Label6.Text = "Tipe"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.BackColor = System.Drawing.Color.Transparent
        Me.Label7.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.ForeColor = System.Drawing.Color.Black
        Me.Label7.Location = New System.Drawing.Point(78, 16)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(11, 13)
        Me.Label7.TabIndex = 60
        Me.Label7.Text = ":"
        '
        'frmLaporanPenjualan
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(489, 202)
        Me.Controls.Add(Me.Grouptipe)
        Me.Controls.Add(Me.GroupBagian)
        Me.Controls.Add(Me.GroupPenempatan)
        Me.Controls.Add(Me.chkTanggal)
        Me.Controls.Add(Me.GroupTanggal)
        Me.Controls.Add(Me.GroupKaryawan)
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.btnPrint)
        Me.Name = "frmLaporanPenjualan"
        Me.Text = "frmLaporanAbsensi"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.GroupTanggal.ResumeLayout(False)
        Me.GroupTanggal.PerformLayout()
        Me.GroupKaryawan.ResumeLayout(False)
        Me.GroupKaryawan.PerformLayout()
        Me.GroupPenempatan.ResumeLayout(False)
        Me.GroupPenempatan.PerformLayout()
        Me.GroupBagian.ResumeLayout(False)
        Me.GroupBagian.PerformLayout()
        Me.Grouptipe.ResumeLayout(False)
        Me.Grouptipe.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents chkTanggal As System.Windows.Forms.CheckBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents dtSampai As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtDari As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents GroupTanggal As System.Windows.Forms.GroupBox
    Friend WithEvents lblNamaBarang As System.Windows.Forms.Label
    Friend WithEvents btnSearchBarang As System.Windows.Forms.Button
    Friend WithEvents txtNIK As System.Windows.Forms.TextBox
    Friend WithEvents LabelBarang As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents GroupKaryawan As System.Windows.Forms.GroupBox
    Friend WithEvents btnExit As System.Windows.Forms.Button
    Friend WithEvents btnPrint As System.Windows.Forms.Button
    Friend WithEvents GroupPenempatan As System.Windows.Forms.GroupBox
    Friend WithEvents cmbPenempatan As System.Windows.Forms.ComboBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents GroupBagian As System.Windows.Forms.GroupBox
    Friend WithEvents CmbBagian As System.Windows.Forms.ComboBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Grouptipe As System.Windows.Forms.GroupBox
    Friend WithEvents cmbtipe As System.Windows.Forms.ComboBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
End Class
