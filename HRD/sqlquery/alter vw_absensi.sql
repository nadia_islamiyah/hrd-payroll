ALTER view [dbo].[vw_absensi] as
SELECT m.nik,m.nama_depan,r.tanggal,j.nama_shift,status_datang as Masuk,

case when masuk_pembulatan='True' then 
	case when masuk_arahpembulatan='True' then
		dbo.f_roundtimeup(datang,0)
	else
		dbo.f_roundtimedown(datang,0)
	end
else 
	datang
end as jam_masuk,


case when pulang_pembulatan='True' then 
	case when pulang_arahpembulatan='True' then
		dbo.f_roundtimeup(pulang,0)
	else
		dbo.f_roundtimedown(pulang,0)
	end
else 
	pulang
end as jam_pulang,


	case when istirahat_pembulatan='True' then 
		case when istirahat_arahpembulatan='True' then
			dbo.f_roundtimeup(istirahat_out,0)
		else
			dbo.f_roundtimedown(istirahat_out,0)
		end
	else 
		istirahat_out
	end as jam_istirahat,
	
	
	case when kembali_pembulatan='True' then 
		case when kembali_arahpembulatan='True' then
			dbo.f_roundtimeup(istirahat_in,0)
		else
			dbo.f_roundtimedown(istirahat_in,0)
		end
	else 
		istirahat_in
	end as jam_kembali,
	
case when not r.total is null and r.total <> 0 then 
r.total
else
datediff(hour,	case when masuk_pembulatan='True' then 
					case when masuk_arahpembulatan='True' then
						dbo.f_roundtimeup(datang,0)
					else
						dbo.f_roundtimedown(datang,0)
					end
				else 
					datang
				end,
case when pulang_pembulatan='True' then 
	case when pulang_arahpembulatan='True' then
		dbo.f_roundtimeup(pulang,0)
	else
		dbo.f_roundtimedown(pulang,0)
	end
else 
	pulang
end
)
end as total,

case when status_istirahat_in='True' and status_istirahat_out='True' then
	datediff(hour,
	case when istirahat_pembulatan='True' then 
		case when istirahat_arahpembulatan='True' then
			dbo.f_roundtimeup(istirahat_out,0)
		else
			dbo.f_roundtimedown(istirahat_out,0)
		end
	else 
		istirahat_out
	end,
	case when kembali_pembulatan='True' then 
		case when kembali_arahpembulatan='True' then
			dbo.f_roundtimeup(istirahat_in,0)
		else
			dbo.f_roundtimedown(istirahat_in,0)
		end
	else 
		istirahat_in
	end
	)
else
	0
end as istirahat,
jumlah_jamkerja,case when t_libur.tanggal is null then 0 else 1 end as libur, lembur
FROM         dbo.ms_karyawan m INNER JOIN
                      dbo.t_rekapabsensi r ON m.nik = r.NIK left JOIN
                      dbo.t_jadwalkerja j ON m.NIK = j.NIK and r.tanggal=j.tanggal left JOIN
                      dbo.ms_jamkerja k ON j.nama_shift = k.nama_shift left join t_libur on r.tanggal=t_libur.tanggal
where r.status_datang='True' and r.status_pulang='True'








GO


