--select * from ms_komponengaji
alter table ms_komponengaji add pc bit
go
update ms_komponengaji set pc=0
go
--select * from tmp_importgroupgaji
alter table tmp_importgroupgaji add nilai3 numeric(18,0)
go
--select * from tmp_countshift
alter table tmp_countshift add jmlpc numeric(18,0)
alter table tmp_countshift add jmlmasuk_libur numeric(18,0)
go

USE [hrd]
GO
/****** Object:  StoredProcedure [dbo].[sp_ProsesPayroll]    Script Date: 06/02/2018 14:14:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER proc [dbo].[sp_ProsesPayroll]	
@nama_template varchar(30)
as
	declare @kode_komponen varchar(5)
	declare @nik varchar(15),
			@kode_groupgaji varchar(20), 
			@nama_shift varchar(20), 
			@golongan varchar(30), 
			@jmlshift	numeric(18,0),
			@jamshift	numeric(18,0),
			@jmllembur	numeric(18,0),
			@jamlembur	numeric(18,0),
			@jmllembur_libur	numeric(18,0),
			@jamlembur_libur	numeric(18,0),
			@jmltelat numeric(18,0),
			@nilai numeric(18,0),
			@value numeric(18,0),
			@hari bit,
			@jam bit,
			@syarat_lembur bit,
			@syarat_telat bit,
			@jmlpc numeric(18,0),
			@syarat_pc bit
	delete from tmp_payroll
	
begin--====
-- Yg komponen gaji mk.normal<>0 or mk.libur<>0
begin--//
declare crsr cursor local for select * from tmp_countshift
open crsr
fetch next from crsr INTO @nik, @kode_groupgaji,@nama_shift,@jmlshift,@jamshift,@jmllembur,@jamlembur,@jmltelat,@jmllembur_libur,@jamlembur_libur,@jmlpc
WHILE @@FETCH_STATUS = 0
BEGIN--==	
	print '====== NIK : '+@nik +' - Nama SHIFT : '+ @nama_shift
	set @golongan=isnull((select golongan from ms_jamkerja where nama_shift=@nama_shift),'')
	print '====== GOLONGAN : '+@golongan 
	declare crsr1 cursor local for select m.kode_komponen,mk.harian,mk.jam,mk.lembur,mk.telat,mk.pc from ms_templategaji m 
	left join ms_komponengaji mk on m.kode_komponen=mk.kode_komponengaji where mk.normal<>0 or mk.libur<>0 and m.nama=@nama_template
	open crsr1
	fetch next from crsr1 INTO @kode_komponen,@hari,@jam,@syarat_lembur,@syarat_telat ,@syarat_pc
	WHILE @@FETCH_STATUS = 0
	BEGIN--	
		print '== Komponen Gaji :'+@kode_komponen
		set @nilai=isnull((select nilai from ms_groupgajid where kode_groupgaji=@kode_groupgaji and golongan=@golongan and kode_komponen=@kode_komponen),0)
		if @syarat_lembur=0 and @syarat_telat=0 and @syarat_pc=0
			BEGIN
			print 'TIDAK LEMBUR DAN TELAT'
			if @hari=1 
				begin 
				if @jmlshift=0 set @value=0 else set @value=@nilai*@jmlshift
				insert into tmp_payroll 
				select @nik,@kode_komponen,@value,@nama_shift
				end
			else if @jam =1
				begin
				if @jamshift=0 set @value=0 else set @value=@nilai*@jamshift
				insert into tmp_payroll 
				select @nik,@kode_komponen,@value,@nama_shift
				end
			END
		Else if @syarat_lembur=1
			BEGIN
			print 'LEMBUR Hari NORMAL'
			print 'Jumlah :'+ cast(@jmllembur as varchar) +' - Jam :'+ cast(@jamlembur as varchar) 
			if @hari=1 
				begin 
				if @jmllembur=0 set @value=0 else set @value=@nilai*@jmllembur
				insert into tmp_payroll 
				select @nik,@kode_komponen,@value,@nama_shift
				end
			else if @jam =1
				begin
				if @jamlembur=0 set @value=0 else set @value=@nilai*@jamlembur
				insert into tmp_payroll 
				select @nik,@kode_komponen,@value,@nama_shift
				end
			END
		Else if @syarat_telat=1
			BEGIN
			set @nilai=isnull((select nilai from ms_groupgajid where kode_groupgaji=@kode_groupgaji and golongan=@golongan and kode_komponen=@kode_komponen),0)
			print 'TELAT'
			print 'Jumlah TELAT :'+ cast(@jmltelat as varchar)
			print 'NILAI TELAT :'+ cast(@nilai as varchar)
			if @jmltelat=0 set @value=0 else set @value=@nilai*@jmltelat
			insert into tmp_payroll 
			select @nik,@kode_komponen,@value,@nama_shift
			END
		Else if @syarat_pc=1
			BEGIN
			set @nilai=isnull((select nilai from ms_groupgajid where kode_groupgaji=@kode_groupgaji and golongan=@golongan and kode_komponen=@kode_komponen),0)
			print 'PC'
			print 'Jumlah PC :'+ cast(@jmlpc as varchar)
			print 'NILAI PC :'+ cast(@nilai as varchar)
			if @jmlpc=0 set @value=0 else set @value=@nilai*@jmlpc
			insert into tmp_payroll 
			select @nik,@kode_komponen,@value,@nama_shift
			END
		fetch next from crsr1 INTO @kode_komponen,@hari,@jam,@syarat_lembur,@syarat_telat  ,@syarat_pc
	END  --
	CLOSE crsr1
	DEALLOCATE crsr1
	fetch next from crsr INTO @nik, @kode_groupgaji,@nama_shift,@jmlshift,@jamshift,@jmllembur,@jamlembur,@jmltelat,@jmllembur_libur,@jamlembur_libur,@jmlpc
END  --==
CLOSE crsr
DEALLOCATE crsr
end--//
-- YG komponen gaji mk.normal=0 and mk.libur=0
begin--//
print '====== TANPA SHIFT ========'
declare crsr cursor local for select distinct NIK,kode_groupgaji from tmp_countshift
open crsr
fetch next from crsr INTO @nik, @kode_groupgaji
WHILE @@FETCH_STATUS = 0
BEGIN--==	
	print '====== NIK :'+@nik 
	declare crsr1 cursor local for select m.kode_komponen,mk.harian,mk.jam,mk.lembur,mk.telat from ms_templategaji m 
	left join ms_komponengaji mk on m.kode_komponen=mk.kode_komponengaji where mk.normal=0 and mk.libur=0 and m.nama=@nama_template
	open crsr1
	fetch next from crsr1 INTO @kode_komponen,@hari,@jam,@syarat_lembur,@syarat_telat 
	WHILE @@FETCH_STATUS = 0
	BEGIN--	
		print '== Komponen Gaji :'+@kode_komponen
		begin
		set @nilai=isnull((select nilai from ms_groupgajid where kode_groupgaji=@kode_groupgaji and kode_komponen=@kode_komponen),0)
		insert into tmp_payroll 
		select @nik,@kode_komponen,@nilai,@nama_shift
		end
		
		fetch next from crsr1 INTO @kode_komponen,@hari,@jam,@syarat_lembur,@syarat_telat 
	END  --
	CLOSE crsr1
	DEALLOCATE crsr1
	fetch next from crsr INTO @nik, @kode_groupgaji
END  --==
CLOSE crsr
DEALLOCATE crsr
end--//

end  --====
go


ALTER view [dbo].[vw_absensi] as
SELECT m.nik,m.nama_depan,r.tanggal,r.nama_shift,status_datang as Masuk,

case when masuk_pembulatan='True' then 
	case when masuk_arahpembulatan='True' then
		dbo.f_roundtimeup(datang,0)
	else
		dbo.f_roundtimedown(datang,0)
	end
else 
	datang
end as jam_masuk,


case when pulang_pembulatan='True' then 
	case when pulang_arahpembulatan='True' then
		dbo.f_roundtimeup(pulang,0)
	else
		dbo.f_roundtimedown(pulang,0)
	end
else 
	pulang
end as jam_pulang,


	case when istirahat_pembulatan='True' then 
		case when istirahat_arahpembulatan='True' then
			dbo.f_roundtimeup(istirahat_out,0)
		else
			dbo.f_roundtimedown(istirahat_out,0)
		end
	else 
		istirahat_out
	end as jam_istirahat,
	
	
	case when kembali_pembulatan='True' then 
		case when kembali_arahpembulatan='True' then
			dbo.f_roundtimeup(istirahat_in,0)
		else
			dbo.f_roundtimedown(istirahat_in,0)
		end
	else 
		istirahat_in
	end as jam_kembali,
	
case when not r.total is null and r.total <> 0 then 
r.total
else
datediff(hour,	case when masuk_pembulatan='True' then 
					case when masuk_arahpembulatan='True' then
						dbo.f_roundtimeup(datang,0)
					else
						dbo.f_roundtimedown(datang,0)
					end
				else 
					datang
				end,
case when pulang_pembulatan='True' then 
	case when pulang_arahpembulatan='True' then
		dbo.f_roundtimeup(pulang,0)
	else
		dbo.f_roundtimedown(pulang,0)
	end
else 
	pulang
end
)
end as total,
case when status_istirahat_in='True' and status_istirahat_out='True' then
	datediff(hour,
	case when istirahat_pembulatan='True' then 
		case when istirahat_arahpembulatan='True' then
			dbo.f_roundtimeup(istirahat_out,0)
		else
			dbo.f_roundtimedown(istirahat_out,0)
		end
	else 
		istirahat_out
	end,
	case when kembali_pembulatan='True' then 
		case when kembali_arahpembulatan='True' then
			dbo.f_roundtimeup(istirahat_in,0)
		else
			dbo.f_roundtimedown(istirahat_in,0)
		end
	else 
		istirahat_in
	end
	)
else
	0
end as istirahat,
k.jumlah_jamkerja,case when t_libur.tanggal is null then 0 else 1 end as libur, lembur,

case when datang=pulang then 0 else
case when datediff(minute,CAST(masuk_jam AS time),CAST(datang AS time))>0 then 
datediff(minute,CAST(masuk_jam AS time),CAST(datang AS time)) else 0 end end as telat,

case when datang=pulang then 0 else
case when datediff(minute,CAST(pulang AS time),CAST(pulang_jam AS time))>0 then 
datediff(minute,CAST(pulang AS time),CAST(pulang_jam AS time)) else 0 end end as pc,

m2.kode_groupgaji,m2.area,m2.divisi,m2.tipe,m2.groupkerja,r.lembur_minimal,r.lembur_kelipatan

FROM dbo.ms_karyawan m INNER JOIN
	  dbo.t_rekapabsensi r ON m.nik = r.NIK 
	  left join ms_karyawan_tipe m2 on m.nik=m2.NIK
	  left JOIN  dbo.ms_jamkerja k ON r.nama_shift = k.nama_shift left join t_libur on r.tanggal=t_libur.tanggal
where r.status_datang='True' and r.status_pulang='True'

--SELECT DATEDIFF(hour ,'13:28:00', '22:21:00' )
--select 
--case when datang=pulang then 0 else
--case when datediff(minute,CAST(masuk_jam AS time),CAST(datang AS time))>0 then 
--datediff(minute,CAST(masuk_jam AS time),CAST(datang AS time)) else 0 end end as telat,
--* from t_rekapabsensi r left join dbo.ms_jamkerja k ON r.nama_shift = k.nama_shift

GO

ALTER proc [dbo].[sp_exportgroupgaji]	
as
begin	
	select nik,cast(nama_depan as varchar) nama_depan,0 as 'gaji_pokok', 0 as '2jamlembur', -10000 as 'potongan_telat' , 0 as 'potongan_pulangcepat'   from ms_karyawan 
end
go


ALTER proc [dbo].[sp_importgroupgaji]	
as
	declare @nik varchar(15),
			@gapok	numeric(18,0),
			@nilai1	numeric(18,0),
			@nilai2	numeric(18,0),
			@nilai3	numeric(18,0),
			@golongan varchar(30)
	delete from ms_groupgajid where kode_groupgaji in (select nik from tmp_importgroupgaji)
begin	
	declare crsr cursor local for select nik,gajipokok,nilai1,nilai2,nilai3 from tmp_importgroupgaji
	open crsr
	fetch next from crsr INTO @nik, @gapok,@nilai1,@nilai2,@nilai3
	WHILE @@FETCH_STATUS = 0
	BEGIN	
		declare crsr1 cursor local for select distinct golongan from ms_jamkerja
		open crsr1
		fetch next from crsr1 INTO @golongan
		WHILE @@FETCH_STATUS = 0
		BEGIN	
			if @golongan='Tgl Merah 7 Jam' 
				BEGIN
				insert into ms_groupgajid
				select @nik,'100',(@gapok+@nilai1),'Hari',0,@golongan
				insert into ms_groupgajid
				select @nik,'400',@nilai2,'',2,@golongan
				insert into ms_groupgajid
				select @nik,'700',@nilai3,'',2,@golongan
				END
			ELSE IF @golongan='Tgl Merah Double' 
				BEGIN
				insert into ms_groupgajid
				select @nik,'100',(@gapok*2),'Hari',0,@golongan
				insert into ms_groupgajid
				select @nik,'400',@nilai2,'',2,@golongan
				insert into ms_groupgajid
				select @nik,'700',@nilai3,'',2,@golongan
				END
			ELSE
				BEGIN
				insert into ms_groupgajid
				select @nik,'100',@gapok,'Hari',0,@golongan
				insert into ms_groupgajid
				select @nik,'400',@nilai2,'',2,@golongan
				insert into ms_groupgajid
				select @nik,'700',@nilai3,'',2,@golongan
				END
			fetch next from crsr1 INTO @golongan
		END
		CLOSE crsr1
		DEALLOCATE crsr1
		
		insert into ms_groupgajid
		select @nik,'300',@gapok,'Hari',1,'Normal'
		insert into ms_groupgajid
		select @nik,'500',0,'',3,''
		insert into ms_groupgajid
		select @nik,'600',0,'',4,''
		fetch next from crsr INTO  @nik, @gapok,@nilai1,@nilai2,@nilai3
	END
    CLOSE crsr
    DEALLOCATE crsr
end

go