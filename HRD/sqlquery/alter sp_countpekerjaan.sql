
ALTER proc [dbo].[sp_countpekerjaan]	
@periode_awal datetime,
@periode_akhir datetime
as
	declare @awal as varchar(8),
			@akhir as varchar(8)
				
	set @awal=convert(varchar(8),@periode_awal,112)
	set @akhir=convert(varchar(8),@periode_akhir,112)
	delete from tmp_jmlpekerjaan 
begin
		print 'Awal: '+ @awal +' ,Akhir: '+@akhir
		insert into tmp_jmlpekerjaan
		select nik,jenis_pekerjaan,
		sum(case when @awal < convert(varchar(8),tanggal_mulai,112) and @akhir > convert(varchar(8),tanggal_berakhir,112) and @akhir >= @awal then
			DATEDIFF(DAY,tanggal_mulai ,dateadd(day,1,tanggal_berakhir))
			when @awal = convert(varchar(8),tanggal_mulai,112) and @akhir = convert(varchar(8),tanggal_berakhir,112) then
			DATEDIFF(DAY,tanggal_mulai ,dateadd(day,1,tanggal_berakhir))
			when @awal = convert(varchar(8),tanggal_mulai,112) and @akhir >= convert(varchar(8),tanggal_berakhir,112) then
			DATEDIFF(DAY,tanggal_mulai ,dateadd(day,1,tanggal_berakhir))
			when @awal <= convert(varchar(8),tanggal_mulai,112) and @akhir = convert(varchar(8),tanggal_berakhir,112) then
			DATEDIFF(DAY,tanggal_mulai ,dateadd(day,1,tanggal_berakhir))
			
			when @awal > convert(varchar(8),tanggal_mulai,112) and @akhir > convert(varchar(8),tanggal_berakhir,112) and @awal < convert(varchar(8),tanggal_berakhir,112) then
			DATEDIFF(DAY,@awal ,dateadd(day,1,tanggal_berakhir))
			when @awal >= convert(varchar(8),tanggal_mulai,112) and @akhir = convert(varchar(8),tanggal_berakhir,112) then
			DATEDIFF(DAY,@awal ,dateadd(day,1,tanggal_berakhir))
			when @awal < convert(varchar(8),tanggal_mulai,112) and @akhir < convert(varchar(8),tanggal_berakhir,112) and @akhir >convert(varchar(8),tanggal_mulai,112)  then
			DATEDIFF(DAY,tanggal_mulai ,dateadd(day,1,@akhir))
			when @awal = convert(varchar(8),tanggal_mulai,112) and @akhir <= convert(varchar(8),tanggal_berakhir,112) then
			DATEDIFF(DAY,tanggal_mulai ,dateadd(day,1,@akhir))
			when @awal > convert(varchar(8),tanggal_mulai,112) and @akhir < convert(varchar(8),tanggal_berakhir,112) and @akhir >= @awal then
			DATEDIFF(DAY,@awal ,dateadd(day,1,@akhir))
			when @awal < convert(varchar(8),tanggal_mulai,112) and @akhir < convert(varchar(8),tanggal_berakhir,112) and @akhir < convert(varchar(8),tanggal_mulai,112) then 0
			when @awal > convert(varchar(8),tanggal_mulai,112) and @akhir > convert(varchar(8),tanggal_berakhir,112) and @awal > convert(varchar(8),tanggal_berakhir,112) then 0
			
		end)
		as  jml from t_hariand 
		--where convert(varchar(20),tanggal_mulai,112) between @awal and @akhir
		group by nik,jenis_pekerjaan
				
		--select nik,jenis_pekerjaan,COUNT(jenis_pekerjaan) jml from t_hariand where convert(varchar(20),tanggal_mulai,112) between @awal and @akhir
		--group by nik,jenis_pekerjaan
	
end


