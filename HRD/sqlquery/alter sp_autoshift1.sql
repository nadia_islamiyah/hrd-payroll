
ALTER procedure [dbo].[sp_autoshift1] 
@periode_awal datetime,
@periode_akhir datetime
as
Begin
declare @awal as varchar(8),
		@akhir as varchar(8)

set @awal=convert(varchar(8),@periode_awal,112)
set @akhir=convert(varchar(8),@periode_akhir,112)

delete from t_jadwalkerja where CONVERT(varchar(8),tanggal,112)  between @awal and @akhir;

insert into t_jadwalkerja  (tanggal,NIK)
(select distinct  convert(varchar(8),tanggal ,112),a.nik from t_rekapabsensi a where CONVERT(varchar(8),tanggal ,112) between @awal and @akhir 
--and  NIK+CONVERT(varchar(8),waktu,112) not in (select NIK+CONVERT(varchar(8),tanggal,112) from t_jadwalkerja )
)

update t_jadwalkerja set nama_shift=
(SELECT top 1 nama_shift
  FROM [vw_closestshift] where nik=t_jadwalkerja.nik and convert(varchar(8),tanggal,112)=convert(varchar(8),t_jadwalkerja.tanggal,112) order by ranking)
 where nama_shift is null and CONVERT(varchar(8),tanggal,112)  between @awal and @akhir
 
  END
  
  