ALTER procedure [dbo].[sp_autoshift] 
@periode_awal datetime,
@periode_akhir datetime
as
Begin
declare @awal as varchar(8),
		@akhir as varchar(8)

set @awal=convert(varchar(8),@periode_awal,112)
set @akhir=convert(varchar(8),@periode_akhir,112)

delete from t_rekapabsensi where CONVERT(varchar(8),tanggal,112)  between @awal and @akhir;

insert into t_rekapabsensi (NIK,tanggal,status_datang,status_pulang) 
(select distinct nik, CONVERT(varchar(8),waktu,112),1,1
 from t_absensi a where CONVERT(varchar(8),waktu,112) between @awal and @akhir 
 --and NIK+CONVERT(varchar(8),waktu,112) not in (select NIK+CONVERT(varchar(8),tanggal,112) from t_rekapabsensi)
)

update t_rekapabsensi set 
NIK= isnull((select NIK from ms_karyawan where finger_id=t_rekapabsensi.nik),t_rekapabsensi.nik),
datang=(select top 1 waktu from t_absensi where nik=t_rekapabsensi.nik and CONVERT(varchar(8),waktu,112)=CONVERT(varchar(8),t_rekapabsensi.tanggal,112) order by waktu),
pulang=(select top 1 waktu from t_absensi where nik=t_rekapabsensi.nik and CONVERT(varchar(8),waktu,112)=CONVERT(varchar(8),t_rekapabsensi.tanggal,112) order by waktu desc)
from t_rekapabsensi where  CONVERT(varchar(8),tanggal,112)  between @awal and @akhir

delete from t_rekapabsensi where CONVERT(VARCHAR(8), datang, 108) = '00:00:00' and CONVERT(VARCHAR(8), pulang, 108) = '00:00:00' and CONVERT(varchar(8),tanggal,112)  between @awal and @akhir;
--exec   @awal , @akhir ;
  END