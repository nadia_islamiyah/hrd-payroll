sp_rename 't_hariand.tanggal', 'tanggal_mulai', 'COLUMN';
go
alter table t_hariand add tanggal_berakhir datetime
go
update t_hariand set tanggal_berakhir =  getdate() where tanggal_berakhir is null
go 
alter table t_rekapabsensi add total numeric(18,0)
go
update t_rekapabsensi set total = 0 where total is null 
go